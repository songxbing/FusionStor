#include "config.h"

#define DBG_SUBSYS S_LIBINTERFACE

#include "iscsi.h"
#include "job_dock.h"
#include "volume.h"
#include "../../storage/controller/volume_ctl.h"
#include "dbg.h"
#include "lichbd_io.h"
#include "net_global.h"
#include "lichstor.h"

/**
 * @file ISCSI Module
 *
 * 通过lich_io.c和block.c，实现iscsi与lichd对接
 *
 * lichd提供的接口：
 * - lichstor.h (iscsid/lichd共存）
 * - lichbd (独立的iscsid进程）
 */

struct lichio_data {
        fileid_t fileid;
};

#define ISCSI_TMO 300

STATIC int __lichio_connect_gateway(struct iscsi_volume *volume, struct uss_lun_entry *lu)
{
        int ret, retry = 0;
        lichbd_ioctx_t *ioctx;

        ret = ymalloc((void **)&ioctx, sizeof(*ioctx));
        if (unlikely(ret))
                GOTO(err_ret, ret);

retry:
        ret = lichbd_connect(lu->pool, lu->path, ioctx, 0);
        if (unlikely(ret)) {
                USLEEP_RETRY(err_free, ret, retry, retry, gloconf.rpc_timeout * 4, (1000 * 1000));
        }

        volume->private = ioctx;

        return 0;
err_free:
        yfree1((void **)&ioctx);
err_ret:
        return ret;
}

STATIC void __lichio_disconnect_gateway(struct iscsi_volume *volume)
{
        lichbd_ioctx_t *ioctx;

        ioctx = volume->private;

        lichbd_disconnect(ioctx);

        yfree((void **)&ioctx);

        return;
}

STATIC int __lichio_connect(struct iscsi_volume *volume, struct uss_lun_entry *lu)
{
        int ret;
        lichbd_ioctx_t *ioctx;

        ret = ymalloc((void **)&ioctx, sizeof(*ioctx));
        if (unlikely(ret))
                GOTO(err_ret, ret);

        ioctx->fileid = lu->fileid;
        volume->private = ioctx;

        return 0;
err_ret:
        return ret;
}

STATIC void __lichio_disconnect(struct iscsi_volume *volume)
{

        struct lichio_data *priv = volume->private;
        fileid_t *fileid;

        (void) fileid;

        fileid = &priv->fileid;

        DINFO("detach %s/%u\n", volume->tname, volume->lun);

        free(priv);
        volume->private = NULL;

        return;
}

STATIC int __lichio_attach(struct iscsi_volume *volume, void *entry)
{
        int ret;
        struct uss_lun_entry *lu;

        if (unlikely(volume->private)) {
                ret = EBUSY;
                GOTO(err_ret, ret);
        }

        if (sanconf.iscsi_gateway) {
                ret = __lichio_connect_gateway(volume, entry);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        } else {
                ret = __lichio_connect(volume, entry);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        }

        /**
         * Block size shift
         *       9 -> 512
         *      12 -> 4096
         */
        lu = entry;
        if (unlikely(lu->blk_shift != 9 && lu->blk_shift != 12)) {
                DWARN("invalid block shift %d\n", lu->blk_shift);
                YASSERT(0);
        }

        volume->blk_shift = lu->blk_shift;
        volume->blk_cnt   = (u64)(lu->blk_size >> volume->blk_shift);
        volume->blk_size  = lu->blk_size;

        /*
         * Set Logic Unit Attribute Feature
         */
#if 0
        SetLURCache(volume);
        SetLUWCache(volume);
        SetLUReadonly(volume);
#endif

        DINFO("attach %s/%u (size: %llu bytes)(block: %u bytes)\n",
              volume->tname, volume->lun,
              (LLU)(lu->blk_size), (1 << volume->blk_shift));

        return 0;
err_ret:
        return ret;
}

STATIC int __lichio_update(struct iscsi_volume *volume, void *entry)
{
        struct uss_lun_entry *lu;

        lu = entry;

        /**
         * Block size shift
         *       9 -> 512
         *      12 -> 4096
         */
        if (lu->blk_shift != 9 && lu->blk_shift != 12) {
                DWARN("invalid block shift %d\n", lu->blk_shift);
                YASSERT(0);
        }

        volume->blk_shift = lu->blk_shift;
        volume->blk_cnt   = (u64)(lu->blk_size >> volume->blk_shift);
        volume->blk_size  = lu->blk_size;

        /*
         * Set Logic Unit Attribute Feature
         */
#if 0
        SetLURCache(volume);
        SetLUWCache(volume);
        SetLUReadonly(volume);
#endif

        DINFO("update %s/%u (size: %llu bytes)(block: %u bytes)\n",
              volume->tname, volume->lun,
              (LLU)(lu->blk_size), (1 << volume->blk_shift));

        return 0;
}

#if ENABLE_ISCSI_CACHE_REUSE
static inline int __get_buffer_seg_count(buffer_t *buf)
{
        int count = 0;
        struct list_head *pos;

        list_for_each(pos, &buf->list) {
                count++;
        }

        return count;
}

STATIC int __lichio_io_read__(struct iscsi_cmd *cmd)
{
        int ret;
        io_t io;
        mcache_entry_t *entry;

        struct iscsi_target *target = cmd->lun->target;

retry:
        entry = target->volume_entrys[cmd->lun->lun];
        if (unlikely(!entry)) {
                ret = volume_ctl_get(&cmd->ioctx->fileid, &entry);
                if (unlikely(ret)) {
                        entry = NULL;
                } else {
                        if (target->volume_entrys[cmd->lun->lun]) {
                                volume_ctl_release(entry);
                                entry = target->volume_entrys[cmd->lun->lun];
                        } else {
                                target->volume_entrys[cmd->lun->lun] = entry;
                        }
                }
        }

        if (likely(entry)) {
                io_init(&io, &cmd->ioctx->fileid, NULL, cmd->tio->io_off, cmd->tio->io_len, 0);
                ret = volume_ctl_read_direct(entry, &io, &cmd->tio->buffer, 1);
                if (unlikely(ret)) {
                        if (ret == EREMCHG || ret == ESTALE || ret == ENOENT) {
                                /* maybe released */
                                if (target->volume_entrys[cmd->lun->lun]) {
                                        volume_ctl_release(entry);
                                        target->volume_entrys[cmd->lun->lun] = NULL;
                                }
                                DWARN(CHKID_FORMAT" moved\n", CHKID_ARG(&cmd->ioctx->fileid));

                                if (ret == EREMCHG) {
                                        DWARN("conn close, write "CHKID_FORMAT" (%llu, %llu)\n",
                                                        CHKID_ARG(&cmd->ioctx->fileid),
                                                        (LLU)cmd->tio->io_off, (LLU)cmd->tio->io_len);
                                        ret = ESHUTDOWN;
                                        GOTO(err_ret, ret);
                                } else {
                                        goto retry;
                                }
                        } else
                                GOTO(err_ret, ret);
                }
        } else {
                ret = stor_read(target->pool, &cmd->ioctx->fileid, &cmd->tio->buffer,
                                cmd->tio->io_len, cmd->tio->io_off);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        }

        if (__get_buffer_seg_count(&cmd->tio->buffer) > 2) {
                DERROR("the io size %lu io offet %lu\n", cmd->tio->io_len, cmd->tio->io_off);
                YASSERT(0);
        }

        return 0;
err_ret:
        return ret;
}

#else

STATIC int __lichio_io_read__(struct iscsi_cmd *cmd)
{
        int ret;
        struct iscsi_target *target = cmd->lun->target;
        nid_t nid;

        if (sanconf.iscsi_gateway) {
                ret = lichbd_pread(cmd->ioctx, &cmd->tio->buffer,
                                    cmd->tio->io_len, cmd->tio->io_off, 0);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        } else {
                ret = stor_location(target->pool, &cmd->ioctx->fileid, &nid);
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                /**
                 * maybe stor_location will take very long time, so check conn state before read.
                 * 2018-04-05 11:01:03/1522897263 18906/18924 frame[9]: /opt/fusionstack/lich/lib/liblich4s.so(locator_rpc_lookup+0x97b) [0x7f06b6e911c3]
                 * 2018-04-05 11:01:03/1522897263 18906/18924 frame[10]: /opt/fusionstack/lich/lib/liblich4s.so(md_map_drop+0x4db) [0x7f06b6ed2675]
                 * 2018-04-05 11:01:03/1522897263 18906/18924 frame[11]: /opt/fusionstack/lich/lib/liblich4s.so(md_getattr+0x258) [0x7f06b6e9ff72]
                 * 2018-04-05 11:01:03/1522897263 18906/18924 frame[12]: /opt/fusionstack/lich/lib/liblich4s.so(__vnode_load_info+0x3b) [0x7f06b6e31e1e]
                 * 2018-04-05 11:01:03/1522897263 18906/18924 frame[13]: /opt/fusionstack/lich/lib/liblich4s.so(__vnode_register+0x494) [0x7f06b6e3248c]
                 * 2018-04-05 11:01:03/1522897263 18906/18924 frame[14]: /opt/fusionstack/lich/lib/liblich4s.so(vnode_get+0x2b4) [0x7f06b6e341ae]
                 * 2018-04-05 11:01:03/1522897263 18906/18924 frame[15]: /opt/fusionstack/lich/lib/liblich4s.so(vnode_location+0x280) [0x7f06b6e3601b]
                 */
                if (unlikely(cmd->conn->state == STATE_CLOSE)) {
                        DINFO("conn close, read "CHKID_FORMAT" (%llu, %llu)\n",
                                        CHKID_ARG(&cmd->ioctx->fileid),
                                        (LLU)cmd->tio->io_off, (LLU)cmd->tio->io_len);
                        ret = EIO;
                        GOTO(err_ret, ret);
                }

                ret = stor_read_withnid(target->pool, &cmd->ioctx->fileid, &cmd->tio->buffer,
                                 cmd->tio->io_len, cmd->tio->io_off, &nid);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        }

        return 0;
err_ret:
        return ret;
}

#endif

STATIC int __lichio_io_read(struct iscsi_cmd *cmd)
{
        int ret, retry = 0;
        time_t begin = gettime();

        ANALYSIS_BEGIN(0);
        
        schedule_task_setname("iscsi_read");

        YASSERT(cmd->tio->buffer.len == 0);
        //	YASSERT(cmd->tio->buffer.list.next == &cmd->tio->buffer.list);
        DBUG("iscsi_lsv read %llu %llu\n", (LLU)cmd->tio->io_off, (LLU)cmd->tio->io_len);

        if (unlikely(cmd->tio->io_off + cmd->tio->io_len > cmd->lun->blk_size)) {
                ret = EIO;
                GOTO(err_ret, ret);
        }

        cmd->ioctx = cmd->lun->private;

#if ISCSI_IO_RECORD
        DINFO("begin read, iscsi io record read "CHKID_FORMAT" (%llu, %llu)\n",
                        CHKID_ARG(&cmd->ioctx->fileid),
                        (LLU)cmd->tio->io_off, (LLU)cmd->tio->io_len);
#endif

retry:
        if (unlikely(cmd->conn->state == STATE_CLOSE || cmd->conn->state == STATE_CLOSED)) {
                DINFO("conn close, read "CHKID_FORMAT" (%llu, %llu)\n",
                                CHKID_ARG(&cmd->ioctx->fileid),
                                (LLU)cmd->tio->io_off, (LLU)cmd->tio->io_len);
                ret = EIO;
                GOTO(err_ret, ret);
        }

        ret = __lichio_io_read__(cmd);
        if (unlikely(ret)) {
                ret = _errno(ret);
                if (retry < 1000 && (ret == EAGAIN || ret == ENOSPC)
                                && ((gettime() - begin) < sanconf.iscsi_timeout + 20)) {
                        if (retry > 100) {
                                DINFO("read "CHKID_FORMAT" (%llu, %llu),"
                                      " ret (%d) %s, need retry %u\n",
                                      CHKID_ARG(&cmd->ioctx->fileid),
                                      (LLU)cmd->tio->io_off, (LLU)cmd->tio->io_len,
                                      ret, strerror(ret), retry);
                        }

                        retry++;
                        schedule_sleep("iscsi_read", 1000 * 100);
                        goto retry;
                } else if (ret == EPERM || ret == ESHUTDOWN) {
                        if (cmd->conn->state != STATE_CLOSE && cmd->conn->state != STATE_CLOSED)
                                cmd->conn->state = STATE_CLOSE;
                        DERROR("conn close, read "CHKID_FORMAT" (%llu, %llu)\n",
                                        CHKID_ARG(&cmd->ioctx->fileid),
                                        (LLU)cmd->tio->io_off, (LLU)cmd->tio->io_len);
                        if (ret == ESHUTDOWN) {
                                return 0;
                        } else {
                                ret = EIO;
                                GOTO(err_ret, ret);
                        }
                } else {
                        if (cmd->conn->state == STATE_CLOSE || cmd->conn->state == STATE_CLOSED) {
                                DWARN("conn close, read "CHKID_FORMAT" (%llu, %llu)\n",
                                                CHKID_ARG(&cmd->ioctx->fileid),
                                                (LLU)cmd->tio->io_off, (LLU)cmd->tio->io_len);
                                ret = EIO;
                                GOTO(err_ret, ret);
                        } else {
                                DERROR("read "CHKID_FORMAT" cmd(%llu, %llu), ret (%d) %s\n",
                                                CHKID_ARG(&cmd->ioctx->fileid), (LLU)cmd->tio->io_off,
                                                (LLU)cmd->tio->io_len, ret, strerror(ret));
                                SERROR(0, "%s, read "CHKID_FORMAT" cmd(%llu, %llu), ret (%d) %s\n",
                                                M_PROTO_ISCSI_ERROR, CHKID_ARG(&cmd->ioctx->fileid), (LLU)cmd->tio->io_off,
                                                (LLU)cmd->tio->io_len, ret, strerror(ret));
                                GOTO(err_ret, ret);
                        }
                }
        }

#if ISCSI_IO_RECORD
        char tmp[MAX_INFO_LEN];

        sprintf(tmp, "iscsi read ok, iscsi io record read "CHKID_FORMAT" (%llu, %llu), ",
                        CHKID_ARG(&cmd->ioctx->fileid),
                        (LLU)cmd->tio->io_off, (LLU)cmd->tio->io_len);

        mbuffer_dump(&cmd->tio->buffer, 8, tmp);
#endif

        ANALYSIS_QUEUE(0, IO_WARN, "lich_io_read");
        
        return 0;
err_ret:
        /* @---------- */
        //cops->scan_async();
        /* @---------- */

        return ret;
}

#if ENABLE_ISCSI_CACHE_REUSE
STATIC int __lichio_io_write__(struct iscsi_cmd *cmd)
{
        int ret;
        io_t io;
        mcache_entry_t *entry;

        struct iscsi_target *target = cmd->lun->target;

retry:
        entry = target->volume_entrys[cmd->lun->lun];
        if (unlikely(!entry)) {
                ret = volume_ctl_get(&cmd->ioctx->fileid, &entry);
                if (unlikely(ret)) {
                        entry = NULL;
                } else {
                        if (target->volume_entrys[cmd->lun->lun]) {
                                volume_ctl_release(entry);
                                entry = target->volume_entrys[cmd->lun->lun];
                        } else {
                                target->volume_entrys[cmd->lun->lun] = entry;
                        }
                }
        }

        if (likely(entry)) {
                io_init(&io, &cmd->ioctx->fileid, NULL, cmd->tio->io_off, cmd->tio->io_len, 0);
                ret = volume_ctl_write_direct(entry, &io, &cmd->tio->buffer, 1);
                if (unlikely(ret)) {
                        if (ret == EREMCHG || ret == ESTALE || ret == ENOENT) {
                                /* maybe released */
                                if (target->volume_entrys[cmd->lun->lun]) {
                                        volume_ctl_release(entry);
                                        target->volume_entrys[cmd->lun->lun] = NULL;
                                }
                                DWARN(CHKID_FORMAT" moved\n", CHKID_ARG(&cmd->ioctx->fileid));

                                if (ret == EREMCHG) {
                                        DWARN("conn close, write "CHKID_FORMAT" (%llu, %llu)\n",
                                                        CHKID_ARG(&cmd->ioctx->fileid),
                                                        (LLU)cmd->tio->io_off, (LLU)cmd->tio->io_len);
                                        ret = ESHUTDOWN;
                                        GOTO(err_ret, ret);
                                } else {
                                        goto retry;
                                }
                        } else
                                GOTO(err_ret, ret);
                }
        } else {
                ret = stor_write(target->pool, &cmd->ioctx->fileid, &cmd->tio->buffer,
                                cmd->tio->io_len, cmd->tio->io_off);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        }

        return 0;
err_ret:
        return ret;
}

#else

STATIC int __lichio_io_write__(struct iscsi_cmd *cmd)
{
        int ret;
        struct iscsi_target *target = cmd->lun->target;
        nid_t nid;

        if (sanconf.iscsi_gateway) {
                ret = lichbd_pwrite(cmd->ioctx, &cmd->tio->buffer,
                                     cmd->tio->io_len, cmd->tio->io_off, 0);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        } else {
                ret = stor_location(target->pool, &cmd->ioctx->fileid, &nid);
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                /**
                 * maybe stor_location will take very long time, so check conn state before write.
                 * 2018-04-05 11:01:03/1522897263 18906/18924 frame[9]: /opt/fusionstack/lich/lib/liblich4s.so(locator_rpc_lookup+0x97b) [0x7f06b6e911c3]
                 * 2018-04-05 11:01:03/1522897263 18906/18924 frame[10]: /opt/fusionstack/lich/lib/liblich4s.so(md_map_drop+0x4db) [0x7f06b6ed2675]
                 * 2018-04-05 11:01:03/1522897263 18906/18924 frame[11]: /opt/fusionstack/lich/lib/liblich4s.so(md_getattr+0x258) [0x7f06b6e9ff72]
                 * 2018-04-05 11:01:03/1522897263 18906/18924 frame[12]: /opt/fusionstack/lich/lib/liblich4s.so(__vnode_load_info+0x3b) [0x7f06b6e31e1e]
                 * 2018-04-05 11:01:03/1522897263 18906/18924 frame[13]: /opt/fusionstack/lich/lib/liblich4s.so(__vnode_register+0x494) [0x7f06b6e3248c]
                 * 2018-04-05 11:01:03/1522897263 18906/18924 frame[14]: /opt/fusionstack/lich/lib/liblich4s.so(vnode_get+0x2b4) [0x7f06b6e341ae]
                 * 2018-04-05 11:01:03/1522897263 18906/18924 frame[15]: /opt/fusionstack/lich/lib/liblich4s.so(vnode_location+0x280) [0x7f06b6e3601b]
                 */
                if (unlikely(cmd->conn->state == STATE_CLOSE)) {
                        DINFO("conn close, write "CHKID_FORMAT" (%llu, %llu)\n",
                                        CHKID_ARG(&cmd->ioctx->fileid), (LLU)cmd->tio->io_off,
                                        (LLU)cmd->tio->io_len);
                        ret = EIO;
                        GOTO(err_ret, ret);
                }

                ret = stor_write_withnid(target->pool, &cmd->ioctx->fileid, &cmd->tio->buffer,
                                  cmd->tio->io_len, cmd->tio->io_off, &nid);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        }

        return 0;
err_ret:
        return ret;
}

#endif


STATIC int __lichio_io_write(struct iscsi_cmd *cmd)
{
        int ret, retry = 0;
        time_t begin = gettime();

        schedule_task_setname("iscsi_write");

        YASSERT(cmd->tio->buffer.len == cmd->tio->io_len);

        DBUG("iscsi_lsv write %llu %llu\n", (LLU)cmd->tio->io_off, (LLU)cmd->tio->io_len);

        ANALYSIS_BEGIN(0);
        
        if (unlikely(cmd->tio->io_off + cmd->tio->io_len > cmd->lun->blk_size)) {
                ret = EIO;
                GOTO(err_ret, ret);
        }

        YASSERT(cmd->tio->buffer.len == cmd->tio->io_len);

        if (likely(cmd->lun->private != NULL)) {
                cmd->ioctx = cmd->lun->private;
        } else {
                ret = EIO;
                GOTO(err_ret, ret);
        }

#if ISCSI_IO_RECORD
        DINFO("iscsi io record write "CHKID_FORMAT" (%llu, %llu)\n",
                        CHKID_ARG(&cmd->ioctx->fileid), (LLU)cmd->tio->io_off,
                        (LLU)cmd->tio->io_len);
#endif

retry:
        if (unlikely(cmd->conn->state == STATE_CLOSED || cmd->conn->state == STATE_CLOSE)) {
                DINFO("conn close, write "CHKID_FORMAT" (%llu, %llu)\n",
                                CHKID_ARG(&cmd->ioctx->fileid), (LLU)cmd->tio->io_off,
                                (LLU)cmd->tio->io_len);
                ret = EIO;
                GOTO(err_ret, ret);
        }

        /**
         * @note 有选择地屏蔽底层的EIO，如拔盘等场景
         */
        ret = __lichio_io_write__(cmd);
        if (unlikely(ret)) {
                ret = _errno(ret);
                if (retry < 1000 && (ret == EAGAIN || ret == ENOSPC)
                                && ((gettime() - begin) < sanconf.iscsi_timeout + 20)) {
                        if (retry > 100) {
                                DINFO("write "CHKID_FORMAT" (%llu, %llu),"
                                      " ret (%d) %s, need retry %u\n",
                                      CHKID_ARG(&cmd->ioctx->fileid),
                                      (LLU)cmd->tio->io_off, (LLU)cmd->tio->io_len,
                                      ret, strerror(ret), retry);
                        }

                        retry++;
                        schedule_sleep("iscsi_write", 1000 * 100);
                        goto retry;
                } else if (ret == EPERM || ret == ESHUTDOWN) {
                        if (cmd->conn->state != STATE_CLOSE && cmd->conn->state != STATE_CLOSED)
                                cmd->conn->state = STATE_CLOSE;
                        DERROR("conn close, write "CHKID_FORMAT" (%llu, %llu)\n",
                                        CHKID_ARG(&cmd->ioctx->fileid),
                                        (LLU)cmd->tio->io_off, (LLU)cmd->tio->io_len);
                        if (ret == ESHUTDOWN) {
                                return 0;
                        } else {
                                ret = EIO;
                                GOTO(err_ret, ret);
                        }
                } else {
                        if (cmd->conn->state == STATE_CLOSE || cmd->conn->state == STATE_CLOSED) {
                                DWARN("conn close, write "CHKID_FORMAT" (%llu, %llu)\n",
                                        CHKID_ARG(&cmd->ioctx->fileid),
                                      (LLU)cmd->tio->io_off, (LLU)cmd->tio->io_len);
                                ret = EIO;
                                GOTO(err_ret, ret);
                        } else {
                                DERROR("write "CHKID_FORMAT" cmd(%llu, %llu), begin %ld ret (%d) %s\n",
                                      CHKID_ARG(&cmd->ioctx->fileid), (LLU)cmd->tio->io_off,
                                       (LLU)cmd->tio->io_len, begin, ret, strerror(ret));
                                SERROR(0, "%s, write "CHKID_FORMAT" cmd(%llu, %llu), begin %ld ret (%d) %s\n",
                                        M_PROTO_ISCSI_ERROR, CHKID_ARG(&cmd->ioctx->fileid), (LLU)cmd->tio->io_off,
                                        (LLU)cmd->tio->io_len, begin, ret, strerror(ret));
                                GOTO(err_ret, ret);
                        }
                }
        }

#if ISCSI_IO_RECORD
        char tmp[MAX_INFO_LEN];
        sprintf(tmp, "lich_io write ok, iscsi io record read "CHKID_FORMAT" (%llu, %llu), ",
                        CHKID_ARG(&cmd->ioctx->fileid),
                        (LLU)cmd->tio->io_off, (LLU)cmd->tio->io_len);

        mbuffer_dump(&cmd->tio->buffer, 8, tmp);
#endif
        ANALYSIS_QUEUE(0, IO_WARN, "lich_io_write");
        
        return 0;
err_ret:
        /* @---------- */
        //cops->scan_async();
        /* @---------- */

        return ret;
}

STATIC int __lichio_io_unmap__(struct iscsi_cmd *cmd, uint64_t offset, uint32_t length)
{
        struct iscsi_target *target = cmd->lun->target;

        DINFO("__lichio_io_unmap__ %ju, %d\r\n", offset, length);

        return stor_unmap(target->pool, &cmd->ioctx->fileid,
                length, offset);
}

STATIC int __lichio_io_unmap(struct iscsi_cmd *cmd, uint64_t lba, uint32_t count)
{
        int ret;
        uint64_t off;
        uint32_t len;

        schedule_task_setname("iscsi_unmap");

        off = lba * (1U << cmd->lun->blk_shift);
        len = count * (1U << cmd->lun->blk_shift);

        off -= off % LICH_CHUNK_SPLIT;          //align to chunk.
        
        off = off / LICH_CHUNK_SPLIT * LICH_CHUNK_SPLIT;
        len = len / LICH_CHUNK_SPLIT * LICH_CHUNK_SPLIT;

        if(len == 0)
                return 0;
        
        DINFO("iscsi_lsv unmap %llu %llu\n", (LLU)off, (LLU)len);

        if (unlikely(lba + count > cmd->lun->blk_size)) {
                ret = EIO;
                GOTO(err_ret, ret);
        }

        if (likely(cmd->lun->private != NULL)) {
                cmd->ioctx = cmd->lun->private;
        } else {
                ret = EIO;
                GOTO(err_ret, ret);
        }

#if ISCSI_IO_RECORD
        DINFO("iscsi io record unmap "CHKID_FORMAT" (%llu, %llu)\n",
                        CHKID_ARG(&cmd->ioctx->fileid), (LLU)off, (LLU)len);
#endif

        if (unlikely(cmd->conn->state == STATE_CLOSE)) {
                DINFO("conn close, unmap "CHKID_FORMAT" (%llu, %llu)\n",
                      CHKID_ARG(&cmd->ioctx->fileid), (LLU)off, (LLU)len);
                ret = EIO;
                GOTO(err_ret, ret);
        }

        ret = __lichio_io_unmap__(cmd, off, len);
        if (unlikely(ret)) {
                ret = _errno(ret);
                GOTO(err_ret, ret);
        }

        return 0;
err_ret:
        /* @---------- */
        //cops->scan_async();
        /* @---------- */

        return ret;
}

STATIC int __lichio_sync(struct iscsi_cmd *cmd)
{
        (void) cmd;
        return 0;
}

STATIC int __lichio_detach(struct iscsi_volume *lu)
{
        if (sanconf.iscsi_gateway) {
                __lichio_disconnect_gateway(lu);
        } else {
                __lichio_disconnect(lu);
        }
        return 0;
}

struct iotype lich_io = {
        .attach = __lichio_attach,
        .detach = __lichio_detach,

        .aio_read = __lichio_io_read,
        .aio_write = __lichio_io_write,

        .update = __lichio_update,
        .unmap = __lichio_io_unmap,
        .sync = __lichio_sync,
};
