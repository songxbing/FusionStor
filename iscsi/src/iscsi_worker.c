#include "config.h"

#include <stdlib.h>

#define DBG_SUBSYS      S_LIBINTERFACE

#include "iscsi.h"
#include "dbg.h"

int worker_thread_queue(struct iscsi_cmd *cmd)
{
        //conn_busy_get(cmd->conn);
        if (cmd->flags & CMD_FLG_TMF_ABORT) {
                //struct iscsi_conn *conn = cmd->conn;
                iscsi_cmd_release(cmd, 1);
                //conn_busy_put(conn);
        } else {
                iscsid_exec(cmd);
        }

        return 0;
}
