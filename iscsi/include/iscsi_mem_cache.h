#ifndef MEM_CACHE_H
#define MEM_CACHE_H

struct iscsi_mem_cache {
        char *name;                     /* Name */

        u32 base_nr;                    /* The base number of current pool */
        u32 max_nr;                     /* The maximum number of current pool */
        u32 idx;                        /* Index of next free slot */

        u32 align;                      /* Align flag */

        u32 unit_size;                  /* Unit size of pool object */
        u32 real_size;                  /* Real size of pool object */

        void **pool;                    /* Array of pointers that point to units */
};

/*
 * Alloc can't fail. If there is no memory, function will block and wait for
 * memory alloc success.
 */
#define MC_FLAG_NOFAIL  0x01

extern struct iscsi_mem_cache *iscsi_mem_mcache_create(char *, u32, u32, u8);
extern void iscsi_mem_mcache_destroy(struct iscsi_mem_cache *);

extern void *iscsi_mem_mcache_alloc(struct iscsi_mem_cache *, u8);
extern void *iscsi_mem_mcache_calloc(struct iscsi_mem_cache *, u8);
extern void iscsi_mem_mcache_free(struct iscsi_mem_cache *, void *);

#endif /* MEM_CACHE_H */
