#ifndef __ISER_HDR_H__
#define __ISER_HDR_H__

#include <linux/types.h>
#include <types.h>

#define ISCSI_LOGIN_MAX_RDSL      (8 * 1024)

/*
 * The IB-extended version from the kernel.  Stags and VAs are in
 * big-endian format.
 */
struct iser_hdr {
        uint8_t   flags;
        uint8_t   rsvd[3];
        uint32_t  write_stag; /* write rkey */
        uint64_t  write_va;
        uint32_t  read_stag;  /* read rkey */
        uint64_t  read_va;
} __attribute__((packed));

#define ISER_WSV                0x08
#define ISER_RSV                0x04

#define ISER_ISCSI_CTRL           0x10
#define ISER_HELLO                0x20
#define ISER_HELLORPLY            0x30

#define ISER_HDRS_SZ                (sizeof(struct iser_hdr) + sizeof(struct iscsi_hdr))

#endif
