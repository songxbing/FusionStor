#ifndef __ISER_RDMA_H__
#define __ISER_RDMA_H__

#include <netdb.h>
#include <infiniband/verbs.h>

#include "iscsi.h"
#include "iser.h"
#include "core.h"

/*
 * Crazy hard-coded linux iser settings need 128 * 8 slots + slop, plus
 * room for our rdmas and send requests.
 */

#define MAX_WQE 1800

int iser_rdma_create_channel(int *fd, core_t *core);
void iser_rdma_release();

int iser_rdma_get_event(struct rdma_cm_event **ev, core_t *core);
int iser_rdma_ack_event(struct rdma_cm_event *ev);

int iser_rdma_create(struct rdma_cm_id **cm_id);
int iser_rdma_destroy_id(struct rdma_cm_id *cm_id);

int iser_rdma_disconnect(struct rdma_cm_id *cm_id);
int iser_rdma_notify(struct rdma_cm_id *cm_id);
int iser_rdma_accept(struct rdma_cm_id *cm_id, struct rdma_conn_param *conn_param);
int iser_rdma_reject(struct rdma_cm_id *cm_id);

int iser_rdma_create_qp(struct rdma_cm_id *cm_id, struct iser_conn *conn);
int iser_rdma_destroy_qp(struct rdma_cm_id *cm_id);

int iser_rdma_add_portal(struct addrinfo *res, short int port, core_t *core);
void iser_rdma_delete_portals(void);

#endif  /* __ISER_RDMA_H__ */
