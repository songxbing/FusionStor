#ifndef YISCSI_H
#define YISCSI_H

#define ISCSI_LUN_MAX           254

#define ISCSI_LUN_NAME_MAX      4
//#define ISCSI_TGT_NAME_MAX      1024
#define ISCSI_IQN_NAME_MAX      223
#define ISCSI_TGT_NAME_MAX      200
#define ISCSI_CHECK_TARGET_CTIME TRUE

#define yiscsi_tgt_magic_key    "iscsi.is_target"
#define yiscsi_lun_alias_key    "iscsi.lun_alias"
#define yiscsi_lun_block_key    "iscsi.blk_shift"

/*
 * The `raw_removexattr' is not impliment now, use a special value
 * to express this.
 */
#define yiscsi_none_value       "____NONE____"

#endif /* YISCSI_H */
