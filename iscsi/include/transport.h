#ifndef __TRANSPORT_H__
#define __TRANSPORT_H__

#include <sys/socket.h>
#include "list.h"

struct iscsi_conn;
struct iscsi_task;

struct iscsi_transport {
	struct list_head iscsi_transport_siblings;

	const char *name;
	int rdma;
	int data_padding;

	int (*ep_init) (void);
	void (*ep_exit) (void);
	int (*ep_login_complete)(struct iscsi_conn *conn);
	struct iscsi_task *(*alloc_task)(struct iscsi_conn *conn,
					 size_t ext_len);
	void (*free_task)(struct iscsi_task *task);
	size_t (*ep_read)(struct iscsi_conn *conn, void *buf,
			  size_t nbytes);
	size_t (*ep_write_begin)(struct iscsi_conn *conn, void *buf,
				 size_t nbytes);
	void (*ep_write_end)(struct iscsi_conn *conn);
	int (*ep_rdma_read)(struct iscsi_conn *conn);
	int (*ep_rdma_write)(struct iscsi_conn *conn);
	size_t (*ep_close)(struct iscsi_conn *conn);
	void (*ep_force_close)(struct iscsi_conn *conn);
	void (*ep_release)(struct iscsi_conn *conn);

	int (*ep_show)(struct iscsi_conn *conn, char *buf, int rest);
	void (*ep_event_modify)(struct iscsi_conn *conn, int events);
	void *(*alloc_data_buf)(struct iscsi_conn *conn, size_t sz);
	void (*free_data_buf)(struct iscsi_conn *conn, void *buf);
	int (*ep_getsockname)(struct iscsi_conn *conn,
			      struct sockaddr *sa, socklen_t *len);
	int (*ep_getpeername)(struct iscsi_conn *conn,
			      struct sockaddr *sa, socklen_t *len);
};

extern int iscsi_transport_register(struct iscsi_transport *);

#endif
