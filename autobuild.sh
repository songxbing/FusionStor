#!/bin/bash

echo 'autobuid>>>> rm -rf /opt/fusionstack/*'
rm -rf /opt/fusionstack/*

#echo 'autobuid>>>> cd /root/lich4/'
#cd /root/lich4/

v_1=`cat ./v_num | cut -d _ -f 1`
vnum_old=`cat ./v_num | cut -d _ -f 2`
vnum_new=`expr $vnum_old + 1`
Vnum=$v_1"_"$vnum_new
echo $Vnum  > ./v_num
echo $Vnum

echo 'autobuid>>>> git pull'
git pull

echo 'autobuid>>>> rm -rf ./storage/build/.deps/*'
rm -rf ./build/.deps/*

#make distclean;
#make autogen;
#make conf;

echo 'autobuid>>>> rm -rf cb'
rm -rf cb

echo 'autobuid>>>> mkdir -p cb;cd cb'
mkdir -p cb;cd cb

echo '''autobuid>>>> cmake .. && make && make install && make tar'''
cmake ..
make
if [ $? -ne 0 ]; then
    echo
    echo "******************  make failed !!! ********************"
    exit $?
fi
make install
if [ $? -ne 0 ]; then
    echo
    echo "******************  make install failed !!! ********************"
    exit $?
fi
make tar
