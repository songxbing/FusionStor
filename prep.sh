#!/bin/bash

yum install -y epel-release
yum install -y libtool
yum install -y cmake
yum install -y redhat-lsb
yum install -y sqlite sqlite-devel
yum install -y uuid-devel
yum install -y flex bison
yum install -y git
yum install -y openssl openssl-devel
yum install -y libuuid libuuid-devel
yum install -y libaio libaio-devel
yum install -y python
yum install -y python-paramiko
yum install -y vim
yum install -y pciutils
#***********RDMA****************
yum install -y rdma-core rdma-core-devel libibverbs
#***********LICH****************
yum install -y hiredis hiredis-devel yajl yajl-devel etcd curl curl-devel python-setuptools net-tools

yum install -y numactl-devel numactl
yum install -y libpciaccess libpciaccess-devel