#ifndef __READCACHE__REDIS_H_
#define __READCACHE__REDIS_H_


#define REDIS_HOST "127.0.0.1"
#define REDIS_PORT (6379)
#define REDIS_UNIX "/tmp/redis.sock"

#undef READCACHE_CHECK

int readcache_init(const char *path);
int readcache_ready();

int readcache_save(const io_t *_io, uint64_t info_version, const buffer_t *_buf);
int readcache_io_split(const io_t *_io, uint64_t info_version, buffer_t *head, buffer_t *tail, io_t *io_new);
int readcache_check(const io_t *_io, uint64_t info_version, const buffer_t *_buf);

#endif
