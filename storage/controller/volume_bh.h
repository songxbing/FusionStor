#ifndef __FILE_BH_H__
#define __FILE_BH_H__

#define BH_FILE_CLEANUP  0x00000001
#define BH_SNAP_CLEANUP  0x00000002
#define BH_SNAP_ROLLBACK 0x00000004
#define BH_SNAP_FLAT     0x00000008

int volume_bh_init(void);

int volume_bh_filecleanup(const chkid_t *chkid, const char *name);
int volume_bh_snapcleanup(const chkid_t *chkid, const char *name);

int volume_bh_add(const chkid_t *chkid, int op, int check_exists);

#endif
