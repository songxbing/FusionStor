#ifndef __FS_SRV__
#define __FS_SRV__

#include "fs_proto.h"
#include "fileinfo.h"
#include "lichstor.h"
#include "stor_rpc.h"

#define STOR_STAT_WHOLLY ((uint64_t)-1)

int stor_ctl_init(uint64_t max_chunk);

// pool

int stor_ctl_mkpool(const fileid_t *parent, const char *name, const char *site_name,
                 const setattr_t *setattr, chkinfo_t *chkinfo);
int stor_ctl_rmpool(const chkid_t *chkid, const char *name);

int stor_ctl_listpool_open(const fileid_t *parent, const char *uuid);
int stor_ctl_listpool(const fileid_t *parent, const char *uuid, uint64_t offset, void *de, int *delen);
int stor_ctl_listpool_close(const fileid_t *parent, const char *uuid);

int stor_ctl_extend_pool(const chkid_t *poolid);

// volume

int stor_ctl_mkvol(const fileid_t *parent, const char *name, const char *site_name,
                   const setattr_t *setattr, chkinfo_t *chkinfo);
int stor_ctl_mkvolwith(const fileid_t *parent, const char *name,
                       const chkinfo_t *chkinfo);
int stor_ctl_rmvol(const chkid_t *chkid, const char *name, int force);

int stor_ctl_lookup(const fileid_t *parent, const char *name,
                    chkinfo_t *chkinfo);
int stor_ctl_lookup_srv(const fileid_t *chkid, const fileid_t *parent);
int stor_ctl_stat(const fileid_t *fileid, filestat_t *filestat, off_t off, size_t size);
int stor_ctl_check_ready(const fileid_t *fileid);

int stor_ctl_cleanup(const chkid_t *chkid, const char *name);
int stor_ctl_cleanup_bh(const chkid_t *chkid, const char *name);
int stor_ctl_rmsnap_bh(const chkid_t *chkid, const char *name);
int stor_ctl_rollback_bh(const chkid_t *chkid);
int stor_ctl_flat_bh(const chkid_t *chkid);

int stor_ctl_getpool(const fileid_t *chkid, char *pool);
int stor_ctl_getattr(const fileid_t *chkid, fileinfo_t *fileinfo);
int stor_ctl_setattr(const fileid_t *chkid, fileinfo_t *fileinfo, const setattr_t *setattr);

int stor_ctl_rename_lock(const fileid_t *fileid, const fileid_t *src, const char *name, int force);
int stor_ctl_rename_unlock(const fileid_t *fileid, const chkinfo_t *chkinfo);
int stor_ctl_rename(const fileid_t *from, const char *fname,
                  const fileid_t *to, const char *tname);
int stor_ctl_move(const chkid_t *chkid, const nid_t *nid, int count);
int stor_ctl_localize(const chkid_t *chkid, int idx);

int stor_ctl_table_read(const fileid_t *fileid, const chkid_t *chkid, buffer_t *buf, uint32_t size, uint64_t offset);
int stor_ctl_table_write(const fileid_t *fileid, const chkid_t *chkid, const buffer_t *buf, uint32_t size, uint64_t offset);

int stor_ctl_connection(const fileid_t *fileid, const nid_t *nid, void *list, int *count);
int stor_ctl_connect(const fileid_t *fileid, const nid_t *nid, const char *addr);
int stor_ctl_disconnect(const fileid_t *fileid, const nid_t *nid, const char *addr);

// snapshot ops

int stor_ctl_snapshot_create(const fileid_t *fileid, const char *name, int p, const char *_site);
int stor_ctl_snapshot_remove(const fileid_t *fileid, const char *name, int force);
int stor_ctl_snapshot_rollback(const fileid_t *fileid, const char *name);

int stor_ctl_snapshot_flat(const fileid_t *fileid, const int idx, int force);
int stor_ctl_snapshot_protect(const fileid_t *fileid, const snap_protect_param_t on);

int stor_ctl_snapshot_listopen(const fileid_t *parent, const char *uuid);
int stor_ctl_snapshot_list(const chkid_t *parent, const char *uuid, uint64_t offset, void *de, int *delen);
int stor_ctl_snapshot_listclose(const fileid_t *parent, const char *uuid);

//int stor_ctl_snapshot_cleanup(const fileid_t *fileid);

/**
 * @return 0
 * @return ENOKEY
 * @return EBUSY
 */
int stor_ctl_snapshot_check(const fileid_t *, const char *);
int stor_ctl_snapshot_isempty(const fileid_t *fileid, int *empty);

int stor_ctl_snapshot_updateparent(const fileid_t *fileid, const char *name, const uint64_t from);
int stor_ctl_snapshot_setfrom(const fileid_t *fileid, const uint64_t from);
int stor_ctl_snapshot_last(const fileid_t *fileid, nid_t *snapnid, fileid_t *snapid, char *snapname, uint64_t *snap_version);
int stor_ctl_snapshot_prev(const fileid_t *fileid, const fileid_t *snapid, fileid_t *previd, char *name, uint64_t *snap_version);

int stor_ctl_group_snapshot_lock(const fileid_t *, uuid_t *);
int stor_ctl_group_snapshot_unlock(const fileid_t *, const uuid_t);
int stor_ctl_group_snapshot_create_nolock(const fileid_t *, const char *, const char *, const uuid_t);
int stor_ctl_group_snapshot_remove_nolock(const fileid_t *, const char *, const uuid_t);

// xattr ops

int stor_ctl_xattr_get(const fileid_t *fileid, const char *key, char *value, int *valuelen);
int stor_ctl_xattr_list(const fileid_t *fileid, char *buf, int *buflen);
int stor_ctl_xattr_set(const fileid_t *fileid, const char *key, const char *value,
                       uint32_t valuelen, int flag);
int stor_ctl_xattr_remove(const fileid_t *fileid, const char *key);

// chunk ops

int stor_ctl_chunk_allocate(const fileid_t *fileid, const chkid_t *chkid, int chknum, int fill);
int stor_ctl_chunk_cleanup(const fileid_t *parent, const chkid_t *chkid, const nid_t *nid, uint64_t meta_version);

int stor_ctl_chunk_getinfo(const fileid_t *parent, const fileid_t *chkid, chkinfo_t *chkinfo);
int stor_ctl_chunk_exist(const fileid_t *fileid, const chkid_t *chkid, int *exist);

int stor_ctl_chunk_set(const fileid_t *parent, const chkid_t *chkid, const nid_t *nid, int status);
int stor_ctl_chunk_check(const fileid_t *parent, const chkid_t *chkid, int idx, int async, int force, int *oflags);
int stor_ctl_chunk_check_multi(const fileid_t *parent, const chkid_t *chkid, int chk_count, nid_t *srcnid, int *retval);
int stor_ctl_chunk_move(const fileid_t *parent, const chkid_t *chkid, const nid_t *nid, int count);
int stor_ctl_chunk_migrate(const fileid_t *parent, const chkid_t *chkid, uint32_t force,
                          chkinfo_t *chkinfo);
int stor_ctl_chunk_update(const fileid_t *parent, const chkinfo_t *chkinfo, const nid_t *owner, uint64_t info_version);
int stor_ctl_chunk_reject(const fileid_t *parent, const chkid_t *chkid, const nid_t *bad,
                          chkinfo_t *chkinfo);

int stor_ctl_vfm_get(const fileid_t *parent, const chkid_t *chkid, vfm_t *vfm);
int stor_ctl_vfm_set(const fileid_t *parent, const chkid_t *chkid, const vfm_t *vfm);
int stor_ctl_vfm_cleanup(const fileid_t *fileid, const chkid_t *tid);
int stor_ctl_vfm_stat(const fileid_t *fileid, int *count);

#endif
