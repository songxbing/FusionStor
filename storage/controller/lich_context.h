#ifndef __LICH_CONTEXT_H
#define __LICH_CONTEXT_H

#include "chunk.h"

typedef struct {
	fileid_t parent;
	int      op;
	int      flags;
	int      tier;
	int      force;
	int      is_push;

	chkinfo_t *chkinfo;
	chkstat_t *chkstat;
	char _chkinfo[CHKINFO_MAX];
	char _chkstat[CHKSTAT_MAX];
} chunk_check_context_t;


static inline int chunk_check_context_init(chunk_check_context_t *ctx)
{
        memset(ctx, 0, sizeof(chunk_check_context_t));

        ctx.chkinfo = (void *)ctx._chkinfo;
        ctx.chkstat = (void *)ctx._chkstat;

        // ctx.parent = volume_proto->chkid;
        ctx.tier = 0;
        ctx.flags = 0;
        ctx.op = __OP_WRITE;
        ctx.force = 1;
        ctx.is_push = FALSE;

        return 0;
}


#endif
