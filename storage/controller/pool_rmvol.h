#ifndef __DIR_UNLINK_H__
#define __DIR_UNLINK_H__

int pool_rmvol_load(pool_proto_t *pool_proto);
int pool_rmvol(pool_proto_t *pool_proto, const char *name, int force);

#endif

