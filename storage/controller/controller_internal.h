#ifndef __CONTROLLER_INTERNAL_H
#define __CONTROLLER_INTERNAL_H

#include "yid.h"
#include "buffer.h"

typedef struct {
        nid_t nid;
        const io_t *io;
        buffer_t *buf;
        int retval;
} remote_io_read_ctx_t;

// from volume_remote_io.c

int volume_remote_io_read(const nid_t *nid, const io_t *io, buffer_t *buf);

#endif