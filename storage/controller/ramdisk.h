#ifndef __RAMDISK_H__
#define __RAMDISK_H__

#include "lich_id.h"
#include "buffer.h"

#define RAMDISK_ENABLE ENABLE_RAMDISK

int ramdisk_open(int *fd, const chkid_t *chkid);
int ramdisk_pread(int fd, buffer_t *buf, size_t size, off_t offset);
int ramdisk_pwrite(int fd, const chkid_t *chkid, const buffer_t *buf, size_t size, off_t offset);
int ramdisk_read(const chkid_t *chkid, buffer_t *buf, size_t size, off_t offset);
int ramdisk_write(const chkid_t *chkid, const buffer_t *buf, size_t size, off_t offset);
int ramdisk_close(int fd, const chkid_t *chkid);

int ramdisk_crc_write(const chkid_t *chkid, const buffer_t *buf, size_t size, off_t offset);

#endif

