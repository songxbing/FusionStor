#ifndef __LICH_CTX_H
#define __LICH_CTX_H

#include "lich_id.h"

#define IO_OPT_SET_PRIORITY 0x00000001
#define IO_OPT_CREAT        0x00000002

typedef struct {
        int localize;
        int priority;
        int fill;
        int flag;
} io_opt_t;

/**
 * @param opt
 * @param localize
 * @param priority -1, 0, 1
 * @param fill
 * @param flag
 */
static inline void io_opt_init(io_opt_t *opt, int localize, int priority, int fill, int flag) {
        memset(opt, 0, sizeof(io_opt_t));

        opt->localize = localize;
        opt->priority = priority;
        opt->fill = fill;
        opt->flag = flag;
}

#endif
