#ifndef __DIR_SRV__
#define __DIR_SRV__

#include "fs_proto.h"
#include "volume_proto.h"
#include "chunk.h"
#include "fileinfo.h"

int pool_ctl_init(uint64_t max_chunk);

int pool_ctl_mkpool(const fileid_t *parent, const char *name, const char *site_name,
                 const setattr_t *setattr, chkinfo_t *chkinfo);
int pool_ctl_rmpool(const chkid_t *chkid, const char *name);

int pool_ctl_listpool_open(const fileid_t *parent, const char *uuid);
int pool_ctl_listpool(const chkid_t *parent, const char *uuid, uint64_t offset, void *de, int *delen);
int pool_ctl_listpool_close(const fileid_t *parent, const char *uuid);
int pool_ctl_extend(const chkid_t *poolid);

int pool_ctl_mkvol(const fileid_t *parent, const char *name, const char *site_name,
                   const setattr_t *setattr, chkinfo_t *chkinfo);
int pool_ctl_mkvolwith(const chkid_t *parent, const char *name, const chkinfo_t *chkinfo);
int pool_ctl_unlink(const chkid_t *chkid, const char *name, int force);
int pool_ctl_cleanup(const chkid_t *chkid, const char *name);
int pool_ctl_cleanup0(const chkid_t *chkid, const char *name);

int pool_ctl_rename_lock(const fileid_t *fileid, const fileid_t *src, const char *name, int force);
int pool_ctl_rename_unlock(const fileid_t *fileid, const chkinfo_t *chkinfo);
int pool_ctl_rename(const fileid_t *from, const char *fname,
                    const fileid_t *to, const char *tname);

int pool_ctl_lookup(const fileid_t *parent, const char *name,
                    chkinfo_t *chkinfo);
int pool_ctl_lookup_srv(const fileid_t *chkid, const fileid_t *parent);

int pool_ctl_getattr(const fileid_t *chkid, fileinfo_t *fileinfo);
int pool_ctl_setattr(const fileid_t *chkid, fileinfo_t *fileinfo, const setattr_t *setattr);

// xattr
int pool_ctl_xattr_set(const chkid_t *chkid, const char *key, const char *value,
                       uint32_t valuelen, int flag);
int pool_ctl_xattr_get(const chkid_t *chkid, const char *key, char *value, int *valuelen);
int pool_ctl_xattr_list(const chkid_t *chkid, char *buf, int *buflen);
int pool_ctl_xattr_remove(const chkid_t *chkid, const char *key);

// chunk
int pool_ctl_chunk_getinfo(const fileid_t *parent, const fileid_t *chkid, chkinfo_t *chkinfo);

int pool_ctl_chunk_iterator(const fileid_t *fileid, func2_t func2, void *_arg);

int pool_ctl_chunk_cleanup(const fileid_t *parent, const chkid_t *chkid,
                           const nid_t *nid, uint64_t meta_version);

int pool_ctl_chunk_sync(const fileid_t *fileid, const chkid_t *chkid);
int pool_ctl_chunk_sync_force(const poolid_t *poolid, const chkid_t *chkid, const nid_t *nid);

int pool_ctl_chunk_check(const fileid_t *fileid, const chkid_t *chkid);

int pool_ctl_chunk_move(const fileid_t *fileid, const chkid_t *chkid, const nid_t *dist, int dist_count);

int pool_ctl_chunk_migrate(const fileid_t *parent, const chkid_t *chkid, uint32_t force, chkinfo_t *chkinfo);
int pool_ctl_chunk_reject(const fileid_t *parent, const chkid_t *chkid, const nid_t *nid, chkinfo_t *chkinfo);

int pool_ctl_chunk_update(const fileid_t *parent, const chkinfo_t *chkinfo, const nid_t *owner, uint64_t info_version);

// replica
int pool_ctl_chunk_set(const fileid_t *parent, const chkid_t *chkid,
                       const nid_t *nid, int status);

#endif
