#ifndef __DIR_PROTO_RENAME_H__
#define __DIR_PROTO_RENAME_H__


int pool_rename_lock(struct __pool_proto *pool_proto,
                                   const fileid_t *src, const char *name, int force);
int pool_rename_unlock(struct __pool_proto *pool_proto,
                                     const chkinfo_t *chkinfo);
int pool_rename_load(pool_proto_t *pool_proto);
int pool_rename_load_lock(pool_proto_t *pool_proto);
void pool_rename_lock_notify(const fileid_t *_fileid);

int pool_rename1(pool_proto_t *pool_proto, const fileid_t *from,
            const char *fromname, const fileid_t *to, const char *toname);

#endif
