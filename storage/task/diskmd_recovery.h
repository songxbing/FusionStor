#ifndef __DISKMD_RECV_H_
#define __DISKMD_RECV_H_

int diskmd_recover_init();
int diskmd_recovery_is_running();

int diskmd_recovery_check(const char *pool);

int diskmd_recover_disk(int diskid);
int diskmd_recovery_stop_disk(int diskid);
int diskmd_recovery_is_stopped(int diskid, int *stop);

int diskmd_get_disk_chunkinfo(int disk);

#endif
