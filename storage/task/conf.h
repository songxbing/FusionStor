#ifndef __CONFIG__H_
#define __CONFIG__H_

#define THREAD_MAX 10

#define THREAD_PATH      "balance/thread"
#define INTERVAL_PATH    "balance/interval"
#define STEP_PATH        "balance/step"
#define STOP_PATH        "balance/stop"
#define RATE_PATH        "balance/fill_rate"
#define INFO_PATH        "balance/info"

#define THRESHOLD 2
#define LICH_NODE_MAX 200

#define DEFAULT_STEP 500
#define DEFAULT_THREAD 10
#define DEFAULT_INTERVAL (60*60*24*365) // 1 year
#define DEFAULT_FILL_RATE 1000

typedef struct {
        int step;
        int interval;
        int fill_rate;
        int thread_num;
} b_conf_t;

void config_init();
int __config_set(const char *key, const char *value);
int __config_get(const char *key, int _default);
void config_set(char *path, int value);
void config_get_stop(char *path, int dft, int *stop);
void config_get_step(char *path, int dft, int *step);
void config_get_fillrate(char *path, int dft, int *rate);
void config_get_thread(char *path, int dft, int *thread);
void config_get_interval(char *path, int dft, int *interval);

#endif
