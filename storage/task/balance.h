#ifndef __BALANCE_H
#define __BALANCE_H

int balance_init();

typedef struct {
        chkid_t chkid;
        uint16_t repnum;
        nid_t disks[LICH_REPLICA_MAX];
} res_t;

typedef struct {
	// chunks
        const res_t *res;
        int count;
	// result
        int success;
        int fail;
	// status
        int stop;
        int stopped;
        int running;
} balance_seg_t;

typedef enum {
        __SCAN__,
        __RUNNING__,
        __WAITING__,
        __STOPPED__,
        __SUSPEND__,
        __FAILED__,
        __DONE__,
        __S_BAL_UNKNOWN__,
} balance_status_t;

typedef struct {
        int fd;
        balance_status_t status;
        uint64_t balance;
        uint64_t success;
        uint64_t fail;
        void *addr;
} arg_t;

#define THREAD_MAX 10

int gen_jobs(const char *pool, const chkid_t *chkid, arg_t *arg);

#endif
