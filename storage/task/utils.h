#ifndef __TASK_UTIL_H___
#define __TASK_UTIL_H___

int vol_is_deleting(const fileid_t* fileid, int *result);

int task_pool_remove(const char *pool);

#endif
