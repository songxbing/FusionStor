/*###################################################################
  > File Name: storage/task/config.c
  > Author: Vurtune
  > Mail: vurtune@foxmail.com
  > Created Time: Tue 21 Nov 2017 11:29:10 PM PST
###################################################################*/
#include "config.h"

#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <dirent.h>

#define DBG_SUBSYS S_LIBTASK

#include "adt.h"
#include "dbg.h"
#include "conf.h"
#include "utils.h"
#include "nodectl.h"
#include "fileinfo.h"
#include "configure.h"

int __config_set(const char *key, const char *value) {
        return nodectl_set(key, value);
}

int __config_get(const char *key, int _default) {
        return nodectl_get_int(key, _default);
}

void config_get_thread(char *path, int dft, int *thread)
{
        int _thread = __config_get(path, dft);
        if (_thread < 1)
                _thread = 1;
        else if (_thread > THREAD_MAX)
                _thread = THREAD_MAX;

        *thread = _thread;
}

void config_get_step(char *path, int dft, int *step)
{
        int _step = __config_get(path, dft);
        if (_step < 1)
                _step = 1;

        *step = _step;
}

void config_get_fillrate(char *path, int dft, int *rate)
{
        int _rate = __config_get(path, dft);
        if (_rate < 1)
                _rate = 1;

        *rate = _rate;
}

void config_get_interval(char *path, int dft, int *interval)
{
        int _itv = __config_get(path, dft);
        if (_itv < 1)
                _itv = 1;

        *interval = _itv;
}

void config_set(char *path, int value)
{
        char buf[MAX_NAME_LEN];
        sprintf(buf, "%d", value);
        __config_set(path, buf);
}

void config_get_stop(char *path, int dft, int *stop)
{
        *stop = 0;

        int _stop = __config_get(path, dft);
        if (_stop != 0)
                _stop = 1;

        config_set(path, dft);

        *stop = _stop;
}

void config_init()
{
        int tmp;
        //init config file, do not care value
        config_get_thread(THREAD_PATH, DEFAULT_THREAD, &tmp);
        config_get_step(STEP_PATH, DEFAULT_STEP, &tmp);
        config_get_fillrate(RATE_PATH, DEFAULT_FILL_RATE, &tmp);
        config_set(INTERVAL_PATH, DEFAULT_INTERVAL);
        config_set(STOP_PATH, 0);
}
