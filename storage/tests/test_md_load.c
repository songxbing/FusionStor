#include "config.h"

#include <sys/statvfs.h>
#include <sys/epoll.h>
#include <time.h>
#include <unistd.h>
#include <string.h>
#include <semaphore.h>
#include <errno.h>
#include <sys/statfs.h>

#define DBG_SUBSYS S_LIBSTORAGE

#include "sysutil.h"
#include "configure.h"
#include "../replica/clock.h"
#include "dbg.h"


int main()
{
        int ret;
        char path[MAX_PATH_LEN];

        ret = conf_init();
        if (unlikely(ret))
                GOTO(err_ret, ret);

        snprintf(path, MAX_LINE_LEN, SHM_ROOT"/test_md");
        DINFO("test in %s\n", path);
        ret = diskmd_init(path);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        return 0;
err_ret:
        return ret;
}
