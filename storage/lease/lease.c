#include "config.h"

#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#define DBG_SUBSYS S_LIBTASK

#include "limits.h"
#include "adt.h"
#include "ynet_rpc.h"
#include "sysy_lib.h"
#include "cluster.h"
#include "chunk.h"
#include "metadata.h"
#include "net_table.h"
#include "configure.h"
#include "lich_md.h"
#include "net_global.h"
#include "squeue.h"
#include "rmvol_bh.h"
#include "../replica/replica.h"
#include "pool_ctl.h"
#include "lease_ctl.h"
#include "rpc_proto.h"
#include "cache.h"
#include "pool_proto.h"
#include "ylog.h"
#include "dbg.h"
#include "timer.h"


typedef enum {
        LEASE_NULL = 500,
        LEASE_SET,
        LEASE_FREE,
        LEASE_GET,
        LEASE_MAX,
} lease_op_t;

typedef struct {
        uint32_t op;
        uint32_t buflen;
        chkid_t  chkid;
        char buf[0];
} msg_t;


static __request_handler_func__  __request_handler__[LEASE_MAX - LEASE_NULL];
static char  __request_name__[LEASE_MAX - LEASE_NULL][__RPC_HANDLER_NAME__ ];

static void __request_set_handler(int op, __request_handler_func__ func, const char *name)
{
        YASSERT(strlen(name) + 1 < __RPC_HANDLER_NAME__ );
        strcpy(__request_name__[op - LEASE_NULL], name);
        __request_handler__[op - LEASE_NULL] = func;
}

static void __request_get_handler(int op, __request_handler_func__ *func, const char **name)
{
        *func = __request_handler__[op - LEASE_NULL];
        *name = __request_name__[op - LEASE_NULL];
}

static void __getmsg(buffer_t *buf, msg_t **_req, int *buflen, char *_buf)
{
        msg_t *req;

        YASSERT(buf->len <= MEM_CACHE_SIZE4K);

        req = (void *)_buf;
        *buflen = buf->len - sizeof(*req);
        mbuffer_get(buf, req, buf->len);

        *_req = req;
}

static int __lease_srv_set(const sockid_t *sockid, const msgid_t *msgid, buffer_t *_buf)
{
        int ret, buflen;
        msg_t *req;
        char *buf = mem_cache_calloc1(MEM_CACHE_4K, PAGE_SIZE);
        const nid_t *nid;
        lease_token_t token;

        __getmsg(_buf, &req, &buflen, buf);

        _opaque_decode(req->buf, buflen, &nid, NULL, NULL);

        ret = lease_ctl_set(&req->chkid, nid, &token);
        if (unlikely(ret)) {
                GOTO(err_ret, ret);
        }

        rpc_reply(sockid, msgid, &token, sizeof(token));

        mem_cache_free(MEM_CACHE_4K, buf);

        return 0;
err_ret:
        mem_cache_free(MEM_CACHE_4K, buf);
        return ret;
}

static int __lease_rpc_set(const nid_t *srv, const chkid_t *chkid, const nid_t *nid, lease_token_t *token)
{
        int ret, len = sizeof(*token);
        uint32_t count;
        msg_t *req;
        char *buf = mem_cache_calloc1(MEM_CACHE_4K, PAGE_SIZE);
 
        DBUG(""CHKID_FORMAT" set @ %s\n", CHKID_ARG(chkid), network_rname(nid));
       
        req = (void *)buf;
        req->op = LEASE_SET;
        req->chkid = *chkid;
        _opaque_encode(req->buf, &count,
                       nid, sizeof(*nid), NULL);

        ret = rpc_request_wait("lease_rpc_set", srv,
                               req, sizeof(*req) + count,
                               token, &len,
                               MSG_LEASE, 0, _get_timeout());
        if (unlikely(ret))
                GOTO(err_ret, ret);

        mem_cache_free(MEM_CACHE_4K, buf);

        return 0;
err_ret:
        mem_cache_free(MEM_CACHE_4K, buf);
        return ret;
}

static int __lease_srv_free(const sockid_t *sockid, const msgid_t *msgid, buffer_t *_buf)
{
        int ret, buflen;
        msg_t *req;
        char *buf = mem_cache_calloc1(MEM_CACHE_4K, PAGE_SIZE);
        const nid_t *nid;

        __getmsg(_buf, &req, &buflen, buf);

        _opaque_decode(req->buf, buflen, &nid, NULL, NULL);

        ret = lease_ctl_free(&req->chkid, nid);
        if (unlikely(ret)) {
                GOTO(err_ret, ret);
        }

        rpc_reply(sockid, msgid, NULL, 0);

        mem_cache_free(MEM_CACHE_4K, buf);

        return 0;
err_ret:
        mem_cache_free(MEM_CACHE_4K, buf);
        return ret;
}

static int __lease_rpc_free(const nid_t *srv, const chkid_t *chkid, const nid_t *nid)
{
        int ret;
        uint32_t count;
        msg_t *req;
        char *buf = mem_cache_calloc1(MEM_CACHE_4K, PAGE_SIZE);

        req = (void *)buf;
        req->op = LEASE_FREE;
        req->chkid = *chkid;
        _opaque_encode(req->buf, &count,
                       nid, sizeof(*nid), NULL);

        ret = rpc_request_wait("lease_rpc_free", srv,
                               req, sizeof(*req) + count,
                               NULL, NULL,
                               MSG_LEASE, 0, _get_timeout());
        if (unlikely(ret))
                GOTO(err_ret, ret);

        mem_cache_free(MEM_CACHE_4K, buf);

        return 0;
err_ret:
        mem_cache_free(MEM_CACHE_4K, buf);
        return ret;
}

static int __lease_srv_get(const sockid_t *sockid, const msgid_t *msgid, buffer_t *_buf)
{
        int ret, buflen;
        msg_t *req;
        char *buf = mem_cache_calloc1(MEM_CACHE_4K, PAGE_SIZE);
        nid_t *nid;
        lease_token_t *token;
        char tmp[MAX_BUF_LEN];

        __getmsg(_buf, &req, &buflen, buf);

        //_opaque_decode(req->buf, buflen, &nid, NULL, NULL);

        nid = (void *)tmp;
        token = (void *)tmp + sizeof(*nid);
        ret = lease_ctl_get(&req->chkid, nid, token);
        if (unlikely(ret)) {
                GOTO(err_ret, ret);
        }

        rpc_reply(sockid, msgid, tmp, sizeof(*nid) + sizeof(*token));

        DINFO(CHKID_FORMAT" nid %s token %x\n", CHKID_ARG(&req->chkid), network_rname(nid), token->seq);
        
        mem_cache_free(MEM_CACHE_4K, buf);

        return 0;
err_ret:
        mem_cache_free(MEM_CACHE_4K, buf);
        return ret;
}

static int __lease_rpc_get(const nid_t *srv, const chkid_t *chkid, nid_t *nid, lease_token_t *token)
{
        int ret, len = sizeof(*nid) + sizeof(*token);
        uint32_t count;
        msg_t *req;
        char *buf = mem_cache_calloc1(MEM_CACHE_4K, PAGE_SIZE);
        char tmp[MAX_BUF_LEN];

        req = (void *)buf;
        req->op = LEASE_GET;
        req->chkid = *chkid;
        //_opaque_encode(req->buf, &count,
        //nid, sizeof(*nid), NULL);
        count = 0;

        ret = rpc_request_wait("lease_rpc_get", srv,
                               req, sizeof(*req) + count,
                               tmp, &len,
                               MSG_LEASE, 0, _get_timeout());
        if (unlikely(ret))
                GOTO(err_ret, ret);
 
        *nid = *(nid_t *)tmp;
        *token = *(lease_token_t *)((void *)tmp + sizeof(*nid));

        DINFO(CHKID_FORMAT" nid %s token %x\n", CHKID_ARG(&req->chkid), network_rname(nid), token->seq);
        
        mem_cache_free(MEM_CACHE_4K, buf);

        return 0;
err_ret:
        mem_cache_free(MEM_CACHE_4K, buf);
        return ret;
}

static void __request_handler(void *arg)
{
        int ret;
        msg_t req;
        sockid_t sockid;
        msgid_t msgid;
        buffer_t buf;
        __request_handler_func__ handler;
        const char *name;

        request_trans(arg, &sockid, &msgid, &buf, NULL);

        if (buf.len < sizeof(req)) {
                ret = EINVAL;
                GOTO(err_ret, ret);
        }

        mbuffer_get(&buf, &req, sizeof(req));

        DBUG("set op %u from %s, id (%u, %x)\n", req.op,
             _inet_ntoa(sockid.addr), msgid.idx, msgid.figerprint);

        if (!netable_connected(net_getadmin())) {
                ret = ENONET;
                GOTO(err_ret, ret);
        }

        __request_get_handler(req.op, &handler, &name);
        if (handler == NULL) {
                ret = ENOSYS;
                DWARN("error op %u\n", req.op);
                GOTO(err_ret, ret);
        }

        schedule_task_setname(name);

        ret = handler(&sockid, &msgid, &buf);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        mbuffer_free(&buf);

        DBUG("reply op %u from %s, id (%u, %x)\n", req.op,
              _inet_ntoa(sockid.addr), msgid.idx, msgid.figerprint);

        return ;
err_ret:
        mbuffer_free(&buf);
        rpc_reply_error(&sockid, &msgid, ret);
        DBUG("error op %u from %s, id (%u, %x)\n", req.op,
             _inet_ntoa(sockid.addr), msgid.idx, msgid.figerprint);
        return;
}

int lease_rpc_init()
{
        if (gloconf.lease_timeout < 4) {
                DERROR("gloconf.lease_timeout must bigger than 3\n");
                EXIT(EINVAL);
        }

        __request_set_handler(LEASE_SET, __lease_srv_set, "lease_srv_set");
        __request_set_handler(LEASE_GET, __lease_srv_get, "lease_srv_get");
        __request_set_handler(LEASE_FREE, __lease_srv_free, "lease_srv_free");

        if (ng.daemon) {
                rpc_request_register(MSG_LEASE, __request_handler, NULL);
        }

        return 0;
}

int lease_rpc_destroy()
{
        if (ng.daemon) {
                rpc_request_register(MSG_LEASE, NULL, NULL);
        }

        return 0;
}

static int __lease_set(const nid_t *srv, const chkid_t *chkid, const nid_t *nid, lease_token_t *token)
{
        YASSERT(chkid->type == __VOLUME_CHUNK__
                || chkid->type == __POOL_CHUNK__);

        if (net_islocal(srv)) {
                return lease_ctl_set(chkid, nid, token);
        } else {
                return __lease_rpc_set(srv, chkid, nid, token);
        }
}

static int __lease_get(const nid_t *srv, const chkid_t *chkid, nid_t *nid, lease_token_t *token)
{
        if (net_islocal(srv)) {
                return lease_ctl_get(chkid, nid, token);
        } else {
                return __lease_rpc_get(srv, chkid, nid, token);
        }
}

static int __lease_free(const nid_t *srv, const chkid_t *chkid, const nid_t *nid)
{
        if (net_islocal(srv)) {
                return lease_ctl_free(chkid, nid);
        } else {
                return __lease_rpc_free(srv, chkid, nid);
        }
}

static int __lease_wrlock(lease_t *lease)
{
        int ret;

        if (lease->plock) {
                ret = plock_wrlock(&lease->u.plock);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        } else {
                ret = sy_rwlock_wrlock(&lease->u.rwlock);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        }

        return 0;
err_ret:
        return ret;
}

inline static int __lease_rdlock(lease_t *lease)
{
        int ret;

        if (lease->plock) {
                ret = plock_rdlock(&lease->u.plock);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        } else {
                ret = sy_rwlock_rdlock(&lease->u.rwlock);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        }

        return 0;
err_ret:
        return ret;
}

static void  __lease_unlock(lease_t *lease)
{
        if (lease->plock) {
                plock_unlock(&lease->u.plock);
        } else {
                sy_rwlock_unlock(&lease->u.rwlock);
        }

        return;
}

#if 1

int lease_set(lease_t *lease)
{
        int ret;
        time_t now;
        const chkid_t *chkid = &lease->chkid;
        lease_token_t token;

        YASSERT(ng.daemon);
        YASSERT(chkid->type == __VOLUME_CHUNK__
                || chkid->type == __POOL_CHUNK__);
        
        now = gettime();
        if (unlikely(now - lease->time > gloconf.lease_timeout / 2
                     || now < lease->time
                     || lease->token.master != ng.master_magic)) {
                ret = __lease_wrlock(lease);
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                if (now < lease->time) {
                        DERROR("time maybe changed, time %u, now %u\n",
                               (int)lease->time, (int)now);

                        lease->time = now - gloconf.lease_timeout * 2;
                }
                
                if (now - lease->time >= gloconf.lease_timeout / 2
                    || lease->token.master != ng.master_magic) {
                        ret = __lease_set(net_getadmin(), chkid, net_getnid(), &token);
                        if (unlikely(ret)) {
                                GOTO(err_lock, ret);
                        }

                        if (token.master != ng.master_magic) {
                                ret = EAGAIN;
                                DWARN(CHKID_FORMAT" wrong master 0x%x -> 0x%x\n",
                                       CHKID_ARG(&lease->chkid), ng.master_magic, token.master);
                                GOTO(err_lock, ret);
                        }
                        
                        lease->time = now;
                        lease->token = token;
                }

                __lease_unlock(lease);
        }

        SCHEDULE_LEASE_SET();
        
        return 0;
err_lock:
        __lease_unlock(lease);
err_ret:
        return ret;
}

#else

int lease_set(lease_t *lease)
{
        int ret;
        time_t now;
        nid_t nid = *net_getnid();
        lease_token_t token;
        const chkid_t *chkid = &lease->chkid;

        YASSERT(ng.daemon);
        YASSERT(chkid->type == __VOLUME_CHUNK__
                || chkid->type == __POOL_CHUNK__);
        
        ret = __lease_wrlock(lease);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        now = gettime();
        ret = __lease_set(net_getadmin(), chkid, &nid, &token);
        if (unlikely(ret)) {
                GOTO(err_lock, ret);
        }

        lease->time = now;
        lease->token = token;

        __lease_unlock(lease);

        SCHEDULE_LEASE_SET();
        
        return 0;
err_lock:
        __lease_unlock(lease);
err_ret:
        return ret;
}

#endif


int lease_timeout(const lease_t *lease)
{
        time_t now;
 
        now = gettime();
        if (now - lease->time > gloconf.lease_timeout || now < lease->time) {
                return 1;
        } else {
                return 0;
        }
}

int lease_free(lease_t *lease)
{
        int ret;
        const chkid_t *chkid = &lease->chkid;

        ret = __lease_wrlock(lease);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        if (lease->time) {
                ret = __lease_free(net_getadmin(), chkid, net_getnid());
                if (unlikely(ret))
                        GOTO(err_lock, ret);
                
                lease->time = 0;
        }

        __lease_unlock(lease);

        return 0;
err_lock:
        __lease_unlock(lease);
err_ret:
        return ret;
}

int lease_create(lease_t *lease, const chkid_t *chkid)
{
        int ret;

        if (chkid_isvol(chkid)) {
                ret = plock_init(&lease->u.plock, "lease.u.plock");
                if (unlikely(ret))
                        GOTO(err_ret, ret);
                
                lease->plock = 1;
        } else {
                lease->plock = 0;
                
                ret = sy_rwlock_init(&lease->u.rwlock, "lease.u.rwlock");
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        }

        lease->chkid = *chkid;
        lease->time = 0;

        return 0;
err_ret:
        return ret;
}

int lease_get(const chkid_t *chkid, nid_t *nid, lease_token_t *_token)
{
        int ret;
        lease_token_t token;

        ret = __lease_get(net_getadmin(), chkid, nid, &token);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        if (_token)
                *_token = token;
        
        return 0;
err_ret:
        return ret;
}
