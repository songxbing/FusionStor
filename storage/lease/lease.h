#ifndef __LEASE__
#define __LEASE__

#include "lease_ctl.h"

typedef struct {
        chkid_t chkid;
        time_t time;
        uint32_t plock;
        lease_token_t token;
        union {
                sy_rwlock_t rwlock;
                plock_t plock;
        } u;
} lease_t;

int lease_rpc_destroy();
int lease_rpc_init();
int lease_create(lease_t *lease, const chkid_t *chkid);

/**
 * @brief 尝试获取lease
 *
 * @param lease
 * @param chkid
 * @return
 */
int lease_set(lease_t *lease);
int lease_free(lease_t *lease);
int lease_get(const chkid_t *chkid, nid_t *nid, lease_token_t *_token);

/**
 * @brief 检查lease有效性，失效的话，重新申请。
 *
 * 受保护资源上的每个操作发生前，都需要检查
 *
 * @param lease
 * @param chkid
 * @param renew
 * @return
 */
int lease_timeout(const lease_t *lease);

#endif
