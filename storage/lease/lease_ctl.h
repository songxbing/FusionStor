#ifndef __LEASE_CTL__
#define __LEASE_CTL__

typedef struct {
        uint32_t master;
        uint64_t seq;
} lease_token_t;

int lease_ctl_set(const chkid_t *chkid, const nid_t *nid, lease_token_t *token);
int lease_ctl_free(const chkid_t *chkid, const nid_t *nid);
int lease_ctl_get(const chkid_t *chkid, nid_t *nid, lease_token_t *token);
int lease_ctl_init();
int lease_ctl_destroy();

#endif

