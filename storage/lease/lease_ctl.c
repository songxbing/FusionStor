#include "config.h"

#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#define DBG_SUBSYS S_LIBTASK

#include "limits.h"
#include "adt.h"
#include "ynet_rpc.h"
#include "sysy_lib.h"
#include "cluster.h"
#include "chunk.h"
#include "metadata.h"
#include "net_table.h"
#include "configure.h"
#include "lich_md.h"
#include "net_global.h"
#include "squeue.h"
#include "rmvol_bh.h"
#include "../replica/replica.h"
#include "pool_ctl.h"
#include "cache.h"
#include "pool_proto.h"
#include "ylog.h"
#include "dbg.h"
#include "timer.h"

/**
 * @file Lease Server-side
 *
 * 通过hashtable管理所有lease，类似于lock table。
 */

typedef struct {
        chkid_t chkid;
        nid_t nid;
        time_t time;
        uint64_t seq;
        sy_spinlock_t lock;
} entry_t;

#define LEASE_CTL_HASH 32

typedef struct {
        hashtable_t tab;
        sy_rwlock_t lock;
} lease_ctl_t;

static lease_ctl_t *__lease_ctl__ = NULL;
static uint64_t __lease_seq__ = 0;
static uint32_t __master_magic__ = 0;

static time_t __uptime__;
static int inited = 0;

static int __cmp(const void *v1, const void *v2)
{
        const entry_t *ent = v1;
        const chkid_t *chkid = v2;

        return chkid_cmp(&ent->chkid, chkid);
}

static uint32_t __key(const void *args)
{
        const chkid_t *id = args;

        return id->id * (1 + id->idx);
}

int lease_ctl_init()
{
        int ret, i;
        char name[MAX_NAME_LEN];

        YASSERT(__lease_ctl__ == NULL);
        YASSERT(inited == 0);
        
        ret = ymalloc((void **)&__lease_ctl__, sizeof(*__lease_ctl__) * LEASE_CTL_HASH);
        if (ret)
                GOTO(err_ret, ret);
        
        for (i = 0; i < LEASE_CTL_HASH; i++) {
                snprintf(name, MAX_NAME_LEN, "leasetable[%u]",  i);
                __lease_ctl__[i].tab = hash_create_table(__cmp, __key, "lease_table");
                if (__lease_ctl__[i].tab == NULL) {
                        ret = ENOMEM;
                        GOTO(err_ret, ret);
                }

                ret = sy_rwlock_init(&__lease_ctl__[i].lock, name);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        }

        __uptime__ = gettime();
        __master_magic__ = ng.master_magic;

        inited = 1;

        return 0;
err_ret:
        return ret;
}

static int __lease_uptime_check()
{
        int ret;
        time_t now;
        int lease_timeout = gloconf.lease_timeout + gloconf.lease_timeout / 2 - gloconf.master_timeout;

        now = gettime();
        if ((int)now - (int)__uptime__ < lease_timeout) {
                if (now < __uptime__) {
                        DERROR("time maybe changed, uptime %ds, now %ds\n", (int)__uptime__, (int)now);
                        __uptime__ = now;
                } else {
                        DBUG("lease_srv will be available int %ds\n",
                             lease_timeout - ((int)(now -  __uptime__)));
                }

                ret = EAGAIN;
                GOTO(err_ret, ret);
        }

        return 0;
err_ret:
        return ret;
}

static int __lease_ctl_online(entry_t *ent)
{
        //if (netable_connected(&ent->nid) || conn_online(&ent->nid)) {
        if (netable_connected(&ent->nid)) {
                DINFO(""CHKID_FORMAT" @ %s still online\n",
                      CHKID_ARG(&ent->chkid), network_rname(&ent->nid));

                return 1;
        } else {
                return 0;
        }
}

static int __lease_ctl_timeout(entry_t *ent)
{
        int tmo;
        time_t now;

        now = gettime();
        tmo = gloconf.lease_timeout + gloconf.lease_timeout / 2;
        if (now < ent->time) {
                DERROR("time maybe changed, time %ds, now %d, restart for safe\n",
                       (int)ent->time, (int)now);

                EXIT(EAGAIN);
        } else if (now - ent->time >= tmo) {
                DBUG(""CHKID_FORMAT" @ %s timeout %ds\n",
                      CHKID_ARG(&ent->chkid), network_rname(&ent->nid),
                      (int)(now - ent->time) - tmo);

                return 1;
        } else {
                DBUG(""CHKID_FORMAT" @ %s still in used, will timeout in %ds\n",
                      CHKID_ARG(&ent->chkid), network_rname(&ent->nid),
                      (int)(tmo - (now - ent->time)));
        }

        return 0;
}

static int __lease_ctl_set(entry_t *ent, const nid_t *nid, lease_token_t *token)
{
        int ret;

        ret = sy_spin_lock(&ent->lock);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        if (nid_cmp(&ent->nid, nid) == 0) {
                DBUG(CHKID_FORMAT" reuse lease, owner %s, token (0x%x, %ju), update %ds\n",
                      CHKID_ARG(&ent->chkid), network_rname(&ent->nid),
                      __master_magic__, ent->seq, gettime() - ent->time);

                ent->time = gettime();
                
                token->seq = ent->seq;
                token->master = __master_magic__;
        } else {
                if (!__lease_ctl_timeout(ent)) {
                        ret = EREMCHG;
                        int wait = (int)((ent->time + gloconf.lease_timeout + gloconf.lease_timeout / 2) - gettime());
                        if (wait % 10 == 0 || 1) {
                                DINFO(CHKID_FORMAT" lease %s -> %s fail, need wait %ds\n",
                                      CHKID_ARG(&ent->chkid), network_rname(&ent->nid),
                                      network_rname(nid), wait);
                        }

                        GOTO(err_lock, ret);
                } else {
                        uint64_t seq = ++__lease_seq__;

                        DBUG(CHKID_FORMAT" update lease, owner %s -> %s token (0x%x, %ju) -> (0x%x, %ju)\n",
                              CHKID_ARG(&ent->chkid), network_rname(&ent->nid),
                              network_rname(nid), __master_magic__, ent->seq, __master_magic__, seq);

                        ent->nid = *nid;
                        ent->seq = seq;
                        ent->time = gettime();

                        token->seq = ent->seq;
                        token->master = __master_magic__;
                }
        }

        sy_spin_unlock(&ent->lock);

        return 0;
err_lock:
        sy_spin_unlock(&ent->lock);
err_ret:
        return ret;
}

static int __lease_ctl_get(entry_t *ent, nid_t *_nid, lease_token_t *token)
{
        int ret;
        time_t now;

        ret = sy_spin_lock(&ent->lock);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        if (__lease_ctl_timeout(ent)) {
                ret = ETIME;
                GOTO(err_lock, ret);
        }

        now = gettime();
        DBUG(CHKID_FORMAT" owner %s, left %ds\n",
              CHKID_ARG(&ent->chkid), network_rname(&ent->nid),
              (int)((ent->time + gloconf.lease_timeout + gloconf.lease_timeout / 2) - now));
        *_nid = ent->nid;
        token->seq = ent->seq;
        token->master = __master_magic__;

        sy_spin_unlock(&ent->lock);

        return 0;
err_lock:
        sy_spin_unlock(&ent->lock);
err_ret:
        return ret;
}

static int __lease_ctl_new__(entry_t **_ent, const chkid_t *chkid, const nid_t *nid, lease_token_t *token)
{
        int ret;
        entry_t *ent;

        ret = ymalloc((void **)&ent, sizeof(*ent));
        if (unlikely(ret))
                GOTO(err_ret, ret);

        ent->nid = *nid;
        ent->chkid = *chkid;
        ent->time = gettime();
        ent->seq = ++__lease_seq__;

        token->seq = ent->seq;
        token->master = __master_magic__;
        
        ret = sy_spin_init(&ent->lock);
        if (unlikely(ret))
                UNIMPLEMENTED(__DUMP__);

        DBUG("new lease "CHKID_FORMAT" @ %s, token (0x%x, %ju)\n",
              CHKID_ARG(&ent->chkid), network_rname(nid), token->master, token->seq);

        *_ent = ent;
        
        return 0;
err_ret:
        return ret;
}

static int __lease_ctl_new(lease_ctl_t *lease_ctl, const chkid_t *chkid, const nid_t *nid, lease_token_t *token)
{
        int ret;
        entry_t *ent;

        ret = sy_rwlock_wrlock(&lease_ctl->lock);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        ent = hash_table_find(lease_ctl->tab, chkid);
        if (unlikely(ent)) {
                ret = EEXIST;
                GOTO(err_lock, ret);
        }

        ret = __lease_ctl_new__(&ent, chkid, nid, token);
        if (unlikely(ret))
                GOTO(err_lock, ret);
        
        ret = hash_table_insert(lease_ctl->tab, (void *)ent, &ent->chkid, 0);
        if (unlikely(ret))
                UNIMPLEMENTED(__DUMP__);

        sy_rwlock_unlock(&lease_ctl->lock);

        return 0;
err_lock:
        sy_rwlock_unlock(&lease_ctl->lock);
err_ret:
        return ret;
}

int lease_ctl_set(const chkid_t *chkid, const nid_t *nid, lease_token_t *token)
{
        int ret;
        entry_t *ent;
        lease_ctl_t *lease_ctl;

        DBUG(""CHKID_FORMAT" new @ %s\n", CHKID_ARG(chkid), network_rname(nid));

        if (inited == 0) {
                ret = EAGAIN;
                GOTO(err_ret, ret);
        }

        ret = __lease_uptime_check();
        if (unlikely(ret))
                GOTO(err_ret, ret);

        lease_ctl = &__lease_ctl__[chkid->id % LEASE_CTL_HASH];
retry:
        ret = sy_rwlock_rdlock(&lease_ctl->lock);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        ent = hash_table_find(lease_ctl->tab, chkid);
        if (ent) {
                ret = __lease_ctl_set(ent, nid, token);
                if (unlikely(ret))
                        GOTO(err_lock, ret);
        } else {
                sy_rwlock_unlock(&lease_ctl->lock);
                
                ret = __lease_ctl_new(lease_ctl, chkid, nid, token);
                if (unlikely(ret)) {
                        if (ret == EEXIST) { 
                                DWARN(""CHKID_FORMAT" new @ %s, exist\n", CHKID_ARG(chkid), network_rname(nid));
                                goto retry;
                        } else {
                                GOTO(err_ret, ret);
                        }
                }

                goto out;
        }

        sy_rwlock_unlock(&lease_ctl->lock);
out:
        return 0;
err_lock:
        sy_rwlock_unlock(&lease_ctl->lock);
err_ret:
        return ret;
}

int lease_ctl_get(const chkid_t *chkid, nid_t *nid, lease_token_t *token)
{
        int ret;
        entry_t *ent;
        lease_ctl_t *lease_ctl;

        DBUG(""CHKID_FORMAT" get\n", CHKID_ARG(chkid));

        if (inited == 0) {
                ret = EAGAIN;
                GOTO(err_ret, ret);
        }

        ret = __lease_uptime_check();
        if (unlikely(ret))
                GOTO(err_ret, ret);

        lease_ctl = &__lease_ctl__[chkid->id % LEASE_CTL_HASH];
        ret = sy_rwlock_rdlock(&lease_ctl->lock);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        ent = hash_table_find(lease_ctl->tab, chkid);
        if (ent) {
                ret = __lease_ctl_get(ent, nid, token);
                if (unlikely(ret)) {
                        GOTO(err_lock, ret);
                }
        } else {
                ret = ENOKEY;
                GOTO(err_lock, ret);
        }
        
        sy_rwlock_unlock(&lease_ctl->lock);

        return 0;
err_lock:
        sy_rwlock_unlock(&lease_ctl->lock);
err_ret:
        return ret;
}

int lease_ctl_free(const chkid_t *chkid, const nid_t *nid)
{
        int ret;

        DBUG(""CHKID_FORMAT" free @ %s\n", CHKID_ARG(chkid), network_rname(nid));

        if (inited == 0) {
                ret = EAGAIN;
                GOTO(err_ret, ret);
        }
        
        ret = __lease_uptime_check();
        if (unlikely(ret))
                GOTO(err_ret, ret);

        return 0;        
err_ret:
        return ret;
}


static int __lease_ctl_destroy(lease_ctl_t *lease_ctl)
{
        int ret;

        ret = sy_rwlock_wrlock(&lease_ctl->lock);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        hash_destroy_table(lease_ctl->tab, NULL);
        lease_ctl->tab = NULL;
        
        sy_rwlock_unlock(&lease_ctl->lock);

        return 0;
err_ret:
        UNIMPLEMENTED(__DUMP__);
        return ret;
}

int lease_ctl_destroy()
{
        int i;
        lease_ctl_t *lease_ctl = __lease_ctl__;

        YASSERT(inited);
        __lease_ctl__ = NULL;
        inited = 0;


        for (i = 0; i < LEASE_CTL_HASH; i++) {
                __lease_ctl_destroy(&lease_ctl[i]);
        }

        yfree((void **)&lease_ctl);
        
        return 0;
}
