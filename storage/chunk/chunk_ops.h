#ifndef __CHUNK_REPLICA_H__
#define __CHUNK_REPLICA_H__

#include <dirent.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdint.h>
#include <sys/statvfs.h>
#include <sys/types.h>

#include "chunk.h"
#include "list.h"
#include "ylock.h"
#include "cache.h"
#include "ynet_rpc.h"

int chunk_push(const char *pool, const chkid_t *chkid, const nid_t *from, const nid_t *to,
               const vclock_t *vclock, uint64_t meta_version, const fileid_t *_parent,
               int tier, int flags, const char *context);
int chunk_push_with(const char *pool, const chkid_t *chkid, const buffer_t *buf, const nid_t *to,
               const vclock_t *vclock, uint64_t meta_version, const fileid_t *parent,
               int tier, int flags);

int chunk_push_newdisk(const char *pool, chkinfo_t *chkinfo, int _from, int _to, const vclock_t *vclock,
                       const fileid_t *_parent, int tier, int flags, int *oflags);
int chunk_push_newdisk_with(const char *pool, chkinfo_t *chkinfo, const buffer_t *buf, int _to, const vclock_t *vclock,
                       const fileid_t *parent, int tier, int flags);
#endif
