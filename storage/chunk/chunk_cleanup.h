#ifndef __CHUNK_CLEANUP_H__
#define __CHUNK_CLEANUP_H__

#include "config.h"
#include "chunk.h"

int chunk_cleanup_init();

void chunk_cleanup_compare(const char *pool, const fileid_t *parent,
                           const chkinfo_t *from, const chkinfo_t *to);
int chunk_cleanup_push(const char *pool, const fileid_t *parent, const chkid_t *chkid,
                       const nid_t *nid, uint64_t meta_version);

#endif
