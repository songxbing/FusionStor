#ifndef __CHUNK_BH_H__
#define  __CHUNK_BH_H__

#include "lich_id.h"

int chunk_bh_sync(const fileid_t *parent, const chkid_t *chkid, int priority);
int chunk_bh_localize(const chkid_t *parent, const chkid_t *chkid);
int chunk_bh_init(void);


#endif
