#ifndef __DISKS_SLOT__
#define __DISKS_SLOT__

// 节点内部磁盘槽位, 维护磁盘数据结构增删

int disk_slot_init();

int disk_slot_get(int idx, disk_t **disk);
void disk_slot_release(int idx);

int disk_slot_empty(int idx, int *empty);

int disk_slot_set(int idx, disk_t *disk, void *worker);
int disk_slot_remove(int idx, disk_t **disk, void **worker);

int disk_slot_iterator(func_int1_t func_cb, void *arg);
int disk_slot_rdlock(disk_t *disk);
int disk_slot_wrlock(disk_t *disk);
void disk_slot_unlock(disk_t *disk);

int disk_slot_private_connect(int idx, disk_t **disk);
int disk_slot_private_disconnect(int idx);
void disk_slot_private_release(int idx);

#endif
