#ifndef __DISK_MAPING_H_
#define __DISK_MAPING_H_

#include <stdint.h>
#include <semaphore.h>
#include <errno.h>
#include <libaio.h>

#include "ynet_rpc.h"
#include "sysy_conf.h"
#include "chunk.h"
#include "diskmd.h"
#include "plock.h"
#include "bmap.h"
#include "buffer.h"

#define DM_FLAG_MD 0x01
#define DM_FLAG_RAW 0x02
#define DM_FLAG_MT 0x04

#define DISK_MAPING_ITERATOR_NEW 1

typedef int (*dm_itor_t)(const chkid_t *chkid, const char *pool, const diskloc_t *loc,
                         const chkid_t *parent, const uint64_t meta, void *ctx);

typedef struct {
        int (*init)(const char *home);
        void (*close)();

        int (*create)(const char *pool, const chkid_t *chkid, const diskloc_t *loc, int chknum,
                              const chkid_t *parent,
                              uint64_t meta_version, int flag);

        int (*del)(const chkid_t *chkids, const diskloc_t *locs, uint32_t count);

        int (*setparent)(const chkid_t *chkid, const chkid_t *parent);
        int (*setloc)(const chkid_t *chkid, const diskloc_t *loc);

        int (*getmetaversion)(const chkid_t *chkid, uint64_t *meta_version);
        int (*setmetaversion)(const chkid_t *chkid, uint64_t meta_version);

        int (*load)(const chkid_t *chkid, diskloc_t *loc, chkid_t *parent, char *pool);
        int (*exist)(const chkid_t *chkids, int chknum, int *exists);
        int (*cleanup)(const char *pool);
        int (*count)(int diskid, uint64_t *count);

        //void (*iterator_raw)(const char *table, func2_t func, void *arg);
        //void (*iterator_mt)(const char *table, func2_t func, void *arg);
        void (*iterator)(const char *table, func2_t func, void *arg);
        void (*iterator_bydisk)(int diskid, dm_itor_t func, void *_arg, int flag);
        void (*iterator_byvol)(const volid_t *volid, dm_itor_t func, void *arg);
        void (*iterator_new)(dm_itor_t func, void *arg, int flag);
        
        //void (*key)(const char *buf, chkid_t *chkid);
        //void (*decode)(const char *buf, char *_pool, diskloc_t *_loc, chkid_t *_parent, uint64_t *_meta_version);
} disk_maping_t; 

extern disk_maping_t *disk_maping;



int disk_maping_init(const char *home);
int disk_maping_register();

#endif
