#ifndef __DISK_REDIS_H_
#define __DISK_REDIS_H_

#if DISK_MAPING_ITERATOR_NEW
#else
void disk_redis_key(const char *buf, chkid_t *chkid);
void disk_redis_decode(const char *buf, char *_pool, diskloc_t *_loc,
                       chkid_t *_parent, uint64_t *_meta_version);
void disk_redis_iterator_byvol(const volid_t *volid, func2_t func, void *arg);
#endif

disk_maping_t * disk_maping_redis();

#endif
