#include "config.h"

#include <sys/types.h>
#include <sys/epoll.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include <libgen.h>
#include <ctype.h>
#include <fcntl.h>
#include <libaio.h>
#include <sqlite3.h>
#include <errno.h>
#include <getopt.h>

#define DBG_SUBSYS S_LIBREPLICA

#include "core.h"
#include "cache.h"
#include "disk.h"
#include "sysy_lib.h"
#include "ynet_rpc.h"
#include "job_dock.h"
#include "net_global.h"
#include "diskmd.h"
#include "bh.h"
#include "tpool.h"
#include "lich_md.h"
#include "configure.h"
#include "dbg.h"
#include "replica.h"
#include "core.h"
#include "coroutine.h"
#include "fnotify.h"
#include "locator_rpc.h"

// -------------------------------------------------------------------------
// begin load_multi

typedef struct {
        co_fork_hook_t fork_hook;

        int chknum;
        int chkidx;
        chkid_t *chkids;
        int *indices;

        int *exists;
} db_load_multi_ctx_t;

int disk_sqlite3_load_request_multi(const chkid_t *chkids, int chknum, int *exists);

static void __db_load_multi_ctx_free(db_load_multi_ctx_t *ctxs, int count)
{
        int i;
        db_load_multi_ctx_t *ctx;

        for (i=0; i < count; i++) {
                ctx = &ctxs[i];
                if (ctx->chkids)
                        yfree((void **)&ctx->chkids);

                if (ctx->indices)
                        yfree((void **)&ctx->indices);

                if (ctx->exists)
                        yfree((void **)&ctx->exists);
        }
}

/**
 * 按db分组，每个task负责一个db，组内批量提交
 *
 * @param ctxs
 * @param count
 * @param db_count OUT
 * @param chkids
 * @param chknum
 * @return
 */
static int __db_load_multi_ctx_group_by(db_load_multi_ctx_t *ctxs, int count, int *db_count,
                                        const chkid_t *chkids, int chknum)
{
        int ret, i, db;
        db_load_multi_ctx_t *ctx;

        *db_count = 0;

        for (i=0; i < chknum; i++) {
                db = __DISK_DB_HASH__(&chkids[i]);
                ctxs[db].chknum++;
        }

        for (i=0; i < __DB_HASH__; i++) {
                ctx = &ctxs[i];
                if (ctx->chknum == 0)
                        continue;

                // 共映射到多少db
                (*db_count)++;

                // 分配所需空间
                ret = ymalloc((void **)&ctx->chkids, sizeof(chkid_t) * ctx->chknum);
                if (unlikely(ret)) {
                        GOTO(err_ret, ret);
                }

                ret = ymalloc((void **)&ctx->indices, sizeof(int) * ctx->chknum);
                if (unlikely(ret)) {
                        GOTO(err_ret, ret);
                }

                ret = ymalloc((void **)&ctx->exists, sizeof(int) * ctx->chknum);
                if (unlikely(ret)) {
                        GOTO(err_ret, ret);
                }
        }

        // group by chkid
        for (i=0; i < chknum; i++) {
                db = __DISK_DB_HASH__(&chkids[i]);
                ctx = &ctxs[db];
                ctx->chkids[ctx->chkidx] = chkids[i];
                ctx->indices[ctx->chkidx] = i;
                ctx->exists[ctx->chkidx] = 0;
                ctx->chkidx++;
        }

        return 0;
err_ret:
        __db_load_multi_ctx_free(ctxs, count);
        return ret;
}

static void __db_load_multi(void *arg)
{
        int ret;
        db_load_multi_ctx_t *ctx = arg;

        ANALYSIS_BEGIN(0);

        ret = disk_sqlite3_load_request_multi(ctx->chkids, ctx->chknum, ctx->exists);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        ANALYSIS_QUEUE(0, 1000*1000, "__db_load_multi");

        DBUG("chknum %d\n", ctx->chknum);

        co_fork_return(ctx->fork_hook.fork, ctx->fork_hook.idx, 0);
        return;
err_ret:
        co_fork_return(ctx->fork_hook.fork, ctx->fork_hook.idx, ret);
}

int disk_sqlite3_load_batch(const chkid_t *chkids, int chknum, int *exists)
{
        int ret, i, j, db_num = 0;
        db_load_multi_ctx_t ctxs[__DB_HASH__], *ctx;
        co_fork_t *fork;

        memset(&ctxs, 0x0, sizeof(ctxs));

        ret = __db_load_multi_ctx_group_by(ctxs, __DB_HASH__, &db_num, chkids, chknum);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        YASSERT(db_num > 0);

        // 传入总的任务数
        ret = co_fork_create(&fork, __FUNCTION__, db_num);
        if (unlikely(ret))
                GOTO(err_ctx, ret);

        for (i=0; i < __DB_HASH__; i++) {
                ctx = &ctxs[i];
                if (ctx->chknum == 0)
                        continue;

                YASSERT(ctx->chkids);

                // schedule_task_new(__FUNCTION__, __db_create_multi, ctx, -1);

                // fork new task
                co_fork_add(fork, __db_load_multi, ctx);
        }

        // wait here, 检查返回值
        ret = co_fork_join(fork);
        if (unlikely(ret))
                GOTO(err_fork, ret);

        // 填充返回值
        for (i=0; i < __DB_HASH__; i++) {
                ctx = &ctxs[i];
                if (ctx->chknum == 0)
                        continue;

                for (j=0; j < ctx->chknum; j++) {
                        if (ctx->exists[j]) {
                                exists[ctx->indices[j]] = 1;
                        }
                }
        }

        yfree((void **)&fork);
        __db_load_multi_ctx_free(ctxs, __DB_HASH__);
        return 0;
err_fork:
        yfree((void **)&fork);
err_ctx:
        __db_load_multi_ctx_free(ctxs, __DB_HASH__);
err_ret:
        return ret;
}

// end load_multi
// -------------------------------------------------------------------------

