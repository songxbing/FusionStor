#ifndef __DISK_SQL_H_
#define __DISK_SQL_H_

#include "config.h"
#include "disk_maping.h"

#define __DB_HASH__ 10
#define __DISK_DB_HASH__(chkid) (((chkid)->idx + (chkid)->id) % __DB_HASH__)

#if DISK_MAPING_ITERATOR_NEW
#else
void disk_sqlite3_iterator_cursor(const char *table, int cursor, const char *condition, func_va_t func, void *arg);
#endif

disk_maping_t * disk_maping_sqlite3();

#endif
