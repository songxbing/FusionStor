#include "config.h"

#include <sys/types.h>
#include <sys/epoll.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include <libgen.h>
#include <ctype.h>
#include <fcntl.h>
#include <libaio.h>
#include <errno.h>
#include <getopt.h>

#define DBG_SUBSYS S_LIBREPLICA

#include "core.h"
#include "cache.h"
#include "disk.h"
#include "sysy_lib.h"
#include "ynet_rpc.h"
#include "job_dock.h"
#include "net_global.h"
#include "diskmd.h"
#include "bh.h"
#include "tpool.h"
#include "lich_md.h"
#include "configure.h"
#include "dbg.h"
#include "replica.h"
#include "core.h"
#include "coroutine.h"
#include "fnotify.h"
#include "redis_utils.h"
#include "disk_maping.h"
#include "disk_redis.h"

int disk_redis_ops_register();
int disk_sqlite3_ops_register();

disk_maping_t *disk_maping;

int disk_maping_init(const char *home)
{
        int ret;
        
        if (gloconf.kv_redis) {
                disk_maping = disk_maping_redis();
        } else {
                disk_maping = disk_maping_sqlite3();
        }

        ret = disk_maping->init(home);
        if (ret)
                GOTO(err_ret, ret);

        return 0;
err_ret:
        return ret;
}

int disk_maping_register()
{
        int ret;

        if (gloconf.kv_redis) {
                ret = disk_redis_ops_register();
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        } else {                
                ret = disk_sqlite3_ops_register();
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        }

        return 0;
err_ret:
        return ret;
}
