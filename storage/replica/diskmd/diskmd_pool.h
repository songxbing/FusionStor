#ifndef __DISKMD_POOL_
#define __DISKMD_POOL_

#include "disk.h"

typedef struct {
        sy_spinlock_t lock;
        struct list_head list;
} allocator_t;

typedef struct {
        //protect pool's disk add/rm
        sy_rwlock_t lock;
        int idx;
        int disk_max;
        int tier_max;
        int tier_min;
        char name[MAX_NAME_LEN];

        allocator_t allocator;
        int disk_allocate_ptr[DISK_TIER_MAX];

        int disk_array[DISK_MAX];
} pool_t;

int  create_pool(const char *name, pool_t **_pool);
void free_pool(pool_t **pool);

int  diskmd_pool_disks_get(pool_t *pool, int tier, int locs_count, disk_t **array, int *disk_count);
void diskmd_pool_disks_release(pool_t *pool, disk_t **array, int disk_count);

int diskmd_pool_add_disk(pool_t* pool, disk_t *disk, int bad);
int diskmd_pool_add_disk_nolock(pool_t* pool, disk_t *disk, int bad);

int diskmd_pool_remove_disk(const char *home, pool_t* pool, int idx);
int diskmd_pool_remove_disk_nolock(pool_t* pool, int idx, disk_t **disk);

int  diskmd_pool_dfree1(pool_t *pool, uint64_t *_pool_total, uint64_t *_pool_used, int print);
void diskmd_pool_dfree_nolock(pool_t *pool, uint64_t *_pool_total, uint64_t *_pool_used, int print);

void disk_manager_pool_update_tier(pool_t *pool);

int diskmd_pool_get_empty(pool_t *pool, diskloc_t *locs, int locs_count, int *_tier);
int diskmd_pool_get_empty_with_tier(pool_t *pool, diskloc_t *locs, int locs_count, int tier);

void diskmd_pool_dump(pool_t *pool);

#endif
