#ifndef __DISK_NVME_H__
#define __DISK_NVME_H__


extern void libnvme_init(void);
extern void nvme_qpairs_poll(void *core, void * ud);

#endif
