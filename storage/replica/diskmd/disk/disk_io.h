#ifndef __DISK_IO__
#define __DISK_IO__
#include "buffer.h"
#include "adt.h"

typedef struct {
        uint32_t diskid;
        uint32_t idx;
        uint32_t disk_type;
} diskloc_t;

/*
typedef enum {
        __DSK_TYPE_START__ = 0xff,
        __SPDK_DISK__ ,
        __DISK_TYPE_RAM_DISK__,
        __NORMAL_DISK__,
        __DSK_TYPE_END__,
}disk_type_t;
*/


/*struct disk_op_t {
        int (*io_pread)(void *fd, char *buf, size_t size, off_t offset);
        int (*io_pwrite)(void *fd, char *buf, size_t size, off_t offset);
        int (*io_preadv)(void *fd,  buffer_t *buf, off_t offset);
        int (*io_pwritev)(void *fd, const buffer_t *buf, off_t offset);
        int (*aio_readv)(const diskloc_t *loc,  buffer_t *buf, int _offset, int prio);
        int (*aio_writev)(const diskloc_t *loc, const buffer_t *buf, int _offset, int prio);
};
*/
void register_disktype_ops(int disk_type, struct disk_op_t *op);

void register_normal_disk_op(struct disk_op_t **dop) ;
void register_spdk_disk_op(struct disk_op_t **dop) ;
void register_ram_disk_op(struct disk_op_t **dop) ;
void* ram_disk_get_fd(const char *dev_name, uint64_t dev_size, const char *dev_uuid);

#endif
