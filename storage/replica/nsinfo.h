#ifndef __NSINFO_H__
#define __NSINFO_H__

#include "chunk.h"
//#include "md_proto.h"

typedef struct {
        uint64_t size;
        uint32_t volid;
        uint32_t __pad__;
} volrept_t;

typedef struct {
        uint32_t  volreptnum;
        uint32_t  __pad__;
        volrept_t volrept[0];
} volinfo_t;

int nsinfo_init(const char *home);
int nsinfo_update(uint32_t volid, int size);
int nsinfo_get(volinfo_t **_info);

#endif
