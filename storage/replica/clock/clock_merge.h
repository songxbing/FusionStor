#ifndef __MD_DISK_H__
#define __MD_DISK_H__

#include <stdint.h>
#include <semaphore.h>
#include <errno.h>
#include <libaio.h>

#include "ynet_rpc.h"
#include "sysy_conf.h"
#include "chunk.h"
#include "buffer.h"

int clock_merge_init(const char *home, clock_mem_t *md);
int clock_merge(const char *log, const char *data, int idx, uint64_t from, uint64_t to, clock_mem_t **megre);

#endif
