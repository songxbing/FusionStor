#ifndef __REPLICA_MD_H__
#define __REPLICA_MD_H__

#include <stdint.h>
#include <semaphore.h>
#include <errno.h>
#include <libaio.h>

#include "ynet_rpc.h"
#include "sysy_conf.h"
#include "clock_mem.h"
#include "buffer.h"

#define CLOCK_HASH 10

#define MD_OP_NULL                    0x00000
#define MD_OP_VCLOCK                  0x00004
#define MD_OP_REMOVE                  0x00008

#define MD_OP_SIZE (40)
#define MD_BUFFER_SIZE (MD_OP_SIZE * 1024)
#define MD_SEG_SIZE (MD_OP_SIZE * 29696)
#define MD_MAX (MD_SEG_SIZE)

extern int clock_table_size;

int clock_init(uint64_t max_chunk);

int clock_set(const chkid_t *id, const vclock_t *vclock, int dirty);
int clock_get(const chkid_t *id, vclock_t *clock, int *dirty);

int clock_remove(const chkid_t *id);
int clock_load(const char *path, int size, clock_mem_t *__clock_mem);

#endif
