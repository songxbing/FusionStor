#ifndef __MD_MEM_H__
#define __MD_MEM_H__

#include <stdint.h>
#include <semaphore.h>
#include <errno.h>
#include <libaio.h>

#include "ynet_rpc.h"
#include "sysy_conf.h"
#include "plock.h"
#include "buffer.h"

#define CLOCK_HASHTABLE_PRE_SIZE_ENABLE 0

typedef struct {
        chkid_t id;
        vclock_t vclock;
        int dirty;
        sy_spinlock_t lock;
} clock_entry_t;

typedef struct {
        int priv;
        hashtable_t tab;
        union {
                sy_rwlock_t rwlock;
                plock_t plock;
        } u;
} clock_mem_t;

int clock_mem_init(clock_mem_t **md, const char *name, int private);
void clock_mem_destroy(clock_mem_t *md);

int clock_mem_add(clock_mem_t *md, const chkid_t *chkid, const vclock_t *vclock);
int clock_mem_remove(clock_mem_t *md, const chkid_t *id);

int clock_mem_vclock_set(clock_mem_t *md, const chkid_t *id, const vclock_t *vclock, int dirty);
int clock_mem_vclock_get(clock_mem_t *md, const chkid_t *id, vclock_t *vclock, int *dirty);

int clock_mem_iterator(clock_mem_t *md,  int (*handler)(void *, void *), void *ctx);

#endif
