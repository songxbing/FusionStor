#ifndef __MD_ROOT__
#define __MD_ROOT__

int md_root_init();
int md_root_del(const char *pool);

int md_root_lookup1(const char *pool, const fileid_t *parent, const fileid_t *chkid,
                    chkinfo_t *chkinfo, nid_t *parentnid);
int md_root_lookup(const char *pool, const fileid_t *parent, const fileid_t *chkid,
                   chkinfo_t *chkinfo, nid_t *parentnid);
int md_root_chunk_set(const nid_t *nid, int status);
int md_root_chunk_update(const char *pool, const chkinfo_t *_chkinfo, const nid_t *owner, uint64_t info_version);
int md_root_chunk_cleanup(const char *pool, const nid_t *nid, uint64_t meta_version);
int md_root_chunk_reject(const char *pool, chkinfo_t *chkinfo, const nid_t *nid, nid_t *parentnid);


#endif

