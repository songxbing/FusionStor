#ifndef __MD_TRANS_H__
#define __MD_TRANS_H__

#include "config.h"

#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include "limits.h"
#include "adt.h"
#include "ynet_rpc.h"
#include "sysy_lib.h"
#include "cluster.h"
#include "chunk.h"
#include "bmap.h"
#include "fs_proto.h"
//#include "metadata.h"
#include "net_table.h"
#include "configure.h"
#include "chunk_proto.h"
#include "fileinfo.h"
#include "ylog.h"
#include "dbg.h"
#include "lichstor.h"


int stor_rpc_init();

// pool
int stor_rpc_mkpool(const nid_t *nid, const fileid_t *parent, const char *name, const char *site_name,
                 const setattr_t *setattr, chkinfo_t *chkinfo);
int stor_rpc_rmpool(const nid_t *nid, const fileid_t *fileid, const char *name);

int stor_rpc_listpool_open(const nid_t *nid, const chkid_t *parent, const char *uuid);
int stor_rpc_listpool(const nid_t *nid, const fileid_t *parent, const char *uuid, uint64_t offset, void *de, int *delen);
int stor_rpc_listpool_close(const nid_t *nid, const chkid_t *parent, const char *uuid);
int stor_rpc_extend_pool(const nid_t *nid, const chkid_t *poolid);

// volume
int stor_rpc_mkvol(const nid_t *nid, const fileid_t *parent, const char *name, const char *site_name,
                  const setattr_t *setattr, chkinfo_t *chkinfo);
int stor_rpc_mkvolwith(const nid_t *nid, const fileid_t *parent, const char *name,
                  const chkinfo_t *chkinfo);
int stor_rpc_rmvol(const nid_t *nid, const fileid_t *fileid, const char *name, int force);
int stor_rpc_cleanup(const nid_t *nid, const fileid_t *fileid, const char *name);

int stor_rpc_rename_lock(const nid_t *nid, const fileid_t *fileid,
                       const fileid_t *src, const char *name, int force);
int stor_rpc_rename_unlock(const nid_t *nid, const fileid_t *fileid, const chkinfo_t *chkinfo);
int stor_rpc_rename_check(const nid_t *nid, const fileid_t *fileid, const fileid_t *locked);
int stor_rpc_rename(const nid_t *nid, const fileid_t *from, const char *fname,
                  const fileid_t *to, const char *tname);
int stor_rpc_move(const nid_t *nid, const fileid_t *fileid, const nid_t *dist, int dist_count);

int stor_rpc_localize(const nid_t *nid, const fileid_t *fileid, int idx);
int stor_rpc_stat(const nid_t *nid, const fileid_t *fileid, filestat_t *filestat, off_t off, size_t size);
int stor_rpc_check_ready(const nid_t *nid, const fileid_t *fileid);

int stor_rpc_write(const nid_t *nid, const io_t *io, const buffer_t *_buf);//, uint32_t size, uint64_t offset);
int stor_rpc_read(const nid_t *nid, const io_t *io, buffer_t *buf);
int stor_rpc_unmap(const nid_t *nid, const io_t *io);

int stor_rpc_getpool(const nid_t *nid, const fileid_t *chkid,  char *pool);
int stor_rpc_getattr(const nid_t *nid, const fileid_t *chkid,  fileinfo_t *fileinfo);
int stor_rpc_setattr(const nid_t *nid, const fileid_t *chkid,  fileinfo_t *fileinfo, const setattr_t *setattr);

int stor_rpc_table_write(const nid_t *nid, const fileid_t *fileid, const chkid_t *chkid,
        const buffer_t *_buf, uint32_t size, uint64_t offset);
int stor_rpc_table_read(const nid_t *nid, const fileid_t *fileid, const chkid_t *chkid,
        buffer_t *_buf, uint32_t size, uint64_t offset);

// chunk
int stor_rpc_newchunk(const nid_t *nid, const fileid_t *fileid, const chkid_t *chkid,
                       const buffer_t *_buf);
int stor_rpc_discard(const nid_t *nid, const fileid_t *fileid, const chkid_t *chkid);

int stor_rpc_chunk_cleanup(const nid_t *nid, const fileid_t *fileid,
                           const chkid_t *chkid, const nid_t *_nid, uint64_t meta_version);

int stor_rpc_chunk_exist(const nid_t *nid, const fileid_t *fileid, const chkid_t *chkid, int *exist);
int stor_rpc_lookup(const nid_t *nid, const fileid_t *parent, const char *name,
                    chkinfo_t *chkinfo);
int stor_rpc_chunk_getinfo(const nid_t *nid, const fileid_t *parent, const chkid_t *chkid,
                           chkinfo_t *chkinfo);

int stor_rpc_chunk_allocate(const nid_t *nid, const fileid_t *fileid, const chkid_t *chkid, int chknum, int fill);
int stor_rpc_chunk_set(const nid_t *parentnid, const fileid_t *parent, const chkid_t *chkid,
                       const nid_t *nid, int status);

int stor_rpc_chunk_update(const nid_t *nid, const fileid_t *parent,
                          const chkinfo_t *chkinfo, const nid_t *owner, uint64_t info_version);
int stor_rpc_chunk_move(const nid_t *nid, const fileid_t *fileid,
                        const chkid_t *chkid, const nid_t *dist, int dist_count);
int stor_rpc_chunk_check(const nid_t *nid, const fileid_t *fileid,
                         const chkid_t *chkid, int idx, int async, int force, int *oflags);
int stor_rpc_chunk_check_multi(const nid_t *nid, const fileid_t *fileid,
                         const chkid_t *chkid, int chk_count, int *retval);
int stor_rpc_chunk_migrate(const nid_t *nid, const fileid_t *parent, const chkid_t *chkid, uint32_t force,
                          chkinfo_t *chkinfo);
int stor_rpc_chunk_reject(const nid_t *nid, const fileid_t *parent, const chkid_t *chkid,
                          const nid_t *bad, chkinfo_t *chkinfo);

// VFM
int stor_rpc_vfm_set(const nid_t *parentnid, const fileid_t *parent, const chkid_t *chkid, const vfm_t *vfm);
int stor_rpc_vfm_get(const nid_t *parentnid, const fileid_t *parent, const chkid_t *chkid, vfm_t *vfm);
int stor_rpc_vfm_stat(const nid_t *nid, const fileid_t *fileid, int *count);
int stor_rpc_vfm_cleanup(const nid_t *nid, const fileid_t *fileid, const chkid_t *tid);

// attr
int stor_rpc_setattr(const nid_t *nid, const fileid_t *chkid,  fileinfo_t *fileinfo, const setattr_t *setattr);
int stor_rpc_getattr(const nid_t *nid, const fileid_t *chkid,  fileinfo_t *fileinfo);

// xattr
int stor_rpc_xattr_set(const nid_t *nid, const chkid_t *chkid, const char *key,
                       const char *value, uint32_t valuelen, int flag);
int stor_rpc_xattr_get(const nid_t *nid, const chkid_t *chkid, const char *key,
                    char *value, int *valuelen);
int stor_rpc_xattr_list(const nid_t *nid, const chkid_t *chkid, char *buf, int *buflen);
int stor_rpc_xattr_remove(const nid_t *nid, const chkid_t *chkid, const char *key);

int stor_rpc_connection(const nid_t *nid, const fileid_t *fileid, void *list, int *_count);
int stor_rpc_connect(const nid_t *nid, const fileid_t *fileid,
                const nid_t *peer, const char *addr);
int stor_rpc_disconnect(const nid_t *nid, const fileid_t *fileid, const nid_t *peer, const char *addr);

// snapshot
int stor_rpc_snapshot_create(const nid_t *nid, const fileid_t *fileid, const char *name, int p, const char *_site);
int stor_rpc_snapshot_listopen(const nid_t *nid, const chkid_t *parent, const char *uuid);
int stor_rpc_snapshot_list(const nid_t *nid, const fileid_t *parent, const char *uuid, uint64_t offset, void *de, int *delen);
int stor_rpc_snapshot_listclose(const nid_t *nid, const chkid_t *parent, const char *uuid);
int stor_rpc_snapshot_read(const nid_t *nid, const fileid_t *parent, const io_t *io, buffer_t *buf);
int stor_rpc_snapshot_diff(const nid_t *nid, const fileid_t *parent, const fileid_t *fileid,
        const fileid_t *snapdst, buffer_t *_buf, uint32_t size, uint64_t offset);

int stor_rpc_snapshot_rollback(const nid_t *nid, const fileid_t *fileid, const char *name);
int stor_rpc_snapshot_flat(const nid_t *nid, const fileid_t *fileid, const int idx, int force);

int stor_rpc_snapshot_remove(const nid_t *nid, const fileid_t *fileid, const char *name, int force);
int stor_rpc_snapshot_protect(const nid_t *nid, const fileid_t *fileid, const snap_protect_param_t on);
int stor_rpc_snapshot_cleanup(const nid_t *nid, const fileid_t *fileid);

int stor_rpc_snapshot_updateparent(const nid_t *nid, const fileid_t *fileid, const char *name, const uint64_t from);
int stor_rpc_snapshot_setfrom(const nid_t *nid, const fileid_t *fileid, const uint64_t from);
int stor_rpc_snapshot_last(const nid_t *nid, const fileid_t *fileid, nid_t *snapnid, fileid_t *snapid, char *snapname, uint64_t *snap_version);
int stor_rpc_snapshot_prev(const nid_t *nid, const fileid_t *fileid, const fileid_t *snapid, fileid_t *previd, char *snapname, uint64_t *snap_version);

// for LSV
int stor_rpc_snapshot_read_meta(const nid_t *nid, const fileid_t *parent, const io_t *io, const char *snap, buffer_t *_buf);
int stor_rpc_lower_read(const nid_t *nid, const fileid_t *parent, const io_t *io, buffer_t *buf);

int stor_rpc_snapshot_check(const nid_t *, const fileid_t *, const char *);
int stor_rpc_snapshot_diff(const nid_t *nid, const fileid_t *parent, const fileid_t *fileid,
        const fileid_t *snapdst, buffer_t *_buf, uint32_t size, uint64_t offset);
int stor_rpc_snapshot_isempty(const nid_t *nid, const fileid_t *fileid, int *empty);

// consistency group

int stor_rpc_group_snapshot_lock(const nid_t *, const fileid_t *, uuid_t *);
int stor_rpc_group_snapshot_unlock(const nid_t *, const fileid_t *, const uuid_t);
int stor_rpc_group_snapshot_create_nolock(const nid_t *, const fileid_t *, const char *, const char *, const uuid_t);
int stor_rpc_group_snapshot_remove_nolock(const nid_t *, const fileid_t *, const char *, const uuid_t);

// background
int stor_rpc_rollback_bh(const nid_t *nid, const fileid_t *fileid);
int stor_rpc_flat_bh(const nid_t *nid, const fileid_t *fileid);
int stor_rpc_rmsnap_bh(const nid_t *nid, const fileid_t *fileid, const char *name);
int stor_rpc_cleanup_bh(const nid_t *nid, const fileid_t *fileid, const char *name);

#endif
