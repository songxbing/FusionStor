#ifndef __MD_PARENT__
#define __MD_PARENT__

int md_parent_init();

int md_parent_get(const chkid_t *chkid, chkid_t *parent);
int md_parent_get1(const chkid_t *chkid, chkid_t *parent);
int md_parent_update(const chkid_t *chkid, const chkid_t *parent);
int md_parent_drop(const chkid_t *chkid, const chkid_t *parent);

#endif
