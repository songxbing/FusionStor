#ifndef __INODE_H__
#define __INODE_H__

#include "sysy_lib.h"
#include "get_version.h"
#include "cluster.h"
#include "volume.h"
#include "storage.h"
//#include "metadata.h"
#include "configure.h"
#include "job_dock.h"
#include "fs_proto.h"
#include "ylog.h"
#include "cache.h"
#include "dbg.h"

typedef struct {
        fileid_t id;
        time_t ltime;      /* Lease time */
        time_t last_update;
        uint64_t info_version;
        nid_t master;
        fileinfo_t fileinfo;
        char pool[MAX_NAME_LEN];
} vnode_entry_t;

typedef int (*vnode_update_handler)(vnode_entry_t *ent, fileinfo_t *, va_list);

int vnode_init();
void vnode_release(mcache_entry_t *cent);

int vnode_drop(mcache_entry_t *cent, const fileid_t *id);

int vnode_get(mcache_entry_t **_cent, const char *pool, const fileid_t *oid, int _retry);
int vnode_isempty(const fileid_t *fid, int *empty);
int vnode_utime(const char *pool, const fileid_t *id, int atime, int mtime);

int vnode_location(const char *pool, const fileid_t *id, nid_t *nid);
int vnode_reload(const char *pool, const fileid_t *oid);
int vnode_force_reload_nolock(vnode_entry_t *ent);

int vnode_chmod(const char *pool, const fileid_t *id, int mode);
int vnode_chown(const char *pool, const fileid_t *id, int uid, int gid);

int vnode_reset(const char *pool, const fileid_t *id);
int vnode_update(const char *pool, const fileid_t *id, uint64_t size, int force);
int vnode_set_repnum(const char *pool, const fileid_t *id, int repnum);

int vnode_setattr(const char *pool, const fileid_t *id, const setattr_t *setattr);
int vnode_getattr(const char *pool, const fileid_t *id, fileinfo_t *fileinfo, int retry);

#endif
