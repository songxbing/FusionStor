#ifndef __STATUS__H__
#define __STATUS__H__

#define STORAGE_STATUS_UPTIME (10)

int storage_status_bh_init();
void storage_status_update();

#endif
