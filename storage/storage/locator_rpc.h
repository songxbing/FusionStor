#ifndef __LOCATOR_RPC_H__
#define __LOCATOR_RPC_H__

int locator_rpc_init();
int locator_rpc_lookup(const chkid_t *chkid, nid_t *nid, int force);
int locator_rpc_boardcast(const chkid_t *chkid);

#endif
