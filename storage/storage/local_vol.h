#ifndef __LOCAL_H__
#define __LOCAL_H__

extern void local_vol_sync_post(BOOL force);
extern int local_vol_sync_bh_init();

#endif
