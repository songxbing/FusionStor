#include "config.h"

#include <time.h>
#include <dirent.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdarg.h>
#include <signal.h>

#define DBG_SUBSYS S_LIBTASK

#include "sysy_lib.h"
#include "cache.h"
#include "get_version.h"
#include "vnode.h"
#include "cluster.h"
#include "volume.h"
#include "lichstor.h"
#include "stor_root.h"
#include "lich_md.h"
#include "md_map.h"
#include "configure.h"
#include "job_dock.h"
#include "chunk.h"
#include "rmvol_bh.h"
#include "ynet_rpc.h"
#include "net_global.h"
#include "ylog.h"
#include "dbg.h"
#include "lichstor.h"
#include "timer.h"
#include "replica.h"
#include "system.h"

static worker_handler_t rmvol_bh_handler;

int rmvol_bh_root(const char *pool, fileid_t *rootid)
{
        int ret;

        ret = stor_root(pool, UNLINK_ROOT, rootid);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        return 0;
err_ret:
        return ret;
}

int rmvol_bh_init()
{
        return 0;
}

STATIC int __pool_rmvol_bh_worker(void *pool, void *_arg)
{
        int ret, delen, success = 0, retry = 0;
        uint64_t offset = 0, offset2 = 0;
        char buf[BIG_BUF_LEN], uuid[MAX_NAME_LEN] = {};
        struct dirent *de;
        fileid_t rootid;

        (void)_arg;
retry0:
        // i.e. /system/unlink
        ret = stor_root(pool, UNLINK_ROOT, &rootid);
        if (unlikely(ret)) {
                if (ret == EAGAIN) {
                        USLEEP_RETRY(err_ret, ret, retry0, retry, 10, (1000 * 1000));
                } else
                        GOTO(err_ret, ret);
        }

        get_uuid(uuid);
        ret = md_listpool_open(&rootid, uuid);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        while(1) {
retry:
                success = 0;
                delen = BIG_BUF_LEN;
                memset(buf, 0, delen);
                ret = md_listpool(&rootid, uuid, offset, buf, &delen);
                if (unlikely(ret)) {
                        USLEEP_RETRY(err_close, ret, retry, retry, 10, (1000 * 1000));
                }

                if (delen == 0)
                        break;

                offset2 = 0;
                dir_for_each(buf, delen, de, offset2) {
                        if (strlen(de->d_name) == 0)
                                goto out;
                        else if (delen - offset2 < sizeof(*de) + MAX_NAME_LEN)
                                break;

                        offset += de->d_reclen;

                        if (!strcmp(de->d_name, ".") || !strcmp(de->d_name, "..") ||
                                        de->d_name[0] == '.') {
                                continue;
                        }

                        DINFO("rmvol_bh file %s\n", de->d_name);

                        ret = volume_cleanup_bh(&rootid, de->d_name);
                        if (unlikely(ret)) {
                                DERROR("rmvol_bh %s fail\n", de->d_name);
                                continue;
                        }

                        success++;
                }

#if 0
                if (success) {
                        DINFO("rmvol retry\n");
                        goto retry;
                }
#endif
        }
out:
        ret = timer1_settime(&rmvol_bh_handler, USEC_PER_MIN);
        if (unlikely(ret)) {
                DERROR("settime error ret %d\n", ret);
                YASSERT(0);
        }
        md_listpool_close(&rootid, uuid);

        return 0;
err_close:
        md_listpool_close(&rootid, uuid);
err_ret:
        DERROR("rmvol_bh file error ret (%d) %s\n", ret, strerror(ret));
        SWARN(0, "%s, rmvol_bh file error ret (%d) %s\n", M_DATA_VOLUME_WARN, ret, strerror(ret));

        ret = timer1_settime(&rmvol_bh_handler, USEC_PER_MIN);
        if (unlikely(ret)) {
                DERROR("settime error ret %d\n", ret);
                YASSERT(0);
        }

        return ret;
}

STATIC int __rmvol_bh_worker(void *_args)
{
        (void) _args;

        return system_pool_iterator(__pool_rmvol_bh_worker, NULL);
}
