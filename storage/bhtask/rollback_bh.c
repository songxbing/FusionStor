#include "config.h"

#include <dirent.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdarg.h>
#include <signal.h>

#define DBG_SUBSYS S_LIBTASK

#include "sysy_lib.h"
#include "cache.h"
#include "get_version.h"
#include "vnode.h"
#include "cluster.h"
#include "volume.h"
#include "lichstor.h"
#include "stor_root.h"
#include "lich_md.h"
#include "md_map.h"
#include "configure.h"
#include "job_dock.h"
#include "chunk.h"
#include "rollback_bh.h"
#include "ynet_rpc.h"
#include "net_global.h"
#include "ylog.h"
#include "dbg.h"
#include "lichstor.h"
#include "timer.h"
#include "system.h"

static worker_handler_t rollback_bh_handler;

int rollback_bh_root(const char *pool, fileid_t *rootid)
{
        int ret;

        ret = stor_root(pool, ROLLBACK_ROOT, rootid);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        return 0;
err_ret:
        return ret;
}

int rollback_bh_create(const char *pool, const fileid_t *fileid)
{
        int ret;
        fileid_t rootid;
        char name[MAX_NAME_LEN];

        ret = rollback_bh_root(pool, &rootid);
        if (ret)
                GOTO(err_ret, ret);

        snprintf(name, MAX_NAME_LEN, CHKID_FORMAT, CHKID_ARG(fileid));
        ret = stor_mkvol(&rootid, name, NULL, NULL);
        if (ret)
                GOTO(err_ret, ret);

        return 0;
err_ret:
        return ret;
}

int rollback_bh_remove(const char *pool, const fileid_t *fileid)
{
        int ret;
        fileid_t rootid;
        char name[MAX_NAME_LEN];

        ret = rollback_bh_root(pool,&rootid);
        if (ret)
                GOTO(err_ret, ret);

        snprintf(name, MAX_NAME_LEN, CHKID_FORMAT, CHKID_ARG(fileid));
        ret = stor_rmvol(&rootid, name, 0);
        if (ret)
                GOTO(err_ret, ret);

        return 0;
err_ret:
        return ret;
}

int rollback_bh_init()
{
        return 0;
}

STATIC int __pool_rollback_bh_worker(void *pool, void *_arg)
{
        int ret, delen, success = 0, retry = 0;
        uint64_t offset = 0, offset2 = 0;
        char buf[BIG_BUF_LEN], uuid[MAX_NAME_LEN] = {};
        struct dirent *de;
        fileid_t rootid;

        (void) _arg;
retry0:
        ret = stor_root(pool, ROLLBACK_ROOT, &rootid);
        if (unlikely(ret)) {
                if (ret == EAGAIN) {
                        USLEEP_RETRY(err_ret, ret, retry0, retry, 10, (1000 * 1000));
                } else
                        GOTO(err_ret, ret);
        }

        get_uuid(uuid);
        ret = md_listpool_open(&rootid, uuid);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        while(1) {
retry:
                success = 0;
                delen = BIG_BUF_LEN;
                memset(buf, 0, delen);
                ret = md_listpool(&rootid, uuid, offset, buf, &delen);
                if (unlikely(ret)) {
                        USLEEP_RETRY(err_close, ret, retry, retry, 10, (1000 * 1000));
                }

                if (delen == 0)
                        break;

                offset2 = 0;
                dir_for_each(buf, delen, de, offset2) {
                        if (strlen(de->d_name) == 0)
                                goto out;
                        else if (delen - offset2 < sizeof(*de) + MAX_NAME_LEN)
                                break;

                        offset += de->d_reclen;

                        if (!strcmp(de->d_name, ".") || !strcmp(de->d_name, "..") ||
                                        de->d_name[0] == '.') {
                                continue;
                        }

                        DINFO("rollback_bh file %s\n", de->d_name);

                        ret = volume_rollback_bh(&rootid, de->d_name);
                        if (unlikely(ret)) {
                                if (ret == ENOENT) {
                                        DWARN("rollback_bh %s not exists, maybe deleted.\n", de->d_name);
                                        ret = stor_rmvol(&rootid, de->d_name, 0);
                                        if (unlikely(ret)) {
                                                DERROR("rollback_bh_remove %s fail ret:%d\n", de->d_name, ret);
                                                continue;
                                        }
                                } else {
                                        DERROR("rollback_bh %s fail ret:%d\n", de->d_name, ret);
                                        continue;
                                }
                        }

                        success++;
                }

#if 0
                if (success) {
                        DINFO("rollback retry\n");
                        goto retry;
                }
#endif
        }
out:
        ret = timer1_settime(&rollback_bh_handler, USEC_PER_MIN);
        if (unlikely(ret)) {
                DERROR("settime error ret %d\n", ret);
                YASSERT(0);
        }
        md_listpool_close(&rootid, uuid);

        return 0;
err_close:
        md_listpool_close(&rootid, uuid);
err_ret:
        DERROR("rollback_bh file error ret (%d) %s\n", ret, strerror(ret));
        SWARN(0, "%s, rollback_bh file error ret (%d) %s\n", M_DATA_VOLUME_WARN, ret, strerror(ret));

        ret = timer1_settime(&rollback_bh_handler, USEC_PER_MIN);
        if (unlikely(ret)) {
                DERROR("settime error ret %d\n", ret);
                YASSERT(0);
        }

        return ret;
}

STATIC int __rollback_bh_worker(void *_args)
{
        (void) _args;
        return system_pool_iterator(__pool_rollback_bh_worker, NULL);
}

#if 0
int rollback_bh_start()
{
        int ret;

        DINFO("rollback_bh file service start\n");
        ret = timer1_create(&rollback_bh_handler, "rollback_bh_file", __rollback_bh_worker, NULL);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        ret = timer1_settime(&rollback_bh_handler, USEC_PER_MIN);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        return 0;
err_ret:
        return ret;
}

#endif
