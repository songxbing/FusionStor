/*
 *  Filename:lsv_volume_proto.h
 *  Description:
 *
 *  Created on: 2017年3月2日
 *  Author: Asdf(825674301)
 */

#ifndef __LSV_VOLUME_PROTO_H_
#define __LSV_VOLUME_PROTO_H_

#include "lsv.h"
#include "lsv_types.h"
#include "list.h"


#define lsv_serialize(base,len,type,para,vaule)\
	para = (type*) (base + len);\
	*para = vaule;\
	len += sizeof(type);
#define lsv_serialize_char(base,len,vaule,vlen)\
	memcpy(base+len,vaule,vlen);\
	len+=vlen;
#define lsv_deserialize(base,len,type,para,vaule)\
	para = (type*) (base + len);\
	vaule =*para ;\
	len += sizeof(type);
#define lsv_deserialize_char(base,len,vaule,vlen)\
	memcpy(vaule,base+len,vlen);\
	len+=vlen;


#endif /* __LSV_VOLUME_PROTO_H_ */
