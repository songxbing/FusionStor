#ifndef __LSV_CHUNK_POOL_H_
#define __LSV_CHUNK_POOL_H_

int lsv_chunk_pool_init(lsv_volume_proto_t *lsv_info, int create_new);

int lsv_chunk_pool_load(lsv_volume_proto_t *lsv_info);

int lsv_chunk_pool_destory(lsv_volume_proto_t *lsv_info);

int lsv_chunk_pool_malloc(lsv_volume_proto_t *lsv_info, lsv_u8_t type, lsv_u32_t *chunk_id);

int lsv_chunk_pool_malloc_nolock(lsv_volume_proto_t *lsv_info, lsv_u8_t type, lsv_u32_t *chunk_id);

int lsv_chunk_pool_free(lsv_volume_proto_t *lsv_info, lsv_u8_t type, lsv_u32_t chunk_id);

int lsv_chunk_pool_malloc_batch(lsv_volume_proto_t *lsv_info, lsv_u8_t type, lsv_u32_t count, lsv_u32_t *chunk_id);

int lsv_chunk_pool_malloc_batch_nolock(lsv_volume_proto_t *lsv_info, lsv_u8_t type, lsv_u32_t count, lsv_u32_t *chunk_id);

int lsv_chunk_pool_free_batch(lsv_volume_proto_t *lsv_info, lsv_u8_t type, lsv_u32_t count, lsv_u32_t *chunk_id);

#endif
