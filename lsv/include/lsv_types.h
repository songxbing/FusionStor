/*
 *  Filename:lsv_types.h
 *  Description:
 *
 *  Created on: 2017年3月2日
 *  Author: Asdf(825674301)
 */

#ifndef __LSV_TYPES_H_
#define __LSV_TYPES_H_

#include <stdint.h>
#include <errno.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <pthread.h>

#include "list.h"
#include "hash_table.h"
#include "lsv_sysy_lib.h"
#include "buffer.h"
#include "plock.h"

typedef int8_t lsv_s8_t;
typedef uint8_t lsv_u8_t;
typedef int16_t lsv_s16_t;
typedef uint16_t lsv_u16_t;
typedef int32_t lsv_s32_t;
typedef uint32_t lsv_u32_t;
typedef int64_t lsv_s64_t;
typedef uint64_t lsv_u64_t;

typedef lsv_u64_t lsv_ino_t;
typedef lsv_u64_t lba_t;

typedef plock_t lsv_lock_t;
typedef plock_t lsv_rwlock_t;

#define LSV_DISABLE_LOCK 0

#if !LSV_DISABLE_LOCK

#define lsv_lock_init(__lckp)     plock_init(__lckp, "lsv")
#define lsv_lock(__lckp)          plock_wrlock(__lckp)
#define lsv_unlock(__lckp)        plock_unlock(__lckp)
#define lsv_lock_destroy(__lckp)  plock_destroy(__lckp)

#define lsv_rwlock_init(__lckp)     plock_init(__lckp, "lsv")
#define lsv_rdlock(__lckp)          plock_rdlock(__lckp)
#define lsv_tryrdlock(__lckp)       plock_tryrdlock(__lckp)
#define lsv_wrlock(__lckp)          plock_wrlock(__lckp)
#define lsv_trywrlock(__lckp)       plock_trywrlock(__lckp)
#define lsv_rwunlock(__lckp)        plock_unlock(__lckp)
#define lsv_rwlock_destroy(__lckp)  plock_destroy(__lckp)

#else

#define lsv_lock_init(__lckp)
#define lsv_lock(__lckp)
#define lsv_unlock(__lckp)
#define lsv_lock_destroy(__lckp)

#define lsv_rwlock_init(__lckp)
#define lsv_rdlock(__lckp)
#define lsv_wrlock(__lckp)
#define lsv_rwunlock(__lckp)
#define lsv_rwlock_destroy(__lckp)

#endif

typedef struct list_head lsv_list_head_t;

#define LSV_CHUNK_NULL_ID (0)

typedef struct _lsv_thread {
        pthread_t thread_id;
        lsv_u32_t seqno; /* sequence number in a thread group */
        lsv_u32_t is_running; /* 1 shows the thread is running */
        lsv_u32_t is_working; /* 1 shows the thread is working on the request */
        lsv_u32_t to_shutdown; /* 1 shows the thread is going to be shutdown */
        void *work_info;
        sem_t startsem;
        sem_t stopsem;
} lsv_thread_t;

// page loc in low-level storage
typedef struct {
        uint32_t chunk_id;
        uint32_t chunk_off;  // [0, 254]
} lsv_page_loc_t;

#endif /* __LSV_TYPES_H_ */
