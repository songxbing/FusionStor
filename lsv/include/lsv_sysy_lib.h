/*
 *  Filename:lsv_sysy_lib.h
 *  Description:
 *
 *  Created on: 2017年3月27日
 *  Author: Asdf(825674301)
 */

#ifndef __LSV_SYSY_LIB_H_
#define __LSV_SYSY_LIB_H_


/*hash_table*/
extern hashtable_t hash_create_table(int (*compare_func)(const void *, const void *),
                                     uint32_t (*key_func)(const void *), const char *name);
extern void *hash_table_find(hashtable_t, const void *comparator);
extern int hash_table_insert(hashtable_t, void *value, void *comparator,
                             int overwrite);
extern int hash_table_remove(hashtable_t, const void *comparator, void **value);
extern int hash_iterate_table_entries(hashtable_t tab, int (*handler)(void *, void *),
                                       void *arg);
extern void hash_filter_table_entries(hashtable_t, int (*handler)(void *, void *),
                                      void *arg, void (*thunk)(void *));
extern void hash_destroy_table(hashtable_t, void (*thunk)(void *));



#endif /* __LSV_SYSY_LIB_H_ */
