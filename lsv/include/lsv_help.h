/*
 *  Filename:lsv_help.h
 *  Description:
 *
 *  Created on: 2017年3月2日
 *  Author: Asdf(825674301)
 */

#ifndef __LSV_DBG_H__
#define __LSV_DBG_H__

#include <stdio.h>
#include <dbg.h>

//#define NDEBUG
#include <assert.h>


#define DLOG_GC (1)//print log malloc/free info
#define DCHUNK_GC (1)//print chunk malloc/free info
#define DRW (1)//print chunk write/read info

#if 1
#define LSV_PRINT(fmt, args...)  \
	 D_MSG(D_INFO, "LSV "fmt, ##args);
//	 do {fprintf(stderr,"LSV %ld:%s:%d: "fmt,pthread_self(), __func__, __LINE__, ##args);fflush(stderr);}while(0);
#else
#define LSV_PRINT(fmt,args...)
#endif


// #define DFATAL(fmt,args...) LSV_PRINT(fmt,##args);
// #define DERROR(fmt,args...)  LSV_PRINT(fmt,##args);
// #define DWARN(fmt,args...) LSV_PRINT(fmt,##args);
// #define DBUG(fmt,args...)  LSV_PRINT(fmt,##args);
// #define DINFO(fmt,args...)   LSV_PRINT(fmt,##args);

#if DLOG_GC
#define DLOG_GC_INFO(fmt,args...)   LSV_PRINT(fmt,##args);
#else
#define DLOG_GC_INFO(fmt,args...)
#endif

#if DCHUNK_GC
#define DCHUNK_GC_INFO(fmt,args...)   LSV_PRINT(fmt,##args);
#else
#define DCHUNK_GC_INFO(fmt,args...)
#endif


#define LSV_TEST_TIME_BEGIN(name) \
        struct timeval lsv_##name##_time1, lsv_##name##_time2;\
        gettimeofday(&lsv_##name##_time1, NULL);

#define LSV_TEST_TIME_END(name) \
        gettimeofday(&lsv_##name##_time2, NULL);\
        DINFO("test_time_"#name" time %llu us\n", (LLU)_time_used(&lsv_##name##_time1, &lsv_##name##_time2));


#endif /* __LSV_DBG_H__ */
