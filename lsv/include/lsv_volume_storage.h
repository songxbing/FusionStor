/*
 *  Filename:lsv_volume_storage.h
 *  Description:
 *
 *  Created on: 2017年3月2日
 *  Author: Asdf(825674301)
 */

#ifndef __LSV_VOLUME_STORAGE_H_
#define __LSV_VOLUME_STORAGE_H_

#include "lsv_types.h"
#include "lsv_conf.h"
#include "lsv_volume_proto.h"


//fill ino and storage_info

/*
lsv_s32_t lsv_volume_storage_create(lsv_volume_proto_t *lsv_volume_proto);

lsv_s32_t lsv_volume_storage_open(lsv_volume_proto_t *lsv_volume_proto);

lsv_s32_t lsv_volume_storage_write(lsv_volume_proto_t *lsv_volume_proto,
		lsv_s8_t *buf, lsv_u8_t type, lsv_u64_t offset, lsv_u64_t size);

lsv_s32_t lsv_volume_storage_read(lsv_volume_proto_t *lsv_volume_proto,
		lsv_u64_t offset, lsv_u64_t size, lsv_s8_t *buf);

lsv_s32_t lsv_volume_storage_close(lsv_volume_proto_t *lsv_volume_proto);

lsv_s32_t lsv_volume_storage_delete(lsv_volume_proto_t *lsv_volume_proto);
*/
typedef struct _lsv_volume_storage_operations {
        int (*create)(lsv_volume_proto_t *lsv_volume_proto);
        int (*delete)(lsv_volume_proto_t *lsv_volume_proto);

        int (*open)(lsv_volume_proto_t *lsv_volume_proto);
        int (*close)(lsv_volume_proto_t *lsv_volume_proto);

        int (*write)(lsv_volume_proto_t *lsv_volume_proto, int8_t *buf,
                        uint8_t type, uint64_t offset, uint32_t size);
        int (*read)(lsv_volume_proto_t *lsv_volume_proto,
                        uint64_t offset, uint32_t size, int8_t *buf);
} lsv_volume_storage_operations;

#endif /* __LSV_VOLUME_STORAGE_H_ */
