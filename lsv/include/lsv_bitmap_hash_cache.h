#ifndef __HASH_CACHE__H_
#define __HASH_CACHE__H_

#include "atomic.h"

typedef struct {
        void *                          volume_context;
        hashtable_t                     ht;
        plock_t                         lock;
        plock_t                         list_lock;
        //pthread_spinlock_t              link_lock;
        atomic_t                        page_count;
        struct list_head                cache_list;
}lsv_hash_cache_context_t;

int lsv_hash_cache_init(lsv_hash_cache_context_t *hc, const char *name, void *volume_context) ;
void lsv_hash_cache_release(lsv_hash_cache_context_t *hc);
int lsv_hash_cache_get(lsv_hash_cache_context_t *hc, uint32_t chunk_id, uint32_t chunk_off, uint32_t length, void *data);
int lsv_hash_cache_update(lsv_hash_cache_context_t *hc, uint32_t chunk_id, uint32_t chunk_off, uint32_t length, void *data);
void lsv_hash_cache_destroy(lsv_hash_cache_context_t *hc);

#endif
