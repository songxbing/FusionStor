#ifndef __ROW3_VOLUME_PROTO_IO_H_
#define __ROW3_VOLUME_PROTO_IO_H_

int row3_volume_proto_write(volume_proto_t *volume_proto, const io_t *io, const buffer_t *buf);
int row3_volume_proto_read(volume_proto_t *volume_proto, const io_t *io, buffer_t *buf);

#endif