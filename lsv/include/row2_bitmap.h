#ifndef __ROW2_BITMAP_BITMAP_H_
#define __ROW2_BITMAP_BITMAP_H_

#ifndef private
#define private
#endif

typedef struct
{
        struct row2_bitmap_unit *bitmap;
        void *lock_ctx;
        private uint64_t offset;
        private uint32_t len;
} chunked_bitmap_accessor_t;

int row_bitmap_chunked_access(void *volume_context, uint64_t off, chunked_bitmap_accessor_t * accessor);
void row_access_end(chunked_bitmap_accessor_t * accessor);

#endif