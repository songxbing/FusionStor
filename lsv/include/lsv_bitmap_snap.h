#ifndef __LSV_BITMAP_BITMAP_SNAP_H_
#define __LSV_BITMAP_BITMAP_SNAP_H_


void lsv_bitmap_print_tree(void * volume_context);

int lsv_bitmap_list_snap(void * volume_context, const int fd);

int lsv_bitmap_create_snapshot(void * volume_context, struct lsv_bitmap_context *parent_node, const char *snap_name, struct lsv_bitmap_context **new_node);

int lsv_bitmap_revert_snapshot(void * volume_context, const char *snap_name);

int lsv_bitmap_delete_snapshot(void * volume_context, const char *snap_name);

int lsv_bitmap_protect_snapshot(void * volume_context, const char *snap, int protect);

int lsv_bitmap_read_snapshot_meta(void * volume_context, const char *snap, uint64_t off, uint32_t len, void * buffer);

uint32_t lsv_bitmap_snap_get_max_id(void * volume_context);

int lsv_bitmap_snap_full_clone(void *volume_context);

#endif
