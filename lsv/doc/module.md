# MODULE

## 项目组织

    {mod}/lsv_{mod}.h
    {mod}/lsv_{mod}_internal.h

    模块API层，考虑协程同步
    internal是内部实现，通常不考虑加锁等同步机制

## 协程的使用模式

- signal
- multicast
- order: one-by-one
