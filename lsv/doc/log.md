
#Log

log是lsv的日志层，负责数据以日志的方式写入、数据的读取、日志的回收。


##数据结构

在__lsv_volume_proto中的log_info上存储日志需要维护的信息。

typedef struct __lsv_volume_proto {

   ………………………………
   
   void* log_info; //filled in log init
    
   ………………………………   
   
} lsv_volume_proto_t;

###日志信息
log_info实际实现类型为lsv_log_info_t

typedef struct _lsv_log_info {

lsv_thread_t* thread;//读取写入数据的队列
    
lsv_hlog_wbuf_t *wbuf;//hlog缓存的write_buffer
    
lsv_bitmap_unit_t bitmap_unit_buf[LSV_CHUNK_SIZE / LSV_PAGE_SIZE];//批量更新bitmap时，缓存的数据，大小等于一个chunk中能存放的页数
    
lsv_u32_t hlog_count;//记录当前写入的hlog数目，暂时用于对hlog的编号

} lsv_log_info_t;

###hlog wbuff
hlog缓存的write_buffer的类型如下，chkid存储用于缓存的chunk_id，wbuf为在内存中的chunk，
head指向hlog的头，record指向hlog存储的数据

typedef struct _lsv_hlog_wbuf {
void* wbuf;

lsv_u32_t chkid;
    
lsv_hlog_head_t *head;
    
lsv_hlog_record_t *record;
    
} lsv_hlog_wbuf_t;

###hlog
hlog包括一个头和多个记录单元，结构如下。

hlog头中记录了该hlog的编号以及记录的数目，hlog头占用记录整数倍的大小

typedef struct _lsv_hlog_head {

lsv_u32_t id;
    
lsv_u32_t record_num;
    
} lsv_hlog_head_t;

hlog记录每个io的lba存储的chkid和chkoffset

typedef struct __lsv_hlog_record {

lsv_u64_t lba;

lsv_u32_t chkid;

lsv_u32_t chkoffset;

} lsv_hlog_record_t;



##Lsv_log.h 功能接口

* lsv_s32_t lsv_log_init(lsv_volume_proto_t *volume_proto);
    * 创建一个卷的日志
    * 在其中对log_info赋值
* lsv_s32_t lsv_log_destroy(lsv_volume_proto_t *volume_proto);
    * 删除一个卷的日志
    * 需要释放log_info
* lsv_s32_t lsv_log_write(lsv_volume_proto_t *volume_proto, lsv_s8_t *sbuf, lsv_s8_t *hbuf, lsv_u64_t hbuf_size);
    * 写入一条日志数据
    * sbuf写入的数据，大小为chunk大小
    * hbuf写入的io，类型为lsv_io_hlog_arg_t的数组
    * hbuf_size hbuf的数据大小
* lsv_s32_t lsv_log_slog_read(lsv_volume_proto_t *volume_proto, lsv_u32_t chunk_id, lsv_s8_t *buf);
    * 读取slog
* lsv_s32_t lsv_log_garbage_collection(lsv_volume_proto_t *volume_proto);
    * 回收日志 