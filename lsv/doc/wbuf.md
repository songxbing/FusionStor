# WBUF

## 配置

    lsv_conf.h: LSV_WBUF_CHUNK_NUM = 16

## 环形队列

    tail指定当前尾chunk，每个chunk的cflag指定状态，page_idx是下一个page的index[0-254]。
    page_idx == 255时，需要切换到下一chunk。若下一个chunk状态为full，需要等待。

    head，chunk_num非必需。

## IO操作

## wbuf2log

满1M，写入底层LOG。

## Flush操作

- 暂停当前ＩＯ
- 刷尾chunk，如果上面有数据(page_idx > 0)
- 等待所有chunk可用
