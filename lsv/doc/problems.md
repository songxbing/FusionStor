# LSV项目问题汇总

## 完成而没有充分测试的功能

- snapshot (create/delete/rollback/clone/list)
- log gc / fullgc
- read cache

## 已知问题

Write Buffer
- write buffer的索引结构

READ cache
- read cache有待优化

Bitmap
- 没有启用bitmap批量存取接口
- bitmap cache有待优化
- 跨卷访问性能

LOG & GC
- log gc代价过高，有无优化空间
- log fullgc策略有待调整

Volume allocator
- volume allocator的malloc/free操作开销过大

各种序的保持和信息
- log append order (重要，可以据此重建bitmap)
- volume malloc order
- log gc order
- log fullgc order

其他
- 分析工具（systemtap, valgrind)
- 数据一致性检查工具
- 代码风格
- 模块说明文档
- GCC警告信息

## 性能方面的要求和优化思路

性能方面的要求
- 精简配置对性能影响小，且表现平稳
- 快照对性能影响小，且表现平稳
- 顺序/随机IO，性能表现合乎预期
- 能充分发挥底层硬件的物理性能
- 提高并发处理能力，充分发挥并行硬件的特性

优化思路
- 减少IO，避免IO放大，需要持久化的数据要尽可能少
- 设计高效的数据结构和算法
- 批量操作
- 充分适应协程的编程模型，提高软件的并行处理能力

## 没有完成的功能

- 保护模式（卷没有加载成功时，禁止存取操作）
- 卷的逻辑大小和物理大小的区分
- 数据强一致性和校验机制
- snapshot (read/protect)
- 同步和异步远程复制
- 重删/压缩

## 一些设计的设计

volume空间管理
- 可以采用bitmap+freelist进行管理
- 按page设定类型，每个类型维持内存freelist

GC & FULLGC
- 分代GC
- valueup

fullgc调度策略
- timer
- gc_count

clone操作下导致的跨卷读取
- 本地卷ID到LICH卷ID的映射关系
