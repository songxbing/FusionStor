# 对接LICH

## 命令行

卷

    lichbd vol create pool1/vol1 --size 10M --format lsv -p iscsi

快照

    lichbd snap create pool1/vol1@snap1 -p iscsi
    lichbd snap ls     pool1/vol1       -p iscsi


## IO

在协议层以下改，对所有协议都有效。

write_buffer层接收的数据是(io_t, buffer_t)

iscsi/lich_io.c

    volume_ctl_write_direct
    stor_write

基本过程

    if vol_format == 'lsv':
        -> write_buffer (SSD)

    ...

    -> OLD (sync chunk and bitmap)
    else:
        -> OLD

## COROUTINE

COROUTINE API：
- core_attach
- core_request
- 
- schedule_task_new
- schedule_task_get
- schedule_task_yield
- schedule_task_resume

注意事项：
- 最大stack size=64K，注意递归调用, array
- lock-free，COROUTINE间用plock同步机制
- no block operations(such as sleep, io, sem_wait)

例子：
- lichbd_rpc.c:
- md_chunk.c
- pool_ctl.c
- volume_ctl.c
- disk_aio.c
