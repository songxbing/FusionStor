# 测试

## 项目简介

- 在原来卷格式的基础上，实现了日志结构(log structured volume, LSV)的卷格式
- 在LSV之上，实现了基于bitmap的快照树（数据ROW，元数据COW，读写皆可一次定位成功，代价是GC）
- 期望通过LSV项目，解决不同条件下的性能抖动，ROW快照树等问题

## 代码分支

- lsv_pipeline

## 命令行变更

创建卷
> lichbd vol create p1/v1 --size 100Gi -F lsv -p iscsi （增加-F格式选项，默认为raw）

快照命令保持不变

查看卷的基本信息，包括格式和clone相关信息
> lich.inspect --stat /iscsi/p1/v1

### lich log对性能有严重影响

设置日志等级：
> echo 5 > /dev/shm/lich4/msgctl/level

等级说明：
> 2: INFO
> 3: WARNING
> 4: ERROR
> 5: FATAL，略等同于关闭所有日志输出

## 测量指标

在日志里输出关键测量指标，方便观察

### 功能（测试用例）
- 功能完备，支持lich卷和快照的全部现有功能
- 无数据一致性问题（通过hazard测试）

### 性能（快照，GC等各种条件下表现平稳，少抖动，dd，fio）

基本要求：
- latency<=10ms
- iops与lich有可比性
- 性能抖动可控，长期运行趋于稳定，不能出现iops降为0的现象

几种操作下的性能：
- 默认精简配置，不需填充
- 创建快照
- 存在快照情况下的IO性能影响

GC：
- 空间利用率可控，无效数据/总数据比例可控
- gc不能破坏数据正确性，gc需要qos机制，不能严重干扰系统性能

系统启动时间：
- 20s以内（有点长，待定）

单卷加载时间：
- 10s以内（有点长，待定）

内存使用量：
- 节点级总量可控
- 卷级可伸缩，动态调整，有上限（明确各个模块的内存使用情况wbuf/bitmap/rcache/gc）

## 故障

## 实现

模块划分：
- wbuffer   (写缓冲)
- rcache    (读缓存)
- bitmap    (元数据)
- log       (用户数据)
- volume    (底层存储空间管理器)
- gc        (垃圾回收)

### GC

策略

指标

