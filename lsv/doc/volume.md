#Volume

volume是lsv的底层存储，负责数据chunk的存储、回收。

volume还提供了抽象的存储接口，可以支持文件存储、lich存储等多种存储方式。


##Lsv_volume_op.h 功能接口

* lsv_s32_t lsv_volume_create(lsv_volume_proto_t *volume_proto);
    * 创建一个卷的存储
* sv_s32_t lsv_volume_delete(lsv_volume_proto_t *volume_proto);
    * 删除一个的存储
* lsv_s32_t lsv_volume_write(lsv_volume_proto_t *volume_proto, lsv_s8_t *buf,lsv_u8_t type, lsv_u32_t *chunk_id);
    * append写入一个数据并返回存储的chunk_id
    * type可以指定存储的类型
* lsv_s32_t lsv_volume_malloc(lsv_volume_proto_t *volume_proto, lsv_u8_t type,lsv_u32_t *chunk_id);
    * 申请一个chunk
    * type可以指定存储的类型
* lsv_s32_t lsv_volume_update(lsv_volume_proto_t *volume_proto, lsv_u32_t chunk_id, lsv_s8_t *buf, lsv_u32_t offset, lsv_u32_t size);
    * 更新一个chunk的数据
    * offset是chunk中的偏移量
* lsv_s32_t lsv_volume_read(lsv_volume_proto_t *volume_proto, lsv_u32_t chunk_id, lsv_s8_t *buf);
    * 读取一个chunk的数据
* lsv_s32_t lsv_volume_free(lsv_volume_proto_t *volume_proto,lsv_u32_t chunk_id);
    * 释放一个chunk


##Lsv_volume_storage.h 底层存储抽象接口

底层存储可以是任意类型的存储介质，满足线性的存储结构，只要实现抽象接口，即可与lsv对接。

在include中定义接口，在src/volume中实现。lsv_volume_storage_file_impl是采用文件方式存储的样例。

在__lsv_volume_proto中的storage_info上可以存储底层存储需要维护的信息，例如：文件描述符、文件编号等。

typedef struct __lsv_volume_proto {

   ………………………………
   
   void* storage_info; //filled in storage create,must init NULL
    
   ………………………………   
   
} lsv_volume_proto_t;

* lsv_s32_t lsv_volume_storage_create(lsv_volume_proto_t *lsv_volume_proto);
    * 创建一个底层存储
    * 可以在其中对storage_info赋值
* lsv_u64_t lsv_volume_storage_write(lsv_volume_proto_t *lsv_volume_proto, lsv_s8_t *buf, lsv_u8_t type, lsv_u64_t offset, lsv_u64_t size);
    * 写入底层存储
    * offset是底层存储的偏移
    * type可以指定存储的类型    
* lsv_u64_t lsv_volume_storage_read(lsv_volume_proto_t *lsv_volume_proto, lsv_u64_t offset, lsv_u64_t size, lsv_s8_t *buf);
    * 读取底层存储
    * offset是底层存储的偏移
* lsv_s32_t lsv_volume_storage_delete(lsv_volume_proto_t *lsv_volume_proto);
    * 删除一个底层存储
    * 需要释放申请的storage_info空间