#include "lsv_rcache_v1.h"
#include "lsv_log.h"
#include "lsv_volume.h"
#include "volume_proto.h"
#include "list.h"

int lsv_rcache_page_search(lsv_volume_proto_t *volume_proto, lsv_u32_t chunk_id,
                lsv_s32_t chunk_offset,lsv_s32_t page_offset, lsv_s32_t size, buffer_t *append_buf){
        lsv_s32_t err = 0;
        lsv_s32_t i = 0;
        lsv_s32_t tail_idx = 0;
        lsv_s32_t chunk_num = 0;
        lsv_s32_t idx_start = 0;
        lsv_s32_t idx_end = 0;
        lsv_rcache_t *rcache = NULL;

        rcache = (lsv_rcache_t *)volume_proto->rcache;
        tail_idx = rcache -> tail;
        chunk_num = rcache -> chunk_num;

        if(chunk_num < LSV_RCACHE_CHUNK_NUM_MAX){
                idx_end = tail_idx;
        }else{
                idx_end = LSV_RCACHE_CHUNK_NUM_MAX;
        }

        for(i = idx_start; i < idx_end; i++){
                if(chunk_id == rcache->chunk_id[i]){
                        mbuffer_appendmem(append_buf,
                                        rcache->chunk_buf[i] + chunk_offset+page_offset, size);
                        return size;
                }
        }
 
	return 0;
}
