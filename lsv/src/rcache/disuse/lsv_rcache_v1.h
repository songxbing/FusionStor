/**
 * rcache.h provide read cache function
 * author:  weizheng
 * date:    201703034
 * */

#ifndef __LSV_RCACHE__
#define __LSV_RCACHE__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"
#include "lsv_volume_proto.h"
#include "lsv_types.h"
#include "lsv_conf.h"
#include "lsv_help.h"

struct __lsv_rcache {
	lsv_u64_t *chunk_id;
	lsv_s8_t **chunk_buf;
	lsv_s32_t chunk_num;
	lsv_s32_t total_chunk;
	lsv_s32_t head;
	lsv_s32_t tail;
	lsv_rwlock_t rwlock;
};
typedef struct __lsv_rcache lsv_rcache_t;


// lsv_rcache.c

int lsv_rcache_init(lsv_volume_proto_t * volume_proto);
int lsv_rcache_release(lsv_volume_proto_t * volume_proto);

// before bitmap
int lsv_rcache_page_lookup(lsv_volume_proto_t *volume_proto, lsv_u64_t lba, lsv_s32_t size, buffer_t *append_buf);

// after bitmap
int lsv_rcache_lookup(lsv_volume_proto_t *volume_proto, lsv_u32_t chunk_id,
		lsv_s32_t chunk_offset, lsv_u64_t lba, lsv_s32_t size,
		const raw_io_t *rio,
		buffer_t *append_buf);

// for bitmap valueup
int lsv_rcache_page_clean(lsv_volume_proto_t * volume_proto, lsv_u32_t chunk_id, lsv_s32_t chunk_off);

// for gc
int lsv_rcache_chunk_clean(lsv_volume_proto_t * volume_proto, lsv_u32_t chunk_id);

int lsv_rcache_clean_all(lsv_volume_proto_t *volume_proto);

/*chunk_id == LSV_CHUNK_NULL, clean all chunks*/
int lsv_rcache_clean(lsv_volume_proto_t *volume_proto, lsv_u32_t chunk_id);


#endif
