#include "lsv_rcache_v1.h"
#include "lsv_log.h"
#include "lsv_volume.h"
#include "list.h"

int lsv_rcache_load_chunk(lsv_volume_proto_t *volume_proto, lsv_u32_t chunk_id,
                lsv_s32_t chunk_offset,lsv_s32_t page_offset, lsv_s32_t size, buffer_t *append_buf){
        lsv_s32_t err = 0;
        lsv_s32_t i = 0;
        lsv_s32_t tail_idx = 0;
        lsv_s32_t chunk_num = 0;
        lsv_s32_t idx_start = 0;
        lsv_s32_t idx_end = 0;
        lsv_rcache_t *rcache = NULL;

	rcache = (lsv_rcache_t *)volume_proto->rcache;
        tail_idx = rcache -> tail;
        chunk_num = rcache -> chunk_num;

	rcache->chunk_id[tail_idx] = 0;
	err = lsv_log_read(volume_proto, chunk_id, rcache->chunk_buf[tail_idx]);
        if(err){
                DERROR("call lsv_log_slog_read read chunk_id[%d] error:%d\n", tail_idx, err);
                return err;
        }

	mbuffer_appendmem(append_buf,
                        rcache->chunk_buf[tail_idx] +  chunk_offset+page_offset, size);

	rcache->chunk_id[tail_idx] = chunk_id;
        if(chunk_num < LSV_RCACHE_CHUNK_NUM_MAX){
                rcache->chunk_num ++;
        }
	YASSERT(rcache->chunk_num <= LSV_RCACHE_CHUNK_NUM_MAX);
        rcache->tail = ++tail_idx  % LSV_RCACHE_CHUNK_NUM_MAX;

	return size;
}
