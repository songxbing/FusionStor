/**
 * rcache.h provide read cache function
 * author:  weizheng
 * date:    201703034
 * */

#ifndef __LSV_RCACHE__
#define __LSV_RCACHE__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"
#include "lsv_volume_proto.h"
#include "lsv_types.h"
#include "lsv_conf.h"
#include "lsv_help.h"

struct __lsv_rcache {
	lsv_u64_t *chunk_id;
	lsv_s8_t **chunk_buf;
	lsv_s32_t chunk_num;
	lsv_s32_t total_chunk;
	lsv_s32_t head;
	lsv_s32_t tail;
	lsv_rwlock_t rwlock;
};
typedef struct __lsv_rcache lsv_rcache_t;

/*init read cache*/
int lsv_rcache_init_v2(lsv_volume_proto_t * volume_proto);

/**/
int lsv_rcache_insert_v2(lsv_volume_proto_t *volume_proto, lsv_u32_t chunk_id,
		lsv_s8_t *buf);

/**
 *
 * @param volume_proto
 * @param lba
 * @param chunk_id
 * @param chunk_offset
 * @param page_offset
 * @param size
 * @param append_buf
 * @return
 */
int lsv_rcache_lookup_v2(lsv_volume_proto_t *volume_proto, uint64_t lba,
		      lsv_u32_t chunk_id, lsv_s32_t chunk_offset, lsv_s32_t page_offset, lsv_s32_t size,
		      buffer_t *append_buf);

/*chunk_id == LSV_CHUNK_NULL, clean all chunks*/
int lsv_rcache_clean_v2(lsv_volume_proto_t *volume_proto, lsv_u32_t chunk_id);

/**/
int lsv_rcache_release_v2(lsv_volume_proto_t * volume_proto);

#endif
