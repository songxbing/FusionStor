/* 
 *  *  Written by Xing Jing */ 
/*
 * $Id: lsv.c $
 */

//#define FUSE_USE_VERSION 26
#define FUSE_USE_VERSION 30

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef linux
/* For pread()/pwrite() */
#define _XOPEN_SOURCE 500
#endif

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>
#ifdef HAVE_SETXATTR
#include <sys/xattr.h>
#endif

#include <fuse.h>

#include "list.h"

#include "lsv_types.h"
#include "lsv_op.h"
#include "lsv_help.h"
#include "lsv_utils.h"


int lsv_log_file_len;
pthread_mutex_t lsv_debug_lock;

lsv_u32_t client_this_id;

struct list_head lsv_volume_proto_head;


int main(int argc, char *argv[])
{
    int rc = 0;
    umask(0);

    pthread_mutex_init(&lsv_debug_lock, NULL);

    client_this_id = 1;//atoi(argv[argc - 1]);

    DINFO("main:client_id:%d\n", client_this_id);

    __lsv_daemonize(client_this_id);

    DINFO("main:enter\n");

    lsv_log_file_len = 0;

	INIT_LIST_HEAD(&lsv_volume_proto_head);

    rc = fuse_main(argc, argv, &lsv_oper, NULL);

	rc = lsv_finalize();

    DINFO("main:stop to say goodbye\n");
    
    return 0;
}

/*This is end of main.c*/
