#include "config.h"

#define DBG_SUBSYS S_LIBSTORAGE

#include "lsv_lib.h"
#include "lsv_conf.h"
#include "dbg.h"

extern int srv_running;

inline int lsv_page_num(uint64_t off, uint32_t size) {
        YASSERT(size > 0);

        int page_num = 0;

        uint32_t size2 = size;
        if (off % LSV_PAGE_SIZE != 0) {
                size2 += off % LSV_PAGE_SIZE;
        }

        if ((off + size) % LSV_PAGE_SIZE != 0) {
                size2 += LSV_PAGE_SIZE - (off + size) % LSV_PAGE_SIZE;
        }

        page_num = size2 / LSV_PAGE_SIZE;
        YASSERT(size2 % LSV_PAGE_SIZE == 0 && page_num <= 1024);

        return page_num;
}
