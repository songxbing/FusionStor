#ifndef __LSV_LNET__
#define __LSV_LNET__
#include "lsv_volume_proto.h"
#include "lsv_conf.h"



typedef struct __lsv_remote_op{
	lsv_s32_t op_type;
	lsv_s8_t  path[255];
	union {
		size_t size;
		mode_t mode;
		struct __id{
			uid_t uid;
			gid_t gid;
		}id;
	}u;
}lsv_remote_op;

lsv_s32_t lnet_init(lsv_volume_proto_t *volume_proto);
lsv_s32_t lnet_send_sync(lsv_volume_proto_t *volume_proto, lsv_s8_t *buf, lsv_s32_t size);
lsv_s32_t lnet_receive_process_loop(lsv_volume_proto_t *volume_proto);
lsv_s32_t lnet_destroy(lsv_volume_proto_t *volume_proto);
#endif
