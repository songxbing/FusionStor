#include "config.h"

#include <stdbool.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <list.h>

#define DBG_SUBSYS S_LIBSTORAGE

#include "dbg.h"

#include "token_bucket.h"
#include "list.h"

#include "lsv.h"
#include "lsv_conf.h"
#include "lsv_wbuffer.h"
#include "lsv_wbuffer_internal.h"

/** @file note
 *
 * - 任务要及时退出，避免lock或wait过多
 */


static inline int _pipeline_is_first_entry(pipeline_t *pipeline, pipeline_chunk_t *entry) {
        return &entry->hook == pipeline->list.list.next;
}

static inline void _print_pipeline_chunk(const char *stage, pipeline_chunk_t *entry) {
        DINFO("[%s] pipeline chunk %p status %d\n", stage, entry, entry->status);
}

static void _print_pipeline(pipeline_t *pipeline, const char *name, int total, int count) {
        uint32_t ca[PIPELINE_CHUNK_MAX];

        for (int i=0; i < PIPELINE_CHUNK_MAX; i++) {
                ca[i] = 0;
        }

        struct list_head *pos, *n;
        pipeline_chunk_t *entry;
        list_for_each_safe(pos, n, &pipeline->list.list) {
                entry = list_entry(pos, pipeline_chunk_t, hook);
                ca[entry->status] += 1;
        }

        DINFO("<- stage %s count %d/%d left %u (%u %u %u %u %u)\n",
               name,
               count,
               total,
               pipeline->list.count,
               ca[0], ca[1], ca[2], ca[3], ca[4]);
}

static int __check_log_chunk_rule(uint32_t chunk_id) {
        // TODO
        static uint32_t last_chunk_id = 0;

        if (last_chunk_id != 0) {
                if (last_chunk_id == WBUF_CHUNK_NUM) {
                        YASSERT(chunk_id == 1);
                } else {
                        YASSERT(chunk_id == last_chunk_id + 1);
                }
        }

        last_chunk_id = chunk_id;

        return 0;
}

static inline int __check_bitmap_chunk_rule(uint32_t chunk_id) {
        // TODO
        static uint32_t last_chunk_id = 0;

        if (last_chunk_id != 0) {
                if (last_chunk_id == WBUF_CHUNK_NUM) {
                        YASSERT(chunk_id == 1);
                } else {
                        YASSERT(chunk_id == last_chunk_id + 1);
                }
        }

        last_chunk_id = chunk_id;

        return 0;
}

/** 有可重入性问题，故引入本地队列，同时为保证bitmap持久化任务的顺序，需控制任务de唯一性.
 *
 * @param arg pipeline
 */
static void _do_stage_bitmap(void *arg) {
        int ret;
        pipeline_t *pipeline = arg;

        if (pipeline->stage_bitmap_running) {
                DINFO("stage bitmapt running %u, quit\n", pipeline->list.count)
                return;
        }

        struct list_head *pos, *n;
        pipeline_chunk_t *entry;
        int post_log_count = 0;

        // 判断有无可以执行的
        list_for_each_safe(pos, n, &pipeline->list.list) {
                entry = list_entry(pos, pipeline_chunk_t, hook);
                if (entry->status == PIPELINE_CHUNK_POST_LOG) {
                        post_log_count ++;
                } else {
                        break;
                }
        }

        if (post_log_count == 0)
                return;

        // 保证只有一个task进入该区域
        // 应尽快处理bitmap，且最好聚合更多的bitmap，两者达到一平衡态
        pipeline->stage_bitmap_running = TRUE;

        count_list_t local_queue;
        count_list_init(&local_queue);

        // shared queue, no yield
        int count = 0;
        list_for_each_safe(pos, n, &pipeline->list.list) {
                entry = list_entry(pos, pipeline_chunk_t, hook);
                if (entry->status == PIPELINE_CHUNK_POST_LOG) {
                        count_list_del_init(pos, &pipeline->list);
                        count_list_add_tail(pos, &local_queue);

                        count += 1;
                        if (count >= PIPELINE_BITMAP_MAX) {
                                break;
                        }
                } else {
                        break;
                }
        }

        YASSERT(local_queue.count > 0);

        // local queue, yield, but no shared status
        DINFO("-> pipeline %p count %u\n", pipeline, local_queue.count);
        int total = local_queue.count;
        count = 0;

        time_count_update(&pipeline->lsv_info->tc, COUNT_BITMAP_IN, local_queue.count);

        // TODO maybe too many items, scheduler timeout
        list_for_each_safe(pos, n, &local_queue.list) {
                entry = list_entry(pos, pipeline_chunk_t, hook);
                if (entry->status == PIPELINE_CHUNK_POST_LOG) {
                        // can bitmap, sort and split
                        entry->status = PIPELINE_CHUNK_PRE_BITMAP;

                        pipeline->lsv_info->commit_bytes += LSV_PAGE_SIZE * (LSV_WBUF_PAGE_NUM - 1);
                        count ++;

                        __check_bitmap_chunk_rule(entry->chunk->id);

                        DINFO("%u/%u stage bitmap %p status %d chunk_id %u chunk %d page_idx %u\n",
                              count, total, entry,
                              entry->status,
                              entry->chunk_id,
                              entry->chunk->id,
                              entry->chunk->page_idx);

                        if (lsv_feature & LSV_FEATURE_BITMAP) {
                                // TODO high priority
                                ret = update_bitmap(pipeline->lsv_info, entry->chunk_id, &entry->log_proto);
                                if (unlikely(ret)) {
                                        YASSERT(0);
                                }
                        }

                        YASSERT(entry->chunk->page_idx == entry->chunk->fill_count);

                        chunk_post_commit(pipeline->lsv_info, entry->chunk, 0);

                        // TODO GC
                        ret = lsv_log_write_gc(pipeline->lsv_info, &entry->log_proto);
                        if (unlikely(ret))
                                YASSERT(0);

                        entry->status = PIPELINE_CHUNK_POST_BITMAP;

                        co_cond_broadcast(&pipeline->too_many_log, ret);

                        // OK
                        count_list_del_init(pos, &local_queue);
                        yfree((void **)&entry);

                        // stat
                        lsv_wbuf_t *wbuf = pipeline->lsv_info->wbuf;
                        lsv_wbuf_qos_update(&wbuf->qos, 0, 255);

                        time_count_update(&pipeline->lsv_info->tc, COUNT_BITMAP_OUT, 1);
                } else {
                        YASSERT(0);
                }
        }

        pipeline->stage_bitmap_running = FALSE;

        _print_pipeline(pipeline, "bitmap", total, count);
        // DERROR("<- stage bitmap count %d/%d left %u\n", count, total, pipeline->list.count);

        // 自我驱动
        if (!list_empty_careful(&pipeline->list.list)) {
                schedule_task_new("stage_bitmap", _do_stage_bitmap, pipeline, -1);
        }
}

/**
 * @note log阶段，不能改变pipeline的元素顺序和多少
 *
 * @param arg
 */
static void _do_stage_log(void *arg) {
        int ret;
        pipeline_t *pipeline = arg;

        struct list_head *pos, *n;
        pipeline_chunk_t *entry;

        int init_count = 0;
        int post_log_count = 0;
        while (TRUE) {
                init_count = 0;
                post_log_count = 0;
                list_for_each_safe(pos, n, &pipeline->list.list) {
                        entry = list_entry(pos, pipeline_chunk_t, hook);
                        // TODO soft lockup
                        if (entry->status == PIPELINE_CHUNK_INIT && chunk_is_fill(entry->chunk)) {
                                init_count++;
                        } else if (entry->status == PIPELINE_CHUNK_PRE_LOG) {
                                // 限制log数，包括已提交和完成两部分
                                post_log_count++;
                        } else if (entry->status == PIPELINE_CHUNK_POST_LOG) {
                                post_log_count++;
                        } else {
                                // break;
                        }
                }

                if (init_count == 0)
                        return;

                // TODO post_log_count
                if (post_log_count >= PIPELINE_LOG_MAX) {
                        // 只有第一个任务wait，别的直接返回，防止占用task
                        if (pipeline->is_too_many_log) {
                                return;
                        }

                        pipeline->is_too_many_log = TRUE;
                        co_cond_wait(&pipeline->too_many_log);
                        pipeline->is_too_many_log = FALSE;
                        continue;
                } else {
                        break;
                }
        }

        YASSERT(post_log_count <= PIPELINE_LOG_MAX);

        // 假设有多个task涌入至此，且不违背以上条件
        // 因为后面有yield，且状态为PIPELINE_CHUNK_PRE_LOG

        count_list_t local_queue;
        count_list_init(&local_queue);

        // TODO first n?
        int count = 0;
        list_for_each_safe(pos, n, &pipeline->list.list) {
                entry = list_entry(pos, pipeline_chunk_t, hook);
                // 已填充完成，才能进入log阶段
                if (entry->status == PIPELINE_CHUNK_INIT && chunk_is_fill(entry->chunk)) {
                        // TODO entry in two or multi lists?
                        count_list_add_tail(&entry->local_hook, &local_queue);
                        entry->status = PIPELINE_CHUNK_PRE_LOG;
                        count++;
                        if (count >= PIPELINE_LOG_MAX)
                                break;
                } else {
                        // break;
                }
        }

        uint32_t chunk_id;

        // TODO maybe too many items, scheduler timeout
        int total = local_queue.count;
        count = 0;
        list_for_each_safe(pos, n, &local_queue.list) {
                entry = list_entry(pos, pipeline_chunk_t, local_hook);
                if (entry->status == PIPELINE_CHUNK_PRE_LOG) {
                        pipeline->lsv_info->log_write_count++;
                        pipeline->lsv_info->log_bytes += LSV_PAGE_SIZE * (LSV_WBUF_PAGE_NUM - 1);

                        count ++;
                        DINFO("%u/%u stage log %p status %d\n", count, total, entry, entry->status);

                        ret = lsv_volume_chunk_write(entry->pipeline->lsv_info,
                                                     entry->log_proto.log,
                                                     LSV_LOG_LOG_STORAGE_TYPE,
                                                     &chunk_id);
                        if (unlikely(ret)) {
                                YASSERT(0);
                        }

                        entry->chunk_id = chunk_id;
                        entry->log_proto.chunk_id = chunk_id;
                        entry->status = PIPELINE_CHUNK_POST_LOG;

                        time_count_update(&pipeline->lsv_info->tc, COUNT_LOG_OUT, 1);

                        schedule_task_new("stage_bitmap", _do_stage_bitmap, pipeline, -1);
                }
        }

        _print_pipeline(pipeline, "log", total, count);
        // DERROR("<- stage log count %d/%d left %u\n", count, total, pipeline->list.count);

        // TODO when check fill, deadlock
        if (!list_empty_careful(&pipeline->list.list)) {
                schedule_task_new("stage_log", _do_stage_log, pipeline, -1);
        }
}

int pipeline_init(pipeline_t *pipeline, lsv_volume_proto_t *lsv_info) {
        count_list_init(&pipeline->list);
        pipeline->lsv_info = lsv_info;
        pipeline->stage_bitmap_running = FALSE;
        pipeline->is_too_many_log = FALSE;
        co_cond_init(&pipeline->too_many_log);

        return 0;
}

int pipeline_add_tail(pipeline_t *pipeline, lsv_chunk_buf_t *chunk) {
        int ret;
        pipeline_chunk_t *entry;

        time_count_update(&pipeline->lsv_info->tc, COUNT_LOG_IN, 1);

        ret = ymalloc((void **)&entry, sizeof(pipeline_chunk_t));
        if (unlikely(ret)) {
                YASSERT(0);
        }

        entry->pipeline = pipeline;
        entry->chunk = chunk;

        entry->log_proto.log = chunk;
        lsv_log_proto_link(&entry->log_proto);

        entry->status = PIPELINE_CHUNK_INIT;
        entry->chunk_id = LSV_CHUNK_NULL_ID;

        YASSERT(entry->log_proto.head->page_count<=LSV_LOG_PAGE_NUM);

        // 进入队列，保证顺序
        count_list_add_tail(&entry->hook, &pipeline->list);

        // TODO
        schedule_task_new("stage_log", _do_stage_log, pipeline, -1);

        DINFO("chunk %d page_count %u entry %p depth %u\n", chunk->id,
              entry->log_proto.head->page_count,
              entry,
              pipeline->list.count);
        __check_log_chunk_rule(chunk->id);

        return 0;
}

inline int pipeline_do_log(pipeline_t *pipeline) {
        if (!list_empty_careful(&pipeline->list.list)) {
                schedule_task_new("stage_log", _do_stage_log, pipeline, -1);
        }

        return 0;
}

int pipeline_commit(pipeline_t *pipeline) {
        return 0;
}
