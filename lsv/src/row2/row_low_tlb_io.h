

int row_test_volume_init(volume_proto_t *volume_proto, int create_new);
int row_test_volume_write(volume_proto_t *volume_proto, const io_t *io, const buffer_t *buf);
int row_test_volume_read(volume_proto_t *volume_proto, const io_t *io, const buffer_t *buf);
int row_test_volume_write_align(volume_proto_t *volume_proto, uint64_t offset, uint32_t length, uint8_t *buffer);
int row_test_volume_read_align(volume_proto_t *volume_proto, uint64_t offset, uint32_t length, uint8_t *buffer);