
int lsv_low_storage_lich_write(lsv_volume_proto_t *lsv_info, int8_t *_buf, lsv_u8_t type, lsv_u64_t offset, lsv_u32_t size);
int lsv_low_storage_lich_read(lsv_volume_proto_t *lsv_info, uint64_t offset, uint32_t size, int8_t *_buf);
