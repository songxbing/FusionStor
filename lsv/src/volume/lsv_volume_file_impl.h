/*
 *  Filename:lsv_volume_storage_file_impl.h
 *  Description:
 *
 *  Created on: 2017年3月2日
 *  Author: Asdf(825674301)
 */

#ifndef __LSV_VOLUME_STORAGE_FILE_IMPL_H_
#define __LSV_VOLUME_STORAGE_FILE_IMPL_H_

#include "lsv_volume_storage.h"

typedef struct __lsv_volume_storage_file_info {
	lsv_u32_t storfile_fd;
	lsv_u32_t storfile_id;
} lsv_volume_storage_file_info_t;

#endif /* __LSV_VOLUME_STORAGE_FILE_IMPL_H_ */
