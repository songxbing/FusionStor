/*
 *  Filename:lsv_log_thread.c
 *  Description:
 *
 *  Created on: 2017年3月4日
 *  Author: Asdf(825674301)
 */

#include "lsv_log.h"
#include "lsv_gc_internal.h"
#include "lsv_help.h"

void* __lsv_log_gc_polling_thread_work(void* arg) {
	lsv_thread_t *thread = (lsv_thread_t *) arg;
	lsv_volume_proto_t *volume_proto = (lsv_volume_proto_t *) thread->work_info;
	lsv_s32_t work_rc = 0;
	lsv_u32_t sleep_second = 0;

	thread->is_running = 1;
	DINFO("create log_gc_polling thread: %u-%u\n", volume_proto->ino,
			thread->seqno);
	sem_post(&thread->startsem);

	while (1) {

		sleep_second = 0;
		while (sleep_second < LSV_LGOGC_GC_POLLING_CYCLE) {
			sleep(1);
			if (thread->to_shutdown) {
				goto EXIT;
			}
			sleep_second++;
		}

		thread->is_working = 1;
		work_rc = 0;
		DINFO("log_gc_polling thread begin:%u-%u\n", volume_proto->ino,
				thread->seqno);

		work_rc = lsv_log_fullgc(volume_proto);
		if (work_rc < 0) {
			DERROR("log_gc_polling thread %u-%u fullgc err, err: %d\n",
					volume_proto->ino, thread->seqno, work_rc);
		}

		DINFO("log_gc_polling thread end:%u-%u\n", volume_proto->ino,
				thread->seqno);
		thread->is_working = 0;

		if (thread->to_shutdown) {
			goto EXIT;
		}
	}

EXIT:
        thread->is_working = 0;
        thread->is_running = 0;
        sem_post(&thread->stopsem);
        DINFO("log_gc_polling thread leave:%u-%u\n", volume_proto->ino,
              thread->seqno);
        return NULL;
}


