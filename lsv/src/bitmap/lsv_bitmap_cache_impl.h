#ifndef __BMAP_CACHE__
#define __BMAP_CACHE__

#include "stdint.h"
#include "sysy_conf.h"
#include "lsv_types.h"
#include "lsv_bitmap.h"

typedef struct __bmap_cache *bm_cache_t;

typedef struct {
        int (*init)(bm_cache_t *cache, const char *name, void *volume_context);
        int (*deinit)(bm_cache_t cache);

        int (*get)(bm_cache_t cache, uint32_t bm_chkid, uint32_t bm_chunk_off, uint32_t length, int allocate, lsv_bitmap_unit_t *bm_buf);
        int (*release)(bm_cache_t cache, uint32_t bm_chkid, uint32_t bm_chunk_off, uint32_t length);

        int (*update)(bm_cache_t cache, uint32_t chunk_id, uint32_t chunk_off, uint32_t length, void *in);
} bm_cache_api_t;

extern bm_cache_api_t *cache_api;

#endif
