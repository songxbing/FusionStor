/*
 *  Filename:lsv_gc_stat.c
 *  Description:
 *
 *  Created on: 2017年3月17日
 *  Author: Asdf(825674301)
 */

#include <time.h>

#include "volume_proto.h"

#include "lsv.h"
#include "lsv_log.h"
#include "lsv_gc_internal.h"

void lsv_gc_stat_init(lsv_gc_context_t* gc_context) {
        gc_context->stat_info.real_pages = 0;
        gc_context->stat_info.storage_pages = 0;
        gc_context->stat_info.all_free_count = 0;
        gc_context->stat_info.merge_free_count = 0;
        gc_context->stat_info.merge_count = 0;
        gc_context->stat_info.task_count = 0;
        gc_context->stat_info.task_check_count = 0;
        gc_context->stat_info.task_heap_insert_count = 0;
        gc_context->stat_info.task_heap_merge_count = 0;

        gc_context->stat_info.task_heap_merge_merge_count = 0;
        gc_context->stat_info.task_heap_merge_write_count = 0;
        gc_context->stat_info.task_heap_merge_update_count = 0;
        gc_context->stat_info.task_heap_merge_free_count = 0;
}

void lsv_gc_stat(lsv_volume_proto_t *lsv_info) {
        lsv_log_info_t *log_info = NULL;
        lsv_gc_context_t *gc_context = NULL;

        volume_proto_t *lich_volume_proto = NULL;
        lsv_gc_old_storage_t *old_storage = NULL;
        lsv_u32_t storage_chunk_num, vol_chunk_num;

        lich_volume_proto = lsv_info->volume_proto;
        log_info = lsv_info->log_info;
        gc_context = log_info->gc_context;
        old_storage = &gc_context->old_storage;

        storage_chunk_num = old_storage->ready_chunk.count
                            + old_storage->chunk_count * LSV_GC_OS_CHUNK_NUM
                            + gc_context->logctrl_tab->num_of_entries;
        vol_chunk_num = lich_volume_proto->table1.fileinfo.logical_size / LSV_CHUNK_SIZE;

        DINFO("chunk <%u,%u,%.2lf> volume <%llu,%llu,%.2lf> cq %u total %u idx %u cq %u heap %u ready %u os %u\n",
              vol_chunk_num,
              storage_chunk_num,
              vol_chunk_num / (double )storage_chunk_num,
              (LLU)lich_volume_proto->table1.fileinfo.logical_size,
              (LLU)lich_volume_proto->table1.fileinfo.size,
              lich_volume_proto->table1.fileinfo.logical_size / (double )lich_volume_proto->table1.fileinfo.size,
              gc_context->check_queue_size,
              gc_context->total_chunk_num,
              gc_context->logctrl_tab->num_of_entries,
              gc_context->check_queue.count,
              gc_context->gc_heap.count,
              gc_context->old_storage.ready_chunk.count,
              gc_context->old_storage.chunk_count);
}
