/*
 *  Filename:lsv_log_page_bitmap.h
 *  Description:
 *
 *  Created on: 2017年3月18日
 *  Author: Asdf(825674301)
 */

#ifndef __LSV_LOG_PAGE_BITMAP_H_
#define __LSV_LOG_PAGE_BITMAP_H_

#include "lsv_conf.h"
#include "lsv_types.h"

#define LSV_LOG_PAGE_BITMAP_SIZE (LSV_CHUNK_SIZE/LSV_PAGE_SIZE)

/**
 * @sa ylib: bmap_t
 */
typedef struct _lsv_log_page_bitmap {
        // 可回收的非活动页数
        lsv_u16_t inactive_page_count;
        lsv_u8_t bitmap[LSV_LOG_PAGE_BITMAP_SIZE / 8];
} lsv_log_page_bitmap_t;

#define lp_bitmap_init(lg_bm) \
        do{ \
                (lg_bm)->inactive_page_count=0; \
                memset((lg_bm)->bitmap,0,LSV_LOG_PAGE_BITMAP_SIZE/8); \
        }while(0)

/**
 * 设置为活动页
 */
#define lp_bitmap_set_active(lg_bm,idx) \
        do { \
                (lg_bm)->bitmap[(idx) >> 3] &= (lsv_u8_t) (~(1 << ((lsv_u8_t)(idx) & (lsv_u8_t) 7))); \
                (lg_bm)->inactive_page_count--; \
        }while(0)

/**
 * 设置为不活动页
 */
#define lp_bitmap_set_inactive(lg_bm,idx) \
        do{ \
                (lg_bm)->bitmap[(idx) >> 3] |= (lsv_u8_t) (1 << ((lsv_u8_t)(idx) & (lsv_u8_t) 7)); \
                (lg_bm)->inactive_page_count++; \
        } while(0)

/**
 *
 * @return 0 is active page
 * @return 1 is inactive page
 */
#define lp_bitmap_get(lg_bm,idx) \
        ((lg_bm)->bitmap[(idx) >> 3] & (lsv_u8_t) (1 << ((lsv_u8_t)(idx) & (lsv_u8_t) 7)))

#endif /* __LSV_LOG_PAGE_BITMAP_H_ */
