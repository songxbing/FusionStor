/**
 * remm_thread.h 
 * funtion:    1: accept conn
 * 	       2: process conn
 * 	       3: return value for client
 * author:     weizheng
 * date:       20170304
 * */
#ifndef __LNET_THREAD__
#define __LNET_THREAD__

#include "remm.h"

void* remm_process_thread_work(void* arg);
lsv_s32_t remm_thread_init(lsv_volume_proto_t *volume_proto);
lsv_s32_t remm_sync_thread_init(lsv_volume_proto_t *volume_proto);
lsv_s32_t remm_sync_thread_destroy(lsv_volume_proto_t * volume_proto);
lsv_s32_t remm_thread_destroy(lsv_volume_proto_t *volume_proto);
#endif
