#ifndef __LSV_LNET_ASYNC__
#define __LSV_LNET_ASYNC__
#include "list.h"
#include "lsv_conf.h"
#include "lsv_types.h"
#include "lsv.h"
#include "remm.h"
#include "volume_proto.h"
typedef struct __lsv_remm_block{
	struct list_head list;
	lsv_s8_t *chunk_buf;
	lsv_u32_t chunk_id;
	lsv_s32_t iterm_num;
	lsv_s32_t iterm_cur;
	lsv_s8_t  dirty_bitmap[LSV_CHUNK_SIZE/LSV_PAGE_SIZE/8];
}lsv_remm_block;

lsv_s32_t remm_truncate_send_async(lsv_volume_proto_t *volume_proto, lsv_s32_t size);
lsv_remm_block * remm_iterm_block_malloc(lsv_volume_proto_t *volume_proto);
lsv_s32_t remm_data_send_async(lsv_volume_proto_t *volume_proto, lsv_s8_t * buf, lsv_u64_t lba, lsv_s32_t size);

#endif
