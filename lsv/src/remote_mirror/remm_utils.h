#ifndef __LSV_LNET_UTILS__
#define __LSV_LNET_UTILS__

#include <libconfig.h>
#include <stdio.h>
#include <stdlib.h>
#include "lsv_volume_proto.h"
#include "lsv_conf.h"
#include "remm.h"

lsv_s32_t remm_conf_load(lsv_volume_proto_t *volume_proto);
lsv_s32_t remm_connect(lsv_volume_proto_t *volume_proto);
lsv_s32_t remm_accept(lsv_volume_proto_t *volume_proto);
lsv_s32_t remm_listen(lsv_volume_proto_t *volume_proto);

#endif
