#ifndef __LSV_LNET__
#define __LSV_LNET__
#include "lsv_volume_proto.h"
#include "lsv_conf.h"
#include "lsv_help.h"
#include "cluster.h"
#include "lichstor.h"
#include "remm_utils.h"
#include "remm_thread.h"
#include "remm_sync.h"
#include "remm_async.h"
#include "lsv_wbuffer.h"
#include <netinet/in.h>
#define LNET_DOT_ADDR_LEN (20)
#define LNET_BUFFER_SIZE (4096)

#define LSV_OP_TYPE_MSG   (0x0001)
#define LSV_OP_TYPE_DATA  (0x0002)
#define LSV_OP_CODE_READ  (0x0010)
#define LSV_OP_CODE_WRITE (0x0020)
#define LSV_OP_CODE_TRUNCATE  (0x0040)
#define LSV_OP_CODE_SETATTR (0x0080)

#define LSV_OP_TYPE_MASK  (0x000F)
#define LSV_OP_CODE_MASK  (0x00F0)

extern lsv_rwlock_t conn_lock;
extern lsv_rwlock_t list_rwlock;

typedef struct __lsv_op_type{
	lsv_s32_t op_code;
	lsv_s32_t size;
	lsv_u64_t offset;
#ifdef LSV
	lsv_s8_t  path[256];
#else
	struct chkid_t   chkid;
#endif
	union {
		size_t size;
		mode_t mode;
		struct __id{
			uid_t uid;
			gid_t gid;
		}id;
	}u;
}lsv_op_type;

typedef struct __lsv_remm_info{
	struct list_head remm_list;
	lsv_u16_t lsv_remm_id;
	lsv_u16_t lsv_remm_slave_listen_port;
	lsv_u32_t lsv_remm_slave_addr;
	lsv_s8_t  lsv_remm_remote_cluster_name[256];
	lsv_s8_t  lsv_remm_remote_node_name[256];
	lsv_s32_t lsv_remm_power;
	lsv_s32_t lsv_remm_mode;
	lsv_s32_t listen_fd;
	lsv_s32_t conn_fd;
	lsv_thread_t thread;
	lsv_thread_t sync_thread;
}lsv_remm_info;

typedef struct __lsv_remm_iterm{
	lsv_s32_t op_code;
	lsv_s32_t size;
	lsv_u64_t lba;
}lsv_remm_iterm;

lsv_s32_t remm_init(lsv_volume_proto_t *volume_proto);
lsv_s32_t remm_truncate_send_sync(lsv_volume_proto_t *volume_proto, lsv_s32_t size);
lsv_s32_t remm_data_send_sync(lsv_volume_proto_t *volume_proto, lsv_s8_t *buf, lsv_u64_t lba, lsv_s32_t size);
lsv_s32_t remm_truncate_send(lsv_volume_proto_t *volume_proto, lsv_s32_t size);
lsv_s32_t remm_data_send(lsv_volume_proto_t *volume_proto, lsv_s8_t *buf, lsv_u64_t lba, lsv_s32_t size);
lsv_s32_t remm_receive_process_loop(lsv_volume_proto_t *volume_proto);
lsv_s32_t remm_destroy(lsv_volume_proto_t *volume_proto);
#endif
