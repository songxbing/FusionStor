#ifndef __LSV_LNET_SYNC__
#define __LSV_LNET_SYNC__
#include "lsv_types.h"
#include "lsv.h"
#include "lsv_conf.h"
#include "remm.h"
lsv_s32_t remm_truncate_send_sync(lsv_volume_proto_t *volume_proto, lsv_s32_t size);
lsv_s32_t remm_data_send_sync(lsv_volume_proto_t *volume_proto, lsv_s8_t *buf, lsv_u64_t lba, lsv_s32_t size);
#endif
