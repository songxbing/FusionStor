#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/fs.h>
#include <linux/version.h>
#include <sys/utsname.h>
#include <sys/types.h>
#include <stdint.h>

uint8_t buffer[1024 * 1024];
void main(int argc, char *argv[])
{
	uint64_t size = atoi(argv[3]) * (uint64_t) 1024 * 1024;
	int ifd = open(argv[1], O_RDWR | O_NONBLOCK);
	int ofd = open(argv[2], O_CREAT|O_RDWR);

	if(ifd > 0 && ofd > 0)
	{
		uint64_t i;

		for(i = 0;i < size;i+= 1048576)
		{
			int len = pread(ifd, buffer, 1048576, i+ (512 * 1024 * 1024));
			len = write(ofd, buffer + 4096, 1048576-4096);
		}

		close(ifd);
		close(ofd);
	}
	else
	{
		printf("open error.\r\n");
		return;
	}
}
