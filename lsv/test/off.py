#!/usr/bin/env python

import sys

def align_off(off, page=512):
    assert off / page * page == off


def main(lba):
    with open('wr.log') as f:
        for line in f:
            line = line.strip(' \t\n')
            line = line.replace('\t', ' ')
            l = line.split(' ')
            print l
            l = l[9].split('<')[1].split('>')[0].split(',')
            off, size = (int(l[0]), int(l[1]))
            print off, size
            align_off(off)
            align_off(size, page=512)
            if lba >= off and lba < off + size:
                print 'lba', line, lba, off, size


if __name__ == '__main__':
    lba = int(sys.argv[1])
    main(lba)
