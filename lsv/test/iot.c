#include <stdio.h>
#include <stdint.h>

#pragma pack(1)
typedef struct
{
       union 
       {
               struct
               {
                       uint32_t off:20;
                       uint32_t id:32;
                       uint32_t pad:12;
               };
               uint64_t offset;
       };
}test_off_t;
#pragma pack()

void main()
{
        test_off_t x;

        x.off = 134345;
        x.id = 2343224;

        printf("%d %d %d \r\n", sizeof(x), x.offset, x.id * 1024 * 1024 + x.off);
}


