#include <stdio.h>
#include<sys/types.h>
#include<sys/stat.h>
#include <sys/ioctl.h>
#include<fcntl.h>
#include <errno.h>


int main(int argc, char *argv[])
{
    int fd = 0;
    int cmd;
    int arg = 0;
    char Buf[4096];
    
    
    /*打开设备文件*/
    fd = open(argv[1],O_RDWR);
    if (fd < 0)
    {
        printf("Open Dev Mem0 Error!\n");
        return -1;
    }
   
    /* 调用命令MEMDEV_IOCPRINT */
    //printf("<--- Call MEMDEV_IOCPRINT --->\n");
    
	if(strcmp(argv[2], "create") == 0)
		cmd =  _IOW('E', 1, 64);
	else if(strcmp(argv[2], "revert") == 0)
		cmd = _IOW('E', 2, 64);
	else if(strcmp(argv[2], "delete") == 0)
		cmd = _IOW('E', 3, 64);
	else
		cmd = _IOW('E', 4, 64);

	
    if (ioctl(fd, cmd, argv[3],"kdfls") < 0)
    {
            printf("Call cmd MEMDEV_IOCPRINT fail, %d\n", -errno);
            return -1;
    }
	
	printf("create snapshot %s\r\n", argv[2]);

    
    
    close(fd);
    return 0;    
}

