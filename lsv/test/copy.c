#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/fs.h>
#include <linux/version.h>
#include <sys/utsname.h>
#include <sys/types.h>
#include <stdint.h>

#define O_DIRECT    00040000 

uint8_t buffer[1024 * 1024];
void main(int argc, char *argv[])
{
	uint64_t size = atoi(argv[3]) * (uint64_t) 1024 * 1024;
	int ifd = open(argv[1], O_CREAT| O_RDWR | O_NONBLOCK | O_SYNC);
	int ofd = open(argv[2], O_CREAT| O_RDWR | O_NONBLOCK | O_SYNC);

	if(ifd > 0 && ofd > 0)
	{
		uint64_t i;

		for(i = 0;i < size;i+= 4096)
		{
			int len = pread(ifd, buffer, 4096, i);
			if(len != 4096)
				printf("read error\r\n");

			len = pwrite(ofd, buffer, 4096,i);
			if(len != 4096)
				printf("write error \r\n");
		}

		close(ifd);
		close(ofd);
	}
	else
	{
		printf("open error.\r\n");
		return;
	}
}
