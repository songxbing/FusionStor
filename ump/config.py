#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os
import sys
import ConfigParser

from Ump import utils
from Ump import defs


CODE_MAIN_PATH = os.path.abspath(os.path.split(os.path.realpath(__file__))[0] + "/..")
ump_main_path = os.path.abspath(os.path.split(os.path.realpath(__file__))[0] + "")

PRODUCT_NAME = os.path.basename(CODE_MAIN_PATH)
CONFIG_NAME = 'ump.conf'

code_config_path = '%s/Ump/etc/%s/%s' % (ump_main_path, PRODUCT_NAME, CONFIG_NAME)

class InstallManage(object):

    def __init__(self):
        self.install_path = self.get_install_path()
        self.etc_install_path = '%s/etc/%s' % (self.install_path, PRODUCT_NAME)
        self.cfg_install_path = os.path.join(self.etc_install_path, CONFIG_NAME)

    def get_install_path(self):
        cp = ConfigParser.ConfigParser()
        cp.read(code_config_path)
        
        try:
            install_path = cp.get('ump_main', 'home')
        except Exception,e:
            install_path = '/opt/fusionstack'
            pass
        return install_path

    def ensure_install_path(self):
        if not os.path.exists(self.etc_install_path):
            os.makedirs(self.etc_install_path)

    def system_release_info(self):
        platform_cmd = '''python -c "import platform;print ','.join(platform.dist())"'''
        returncode, stdout, stderr = utils._exec_pipe(platform_cmd)
        (distro, release, codename) = stdout.split(',')
        return distro, release, codename

    def install_ump_config(self):
        """ copy codepath/etc/ump.conf to install_path/etc/product_name
        
        """
        if os.path.isfile(self.cfg_install_path):
            print '*'*10
            print 'WARN: %s is existed. use the old.' % self.cfg_install_path
            print '*'*10
        else:
            cp_config_cmd = "cp %s %s" % (code_config_path, self.etc_install_path)
            returncode, stdout, stderr = utils._exec_pipe(cp_config_cmd)
            if not returncode == 0:
                print >>sys.stderr, stderr
                sys.exit(1)
        
    def run(self):
        self.ensure_install_path()
        self.system_release_info()
        self.install_ump_config()
        print '=================='
        print >>sys.stdout, self.etc_install_path
        print '=================='
    

if __name__ == '__main__':
    install_manage = InstallManage()
    install_manage.run()

