#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import gettext
import os
import time
import sys
import ConfigParser
from setuptools import setup, find_packages
from setuptools.command.sdist import sdist

from Ump import utils
import config

install_manage = config.InstallManage()
install_manage.run()

class local_sdist(sdist):
    """Customized sdist hook - builds the ChangeLog file from VC first"""
    def run(self):
        print "hello"
        sdist.run(self)

name = 'Ump'

setup(
    name=name,
    version="4.0.0",
    description='Ump',
    license='MDS License',
    author='www.meidisen.com',
    author_email='',
    url='www.meidisen.com',
    packages=find_packages(exclude=['test', 'bin']),
    test_suite='nose.collector',
    cmdclass={'sdist': local_sdist},
    classifiers=[
        'Development Status :: 4 - Beta',
        'License :: OSI Approved :: MDS License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 2.6',
        'Environment :: Wep app',
    ],
    install_requires=[],  #removed for better compat
    scripts=[
        'Ump/bin/ump-config',
        'Ump/bin/ump-sync',
        'Ump/bin/ump-iops',
        'Ump/bin/ump-latency',
        'Ump/bin/ump-day-sync',
        'Ump/bin/ump-version',
        'Ump/bin/ump-event-clean',
        'Ump/bin/ump-license-sync',        
        'Ump/bin/ump-proxy-shell',        
        'Ump/bin/ump-proxy-download',        
        'Ump/bin/ump-node-sync',        
        'Ump/bin/ump-volume-sync',        
        'Ump/bin/ump-controller-server',
        'Ump/bin/ump-initdb',
        'Ump/bin/ump-upgradedb',
        'Ump/bin/ump-check-agent',
        'Ump/bin/ump-snapshot-manage',
        'Ump/bin/ump-monitor',
        'Ump/bin/ump-ctrl'
       ],
    )

