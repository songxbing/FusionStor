 
echo "Version:  `git branch |grep '^\*' |awk '{print $2}'`_`cat v_num`" 
echo "BuildId:  `git show |grep -i ^commit |awk '{print $2}'`" 
echo "Date:     `git show |grep ^Date:|cut -c 9-`" 
echo "Branch:   ump/`git branch |grep '^\*' |awk '{print $2}'`" 
echo "System:   `uname -ir`" 
echo "Glibc:    `/lib64/libc.so.6 | head -n 1`"
