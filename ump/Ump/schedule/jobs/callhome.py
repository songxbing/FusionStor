#!/usr/bin/env python2
#-*- coding: utf-8 -*-


import os
import traceback

from Ump.schedule.jobs.base import JobBase
from Ump.schedule.utils import log_info_run
from Ump import utils


from Ump.common import log

job_log = log.get_logger('Schedule')

     
class CallhomeJob(JobBase):

    @log_info_run()
    def run(self, context):
        self.callhomem.callhome()
