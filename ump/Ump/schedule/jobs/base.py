#!/usr/bin/env python2
#-*- coding: utf-8 -*-

from Ump.objs.cluster.manager import ClusterManager
from Ump.objs.host.manager import HostManager
from Ump.objs.disk.manager import DiskManager
from Ump.objs.volume.manager import VolumeManager
from Ump.objs.pool.manager import PoolManager
from Ump.objs.iops.manager import IopsManager
from Ump.objs.snapshot.manager import SnapshotManager
from Ump.objs.latency.manager import LatencyManager
from Ump.objs.monitor.manager import MonitorManager
from Ump.objs.oplog.manager import OplogManager
from Ump.objs.recover.manager import RecoverManager
from Ump.objs.license.manager import LicenseManager
from Ump.objs.callhome.manager import CallhomeManager
from Ump.objs.task.manager import TaskManager

from Ump import utils
from Ump import defs

class JobBase(object):

    def __init__(self):
        self.cluster_manager = ClusterManager()
        self.vom = VolumeManager()
        self.hom = HostManager()
        self.diskm = DiskManager()
        self.poolm = PoolManager()
        self.iopsm = IopsManager()
        self.latencym = LatencyManager()
        self.snapshotm = SnapshotManager()
        self.monitorm = MonitorManager()
        self.oplogm = OplogManager()
        self.recoverm = RecoverManager()
        self.licensem = LicenseManager()
        self.callhomem = CallhomeManager()
        self.taskm = TaskManager()
        self.defs = defs
        
        self._run_with_threadpool = utils._run_with_threadpool
        self.utils = utils
        self._unlock_file1 = utils._unlock_file1
        self._lock_file1 = utils._lock_file1
        self.is_fusionnas = defs.PRODUCT_NAME == 'fusionnas'

    
    def run(self, context):
        pass
    
