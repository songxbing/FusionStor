#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import datetime

from Ump.schedule.jobs.base import JobBase
from Ump.schedule.utils import log_info_run

from Ump import utils


from Ump.common import log

job_log = log.get_logger(__name__)

class UmpDaySyncJob(JobBase):
     
    @log_info_run()
    def run(self, context, cluster_id=1):
        print 'DaySync'
        is_create = False
        self.poolm.pool_sync(cluster_id, is_create=is_create)
        self.vom.volume_sync(cluster_id, isDay=True, is_create=is_create)
        self.snapshotm.sync_snapshot(cluster_id)

    
class UmpCleanJob(JobBase):

    @log_info_run()
    def run(self, context):
        # utils.exception_pass(self.iopsm.delete_volume_iops)
        utils.exception_pass(self.iopsm.delete_iops, model_name='Host', model_iops='HostIops')
        utils.exception_pass(self.iopsm.delete_iops, model_name='Pool', model_iops='PoolIops')
        utils.exception_pass(self.iopsm.delete_iops, model_name='Volume', model_iops='VolumeIops')
        utils.exception_pass(self.oplogm.delete_older)
        utils.exception_pass(self.monitorm.delete_older)
        utils.exception_pass(self.taskm.delete_older)
