#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os
from Ump.schedule.jobs.base import JobBase
from Ump.schedule.utils import log_info_run
from Ump.objs.session_wrapper import _sw
from Ump.common import log
from Ump.objs.db import models

job_log = log.get_logger('Schedule')

class RecoverJob(JobBase):

    @log_info_run()
    def run(self, context):
        host_list = []
        clusters = _sw.get_cluster()
        sysconfig_for_ump = _sw.get_one(models.SysconfigForUMP, id_or_spec={'name':'data_recover.qos_limit'})
        hosts = _sw.db_hosts(spec={'lichd_status':'running'})
        #hosts = _sw.db_hosts()
        for host in hosts:
            host_list.append(host.ip)
        qos_maxbw = float(sysconfig_for_ump.value_setting)
        if qos_maxbw < 0:
            qos_policy = 1
        else:
            qos_policy = 0
        kwargs = {
            'hosts': host_list,
            'qos_maxbw': qos_maxbw,
            'qos_policy': qos_policy,

        }
        self.recoverm.sync_recover_qos(kwargs)
