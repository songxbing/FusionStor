#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import datetime

from Ump.schedule.jobs.base import JobBase
from Ump.schedule.utils import log_info_run
from Ump.common import log


job_log = log.get_logger(__name__)


class SnapshotManagerJob(JobBase):
     
    @log_info_run()
    def run(self, context):
        if not self.is_fusionnas:
            return self.snapshotm.snapshot_manager()
    

