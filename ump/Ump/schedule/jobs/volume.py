#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import datetime

from Ump.schedule.jobs.base import JobBase
from Ump.schedule.utils import log_info_run

from Ump import utils


from Ump.common import log

job_log = log.get_logger(__name__)


class VolumeConnectionsSyncJob(JobBase):
     
    @log_info_run()
    def run(self, context):
        self.vom.sync_volume_connections()
    

class VolumeUsedSyncJob(JobBase):
     
    @log_info_run()
    def run(self, context):
        self.vom._sync_volume_used()


class VolumeAutoDeleteExpungeJob(JobBase):

    @log_info_run()
    def run(self, context):
        self.vom.expunge_auto()
    

