#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import datetime

from Ump.schedule.jobs.base import JobBase
from Ump.common import log

job_log = log.get_logger(__name__)

class DiskSyncJob(JobBase):
     
    def run(self, context):
        job_log.info('DiskJob! The time is: %s' % datetime.datetime.now())

