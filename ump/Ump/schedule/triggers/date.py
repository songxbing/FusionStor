#!/usr/bin/env python2
#-*- conding: utf-8 -*-


import  json

from Ump.schedule.triggers.base import TriggerBase

class DateJob(TriggerBase):
    

    def add_job(self, func, sql_job, _scheduler):
        trigger_args = self._get_trigger_args(sql_job)
        args = [sql_job.context_dict]
        _scheduler.add_job(func, id=str(sql_job.id), args=args, **trigger_args)
        return _scheduler

    def update_job(self, sql_job, _scheduler):
        trigger_args = self._get_trigger_args(sql_job)
        _scheduler.reschedule_job(str(sql_job.id), **trigger_args)

    def _get_trigger_args(self, sql_job):
        return dict(trigger='date', run_date=sql_job.run_date)
