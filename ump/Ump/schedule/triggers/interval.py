#!/usr/bin/env python2
#-*- conding: utf-8 -*-


import  json

from Ump.schedule.triggers.base import TriggerBase

class IntervalJob(TriggerBase):
    
    def add_job(self, func, sql_job, _scheduler):

        trigger_args = self._get_trigger_args(sql_job)

        trigger_args['args'] = [sql_job.context_dict]
        _scheduler.add_job(func,
            id=str(sql_job.id), **trigger_args)

    def update_job(self, sql_job, _scheduler):
        trigger_args = self._get_trigger_args(sql_job)
        _scheduler.reschedule_job(str(sql_job.id), **trigger_args)

    def _get_trigger_args(self, sql_job):
        trigger_args = dict()
        trigger_args[sql_job.period] = int(sql_job.every)
        trigger_args['trigger'] = "interval"

        if sql_job.start_date:
            trigger_args['start_date'] = sql_job.start_date

        return trigger_args
