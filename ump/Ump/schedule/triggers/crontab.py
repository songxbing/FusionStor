#!/usr/bin/env python2
#-*- conding: utf-8 -*-


import  json

from Ump.schedule.triggers.base import TriggerBase

DEFAULT_VALUES = {'year': '*', 'month': 1, 'day': 1, 'week': '*', 'day_of_week': '*', 'hour': 0,
                  'minute': 0, 'second': 0}

class CrontabJob(TriggerBase):
    
    def add_job(self, func, sql_job, _scheduler):
        args = [sql_job.context_dict]
        trigger_args = self._get_trigger_args(sql_job)

        _scheduler.add_job(func, 
            args=args, id=str(sql_job.id), trigger="cron",  **trigger_args)
        return _scheduler

    def update_job(self, sql_job, _scheduler):
        trigger_args = self._get_trigger_args(sql_job)

        _scheduler.reschedule_job(str(sql_job.id), trigger="cron", **trigger_args)

    def _get_trigger_args(self, sql_job):
        crontab = sql_job
        trigger_args = {
            'hour': crontab.hour,
            'second': (crontab.second),
            'minute': crontab.minute,
        }
        if crontab.start_date:
            trigger_args['start_date'] = crontab.start_date
        return trigger_args 
