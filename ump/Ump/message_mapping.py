#!/usr/bin/env python2
#-*- coding: utf-8 -*-
"""
Created on 20151124
"""

from Ump.objs.cluster.manager import ClusterManager
from Ump.objs.volume.manager import VolumeManager
from Ump.objs.pool.manager import PoolManager
from Ump.objs.host.manager import HostManager
from Ump.objs.disk.manager import DiskManager
from Ump.objs.sync.manager import SyncManager
from Ump.objs.user.manager import UserManager
from Ump.objs.token.manager import TokenManager
from Ump.objs.license.manager import LicenseManager
from Ump.objs.db.manager import DBManager
from Ump.objs.snapshot.manager import SnapshotManager
from Ump.objs.cgsnapshot.manager import CGSnapshotManager
from Ump.objs.vgroup.manager import VGroupManager
from Ump.objs.remotecopy.manager import RRServerManager
from Ump.objs.remotecopy.manager import RRClientManager
from Ump.objs.snappolicy.manager import SnapshotPolicyManager
from Ump.objs.access_policy.manager import AccessPolicyManager
from Ump.objs.qos.manager import QOSManager
from Ump.objs.recover.manager import RecoverManager
from Ump.objs.alert.manager import AlertManager
from Ump.objs.snmp.manager import SnmpManager
from Ump.objs.sysconfig.manager import SysconfigManager
from Ump.objs.task.manager import TaskManager
from Ump.objs.sysconfigforump.manager import SysconfigForUMPManager
from Ump.objs.oplog.manager import OplogManager
from Ump.objs.schedule.manager import ScheduleManager
from Ump.objs.customer.manager import MailManager, CustomerManager
from Ump.objs.folder.manager import FolderManager
#from Ump.objs.customer.manager import MailManager, CustomerManager

method_map = {
    'ClusterCreate':        'cluster._create',
    'ClusterImport':        'cluster._create',
    'ClusterDelete':        'cluster.delete',
    'ClusterStart':         'cluster.start_cluster',
    'ClusterStop':          'cluster.stop_cluster',
    'ClusterLogBackup':     'cluster.log_dump',
    'ClusterLogClean':      'cluster.log_clean',
    'ClusterSetIscsiPort':  'cluster.set_iscsi_port',
    'ClusterSetName':       'cluster.set_name',
    'ClusterGetArbitorIP':  'cluster.get_arbitor_IP',
    'ClusterSetArbitorIP':  'cluster.set_arbitor_IP',
    'ClusterInitLichConfig':'cluster.init_lichconfig',

    'HostAdd':              'host.addnode',
    'HostDelete':           'host.host_delete',
    'HostStart':            'host.start',
    'HostStop':             'host.stop',

    'DiskAdd':              'disk.disk_add',
    'DiskDelete':           'disk.disk_delete',
    'DiskLight':            'disk.disk_light_operation',
    'RaidAdd':              'disk.raid_add',
    'RaidDelete':           'disk.raid_delete',
    'RaidMissing':          'disk.raid_missing',

    'PoolCreate':           'pool.create',
    'PoolDelete':           'pool.delete',
    'PoolUpdate':           'pool.update',

    'VolumeCreate':         'volume.create',
    'VolumeCopy':           'volume.copy',
    'VolumeDelete':         'volume.delete',
    'VolumeExpunge':        'volume.expunge',
    'VolumeRestore':        'volume.restore',
    'VolumeUpdate':         'volume.update',
    'VolumeFlatten':        'volume.flatten',
    'VolumeGetUsed':        'volume.volume_get_used',
    
    'AccessControlCreate':   'isolation.create',
    'AccessControlDelete':   'isolation.delete',
    'AccessControlUpdate':   'isolation.update',

    'QOSCreate':            'qos.create',
    'QOSDelete':            'qos.delete',
    'QOSUpdate':            'qos.update',

    'RecoverQos':           'recover.qos',
    'GetRecoverQos':        'recover.get_qos',

    'AlertHandler':         'alert.handler',

    'SnapshotCreate':       'snapshot.create',
    'SnapshotDelete':       'snapshot.delete',
    'SnapshotUpdate':       'snapshot.update',
    'SnapshotRollback':     'snapshot.rollback',
    'SnapshotClone':        'snapshot.clone',
    'SnapshotTask':         'snapshot.snapshot_time',

    'VGroupCreate':         'vgroup.create',
    'VGroupDelete':         'vgroup.delete',
    'VGroupUpdate':         'vgroup.update',

    'CGSnapshotCreate':     'cgsnapshot.create',
    'CGSnapshotDelete':     'cgsnapshot.delete',
    'CGSnapshotRollback':   'cgsnapshot.rollback',

    'SnapshotPolicyCreate': 'snapshot_policy.create',
    'SnapshotPolicyDelete': 'snapshot_policy.delete',

    'RRServerCreate':       'rrserver.create',
    'RRServerUpdate':       'rrserver.update',
    'RRServerDelete':       'rrserver.delete',

    'RRClientCreate':       'rrclient.create',
    'RRClientUpdate':       'rrclient.update',
    'RRClientDelete':       'rrclient.delete',

    'SyncCluster':          'sync.ump_sync',
    'SyncClusterDay':       'sync.ump_day_sync',
    'SyncNode':             'sync.ump_node_sync',
    'SyncLicense':          'sync.ump_license_sync',
    'AsyncTask':            'sync.ump_async_task',
    'SyncTask':             'sync.ump_sync_task',

    'Login':                'user.login',
    'Logout':               'user.logout', 
    'UserPassword':         'user.password', 
    'UserCreate':           'user.create', 
    'UserCreateForce':      'user.create', 
    'UserUpdate':           'user.update', 
    'UserDelete':           'user.delete', 

    'LicenseRegister':      'license.register',
    'LicenseSniffer':       'license.sniffer',
    'LicenseCreate':        'license.create',
    'LicenseActivity':      'license.activity',

    'QueryDB':              'db.query',
    'EventCreate':          'db.oplog_create',
    'EventDelete':          'oplog.delete',
    
    'SysconfigUpdate':      'sysconfig.update',
    'GetSnmpHosts':         'snmp.get_snmp_hosts',
    'SetSnmpHost':          'snmp.set_snmp_host',
    'SnmpHostDelete':       'snmp.delete',
    'SnmpHostTest':         'snmp.send_test',

    'SysconfigForUMPUpdate':    'sysconfig_for_ump.update',
    'SysconfigForUMPCreate':    'sysconfig_for_ump.create',
    'ScheduleUpdate':           'schedule.update',
    'ScheduleCreate':           'schedule.create',
    'ScheduleJobCreate':        'schedule.job_create',
    'ScheduleJobDelete':        'schedule.job_delete',
    'ScheduleReportDelete':     'schedule.report_delete',

    'MailCreate':           'mail.create',
    'MailDelete':           'mail.delete',
    'MailTest':             'mail.test',
    'CustomerCreate':        'customer.create',
    'CustomerDelete':        'customer.delete',

    'FolderCreate':        'folder.create',
    'FolderUpdate':        'folder.update',
    'FolderDelete':        'folder.delete',

}

class InstanceMap:

    def __init__(self):
        self.cluster = ClusterManager()
        self.host = HostManager()
        self.disk = DiskManager()
        self.volume = VolumeManager()
        self.pool = PoolManager()
        self.sync = SyncManager()
        self.user = UserManager()
        self.token = TokenManager()
        self.license = LicenseManager()
        self.db = DBManager()
        self.qos = QOSManager()
        self.snapshot = SnapshotManager()
        self.vgroup = VGroupManager()
        self.cgsnapshot = CGSnapshotManager()
        self.snapshot_policy = SnapshotPolicyManager()
        self.rrserver = RRServerManager()
        self.rrclient = RRClientManager()
        self.isolation = AccessPolicyManager()
        self.alert = AlertManager()
        self.snmp = SnmpManager()
        self.sysconfig = SysconfigManager()
        self.sysconfig_for_ump = SysconfigForUMPManager()
        self.recover = RecoverManager()
        self.task = TaskManager()
        self.oplog = OplogManager()
        self.schedule = ScheduleManager()
        self.mail = MailManager()
        self.customer = CustomerManager()
        self.folder = FolderManager()
