#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import ConfigParser
import os
import uuid
import Ump
import logging
import logging.handlers

from Ump.common import utils
from Ump import defs

LOG = logging.getLogger('Ump.common.config')


def get_install_dir():
    return os.path.abspath(os.path.split(os.path.realpath(Ump.__file__))[0] + "/../../..")

install_path = get_install_dir()
log_dir = defs.UMP_LOG_FOLER

cp = ConfigParser.ConfigParser()
cp.read(defs.CONFIG_PATH)


class UmpSetting(object):
    def __init__(self, cp=None, filename=None):
        if cp:
            self.cp = cp
        elif filename:
            cp = ConfigParser.ConfigParser()
            cp.read(filename)
            self.cp = cp

    def get(self, section='ump_main', key='', _default=None):
        try:
            res = self.cp.get(section, key)
        except Exception, e:
            res = _default
        return res

_ump_setting = UmpSetting(cp=cp)

debug = cp.get('ump_main', 'debug').lower() == 'true'

proxy_download_port = _ump_setting.get(key='proxy_download_port', _default=27904)
lich_home = _ump_setting.get(key='lich_home', _default='/opt/fusionstack')
expired_time = _ump_setting.get(key='expired_time', _default=10)
is_save_iops = _ump_setting.get(key='is_save_iops', _default='false')
storagent_port = _ump_setting.get(key='storagent_port', _default=27911)
ui_server_ip = _ump_setting.get(key='ui_server_ip', _default=None)
ui_server_ip = ui_server_ip.strip('\'"')

main_log = cp.get('ump_main', 'main_log')
main_log = os.path.join(log_dir, main_log)
utils.ensure_dir(main_log)
main_log_maxbytes = int(cp.get('ump_main', 'main_log_maxbytes'))
main_log_backupCount = int(cp.get('ump_main', 'main_log_backupCount'))

controller_log = cp.get('ump_main', 'controller_log')
controller_log = os.path.join(log_dir, controller_log)
utils.ensure_dir(controller_log)
controller_log_maxbytes = int(cp.get('ump_main', 'controller_log_maxbytes'))
controller_log_backupCount = int(cp.get('ump_main', 'controller_log_backupCount'))

history_log = cp.get('ump_main', 'history_log')
utils.ensure_dir(history_log)
history_log_maxbytes = int(cp.get('ump_main', 'history_log_maxbytes'))
history_log_backupCount = int(cp.get('ump_main', 'history_log_backupCount'))
protocols = [x.strip() for x in cp.get('ump_main', 'protocol').split(',')]

message_validity = int(cp.get('ump_main', 'message_validity'))
task_validity = int(cp.get('ump_main', 'task_validity'))
event_validity = int(cp.get('ump_main', 'event_validity'))
alert_validity = int(cp.get('ump_main', 'alert_validity'))

sso_login = cp.get('ump_main', 'sso_login').lower() == 'true'

controller_port = int(cp.get('ump_main', 'controller_port'))
controller_agent_port = int(cp.get('ump_main', 'controller_agent_port'))
agent_port = int(cp.get('ump_main', 'agent_port'))
snmp_host = cp.get('ump_main', 'snmp_host').split(',')
snmp_host = [x for x in snmp_host if x]
is_check_suspend = cp.get('ump_main', 'is_check_suspend').lower() == 'true'
lich_check_suspend_admin_port = cp.get('ump_main', 'lich_check_suspend_admin_port')
lich_check_suspend_agent_port = cp.get('ump_main', 'lich_check_suspend_agent_port')

#OPENID_SERVER
ois = 'http://121.8.209.158:8082'   
imp = 'http://222.128.1.105:8888' 
ois_host = ois.split('/')[-1]
rest = '/sso/v2'
callback = '%s/callback'%(imp) 
logout_callback = '%s/logout_callback'%(imp) 
client_id = 'client_id'
client_secret = 'client_secret'


if __name__ == '__main__':
    LOG.info(history_log_maxbytes)
