#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os
from Ump import defs
from Ump.objs.db.session import init_db_session
from Ump import utils
from Ump.objs.db import models
from Ump import create_app


class UpgradeDbShell(object):

    def main(self):
        app, db = create_app('default')
        init_db_session(db)
        with app.app_context():
            if not models.AlembicVersion.__table__.exists(db.session.bind):
                models.AlembicVersion.__table__.create(db.session.bind)
                models.AlembicVersion({'version_num': '444f7fc95199'}).save()

        os.chdir(defs.UMP_PATH)
        cmd = "python %s db upgrade" % defs.CONTROLLER_PATH
        utils._exec_pipe(cmd, is_raise=True)
        print "Database was upgraded sucessfully!"
