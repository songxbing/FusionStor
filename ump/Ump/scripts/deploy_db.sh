#!/bin/bash

USER='root'
PASSWORD=''
DATABASE='fusionstor'
FUSIONSTOR_USER='fusionstor'
FUSIONSTOR_PASSWORD='password'

CREATE_CMD=$(printf 'create database %s;' $DATABASE)
GRANT_LOCALHOST_CMD=$(printf "GRANT ALL PRIVILEGES ON %s.* TO '%s'@'localhost' \
    IDENTIFIED BY '%s';" $DATABASE $FUSIONSTOR_USER $FUSIONSTOR_PASSWORD)
GRANT_ALL_CMD=$(printf "GRANT ALL PRIVILEGES ON %s.* TO '%s'@'%%' \
    IDENTIFIED BY '%s'" $DATABASE $FUSIONSTOR_USER $FUSIONSTOR_PASSWORD);
echo $GRANT_LOCALHOST_CMD
echo $GRANT_ALL_CMD

deploy_mysql_config(){
    mysql -u$USER --password="$PASSWORD" --execute="$CREATE_CMD"
    mysql -u$USER --password="$PASSWORD" --execute="$GRANT_LOCALHOST_CMD"
    mysql -u$USER --password="$PASSWORD" --execute="$GRANT_ALL_CMD"
}

deploy_mysql_config

