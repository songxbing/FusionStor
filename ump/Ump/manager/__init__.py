from Ump.objs.cluster.manager import ClusterManager
from Ump.objs.host.manager import HostManager

from Ump.objs.user.manager import UserManager
from Ump.objs.pool.manager import PoolManager
from Ump.objs.volume.manager import VolumeManager
from Ump.objs.snapshot.manager import SnapshotManager


cluster_man = ClusterManager()
host_man = HostManager()

user_man = UserManager()
pool_man = PoolManager()
volume_man = VolumeManager()
snapshot_man = SnapshotManager()
