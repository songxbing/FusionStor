#!/usr/bin/env python2
# -*- coding: utf-8 -*-


from Ump.objs.db import config as db_config


class Config(object):

    SECRET_KEY = 'dfasfafadsfasf'
    SSL_DISABLE = False
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_RECORD_QUERIES = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    def __init__(self, **kwargs):
        for key, value in kwargs.iteritems():
            setattr(self, key, value)

        if not kwargs.get('SQLALCHEMY_DATABASE_URI'):
            self.SQLALCHEMY_DATABASE_URI = db_config.sqlalchemy_database_uri()

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    # DEBUG = True
    #SQLALCHEMY_DATABASE_URI = db_config.sqlalchemy_database_uri()
    pass


class ProductionConfig(Config):
    pass


