#!/usr/bin/env python2
#-*- coding:utf-8 -*-

from flask import Blueprint
from Ump import webutil
from Ump.objs.folder.manager import FolderManager

from base import BaseHandler, add_manager


folderm = FolderManager()


@add_manager(folderm)
class FolderBase(BaseHandler):

    _post_params = {
        'need': [
            'username',
            'folder_name',
        ],
    }

    _get_params = {
        'need': [
            'username',
            'folder_name',
        ],
        'optional': [
            'skip',
            'limit',
            'order',
            'desc',
        ]
    }


class FolderLs(FolderBase):

    @webutil.authenticated(auth=False)
    def get(self):
        return self._GET()


class FolderStat(FolderBase):

    @webutil.authenticated(auth=False)
    def get(self):
        params = self.check_params()
        return self._run_func('get_folder', params)


class FolderCreate(FolderBase):

    _post_params = {
        'need': [
            'username',
            'folder_name',
            'size',
        ],
    }

    @webutil.authenticated(auth=False)
    def post(self):
        params = self.check_params()
        return self._run_func('create', params)


class FolderDelete(FolderBase):

    _post_params = {
        'need': [
            'username',
            'folder_name',
        ],
    }

    @webutil.authenticated(auth=False)
    def post(self):
        params = self.check_params()
        return self._run_func('delete', params, skip2dict=True)


class FolderUpdate(FolderBase):

    _post_params = {
        'need': [
            'username',
            'folder_name',
            'size',
        ],
    }

    @webutil.authenticated(auth=False)
    def post(self):
        params = self.check_params()
        return self._run_func('update', params)

app = Blueprint('folder',__name__,template_folder='templates')
app.add_url_rule('/folder/create', view_func=FolderCreate.as_view('folder_create'))
app.add_url_rule('/folder/delete', view_func=FolderDelete.as_view('folder_delete'))
app.add_url_rule('/folder/update', view_func=FolderUpdate.as_view('folder_update'))
app.add_url_rule('/folder/stat', view_func=FolderStat.as_view('folder_stat'))
app.add_url_rule('/folder/ls', view_func=FolderLs.as_view('folder_ls'))