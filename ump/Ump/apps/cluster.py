#!/usr/bin/env python2 
#-*- coding:utf-8 -*-

from flask import Blueprint

from Ump.objs.cluster.manager import ClusterManager
from Ump import webutil
from base import BaseHandler, add_manager
#
# urls = (
#     '/?',               'Cluster',
#     '/(health)',        'ClusterHealth',
#     '/(\d+)',           'ClusterOne',
#     '/(\d+)/(op1|op2)', 'ClusterOp',
# )

cluster_m = ClusterManager()


@add_manager(cluster_m)
class Cluster(BaseHandler):
    pass


@add_manager(cluster_m)
class ClusterHealth(BaseHandler):

    @webutil.authenticated()
    def get(self, op):
        params = self.check_params()
        return self._run_func(op, params)


@add_manager(cluster_m)
class ClusterOne(BaseHandler):

    @webutil.authenticated()
    def post(self, _id):
        return self._POST(_id)


@add_manager(cluster_m)
class ClusterOp(BaseHandler):

    @webutil.authenticated()
    def post(self, _id, op):
        return self._POST(_id, op)

app = Blueprint('cluster', __name__, template_folder='templates')
app.add_url_rule('/health', view_func=ClusterHealth.as_view('cluster_health'))
app.add_url_rule('/cluster', view_func=Cluster.as_view('cluster'))
app.add_url_rule('/cluster/<int:_id>', view_func=Cluster.as_view('cluster_one'))
app.add_url_rule('/cluster/<int:_id>/(op1|op2)', view_func=Cluster.as_view('cluster_op'))
