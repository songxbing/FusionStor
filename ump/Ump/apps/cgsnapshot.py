#!/usr/bin/env python2 
#-*- coding:utf-8 -*-

from flask import Blueprint

from Ump import webutil
from Ump.objs.cgsnapshot.manager import CGSnapshotManager

from base import BaseHandler, add_manager


man = CGSnapshotManager()


@add_manager(man)
class CGSnapshotBase(BaseHandler):
    _post_params = {
        'need': [
            'username',
            'vgroup_name',
            'cgsnapshot_name',
        ],
    }

    _get_params = {
        'need': [
            'username',
            'vgroup_name',
            'cgsnapshot_name',
        ],
        'optional': [
            'skip',
            'limit',
            'order',
            'desc',
        ]
    }


class CGSnapshotCreate(CGSnapshotBase):

    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('create', params)


class CGSnapshotDelete(CGSnapshotBase):

    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('delete', params, skip2dict=True)


class CGSnapshotLs(CGSnapshotBase):
    @webutil.authenticated(auth=False)
    def get(self):
        return self._GET()


class CGSnapshotStat(CGSnapshotBase):
    @webutil.authenticated(auth=False)
    def get(self):
        params = self.check_params()
        return self._run_func('get_cgsnapshot', params)


class CGSnapshotRollback(CGSnapshotBase):
    @webutil.authenticated()
    def post(self, _id):
        return webutil.web_return(man.rollback, {'id': _id})

app = Blueprint('cgsnapshot', __name__, template_folder='templates')
app.add_url_rule('/cgsnapshot', view_func=CGSnapshotCreate.as_view('cgsnapshot_create'))
app.add_url_rule('/cgsnapshot/delete', view_func=CGSnapshotDelete.as_view('cgsnapshot_delete'))
app.add_url_rule('/cgsnapshot/stat', view_func=CGSnapshotStat.as_view('cgsnapshot_stat'))
app.add_url_rule('/cgsnapshot/ls', view_func=CGSnapshotLs.as_view('cgsnapshot_list'))
app.add_url_rule('/cgsnapshot/<int:_id>/rollback', view_func=CGSnapshotRollback.as_view('cgsnapshot_rollback'))