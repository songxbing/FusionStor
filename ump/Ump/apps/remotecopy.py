#!/usr/bin/env python2 
#-*- coding:utf-8 -*-

from flask import Blueprint, request
from Ump import webutil
from Ump.objs.remotecopy.manager import RRServerManager

from base import BaseHandler, add_manager

man = RRServerManager()

@add_manager(man)
class RRBase(BaseHandler):
    pass


class RRList(RRBase):
    pass


class RROne(RRBase):

    @webutil.authenticated()
    def post(self, _id):
        return self._POST(_id)

app = Blueprint('rrbase', __name__, template_folder='templates')
app.add_url_rule('/remotecopy/<int:_id>', view_func=RRBase.as_view('rrone'))
app.add_url_rule('/remotecopy/', view_func=RRBase.as_view('rrbase'))
