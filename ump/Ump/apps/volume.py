#!/usr/bin/env python2 
# -*- coding:utf-8 -*-

from flask import Blueprint

from Ump import webutil
from Ump.umptypes import UmpPath
from Ump.common.utils import inspect_func
from Ump.objs.volume.manager import VolumeManager

from base import BaseHandler, add_manager


vom = VolumeManager()


@add_manager(vom)
class VolumeBase(BaseHandler):

    _post_params = {
        'need': [
            'protocol',
            'username',
            ('path', 'volume_path'),
        ],
        'optional': [
            ('path2', 'volume_path'),
            'size',
            ('qos_name', 'skip'),
            'provisioning',
            'repnum',
            'protection_domain_id',
            ('priority', 'skip'),
        ]
    }

    _get_params = {
        'need': [
            'protocol',
            'username',
            ('path', 'volume_path'),
        ],
        'optional': [
            'skip',
            'limit',
            'order',
            'desc',
        ]
    }

    @inspect_func
    def check_params(self):
        params = super(VolumeBase, self).check_params()

        if 'path' in params:
            params['path'] = UmpPath(params['path'], protocol=params['protocol'], username=params['username'])

        if 'path2' in params:
            params['path2'] = UmpPath(params['path2'], protocol=params['protocol'], username=params['username'])

        return params


class VolumeHelp(VolumeBase):

    def get(self):
        doc = {
            'ls': 'list volumes',
            'stat': 'get volume info',
            'snapshots': 'list snapshots',
        }
        return webutil.success2json(doc)


class VolumeLs(VolumeBase):

    @webutil.authenticated(auth=False)
    def get(self):
        return self._GET()


class VolumeStat(VolumeBase):

    @webutil.authenticated(auth=False)
    def get(self):
        params = self.check_params()
        return self._run_func('get_volume', params)


class VolumeCreate(VolumeBase):

    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('sync_create', params)


class VolumeDelete(VolumeBase):

    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('delete', params, skip2dict=True)


class VolumeResize(VolumeBase):

    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('resize', params)


class VolumeRename(VolumeBase):

    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('rename', params)


class VolumeCopy(VolumeBase):

    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('copy', params)


class VolumeFlatten(VolumeBase):

    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('flatten', params)


class VolumeSnapshots(VolumeBase):

    @webutil.authenticated(auth=False)
    def get(self):
        params = self.check_params()
        return self._run_func('list_snapshots', params)


app = Blueprint('volume', __name__, template_folder='templates')
app.add_url_rule('/volume/create', view_func=VolumeCreate.as_view('volume_create'))
app.add_url_rule('/volume/delete', view_func=VolumeDelete.as_view('volume_delete'))
app.add_url_rule('/volume/resize', view_func=VolumeResize.as_view('volume_resize'))
app.add_url_rule('/volume/rename', view_func=VolumeRename.as_view('volume_rename'))
app.add_url_rule('/volume/copy', view_func=VolumeCopy.as_view('volume_copy'))
app.add_url_rule('/volume/flatten', view_func=VolumeFlatten.as_view('volume_flatten'))
app.add_url_rule('/volume/snapshots', view_func=VolumeSnapshots.as_view('volume_snapshots'))
app.add_url_rule('/volume/stat', view_func=VolumeStat.as_view('volume_stat'))
app.add_url_rule('/volume/ls', view_func=VolumeLs.as_view('volume_list'))
