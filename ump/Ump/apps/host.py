#!/usr/bin/env python2 
#-*- coding:utf-8 -*-


from flask import Flask, Blueprint, request

from Ump import webutil
from Ump.objs.host.manager import HostManager

from base import BaseHandler, add_manager

# urls = (
#     '/?',                   'Hosts',
#     '/(\d+)',               'Host',
# )



hostm = HostManager()


@add_manager(hostm)
class Hosts(BaseHandler):

    @webutil.authenticated()
    def post(self):
        params = self.custom_data()
        return webutil.web_return(hostm.addnode, params)


@add_manager(hostm)
class Host(BaseHandler):

    @webutil.authenticated()
    def delete(self, _id):
        params = self.custom_data()
        params.update(id=_id)
        return webutil.web_return(hostm.host_delete, params, skip2dict=True)

    @webutil.authenticated()
    def put(self, host_id):
        ''' update host no use'''
        params = self.custom_data()
        params.update(id=host_id)
        return webutil.web_return(hostm.update, params)

app = Blueprint('host', __name__, template_folder='templates')
app.add_url_rule('/host/<int:_id>', view_func=Host.as_view('host'))
app.add_url_rule('/host', view_func=Hosts.as_view('hosts'))

