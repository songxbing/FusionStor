#!/usr/bin/env python2 
#-*- coding:utf-8 -*-

from flask import Blueprint

from Ump import webutil
from Ump.umptypes import UmpPath
from Ump.common.utils import inspect_func
from Ump.objs.snapshot.manager import SnapshotManager
from Ump.objs.snappolicy.manager import SnapshotPolicyManager

from base import BaseHandler, add_manager


snapshotm = SnapshotManager()
snapshot_plcm = SnapshotPolicyManager()


@add_manager(snapshotm)
class SnapshotBase(BaseHandler):

    _post_params = {
        'need': [
            'protocol',
            'username',
            ('path', 'snap_path'),
        ],
        'optional': [
            'is_protect',
            ('path2', 'volume_path'),
        ]
    }

    _get_params = {
        'need': [
            'protocol',
            'username',
            ('path', 'snap_path'),
        ],
        'optional': [
            'skip',
            'limit',
            'order',
            'desc',
        ]
    }

    @inspect_func
    def check_params(self):
        params = super(SnapshotBase, self).check_params()

        if 'path' in params:
            params['path'] = UmpPath(params['path'], protocol=params['protocol'], username=params['username'])

        if 'path2' in params:
            params['path2'] = UmpPath(params['path2'], protocol=params['protocol'], username=params['username'])

        return params


class SnapshotLs(SnapshotBase):
    @webutil.authenticated(auth=False)
    def get(self):
        return self._GET()


class SnapshotStat(SnapshotBase):
    @webutil.authenticated(auth=False)
    def get(self):
        params = self.check_params()
        return self._run_func('get_snapshot', params)


class SnapshotCreate(SnapshotBase):
    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('create', params)


class SnapshotDelete(SnapshotBase):
    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('delete', params, skip2dict=True)


class SnapshotRollback(SnapshotBase):
    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('rollback', params)


class SnapshotClone(SnapshotBase):
    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('clone', params)


class SnapshotProtect(SnapshotBase):
    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('protect', params)


@add_manager(snapshot_plcm)
class SnapshotPolicy(BaseHandler):

    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return webutil.web_return(snapshot_plcm.create, params)


@add_manager(snapshot_plcm)
class SnapshotPolicyOne(BaseHandler):

    @webutil.authenticated()
    def delete(self, _id):
        return self._DELETE(_id)

    @webutil.authenticated()
    def put(self, _id):
        params = self.check_params()
        params.update(id=_id)

        return webutil.web_return(snapshot_plcm.update, params)

app = Blueprint('snapshot', __name__, template_folder='templates')
app.add_url_rule('/snapshot/create', view_func=SnapshotCreate.as_view('snapshot_create'))
app.add_url_rule('/snapshot/delete', view_func=SnapshotDelete.as_view('snapshot_delete'))
app.add_url_rule('/snapshot/rollback', view_func=SnapshotRollback.as_view('snapshot_rollback'))
app.add_url_rule('/snapshot/clone', view_func=SnapshotClone.as_view('snapshot_clone'))
app.add_url_rule('/snapshot/protect', view_func=SnapshotProtect.as_view('snapshot_protect'))
app.add_url_rule('/snapshot/policy', view_func=SnapshotPolicy.as_view('snapshot_policy'))
app.add_url_rule('/snapshot/policy/<int:_id>', view_func=SnapshotPolicyOne.as_view('snapshot_policy_one'))
app.add_url_rule('/snapshot/stat', view_func=SnapshotStat.as_view('snapshot_stat'))
app.add_url_rule('/snapshot/ls', view_func=SnapshotLs.as_view('snapshot_list'))