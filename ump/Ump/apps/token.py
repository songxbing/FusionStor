#!/usr/bin/env python2 
# -*- coding:utf-8 -*-


from flask import Blueprint
from Ump.objs.token.manager import TokenManager
from base import BaseHandler, add_manager


token_m = TokenManager()


@add_manager(token_m)
class Tokens(BaseHandler):
    _post_params = {
        'need': [
            'username',
            'password',
        ]
    }

    def post(self):
        params = self.check_params()
        return self._run_func('create', params)


app = Blueprint('token', __name__, template_folder='templates')
app.add_url_rule('/token', view_func=Tokens.as_view('tokens'))
