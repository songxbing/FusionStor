#!/usr/bin/env python2 
#-*- coding:utf-8 -*-

from flask import Blueprint
from Ump import webutil
from Ump.objs.qos.manager import QOSManager
from base import BaseHandler, add_manager


qos_m = QOSManager()


@add_manager(qos_m)
class QoSCreate(BaseHandler):
    _post_params = {
        'need': [
            ('name', 'skip'),
            ('throt_burst_iops_max', 'skip'),
            ('throt_burst_mbps_max', 'skip'),
        ],
        'optional': [
            'username',
        ],
    }

    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return webutil.web_return(qos_m.create, params)


@add_manager(qos_m)
class QoSDelete(BaseHandler):
    _delete_params = {
        'need': [
            ('name', 'skip'),
        ],
    }

    @webutil.authenticated()
    def delete(self):
        params = self.check_params()
        return webutil.web_return(qos_m.delete, params)


@add_manager(qos_m)
class QosUpdate(BaseHandler):
    _put_params = {
        'need': [
            ('name', 'skip'),
        ],
        'optional': [
            ('throt_burst_iops_max', 'skip'),
            ('throt_burst_mbps_max', 'skip'),
        ],
    }

    @webutil.authenticated()
    def put(self):
        params = self.check_params()
        return webutil.web_return(qos_m.update, params)


@add_manager(qos_m)
class QoSStat(BaseHandler):
    _get_params = {
        'need': [
            ('name', 'skip'),
        ],
    }
    @webutil.authenticated(auth=False)
    def get(self):
        params = self.check_params()
        return self._run_func('get_qos', params)


@add_manager(qos_m)
class QoSLs(BaseHandler):
    @webutil.authenticated(auth=False)
    def get(self):
        return self._GET()


app = Blueprint('qos', __name__, template_folder='templates')
app.add_url_rule('/qos/stat', view_func=QoSStat.as_view('qos_stat'))
app.add_url_rule('/qos/ls', view_func=QoSLs.as_view('qos_list'))
app.add_url_rule('/qos/create', view_func=QoSCreate.as_view('qos_create'))
app.add_url_rule('/qos/update', view_func=QosUpdate.as_view('qos_update'))
app.add_url_rule('/qos/delete', view_func=QoSDelete.as_view('qos_delete'))
