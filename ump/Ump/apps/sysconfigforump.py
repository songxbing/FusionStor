#!/usr/bin/env python2
#-*- coding:utf-8 -*-

from flask import Blueprint

from Ump import webutil
from Ump.objs.sysconfigforump.manager import SysconfigForUMPManager
from base import BaseHandler, add_manager


man = SysconfigForUMPManager()


@add_manager(man)
class SysconfigForUMPLs(BaseHandler):

    @webutil.authenticated(auth=False)
    def get(self):
        return self._GET()


@add_manager(man)
class SysconfigForUMP(BaseHandler):
    _put_params = {
        'need': [
            'value',
        ],
    }

    @webutil.authenticated(auth=False)
    def put(self, _id):
        params = self.check_params()
        params.update(id=_id)
        return webutil.web_return(man.update, params)


app = Blueprint('sysconfigforump', __name__, template_folder='templates')
app.add_url_rule('/sysconfigforump', view_func=SysconfigForUMPLs.as_view('sysconfigforump'))
app.add_url_rule('/sysconfigforump/<int:_id>', view_func=SysconfigForUMP.as_view('sysconfigforump_update'))