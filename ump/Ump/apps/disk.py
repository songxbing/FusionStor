#!/usr/bin/env python2 
#-*- coding:utf-8 -*-

from flask import Blueprint
from Ump.objs.host.manager import HostManager
from Ump.objs.disk.manager import DiskManager
from Ump import webutil
from base import BaseHandler, add_manager


hostm = HostManager()
diskm = DiskManager()


@add_manager(diskm)
class Disks(BaseHandler):
    pass


@add_manager(diskm)
class Disk(BaseHandler):

    @webutil.authenticated()
    def post(self, disk_id):
        params = self.custom_data()
        return webutil.web_return(diskm.disk_add, params)

    @webutil.authenticated()
    def delete(self, _id):
        params = self.custom_data()
        params.update(id=_id)
        return webutil.web_return(diskm.disk_delete, params, skip2dict=True)

        
class AddRaid(BaseHandler):

    @webutil.authenticated()
    def post(self, _id):
        params = self.custom_data()
        params.update(id=_id)
        return webutil.web_return(diskm.raid_add, params)


class DeleteRaid(BaseHandler):

    @webutil.authenticated()
    def post(self, _id):
        params = self.custom_data()
        params.update(id=_id)
        return webutil.web_return(diskm.raid_delete, params, skip2dict=True)

app = Blueprint('disk', __name__, template_folder='templates')
app.add_url_rule('/disk/<int:_id>/add_raid', view_func=AddRaid.as_view('add_raid'))
app.add_url_rule('/disk/<int:_id>/delete_raid', view_func=DeleteRaid.as_view('delete_raid'))
app.add_url_rule('/disk/<int:_id>', view_func=Disk.as_view('disk'))
