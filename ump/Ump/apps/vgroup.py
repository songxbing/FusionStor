#!/usr/bin/env python2 
# -*- coding:utf-8 -*-

from flask import Blueprint

from Ump import webutil
from Ump.objs.vgroup.manager import VGroupManager
from base import BaseHandler, add_manager


man = VGroupManager()


@add_manager(man)
class VGBase(BaseHandler):
    _post_params = {
        'need': [
            'username',
            'vgroup_name',
        ],
        'optional': [
            'volume_ids',
        ]
    }

    _put_params = {
        'need': [
            'username',
            'vgroup_name',
        ],
        'optional': [
            'add_volume_list',
            'remove_volume_list',
            'volume_ids',
        ]
    }
    _get_params = {
        'need': [
            'username',
            'vgroup_name',
        ],
        'optional': [
            'skip',
            'limit',
            'order',
            'desc',
        ]
    }


class VGroupCreate(VGBase):
    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('create', params)


class VGroupUpdate(VGBase):
    @webutil.authenticated()
    def put(self):
        params = self.check_params()
        return self._run_func('update', params)


class VGroupDelete(VGBase):
    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('delete', params, skip2dict=True)


class VGroupLs(VGBase):
    @webutil.authenticated(auth=False)
    def get(self):
        return self._GET()


class VGroupStat(VGBase):
    @webutil.authenticated(auth=False)
    def get(self):
        params = self.check_params()
        return self._run_func('get_vgroup', params)


class VGroupSnapshots(VGBase):
    @webutil.authenticated(auth=False)
    def get(self):
        params = self.check_params()
        return self._run_func('list_cgsnapshots', params)

app = Blueprint('vgroup', __name__, template_folder='templates')
app.add_url_rule('/vgroup', view_func=VGroupCreate.as_view('vgroup_create'))
app.add_url_rule('/vgroup/update', view_func=VGroupUpdate.as_view('vgroup_update'))
app.add_url_rule('/vgroup/delete', view_func=VGroupDelete.as_view('vgroup_delete'))
app.add_url_rule('/vgroup/cgsnapshots', view_func=VGroupSnapshots.as_view('vgroup_cgsnapshots'))
app.add_url_rule('/vgroup/stat', view_func=VGroupStat.as_view('vgroup_stat'))
app.add_url_rule('/vgroup/ls', view_func=VGroupLs.as_view('vgroup_list'))
