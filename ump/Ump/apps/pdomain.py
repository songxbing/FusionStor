#!/usr/bin/env python2 
#-*- coding:utf-8 -*-

from flask import Blueprint


from Ump import webutil
from Ump.objs.pdomain.manager import PDomainManager

from base import BaseHandler, add_manager


pdomain_m = PDomainManager()


@add_manager(pdomain_m)
class PDomainBase(BaseHandler):
    pass


class PDomainList(PDomainBase):
    pass


class PDomainOne(PDomainBase):

    @webutil.authenticated()
    def post(self, _id):
        return self._POST(_id)

app = Blueprint('pdomain', __name__, template_folder='templates')
app.add_url_rule('/pdomain', view_func=PDomainList.as_view('pdomain_list'))
app.add_url_rule('/pdomain/<int:_id>', view_func=PDomainOne.as_view('pdomain_one'))