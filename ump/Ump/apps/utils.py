#!/usr/bin/env python
# -*- coding: utf-8 -*-


def pool_get_path(name, username='cinder'):
    return '%s:%s' % (username, name)


def volume_get_path(name, username='cinder'):
    return '%s:%s' % (username, name)


def volume_parse_path(name):
    return tuple(name.split('/'))



if __name__ == '__main__':
    print pool_get_path('pool')
    print volume_parse_path('a/b')
