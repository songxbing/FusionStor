#!/usr/bin/env python2 
#-*- coding:utf-8 -*-

"""The schedule job api is not opened yet!!!
"""
from flask import Blueprint, request
import json

from Ump import webutil
from Ump.umptypes import UmpPath

from Ump.objs.pool.manager import PoolManager
from Ump.schedule.api import ScheduleApi

from base import BaseHandler, add_manager

schedule_api = ScheduleApi()


class SJobBase(BaseHandler):
    pass

class ScheduleJobCreate(SJobBase):

    @webutil.authenticated(auth=False)
    def post(self):
        params = self.check_params()

        values = { 
            "schedule_type": 'cron',
            "schedule_string": '*/3',
            "class_name": "host",
            "context": json.dumps({'host':'test'}),
        }
        return schedule_api.create_job(values)


class ScheduleJobUpdate(SJobBase):

    @webutil.authenticated(auth=False)
    def post(self):
        params = self.custom_data()

        job_id = params.get('job_id')
        values = {'schedule_string': '*/10'} 
        return schedule_api.update_job(job_id, values)


class ScheduleJobDelete(SJobBase):

    @webutil.authenticated(auth=False)
    def post(self):
        params = self.custom_data()
        job_id = params.get('job_id')
        return schedule_api.delete_job(job_id)

app = Blueprint('schedule', __name__, template_folder='templates')
app.add_url_rule('/schedule/create', view_func=ScheduleJobCreate.as_view('schedulejob_create'))
app.add_url_rule('/schedule/update', view_func=ScheduleJobUpdate.as_view('schedulejob_update'))
app.add_url_rule('/schedule/delete', view_func=ScheduleJobDelete.as_view('schedulejob_delete'))