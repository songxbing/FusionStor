#!/usr/bin/env python2 
#-*- coding:utf-8 -*-


import json
from flask import  Blueprint, request
import traceback

from Ump.objs.alert.manager import AlertManager
from Ump import webutil

from base import BaseHandler

alertm = AlertManager()

class Alert(BaseHandler):

    @webutil.authenticated()
    def get(self):
        alerts = alertm.db_api.alert_get_with_is_handled(False)
        return webutil.success2json(alerts)


app = Blueprint('alert', __name__, template_folder='templates')
app.add_url_rule('/alert', view_func=Alert.as_view('alert'))
