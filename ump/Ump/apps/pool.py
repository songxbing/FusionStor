#!/usr/bin/env python2 
#-*- coding:utf-8 -*-


from flask import Blueprint
from Ump import webutil
from Ump.umptypes import UmpPath
from Ump.objs.pool.manager import PoolManager
from base import BaseHandler, add_manager

poolm = PoolManager()

@add_manager(poolm)
class PoolBase(BaseHandler):

    _post_params = {
        'need': [
            'protocol',
            'username',
            ('path', 'pool_path'),
        ],
        'optional': [
            'quota',
            'repnum',
            'ec_data',
            'ec_code',
            'protection_domain_ids',
        ]
    }

    _get_params = {
        'need': [
            'protocol',
            'username',
            ('path', 'pool_path'),
        ],
        'optional': [
            'skip',
            'limit',
            'order',
            'desc',
        ]
    }

    def check_params(self):
        params = super(PoolBase, self).check_params()
        if 'path' in params:
            params['path'] = UmpPath(params['path'], protocol=params['protocol'], username=params['username'])
        return params


class PoolLs(PoolBase):

    @webutil.authenticated(auth=False)
    def get(self):
        return self._GET()


class PoolStat(PoolBase):

    @webutil.authenticated(auth=False)
    def get(self):
        params = self.check_params()
        return self._run_func('get_pool', params)


class PoolCreate(PoolBase):

    @webutil.authenticated()
    def post(self):
        params = self.check_params()

        return self._run_func('create', params)


class PoolDelete(PoolBase):

    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('delete', params, skip2dict=True)


class PoolUpdate(PoolBase):

    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return self._run_func('update', params)

app = Blueprint('pool', __name__, template_folder='templates')
app.add_url_rule('/pool/create', view_func=PoolCreate.as_view('pool_create'))
app.add_url_rule('/pool/delete', view_func=PoolDelete.as_view('pool_delete'))
app.add_url_rule('/pool/update', view_func=PoolUpdate.as_view('pool_update'))
app.add_url_rule('/pool/stat', view_func=PoolStat.as_view('pool_stat'))
app.add_url_rule('/pool/ls', view_func=PoolLs.as_view('pool_ls'))
