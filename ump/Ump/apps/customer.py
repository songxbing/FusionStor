#!/usr/bin/env python2
# -*- coding:utf-8 -*-


from flask import Blueprint
from Ump import webutil
from Ump.objs.customer.manager import CustomerManager
from base import BaseHandler, add_manager


mail_m = CustomerManager()


@add_manager(mail_m)
class MailCreate(BaseHandler):

    @webutil.authenticated()
    def post(self):
        params = self.check_params()
        return webutil.web_return(mail_m.create, params)


@add_manager(mail_m)
class MailLs(BaseHandler):

    @webutil.authenticated()
    def delete(self, _id):
        return webutil.web_return(mail_m.delete, {'id': _id})

    @webutil.authenticated()
    def put(self, _id):
        params = self.check_params()
        params.update(id=_id)
        return webutil.web_return(mail_m.update, params)


app = Blueprint('mailcreate', __name__, template_folder='templates')
app.add_url_rule('/mail/create', view_func=MailCreate.as_view('mail_create'))
app.add_url_rule('mail/<int:_id>', view_func=MailLs.as_view('mails_delete_update'))
