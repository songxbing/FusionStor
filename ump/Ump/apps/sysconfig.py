#!/usr/bin/env python2
#-*- coding:utf-8 -*-

"""The sysconfig api is not opened yet!!!
"""

from flask import Blueprint

from Ump import webutil
from Ump.objs.sysconfig.manager import SysconfigManager
from base import BaseHandler, add_manager


syscfgm = SysconfigManager()


@add_manager(syscfgm)
class SysconfigLs(BaseHandler):

    @webutil.authenticated()
    def get(self):
        return self._GET()


@add_manager(syscfgm)
class Sysconfig(BaseHandler):
    _put_params = {
        'need': [
            'value',
        ],
    }

    @webutil.authenticated(auth=False)
    def put(self, _id):
        params = self.check_params()
        params.update(id=_id)
        return webutil.web_return(syscfgm.update, params)

app = Blueprint('sysconfig', __name__, template_folder='templates')
app.add_url_rule('/sysconfig', view_func=Sysconfig.as_view('sysconfig'))
app.add_url_rule('/sysconfig/<int:_id>', view_func=Sysconfig.as_view('sysconfig_id'))
app.add_url_rule('/sysconfig/ls', view_func=SysconfigLs.as_view('sysconfig_ls'))