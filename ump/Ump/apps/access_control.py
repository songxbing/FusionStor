#!/usr/bin/env python2 
#-*- coding:utf-8 -*-


from flask import Blueprint

from Ump.objs.access_policy.manager import AccessPolicyManager
from Ump import webutil
from base import BaseHandler, add_manager


acl_m = AccessPolicyManager()


@add_manager(acl_m)
class AccessPolicyLs(BaseHandler):

    @webutil.authenticated()
    def post(self):
        params = self.custom_data
        return webutil.web_return(acl_m.create, params)


@add_manager(acl_m)
class AccessPolicy(BaseHandler):

    @webutil.authenticated()
    def delete(self, _id):
        return webutil.web_return(acl_m.delete, {'id': _id})

    @webutil.authenticated()
    def put(self, _id):
        params = self.check_params()
        params.update(id=_id)
        return webutil.web_return(acl_m.update, params)


app = Blueprint('access_policy', __name__, template_folder='templates')
app.add_url_rule('/access_control', view_func=AccessPolicyLs.as_view('access_policy_create'))
app.add_url_rule('/access_control/<int:_id>', view_func=AccessPolicy.as_view('access_policy_update_delete'))
# app.add_url_rule('/access_control/<int:_id>', view_func=AccessPolicy.as_view('access_policy_update'))