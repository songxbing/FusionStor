#!/usr/bin/env python 
# -*- coding:utf-8 -*-

import sys, os
import getopt
import logging

from Ump import controller 
from Ump import defs
from Ump.common.daemon import Daemon
from Umpweb.common.wsgilog import WsgiLog, LogStdout


class ControllerDaemon(Daemon):
    def __init__(self, port=None):
        self.pidfile = defs.CONTROLLER_PID_FILE
        self.name = 'controller_server'
        self.prog_name = 'controller_server'
        super(ControllerDaemon, self).__init__(self.pidfile, name=self.name, prog_name=self.prog_name, port=port)

    def run(self):
        controller.main(self.port)

    def test(self):
        controller.main(self.port, is_log=False)


def usage():
    print ("usage:")
    name = 'ump-controller-server'
    print (name + " --start")
    print (name + " --stop")
    print (name + " --status")
    print (name + " --test")
    print (name)
    exit(1)


def main():
    op = ''
    ext = None
    try:
        opts, args = getopt.getopt(
                sys.argv[1:],
            'h', ['start', 'stop', 'help', 'test','status']
                )
        if not opts:
            usage()
    except getopt.GetoptError, err:
        usage()

    controller_daemon = ControllerDaemon()
    for o, a in opts:
        if o in ('--help'):
            usage()
            exit(0)
        elif (o == '--start'):
            op = o
            controller_daemon.start()
            print ('%s is running' % (controller_daemon.name))
        elif (o == '--stop'):
            op = o
            res = controller_daemon.stop()
            if res:
                print ('%s is stopped' % (controller_daemon.name))
        elif (o == '--test'):
            controller_daemon.test()
        elif (o == '--status'):
            is_run,pid = controller_daemon.status()
            if is_run:
                print ('%s is running, pid is %s' %(controller_daemon.name, pid))
            else:
                print ('%s is stopped' % (controller_daemon.name))
        else:
            assert False, 'oops, unhandled option: %s, -h for help' % o
            exit(1)


if __name__ == '__main__':
    if (len(sys.argv) == 1):
        usage()
    else:
        main()
