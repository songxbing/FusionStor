#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os
import re
import sys
import json
import logging
import time
import traceback


from Ump import utils
from Ump.common import log
from Ump.common import exception 
from Ump.objs.db import models
from Ump.objs.manager_base import Manager
from Ump.objs.session_wrapper import enable_log_and_session


LOG = log.get_log('Ump.objs.sysconfig.manager')


@models.add_model(models.Sysconfig)
class SysconfigManager(Manager):

    def __init__(self):
        self.super_ = super(SysconfigManager, self)
        self.super_.__init__()

    @enable_log_and_session(resource='settings', event='update')
    def update(self, _logger, kwargs):
        sysconfig_id = kwargs.get('id')
        sysconfig =  _sw.get_one(models.Sysconfig, sysconfig_id)
        if not sysconfig:
            raise exception.NotFound(_id=sysconfig_id)

        _logger.update_props(oplog_obj=sysconfig.key)
        sysconfig = self._update(sysconfig, **kwargs)

        return sysconfig

    def _update(self, sysconfig, value=None, **kwargs):

        values = {}
        utils.update_values(values, 'value', value)
        
        sysconfig.update(values)
        return sysconfig
        

if __name__ == '__main__':
    snm = SysconfigManager()
    snm.update()
