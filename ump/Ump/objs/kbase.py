#!/usr/bin/env python2
#-*- coding: utf-8 -*-

PPOOL_TYPE_ROOT       = 'cluster_root'
PPOOL_TYPE_DATACENTER = 'cluster_datacenter'
PPOOL_TYPE_CLUSTER    = 'cluster_cluster'

'''
Event name
'''
instance_start_create = 'instance start create'
instance_create_end = 'instance complete create'
instance_start_boot = 'instance start boot'
instance_boot_end = 'instance complete boot'
instance_start_shutdown = 'instance start shutdown'
instance_shutdown_end = 'instance complete shutdown'
instance_start_delete = 'instance start delete'
instance_delete_end = 'instance complete delete'
instance_start_edit = 'instance start edit'
instance_error = 'instance error'
