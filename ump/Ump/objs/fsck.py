#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from Ump.objs.root import ROOT

from Ump.objs.cluster.manager import ClusterManager
from Ump.objs.pdomain.manager import PDomainManager
from Ump.objs.host.manager import HostManager

from Ump.objs.user.manager import UserManager
from Ump.objs.pool.manager import PoolManager
from Ump.objs.volume.manager import VolumeManager
from Ump.objs.snapshot.manager import SnapshotManager


def list_m(M):
    m = M()
    xs = m.get_list()
    for x in xs:
        print x


def fsck():
    list_m(ClusterManager)
    list_m(PDomainManager)
    list_m(HostManager)

    list_m(UserManager)
    list_m(PoolManager)
    list_m(VolumeManager)
    list_m(SnapshotManager)



if __name__ == '__main__':
    fsck()
