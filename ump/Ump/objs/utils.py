#!/usr/bin/env python2
# -*- coding: utf-8 -*-


def is_valid_repnum(repnum):
    return repnum in range(2, 5)


if __name__ == '__main__':
    for repnum in range(1, 6):
        print repnum, is_valid_repnum(repnum)
