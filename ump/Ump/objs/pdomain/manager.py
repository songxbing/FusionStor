#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import json
import os
import re
import datetime
import time

import Ump
from Ump import utils
from Ump.common.log import log_init
from Ump.common import log
from Ump.objs.db import models
from Ump.objs.manager_base import Manager


LOG = log.get_log('Ump.objs.pdomain.manager')


@models.add_model(models.ProtectionDomain)
class PDomainManager(Manager):

    def __init__(self):
        super(PDomainManager, self).__init__()


 
if __name__ == '__main__':
    log_init()
