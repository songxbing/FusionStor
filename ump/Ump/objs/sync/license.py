#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os
import traceback
from Ump.objs.sync.kbase import Base

class License(Base): 
    def __init__(self):
        super(License, self).__init__()

    def _ump_license_sync(self):

        try:
            self.cluster_manager.license_dispatch()
        except Exception, e:
            traceback.print_exc()

        try:
            self.cluster_manager.license_sync()
        except Exception, e:
            traceback.print_exc()
            LOG.info(e)
