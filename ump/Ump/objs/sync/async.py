#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os
import sys
import traceback
from Ump.objs.sync.kbase import Base

class Async(Base): 
    def __init__(self):
        super(Async, self).__init__()

    def sync_task(self, cluster):
        msgs = []
        if cluster.hosts == [] :
            return ''

        try:
            self.vom.do_thick_volume_creating(cluster.id)
        except Exception, e:
            traceback.print_exc(file=sys.stderr)
            msg = 'Error do_volume_creating fail: %s, %s'%(\
                    e, cluster)
            msgs.append(msg)
        return msgs

    

