#!/usr/bin/env python2
#-*- coding: utf-8 -*-

from Ump.objs.cluster.manager import ClusterManager
from Ump.objs.host.manager import HostManager
from Ump.objs.disk.manager import DiskManager
from Ump.objs.volume.manager import VolumeManager
from Ump.objs.pool.manager import PoolManager
from Ump.objs.iops.manager import IopsManager
from Ump.objs.snapshot.manager import SnapshotManager

from Ump import utils

class Base(object):

    def __init__(self):
        self.cluster_manager = ClusterManager()
        self.vom = VolumeManager()
        self.hom = HostManager()
        self.diskm = DiskManager()
        self.poolm = PoolManager()
        self.iopsm = IopsManager()
        self.snapshotm = SnapshotManager()
        self._run_with_threadpool = utils._run_with_threadpool
        self.utils = utils

    def _set_attr_server(self, cls):
        self.cluster_manager.cls = cls
        self.vom.cls = cls 
        self.hom.cls = cls 
        self.poolm.cls = cls 
        self.iopsm.cls = cls
        
