#!/usr/bin/env python2
# -*- coding: utf-8 -*-


from Ump.objs.db import models

from manager_base import Manager


class Root(Manager):

    def __init__(self, cluster=None, cluster_id=1):
        super(Root, self).__init__()

        self.root = models.ROOT

        self.cluster_id = cluster_id
        # print '--- init root'
        self._sync_cluster(cluster=cluster)

    def __getattr__(self, item):
        return getattr(self.root, item)

    def __getattribute__(self, attr):
        if attr in ['root.cluster']:
            if object.__getattribute__(self, attr):
                self._sync_cluster(self.cluster_id)
        else:
            return object.__getattribute__(self, attr)

    def _sync_cluster(self, cluster=None, cluster_id=None):
        self.root.cluster = cluster



ROOT = None


def init_root(cluster_id=1):
    global ROOT

    if not ROOT:
        ROOT = Root(cluster_id=cluster_id)
    return ROOT


if __name__ == '__main__':
    init_root()
    print ROOT.root.cluster
