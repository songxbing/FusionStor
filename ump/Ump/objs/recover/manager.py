#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import traceback
import os

from Ump import utils
from Ump.common import log
from Ump.objs.db import models
from Ump.common import exception
from Ump.objs.manager_base import Manager
from Ump.lich.cluster import LichClusterParam, LichCluster
from  Ump.lich.recover import LichRecoverParam, LichRecover
from Ump.defs import  REMOTE_QOS_DIR, MAX_BW_FILE, QOS_FILE

LOG = log.get_log('Ump.objs.recover.manager')


class RecoverManager(Manager):

    def __init__(self):
        super(RecoverManager, self).__init__()
        self.lich_cluster = LichCluster()
        self.lichRecover = LichRecover()
    
    def qos(self, kwargs):
        self._exec(kwargs)

    def get_qos(self, kwargs):
        policy = None
        maxbw = None

        hosts = kwargs.get('hosts')
        maxbw_cmd = 'cat %s' % MAX_BW_FILE
        policy_cmd = 'cat %s' % QOS_FILE

        for host in hosts:
            try:
                maxbw = res = self._exec_remote(host, maxbw_cmd)
                policy = res = self._exec_remote(host, policy_cmd)

            except Exception, e:
                LOG.info(e)
                pass
            if policy is not None and maxbw is not None:
                break

        if policy is None:
            policy = '1'
        if maxbw is None:
            maxbw = '-1'
        if maxbw != '-1':
            maxbw = int(maxbw) * 1024 * 1024

        values = {
                    'policy':policy,
                    'maxbw': maxbw
        }

        return values


    def _check_remote_files(self, hosts):
        result = {}
        for host in hosts:
            param = LichRecoverParam(host)
            param.remote_dir = REMOTE_QOS_DIR
            param.qos_file = QOS_FILE
            param.maxbw_file = MAX_BW_FILE
            qos_recode = self.lichRecover._check_qos_file(param)
            maxbw_recode = self.lichRecover._check_maxbw_file(param)

            result[host] = {'qos_recode': qos_recode,'max_recode': maxbw_recode,}

        return  result


    def _exec(self, kwargs, qos=True, maxbw=True):
        qos_maxbw = kwargs.get('qos_maxbw')
        qos_policy = kwargs.get('qos_policy')
        hosts = kwargs.get('hosts')
        if qos_maxbw is not None and qos :
            qos_maxbw = int(qos_maxbw)/1024/1024

        maxbw_cmd = 'touch %s;echo %s > %s' % (MAX_BW_FILE, qos_maxbw, MAX_BW_FILE)
        policy_cmd = 'touch %s;echo %s > %s' % (QOS_FILE, qos_policy, QOS_FILE)
        for host in hosts:
            if qos_maxbw is not None and qos :
                self._exec_remote(host, maxbw_cmd)
            if qos_policy is not None and maxbw:
                self._exec_remote(host, policy_cmd)

        host_ip = self.lich_cluster._select_host()
        kwargs = LichClusterParam(host_ip=host_ip)

    def sync_recover_qos(self, kwargs):
        hosts = kwargs.get('hosts')
        #m = self._check_remote_files(hosts)

        results = self._check_remote_files(hosts)
        for host in results:
            if not results[host]['qos_recode']:
                pass
            else:
                self._exec(kwargs, maxbw=False)
            if not results[host]['max_recode']:
                pass
            else:
                self._exec(kwargs, qos=False)

if __name__ == '__main__':
    rec = RecoverManager()
    #rec.sync_recover_qos('192.168.120.52')
