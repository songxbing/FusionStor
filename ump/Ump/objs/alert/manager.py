#!/usr/bin/env python2
#-*- coding: utf-8 -*-


from Ump.common import  log

from Ump import utils
from Ump.objs.db import models

from Ump.objs.manager_base import Manager
from Ump.objs.session_wrapper import enable_log_and_session

LOG = log.get_log('Ump.objs.qos.manager')


class AlertManager(Manager):
    def __init__(self):
        self.super_ = super(AlertManager, self)

    @enable_log_and_session(resource='sync', event='sync', disable_oplog=True)
    def handler(self, _logger, params):

        alert = self._handler(**params)
        return alert

    def _handler(self, ids, **kwargs):
        values = {'is_handled':True}
        ids = ids.split(',')
        for aid in ids:
            if not aid :
                continue
            alert = models.Alert.query.get(aid)
            if alert:
                alert.update(values)

        return alert 
        

   
