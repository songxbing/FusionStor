#!/usr/bin/env python2
#-*- coding: utf-8 -*-

from sqlalchemy.exc import DisconnectionError

def instance_host_sync(target, value, old_value, initiator):
    if value == 'shutoff':
        setattr(target, 'host', '')

def my_connect(dbapi_con, con_record):
    """Switch sqlite connections to non-synchronous mode
        OFF, 最快但不安全
        NORMAL， 有很小的概率在发生电源故障时丢失数据，
                实际上此时磁盘已不可用或着有不可恢复的硬件错误
        FULL, 很安全但很慢
    """
    dbapi_con.execute("PRAGMA synchronous = OFF")

def my_checkout(dbapi_con, con_record, con_proxy):
    """
    Ensures that MySQL connections checked out of the
    pool are alive.
    
    Borrowed from:
    http://groups.google.com/group/sqlalchemy/msg/a4ce563d802c929f
    """
    try:
        dbapi_con.cursor().execute('select 1')
    except dbapi_con.OperationalError, ex:
        if ex.args[0] in (2006, 2013, 2014, 2045, 2055):
#                LOG.warn('Got mysql server has gone away: %s', ex)
            raise DisconnectionError("Database server went away")
        else:
            raise

#if __name__ == '__main__':
#    x  = get_engine()
