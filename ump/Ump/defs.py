#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os
import Ump

def get_install_dir():
    return os.path.abspath(os.path.split(os.path.realpath(Ump.__file__))[0] + "/../../..")

CODE_MAIN_PATH = os.path.abspath(os.path.split(os.path.realpath(Ump.__file__))[0] + "/../..")

#install_path /opt/fusionstack
install_path = get_install_dir()


LOCAL_HOST = ['0.0.0.0', 'localhost', '127.0.0.1']
LOG_FOLER = os.path.join(install_path, 'log')

UMP_HOME = os.path.dirname(os.path.realpath(Ump.__file__))

UMP_NAME = 'ump'
ETC_NAME = 'etc'
CONFIG_NAME = 'ump.conf'

#fusionstor or fusionnas
PRODUCT_NAME = os.path.basename(CODE_MAIN_PATH)
PRODUCT_DIR = os.path.join(install_path, PRODUCT_NAME)

ETC_DIR_PATH = os.path.join(install_path, ETC_NAME, PRODUCT_NAME)

DB_CFG = 'db.cfg'
DB_CFG_PATH = os.path.join(ETC_DIR_PATH, DB_CFG)

#/opt/fusionstack/log/product_name
UMP_LOG_FOLER = os.path.join(LOG_FOLER, PRODUCT_NAME)

CONTROLLER_SERVER_LOG_PATH = os.path.join(UMP_LOG_FOLER, 'controller_server.log')

CONFIG_PATH = os.path.join(ETC_DIR_PATH, CONFIG_NAME)

UMP_TREE = "%s/%s" % (PRODUCT_NAME, 'ump/Ump')
UMP_PATH = os.path.join(install_path, UMP_TREE)
CONF_PATH = "%s/conf" % (UMP_PATH)

#/opt/fusionstack/log/ump/ump-schduler.log
SCHEDULER_LOG_PATH = os.path.join(UMP_LOG_FOLER, 'ump-schduler.log')

#'/opt/fusionstack/backup' 
BACKUP_PATH  =  os.path.join(install_path, 'backup')
CONTROLLER_PATH = os.path.join(UMP_PATH, 'controller.py')

REMOTECOPY_CLIENT_CONFIG_PATH = '/opt/fusionstack/remotecopy/etc/remotecopy_client.conf'

UMP_LICENSE_CMD_DIR = '%s/%s' %(install_path, 'fusionstor/ump/manage')
UMP_LICENSE_DIR = '%s/%s' %(install_path, 'fusionstor/ump/manage/license')
DOWNLOAD_TAR_DIR = '/tmp/fusionstor/license'
DECOMMPRESS_DIR = '/tmp/fusionstor/delicense'
LICH_HOST_IP_FILE = '/tmp/fusionstor/node_info'

CALLHOME_PATH =  '/tmp/fusionstor/callhome'

REMOTE_QOS_DIR = '/dev/shm/lich4/nodectl/recovery'
QOS_FILE = '%s/%s' %(REMOTE_QOS_DIR, 'qos_policy')
MAX_BW_FILE = '%s/%s' %(REMOTE_QOS_DIR, 'fill_rate')

is_fusionnas = PRODUCT_NAME == 'fusionnas'

CONTROLLER_PID_FILE = '/var/run/%s_controller_server.pid' % (PRODUCT_NAME)
if is_fusionnas:
    CONTROLLER_PORT = 28914
    AGENT_PORT = 28911
    WEB_SOCKET_PORT = 28905
    WEB_SOCKET_PID_FILE = '/var/run/fusionnas_websocket-server.pid'

else:
    AGENT_PORT = 27911
    CONTROLLER_PORT = 27914
    WEB_SOCKET_PORT = 27905
    WEB_SOCKET_PID_FILE = '/var/run/ump_websocket-server.pid'

MAILSUPPORT_ADDRESS = 'mailsupport@fusionstack.cn'


if __name__ == '__main__':
    print DB_CFG_PATH
    print LOG_FOLER
    print SCHEDULER_LOG_PATH
    print BACKUP_PATH
    print PRODUCT_NAME
    print CONFIG_PATH
    print PRODUCT_DIR
