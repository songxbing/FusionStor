#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import socket
import time  
import logging

from Ump import defs

LOG = logging.getLogger('Ump.websocket.send')


def send_message(message, host='127.0.0.1', port=defs.WEB_SOCKET_PORT):
    try:
        message = str(message)
        s = socket.socket()  
        s.connect((host, port))  
        s.send(message)
        s.close()
    except Exception,e:
        LOG.info(e)
        pass

