#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import sys
import time
import web
import logging

from Ump.objs.latency.manager import LatencyManager
from Ump.utils import _lock_file

LOG = logging.getLogger('Ump.bin.ump-iops')
latencym = LatencyManager()

        
def main(): 
    lfile = '/dev/shm/ump_latency_lock'
    lock =  _lock_file(lfile)

    latencym = LatencyManager()
    latencym.cluster_volume_latency()


if __name__ == "__main__":
    main()
