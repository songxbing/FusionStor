#!/usr/bin/env python2
#-*- coding: utf-8 -*-


from Ump import utils
from Ump.objs.iops.manager import IopsManager
from Ump.objs.monitor.manager import MonitorManager
from Ump.objs.oplog.manager import OplogManager

iom = IopsManager()
oplogm = OplogManager()
monitorm = MonitorManager()

def main():
    utils.exception_pass(iom.delete_volume_iops)
    utils.exception_pass(oplogm.delete_older)
    utils.exception_pass(monitorm.delete_older)

if __name__ == "__main__":
    main()
