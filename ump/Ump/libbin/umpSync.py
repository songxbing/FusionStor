#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import sys
import logging

from optparse import OptionParser
from Ump.utils import _lock_file
from Ump.objs.sync.manager import SyncManager


LOG = logging.getLogger('Ump.bin.ump-sync')


def main():
    lfile = '/dev/shm/ump_lock'
    lock =  _lock_file(lfile)

    usage = "usage: %prog -p ALL|cluster_id"
    parser = OptionParser(usage=usage)
    parser.add_option("-p", "--cluster_id", default="ALL",
            help="cluster cluster id, if -p ALL, 同步所有的, 默认是all"),
    (options, args) = parser.parse_args()
    cluster_id = options.cluster_id
    
    if cluster_id is None:
        parser.print_help()
        sys.exit(1)

    sync = SyncManager()
    sync.ump_sync(cluster_id)


if __name__ == "__main__":
    msgs = main()
