#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os
import sys
import time
import logging

from optparse import OptionParser, Option
from Ump.common import config
from Ump.utils import ensure_dir, _lock_file
from Ump.utils import install_path
from Ump.common.log import log_init
from Ump.objs.sync.base import Server
from Ump import utils

from Ump.objs.monitor.manager import MonitorManager

LOG = logging.getLogger('Ump.bin.ump-monitor')

def main():
    #log_init()
    #log_customize()
    lfile = '/dev/shm/ump_trap_message_lock'
    lock =  _lock_file(lfile)

    usage = "usage: %prog -p ALL|cluster_id"
    parser = OptionParser(usage=usage)
    parser.add_option("-p", "--cluster_id", default="ALL",
            help="cluster cluster id, if -p ALL, 同步所有的, 默认是all"),
    (options, args) = parser.parse_args()
    cluster_id = options.cluster_id
    
    monitor_manager = MonitorManager()
    monitor_manager.monitor_main_for_bin()

    LOG.info("ump-monitor -p %s"%cluster_id)

if __name__ == "__main__":
    msgs = main()
