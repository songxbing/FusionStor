#!/usr/bin/env python2
#-*- coding: utf-8 -*-
#这个文件用来定时执行，更新相应的数据库

import errno
import fcntl
import os
import socket
import sys
import time
import web
import json
import logging
import traceback
import datetime
import threading

from optparse import OptionParser, Option
from Ump.common import config
from Ump.utils import install_path
from Ump.common.log import log_init

from Ump.objs.iops.manager import IopsManager
from Ump.objs.iops._iops import mkdir_
from Ump.utils import ensure_dir, _lock_file

LOG = logging.getLogger('Ump.bin.ump-iops')
iom = IopsManager()

        
def main(): 
    lfile = '/dev/shm/ump_iops_lock'
    lock =  _lock_file(lfile)

    iom = IopsManager()
    msgs = iom.cluster_iops_sync()

    msgs = [m for m in msgs if '' and not None]
    if not msgs == []:
        print >>sys.stderr, msgs
        LOG.error(str(msgs))
        sys.exit(1)
    sys.exit(0)
        
        
    return msgs

if __name__ == "__main__":
    main()
