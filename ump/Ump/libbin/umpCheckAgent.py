#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os
import sys
import logging
import datetime
import time
import traceback

from optparse import OptionParser, Option
from Ump.common import config
from Ump.utils import _lock_file, _unlock_file
from Ump.common.log import log_init
from Ump.objs.cluster.manager import ClusterManager

clm = ClusterManager()
def agent_sync():
    error = None
#    LOG.info("ump-check_agent_sync ")
    try:
        clm.check_agent_sync()
    except Exception,e:
        traceback.print_exc(file=sys.stderr)
        error = str(e)

    return error


def main():
    lfile = '/dev/shm/ump_check_agent_lock'
    lock_fd =  _lock_file(lfile)
    start = datetime.datetime.now()
    error = agent_sync()
    _unlock_file(lock_fd)
    #clm._start_controller()
    #end = datetime.datetime.now()
    #delta = datetime.timedelta(seconds=30)

    #if (end - start) < delta:
    #    time.sleep(5)
    #    msgs = agent_sync()

    if  error:
        print >>sys.stderr, error 
        sys.exit(1)
    sys.exit(0)

if __name__ == "__main__":
    main()
