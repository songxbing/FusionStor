#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os
import sys
import logging
import datetime
import time

from optparse import OptionParser, Option
from Ump.common import config
from Ump.utils import ensure_dir,_lock_file
from Ump.utils import install_path

LOG = logging.getLogger('Ump.bin.imp-sync')


server = None 
def node_sync():
    global server

    cluster_id = None
    from Ump.objs.sync.manager import SyncManager
    sync = SyncManager()
    sync.ump_node_sync('dd')
    LOG.info("ump-node_sync ")

    return []

def main():
    lfile = '/dev/shm/ump_node_lock'
    lock =  _lock_file(lfile)
    start = datetime.datetime.now()
    msgs = node_sync()
    end = datetime.datetime.now()
    delta = datetime.timedelta(seconds=30)

    if (end - start) < delta:
        time.sleep(5)
        msgs = node_sync()

    os.system('rm -rf %s'%(lfile))
    if not msgs == []:
        print >>sys.stderr, msgs
        LOG.error(str(msgs))
        sys.exit(1)
    sys.exit(0)

if __name__ == "__main__":
    main()
