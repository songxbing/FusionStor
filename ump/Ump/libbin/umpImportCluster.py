#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os
import sys
import time
import logging
import getopt
import json

from Ump import utils
from Ump import create_app
from Ump.common import config
from Ump.objs.cluster.manager import ClusterManager


LOG = logging.getLogger('Ump.bin.ump-sync')

def log_customize():
    level = logging.DEBUG
    log_file = '%s/log/ump/sync.log' % utils.install_path
    log_dir = os.path.dirname(log_file)
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)

    logging.getLogger("Ump").setLevel(level)
    handler = logging.handlers.RotatingFileHandler(
        log_file,
        maxBytes=config.main_log_maxbytes,
        backupCount=config.main_log_backupCount)
    handler.setLevel(level)
    formatter = logging.Formatter(
            "%(asctime)s-%(name)s-%(levelname)s-%(message)s")
    handler.setFormatter(formatter)
    logging.getLogger("Ump").addHandler(handler)


class ImportCluster:
    def __init__(self, host, password):
        self.host = host
        self.password = password
        self.cluster_manager = ClusterManager()
    
    def main(self):
        lfile = '/dev/shm/ump_import_cluster_lock'
        lock =  utils._lock_file(lfile)

        cluster_id = 1
        params = {'option': u'import', 'cluster_type': None, 'cluster_id': 1, 'name': u'test'}
        params['node_ip'] = self.host
        params['passwd'] = self.password

        app, db = create_app('default')
        from Ump.objs.db.session import init_db_session
        init_db_session(db)

        with app.app_context():
            return self.cluster_manager._create(params)

def usage():
    print ("usage:")
    print ("ump-import-cluster" + " --host <ip> --password <password>")
    print ("ump-import-cluster" + " --help")
    exit(1)

def main():
    op = ''
    host = ''
    password = ''
    ext = None
    try:
        opts, args = getopt.getopt(
                sys.argv[1:],
            'h', ['host=', 'help', 'password=']
                )
        if not opts:
            usage()
    except getopt.GetoptError, err:
        usage()

    for o, a in opts:
        if o in ('--help'):
            usage()
            exit(0)
        elif (o == '--host'):
            host = a
        elif (o == '--password'):
            password = a
        else:
            assert False, 'oops, unhandled option: %s, -h for help' % o
            exit(1)

    print host, password
    cm = ImportCluster(host, password)
    cm.main()

if __name__ == "__main__":
    if (len(sys.argv) == 1):
        usage()
    else:
        main()
