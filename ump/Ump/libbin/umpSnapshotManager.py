#!/usr/bin/env python2
#-*- coding: utf-8 -*-

from Ump.utils import _lock_file
from Ump.objs.snapshot.manager import SnapshotManager


def main():
    lfile = '/dev/shm/ump_license_lock'
    lock =  _lock_file(lfile)

    snapshotm = SnapshotManager()
    snapshotm.snapshot_manager()

if __name__ == "__main__":
    main()
