#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import sys
import logging

from optparse import OptionParser
from Ump.utils import _lock_file
from Ump.objs.volume.manager import VolumeManager


LOG = logging.getLogger('Ump.bin.ump-volume-sync')


def main():
    lfile = '/dev/shm/ump_volume_sync_lock'
    lock =  _lock_file(lfile)

    usage = "usage: %prog -p ALL|cluster_id"
    parser = OptionParser(usage=usage)
    parser.add_option("-p", "--cluster_id", default="ALL",
            help="cluster cluster id, if -p ALL, 同步所有的, 默认是all"),
    (options, args) = parser.parse_args()
    cluster_id = options.cluster_id
    
    if cluster_id is None:
        parser.print_help()
        sys.exit(1)

    vom = VolumeManager()
    vom.sync_volume_connections()

if __name__ == "__main__":
    msgs = main()
