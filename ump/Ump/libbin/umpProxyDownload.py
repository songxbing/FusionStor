#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import sys
from Ump.proxy.lich_dump import main,usage 


if __name__ == '__main__':
    if (len(sys.argv) == 1):
        usage()
    else:
        main()
