#!/usr/bin/env python2
#-*- coding: utf-8 -*-

"""
Created on 20160522
"""
#TODO : Create volume
import errno
import fcntl
import os
import socket
import sys
import time
import web
import json
import logging
import traceback
import datetime
import threading
import multiprocessing
 
from optparse import OptionParser, Option
from Ump.common import config
from Ump.utils import ensure_dir, _lock_file

from Ump.objs.sync.manager import SyncManager

LOG = logging.getLogger('Ump.bin.ump-task-scan')

def main():
    lfile = '/dev/shm/ump_task_lock'
    lock =  _lock_file(lfile, 60*60*24)

    start = datetime.datetime.now()
    sync = SyncManager()
    sync.ump_sync_task('')

    end = datetime.datetime.now()
    delta = (end - start).seconds
    if int(delta) < 30 :
        time.sleep(3)
        sync.ump_sync_task('')
    else:
        pass

    sys.exit(0)
    return msgs

if __name__ == "__main__":
    main()


