#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import sys
from Ump.scripts.upgradedb import UpgradeDB 

def main():
    upgradeM = UpgradeDB()
    upgradeM.run()

if __name__ == "__main__":
    main()
    #try:
    #    InitDbShell().main(sys.argv[1:])
    #except Exception, e:
    #    print >> sys.stderr, e
    #    sys.exit(1)


