#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os
import sys
import time
import logging
import traceback

from optparse import OptionParser, Option
from Ump.common import config
from Ump.utils import _lock_file
from Ump.common.log import log_init
from Ump.objs.sync.manager import SyncManager

LOG = logging.getLogger('Ump.bin.imp-sync')

def log_customize():
    level = logging.DEBUG
    log_file = '%s/log/ump/sync.log'%cm.home
    log_dir = os.path.dirname(log_file)
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)
    #if 'DEBUG' in dir(config) and getattr(config, 'DEBUG'):
        #pass
    #else:
        #level = logging.INFO

    logging.getLogger("Ump").setLevel(level)
    handler = logging.handlers.RotatingFileHandler(
        log_file,
        maxBytes=config.main_log_maxbytes,
        backupCount=config.main_log_backupCount)
    handler.setLevel(level)
    formatter = logging.Formatter(
            "%(asctime)s-%(name)s-%(levelname)s-%(message)s")
    handler.setFormatter(formatter)
    logging.getLogger("Ump").addHandler(handler)

def main():
    lfile = '/dev/shm/ump_day_lock'
    lock =  _lock_file(lfile)

    usage = "usage: %prog -p ALL|cluster_id"
    parser = OptionParser(usage=usage)
    parser.add_option(
            "-p", "--cluster_id", default="ALL",
            help="cluster cluster id, if -p ALL, 同步所有的, 默认是all")
    parser.add_option(
            "-c", "--is_create", default=False,
            help="create pool & vol from lich; must be lich")
    (options, args) = parser.parse_args()
    cluster_id = options.cluster_id
    is_create = options.is_create == 'lich'

    if cluster_id is None:
        parser.print_help()
        sys.exit(1)

    sync = SyncManager()
    sync.ump_day_sync(cluster_id, is_create)
    LOG.info("ump-sync -p %s"%cluster_id)


if __name__ == "__main__":
    msgs = main()
