#!/usr/bin/env python2
#-*- coding: utf-8 -*-

from Ump.utils import _lock_file
from Ump.objs.cluster.manager import ClusterManager


def main():
    lfile = '/dev/shm/ump_license_lock'
    lock =  _lock_file(lfile)

    clusterm = ClusterManager()
    clusterm.license_sync()

if __name__ == "__main__":
    main()
