#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import datetime

from base import LichBase, RemoteLocation
from runner import http_runner, ssh_runner


class LichLicenseParam(RemoteLocation):

    def __init__(self, host_ip=None):
        super(LichLicenseParam, self).__init__(host_ip)


class LichLicense(LichBase):

    @http_runner()
    def sniffer(self, param):
        cmd = '%s -m sniffer' % (self.lich_license)
        return cmd
    
    @http_runner()
    def list(self, param):
        cmd = '%s -m list' % (self.lich_license)
        return cmd

    def register_date(self, param):
	stdout = self._fulltime(param)
	register_date = None
	if stdout:
            stds = [x for x in stdout.split('\n') if x]
            rdate = stds[-1]
            rdates = [x for x in rdate.split(' ') if x]
            rdatetime = "%s %s"%(rdates[5],rdates[6].split('.')[0])
            temp_license_create_datetime = datetime.datetime.strptime(rdatetime.strip(), '%Y-%m-%d %H:%M:%S')
            register_date = temp_license_create_datetime
	return register_date

    @http_runner()
    def _fulltime(self, param):
	license_path = ' %s/license' % (self.lich_home)
        cmd = 'if [ -f %s ]; then ls --full-time %s ;fi' % (license_path, license_path)
        return cmd


    @http_runner()
    def register_fusionnas(self, param):
        if self.is_fusionnas:
            cmd = "echo -n '%s' >/opt/fusionnas/license" % (param.license_id)
        else:
            cmd = "echo -n '%s' >/opt/fusionstack/license" % (param.license_id)
        return cmd


if __name__ == '__main__':
    pass
