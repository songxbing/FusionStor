#!/usr/bin/env python2
# -*- coding: utf-8 -*-


import utils

from base import RemoteLocation, LichBase
from runner import http_runner
from shell import LichShellParam, LichShell

class LichRecoverParam(RemoteLocation):
    def __init__(self, host_ip, remote_dir=None, qos_file=None, maxbw_file=None):
        super(LichRecoverParam, self).__init__(host_ip)
        self.host_ip = host_ip
        self.qos_file = qos_file
        self.maxbw_file = maxbw_file
        self.is_http = True
        self.ssh_run = False

class LichRecover(LichBase):
    def __init__(self):
       super(LichRecover, self).__init__()
       self.Lichshell = LichShell()
    def _check_qos_file(self, param):
        recode = self.Lichshell.file_exists(param, param.qos_file)
        return recode
    def _check_maxbw_file(self, param):
        recode = self.Lichshell.file_exists(param, param.maxbw_file)
        return  recode

if __name__ == '__main__':
    pass
