Git_path="/root/ump"
Branch=`git branch|sed -n '/\*/p' |awk '{print $2}' `
 
NOW=`date --rfc-3339=seconds | sed 's/\://g' | sed 's/\-//g' | sed 's/\ //g' | sed 's/\+.*//g'`
Time=`date +%y%m%d%H`
Vnum=`cat v_num`
Pwd=`pwd`
cd ${Git_path}
#git checkout $Branch
#echo "put your git passwd:"
#echo "the branch is $Branch"
#git pull

bash -f git-info3.sh >git-info

Commit=`git log|sed -n 1p|awk '{print $2}'|cut -b 1-5`
git archive --format tar -o /tmp/tmp.tar HEAD
rm -rf /tmp/ump/
mkdir -p /tmp/ump/
tar xf /tmp/tmp.tar -C /tmp/ump/
cp ${Git_path}/git-info /tmp/ump/
systems='U C' #
for sys in ${systems}       #
do
    cd /tmp
    Tar_path="/var/www/html/ump/$Branch/$NOW/$sys"
    if [ ! -d $Tar_path ];then
        mkdir -p $Tar_path 
    fi
    #mkdir ${Tar_path}/$sys     #
    rm /tmp/ump/manage -rf
    cp -r ${Git_path}/$sys/manage /tmp/ump/
    Target_file=${Tar_path}/ump-$Branch-${Vnum}-${Commit}-${Time}-$sys.tar.gz
    ss=ump-$Branch-${Vnum}-${Commit}-${Time}.tar.gz
    echo $Tar_path
    echo $Target_file
    tar czf $Target_file ump/ --exclude ump/Ump/dragon 
    echo $sys,'------ the file is:'
    echo '------', $Target_file
    cd ${Pwd}
    sleep 5 
done
