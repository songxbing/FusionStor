#include "config.h"

#include <stdint.h>
#include <ctype.h>

#define DBG_SUBSYS S_LIBYLIB

#include "dbg.h"

int str_replace_char(char *str, char from, char to)
{
        int i;

        if (str == NULL)
                return 0;

        i = 0;
        while (str[i] != '\0') {
                if (str[i] == from)
                        str[i] = to;

                i++;
        }

        return 0;
}

int str_replace_str(char *str, char *from, char *to)
{
        (void) str; (void) from; (void) to;

        return 0;
}

int str_upper(char *str)
{
        int i;

        if (str == NULL)
                return 0;

        i = 0;
        while (str[i] != '\0') {
                str[i] = toupper(str[i]);

                i++;
        }

        return 0;
}

unsigned int str_octal_to_uint(const char *str)
{
        unsigned int res = 0;
        int digit, seen_non_zero_digit = 0;

        /* note - avoiding using sscanf() parser */

        while (*str != '\0') {
                digit = *str;

                if (!isdigit(digit) || digit > '7')
                        break;

                if (digit != '0')
                        seen_non_zero_digit = 1;

                if (seen_non_zero_digit) {
                        res <<= 3;
                        res += (digit - '0');
                }

                str++;
        }

        return res;
}
