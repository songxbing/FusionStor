#include "config.h"

#define DBG_SUBSYS S_LIBYLIB

#include "yid.h"
#include "mem_cache.h"
#include "sysutil.h"
#include "dbg.h"

int int_cmp(const void *key, const void *data)
{
        int ret, kv, dv;

        kv = *(int *)key;
        dv = *(int *)data;
        DBUG("key %d, data %d\n", kv, dv);

        if (kv < dv)
                ret = -1;
        else if (kv > dv)
                ret = 1;
        else
                ret = 0;

        DBUG("ret %d\n", ret);
        return ret;
}

int int64_cmp(const void *key, const void *data)
{
        int ret;
        uint64_t kv, dv;

        kv = *(uint64_t *)key;
        dv = *(uint64_t *)data;

        if (kv < dv)
                ret = -1;
        else if (kv > dv)
                ret = 1;
        else
                ret = 0;

        DBUG("ret %d\n", ret);
        return ret;
}

int verid64_cmp(const verid64_t *keyid, const verid64_t *dataid)
{
        int ret;

        if (keyid->id < dataid->id)
                ret = -1;
        else if (keyid->id > dataid->id)
                ret = 1;
        else {
                if (keyid->version < dataid->version)
                        ret = -1;
                else if (keyid->version > dataid->version)
                        ret = 1;
                else
                        ret = 0;
        }

        return ret;
}

int verid64_equal(const verid64_t *id1, const verid64_t *id2)
{
        if (id1->id == id2->id && id1->version == id2->version)
                return 1;
        else
                return 0;
}

int verid64_void_cmp(const void *keyid, const void *dataid)
{
        return verid64_cmp((verid64_t *)keyid, (verid64_t *)dataid);
}

int verid64_void_equal(const void *key, const void *data)
{
        const verid64_t *id1 = key;
        const verid64_t *id2 = data;

        if (id1->id == id2->id && id1->version == id2->version)
                return 1;
        else
                return 0;
}
