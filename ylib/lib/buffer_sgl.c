
//code rewrited.
//#ifdef RDMA_SPDK    sperated, no need 'if' at all.

#include "align.h"
#include "adt.h"
#include "configure.h"
#include "sysutil.h"
#include "buffer.h"
#include "pipe_pool.h"
#include "sysy_lib.h"
#include "mem_cache.h"
#include "analysis.h"
#include "mem_hugepage.h"
#include "job.h"
#include "dbg.h"
#include "core.h"
#include "../../schedule/spdk.h"

#if 0
buffer_t * mbuffer_merge_if(buffer_t *buf)
{
        int sz = list_size(&buf->list);
        seg_t *first_seg = (seg_t *)buf->list.next;
        mem_handler_t *handler = &first_seg->handler;

        if(unlikely(sz > 1 || (uint64_t)handler->ptr % PAGE_SIZE == 0 ||  first_seg->len % PAGE_SIZE != 0)) {
        }

        return buf;
}

int mbuffer_is_aligned(buffer_t *buf, int align)
{
        int sz = list_size(&buf->list);
        seg_t *first_seg = (seg_t *)buf->list.next;
        mem_handler_t *handler = &first_seg->handler;

        (void) align;
        if(unlikely(sz > 1 || (uint64_t)handler->ptr % PAGE_SIZE == 0 ||  first_seg->len % PAGE_SIZE != 0)) {
                return 0;
        }

        return 1;
}

int mbuffer_trans_sgl(void *__sgl, int *sgl_count, buffer_t *buf)
{
        sgl_element_t *_sgl = (sgl_element_t *)__sgl;
        int first_sgl_len, offset = 0;
        mem_handler_t *handler = &seg->handler;
        sgl_element_t *sgl = _sgl;
        size_t left = seg->len, tmp;
        int _sgl_count = 1;

        //YASSERT(((uint64_t)seg->handler.ptr % 4) == 0);

        if (likely(((uint64_t)handler->ptr % PAGE_SIZE == 0 &&  left % PAGE_SIZE == 0))
                        || ((left + (uint64_t)handler->ptr % PAGE_SIZE) <= PAGE_SIZE)) {

                sgl->offset = 0;
                sgl->sgl_base = handler->ptr;
                sgl->sgl_len = left;
                sgl->phyaddr = handler->phyaddr;

                return 0;
        }

        if ((uint64_t)handler->ptr % PAGE_SIZE) {
                first_sgl_len = PAGE_SIZE - ((uint64_t)handler->ptr % PAGE_SIZE);
        } else {
                first_sgl_len = left - left % PAGE_SIZE;
        }

        if (likely(first_sgl_len)) {
                sgl->offset = 0;
                sgl->sgl_base = handler->ptr;
                sgl->sgl_len = first_sgl_len;
                sgl->phyaddr = handler->phyaddr;
                left -= first_sgl_len;

                if (left == 0) {
                       goto exit;
                }

                offset = first_sgl_len;
                sgl++;
                _sgl_count++;
        }

        tmp = left < PAGE_SIZE ? left : (left / PAGE_SIZE) * PAGE_SIZE;
        sgl->offset = 0;
        sgl->sgl_base = handler->ptr + offset;
        sgl->sgl_len = tmp;
        sgl->phyaddr = handler->phyaddr + offset;

        offset += tmp;
        left -= tmp ;

        if (likely(left)) {
                sgl++;
                _sgl_count++;

                sgl->offset = 0;
                sgl->sgl_base = handler->ptr + offset;
                sgl->sgl_len = left;
                sgl->phyaddr = handler->phyaddr + offset;
        }

exit:

        if(unlikely(_sgl_count > *sgl_count))
                return -1;
        else {
                *sgl_count = _sgl_count;
                YASSERT(0);
                return 0;
        }
}
#endif

int mbuffer_sg_trans_sgl(void *__sgl, int *sgl_count, seg_t *seg)
{
        sgl_element_t *sgl = (sgl_element_t *)__sgl;
        mem_handler_t *handler = &seg->handler;
        int left = seg->len;

        if (likely(((uint64_t)handler->ptr % PAGE_SIZE == 0 &&  left % PAGE_SIZE == 0))
                        || ((left + (uint64_t)handler->ptr % PAGE_SIZE) <= PAGE_SIZE)) {

                sgl->offset = 0;
                sgl->sgl_base = handler->ptr;
                sgl->sgl_len = left;
                sgl->phyaddr = handler->phyaddr;

                *sgl_count = 1;
                return 0;
        } else {
                YASSERT(0);
        }

        return 0;
}

//#endif
