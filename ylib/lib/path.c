#include "config.h"

#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>

#define DBG_SUBSYS S_LIBYLIB

#include "sysutil.h"
#include "adt.h"
#include "sysy_lib.h"
#include "ypath.h"
#include "dbg.h"

void rorate_id2path(char *path, uint32_t pathlen, uint32_t pathlevel,
                    const char *id)
{
        int j, k;
        char chk[9], *p;
        uint32_t i, len, hash;

        j = 0;
        chk[8] = '\0';
        p = path;
        len = pathlen;

        for (i = 0; i < pathlevel; i++) {
                for (k = 0; k < 8; k++) {
                        if (id[j] == '\0')
                                j = 0;

                        chk[k] = id[j++];
                }

                hash = hash_str(chk);

                DBUG("chk[%d] %s hash (%u)\n", i, chk, hash);

                hash %= DIR_SUB_MAX;

                snprintf(p, len, "/%u", hash);

                while (*p != '\0') {
                        p++;
                        len--;
                }
        }

        snprintf(p, len, "/%s", id);
}

void cascade_id2path(char *path, uint32_t pathlen, uint64_t _id)
{
        uint64_t id, dirid;
        char dpath[MAX_PATH_LEN], cpath[MAX_PATH_LEN];

        id = _id;

        cpath[0] = '\0';

        while (id > DIR_SUB_MAX) {
                dirid = id % DIR_SUB_MAX;
                id /= DIR_SUB_MAX;

                snprintf(dpath, MAX_PATH_LEN, "%llu%s", (LLU)dirid, cpath);
                snprintf(cpath, MAX_PATH_LEN, "/%s", dpath);
        }

        snprintf(path, pathlen, "%s", cpath);
}

int cascade_path2idver(const char *_path, uint64_t *id, uint32_t *version)
{
        int ret;
        const char *path;
        char *head, *tail;
        uint64_t _id, i = 1;

        ret = ymalloc((void **)&head, MAX_PATH_LEN);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        path = _path;
        _id = 0;

        while(*++path);

        //get version
        tail = head + MAX_PATH_LEN - 2;
        while (srv_running) {
                *--tail = *path--;

                if (*path == 'v') {
                        path -= 2;
                        break;
                }

                if (path == _path) {
                        ret = EINVAL;
                        GOTO(err_free, ret);
                }
        }

        *version = strtoul(tail, NULL, 0);

        //get id
        while (srv_running) {
                _memset((void *)head, 0x0, MAX_PATH_LEN);
                tail = head + MAX_PATH_LEN - 2;

                while(*path != '/')
                        *--tail = *path--;

                _id += strtoul(tail, NULL, 0) * i;
                i *= 1024;

                if (path-- == _path)
                        break;
        }

        *id = _id;
        yfree((void **)&head);

        return 0;
err_free:
        yfree((void **)&head);
err_ret:
        return ret;
}

int cascade_iterator(char *path, void *args, int (*callback)(const char *path, void *args))
{
        int ret;
        struct stat statbuf;
        struct dirent *dirp;
        char childpath[MAX_PATH_LEN], *name;
        DIR *dp;
        uint64_t id;
        uint32_t version;

        ret = lstat(path, &statbuf);
        if (ret < 0) {
                ret = -ret;
                GOTO(err_ret, ret);
        }

        name = strrchr(path, '/');

        if (name == NULL)
                return 0;

        DBUG("name %s\n", name);

#ifdef __x86_64__
        ret = sscanf(name, "/%lu_v%u", &id, &version);
#else
        ret = sscanf(name, "/%llu_v%u", &id, &version);
#endif
        if (ret == 2) {
                ret = callback(path, args);
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                return 0;
        }

        DBUG("Enter dir: %s\n", path);
        dp = opendir(path);
        if (dp == NULL) {
                ret = errno;
                GOTO(err_ret, ret);
        }

        while ((dirp = readdir(dp)) != NULL) {
                if (strcmp(dirp->d_name, ".") == 0 ||
                    strcmp(dirp->d_name, "..") == 0)
                        continue;

                snprintf(childpath, MAX_PATH_LEN,
                         "%s/%s", path, dirp->d_name);

                ret = cascade_iterator(childpath, args, callback);
                if (unlikely(ret))
                        GOTO(err_dir, ret);
        }

        closedir(dp);

        return 0;
err_dir:
        closedir(dp);
err_ret:
        return ret;
}

int path_build(char *dpath)
{
        int ret;
        char *sla;

        sla = strchr(dpath + 1, '/');

        while (sla) {
                *sla = '\0';

                ret = access(dpath, F_OK);
                if (ret == -1) {
                        ret = errno;
                        if (ret == ENOENT) {
                                ret = mkdir(dpath, 0755);
                                if (ret == -1) {
                                        ret = errno;
                                        if (ret != EEXIST) {
                                                DERROR("mkdir(%s, ...) ret (%d) %s\n",
                                                       dpath, ret, strerror(ret));
                                                GOTO(err_ret, ret);
                                        }
                                }
                        } else
                                GOTO(err_ret, ret);
                }

                *sla = '/';
                sla = strchr(sla + 1, '/');
        }

        ret = access(dpath, F_OK);
        if (ret == -1) {
                ret = errno;
                if (ret == ENOENT) {
                        ret = mkdir(dpath, 0755);
                        if (ret == -1) {
                                ret = errno;
                                GOTO(err_ret, ret);
                        }
                } else
                        GOTO(err_ret, ret);
        }

        return 0;
err_ret:
        return ret;
}

int path_validate(const char *path, int isdir, int dircreate)
{
        int ret, len, i;
        char dpath[MAX_PATH_LEN];
        char *end, *sla, *str;

        if (path == NULL
            || (path[0] != '/' /* [1] */)) {
                ret = EFAULT;
                GOTO(err_ret, ret);
        }

        len = _strlen(path) + 1;
        end = (char *)path + len;    /* *end == '\0' */

        if (!isdir && path[len - 2] == '/') {    /* "/file_name/" */
                ret = EINVAL;
                GOTO(err_ret, ret);
        }

        sla = (char *)path;
        while (sla < end && sla[1] == '/')   /* remove leading redundant '/' */
                sla++;

        if (sla == end)         /* all '/', -> "/////" */
                goto out;

        /* non-NULL for above [1] */
        str = strrchr(path, '/');
        while (str > sla && str[-1] == '/')
                str--;

        if (sla == str && !isdir)       /* "/file_name" */
                goto out;

        if (isdir) {    /* *end == '\0' */
                ;
        } else {        /* *end == '/' */
                end = str;
        }

        i = 0;
        while (1) {
                if (i == MAX_PATH_LEN) {
                        ret = ENAMETOOLONG;
                        GOTO(err_ret, ret);
                }

                while (sla < end && sla[0] == '/' && sla[1] == '/')
                        sla++;
                if (sla == end)
                        break;

                dpath[i] = *sla;
                i++;
                sla++;
        }
        dpath[i] = '\0';

        ret = access((const char *)dpath, F_OK);
        if (ret == -1) {
                ret = errno;

                if (ret == ENOENT && dircreate)
                        ret = path_build(dpath);

                if (unlikely(ret)) {
                        DBUG("validate(%s(%s), %s dir, %s create) (%d) %s\n",
                             path, dpath, isdir ? "is" : "not",
                             dircreate ? "" : "no", ret, strerror(ret));

                        if (ret != EEXIST) {
                                DWARN("path %s\n", path);
                                GOTO(err_ret, ret);
                        }
                }
        }

out:
        return 0;
err_ret:
        return ret;
}

/* deepth first */
int scan_one_dir_df(char *d_name, off_t d_off, void (*func)(void *),
                    df_dir_list_t *dflist)
{
        int ret;
        DIR *dp;
        struct dirent *de;
        char dpath[MAX_PATH_LEN];
        struct stat stbuf;
        uint32_t len;
        df_dir_list_t *nextde, *deepde;

        dp = opendir(d_name);
        if (dp == NULL) {
                ret = errno;
                if (ret == ENOENT)
                        goto out0;
                else {
                        DWARN("opendir(%s, ...) ret (%d) %s\n", d_name, ret,
                              strerror(ret));
                        GOTO(err_ret, ret);
                }
        }

        seekdir(dp, d_off);

        while ((de = readdir(dp)) != NULL) {
                if (_strcmp(".", de->d_name) == 0
                    || _strcmp("..", de->d_name) == 0)
                        continue;

                snprintf(dpath, MAX_PATH_LEN, "%s/%s", d_name, de->d_name);

                ret = stat(dpath, &stbuf);
                if (ret == -1) {
                        ret = errno;
                        DWARN("stat(%s, ...) ret (%d) %s\n", dpath,
                              ret, strerror(ret));
                        GOTO(err_dp, ret);
                }

                func(dpath);

                if (!S_ISDIR(stbuf.st_mode))
                        continue;
                else
                        break;
        }

        if (de == NULL)
                goto out;

        len = sizeof(df_dir_list_t) + _strlen(d_name) + 1;

        ret = ymalloc((void **)&nextde, len);
        if (unlikely(ret)) {
                DWARN("ymalloc ret (%d) %s\n", ret, strerror(ret));
                GOTO(err_dp, ret);
        }

        INIT_LIST_HEAD(&nextde->list);
        nextde->d_off = de->d_off;
        snprintf(nextde->d_name, _strlen(d_name) + 1, "%s", d_name);

        list_add(&nextde->list, &dflist->list);

        len = sizeof(df_dir_list_t) + _strlen(dpath) + 1;

        ret = ymalloc((void **)&deepde, len);
        if (unlikely(ret)) {
                DWARN("ymalloc ret (%d) %s\n", ret, strerror(ret));
                GOTO(err_de, ret);
        }

        INIT_LIST_HEAD(&deepde->list);
        deepde->d_off = 0;
        snprintf(deepde->d_name, _strlen(dpath) + 1, "%s", dpath);

        list_add(&deepde->list, &dflist->list);

out:
        ret = closedir(dp);
        if (ret == -1) {
                ret = errno;
                DWARN("closedir(%s) ret (%d) %s\n", d_name, ret,
                      strerror(ret));
                GOTO(err_ret, ret);
        }
out0:

        return 0;
err_de:
        (void) yfree((void **)&nextde);
err_dp:
        (void) closedir(dp);
err_ret:
        return ret;
}

/*
 * deepth first
 *
 * basedir will not be processed by func
 */
int df_iterate_tree(const char *basedir, df_dir_list_t *dflist,
                    void (*func)(void *))
{
        int ret;
        char d_name[MAX_PATH_LEN];
        off_t d_off;
        df_dir_list_t *nextde;

        snprintf(d_name, MAX_PATH_LEN, "%s", basedir);
        d_off = 0;

        INIT_LIST_HEAD(&dflist->list);

        while (srv_running) {
                ret = scan_one_dir_df(d_name, d_off, func, dflist);
                if (unlikely(ret)) {
                        DWARN("ret (%d) %s\n", ret, strerror(ret));
                        GOTO(err_ret, ret);
                }

                //func(d_name);

                if (list_empty(&dflist->list))
                        break;

                nextde = list_entry(dflist->list.next, df_dir_list_t, list);

                list_del_init(&nextde->list);

                snprintf(d_name, MAX_PATH_LEN, "%s", nextde->d_name);
                d_off = nextde->d_off;
        }

        return 0;
err_ret:
        return ret;
}

/**
 * @param is_vol -1:0:1
 * @note /./aa
 */
int path_getvolume(char *path, int *is_vol, char *vol_name)
{
        int i, len, begin, end;
        enum { VOL_NULL, VOL_BEGIN, VOL_END, VOL_NEXT } state;

        /* @init */
        *is_vol = - 1;
        vol_name[0] = '\0';

        if (path == NULL)
                goto out;

        DBUG("path %s\n", path);

        len = strlen(path);

        state = VOL_NULL;
        begin = end = -1;
        for (i = 0; i <  len && state < VOL_NEXT; ++i) {
                switch (state) {
                case VOL_NULL:
                        if (path[i] == '/' ||
                            (path[i] == '.' && i < len - 1 && path[i+1] == '/'))
                                continue;

                        *is_vol = 1;
                        begin = i;
                        state = VOL_BEGIN;
                        break;
                case VOL_BEGIN:
                        if (path[i] == '/') {
                                end = i - 1;
                                state = VOL_END;
                        } else if (i == len -1) {
                                end = i;
                                state = VOL_NEXT;
                        }
                        break;
                case VOL_END:
                        if (path[i] != '/') {
                                *is_vol = 0;
                                state = VOL_NEXT;
                        }
                        break;
                case VOL_NEXT:
                        break;
                }
        }

        if (begin == -1 || end == -1)
                goto out;

        len = end - begin + 1;
        strncpy(vol_name, path+begin, len);
        vol_name[len] = '\0';

out:
        return 0;
}

int path_droptail(char *path, int len)
{
        while (len > 1) {
                if (path[len - 1] == '/') {
                        path[len - 1] = '\0';
                        len--;
                } else
                        break;
        }

        return len;
}

int path_drophead(char *path)
{
        int i = 0, len;

        while (path[i] != '\0') {
                if (path[i] != '/')
                        break;

                i++;
        }

        len = strlen(path);

        if (i) {
                ARRAY_POP(path, i, len - i);
        }

        return len - i;
}

void __rmrf(void *arg)
{
        int ret;
        struct stat stbuf;
        char *path = arg;

        DBUG("%s\n", path);

        ret = stat(path, &stbuf);
        if (ret < 0) {
                ret = errno;
                GOTO(err_ret, ret);
        }

        if (S_ISREG(stbuf.st_mode)) {
                DBUG("remove %s\n", path)
                        ret = unlink(path);
                if (ret < 0) {
                        ret = errno;
                        GOTO(err_ret, ret);
                }
        } else if (S_ISDIR(stbuf.st_mode)) {
                DBUG("keep %s\n", path)
                        }

        return ;
err_ret:
        return ;
}

int rmrf(const char *path)
{
        int ret;
        df_dir_list_t bflist;

        ret = df_iterate_tree(path, &bflist, __rmrf);
        if (unlikely(ret)) {
                fprintf(stderr, "ret (%d) %s\n", ret, strerror(ret));
                GOTO(err_ret, ret);
        }

        return 0;
err_ret:
        return ret;
}

/**
 * normalize path
 *
 * @param path src
 * @param path2 target
 * @return 0
 */
int path_normalize(const char *path, char *path2)
{
        int i, len, begin, off, flag;

        len = strlen(path);
        off = 0;
        begin = -1;
        flag = 0;
        for(i = 0; i < len; ++i) {
                if (path[i] == '/') {
                        if (begin == -1) {
                                continue;
                        }
                        path2[off++] = '/';
                        strncpy(path2 + off, path + begin, i - begin);
                        off += i - begin;
                        begin = -1;
                } else {
                        flag = 1;
                        if (begin == -1) {
                                begin = i;
                        }
                }
        }

        if (begin != -1 && begin < i) {
                path2[off++] = '/';
                strncpy(path2 + off , path + begin , i - begin);
                off += i - begin;
        }

        if (flag == 0) {
                path2[0] = '/';
                off = 1;
        }
        path2[off] = '\0';

        return 0;
}

int path_encode(const char *pool, const char *protocol, const char *vol, const char *snap, char *path) {
        int ret;

        if (pool == NULL || pool[0] == '\0') {
                ret = EINVAL;
                GOTO(err_ret, ret);
        }

        if (vol == NULL || vol[0] == '\0') {
                sprintf(path, "%s/%s", pool, protocol);
        } else {
                if (snap == NULL || snap[0] == '\0') {
                        sprintf(path, "%s/%s/%s", pool, protocol, vol);
                } else {
                        sprintf(path, "%s/%s/%s@%s", pool, protocol, vol, snap);
                }
        }

        return 0;
err_ret:
        return ret;
}

inline int path_encode2(const char *pool, const char *protocol, char *path) {
        return path_encode(pool, protocol, NULL, NULL, path);
}

inline int path_encode3(const char *pool, const char *protocol, const char *vol, char *path) {
        return path_encode(pool, protocol, vol, NULL, path);
}

/**
 * state:
 * - 0: to find pool
 * - 1: to find protocol
 * - 2: to find vol
 * - 3: to find snap
 *
 * @param path
 * @param pool
 * @param protocol
 * @param vol
 * @param snap
 * @return
 */
int path_decode(const char *path, char *pool, char *protocol, char *vol, char *snap) {
        char _path[MAX_PATH_LEN];
        int state = 0;
        char sep, *p;
        int begin = -1, i;

        path_normalize(path, _path);

        if (pool) pool[0] = '\0';
        if (protocol) protocol[0] = '\0';
        if (vol) vol[0] = '\0';
        if (snap) snap[0] = '\0';

        sep = '/';
        size_t len = strlen(_path);
        for (i=0; i < len; i++) {
                if (_path[i] == sep) {
                        if (begin == -1)
                                continue;

                        // end component
                        if (state == 0) {
                                p = pool;
                        } else if (state == 1) {
                                p = protocol;
                        } else if (state == 2) {
                                p = vol;
                        } else if (state == 3) {
                                p = snap;
                        } else {
                                begin = -1;
                                break;
                        }

                        if (p) {
                                strncpy(p, _path + begin, i - begin);
                                p[i-begin] = '\0';
                        }

                        // next component
                        begin = -1;
                        state++;
                } else {
                        // begin component
                        if (begin == -1) {
                                begin = i;
                                if (state == 2) {
                                        sep = '@';
                                } else if (state == 3) {
                                        sep = '/';
                                }
                        }
                }
        }

        if (state < 4 && begin != -1 && begin < i) {
                // end component
                if (state == 0) {
                        p = pool;
                } else if (state == 1) {
                        p = protocol;
                } else if (state == 2) {
                        p = vol;
                } else if (state == 3) {
                        p = snap;
                }

                if (p) {
                        strncpy(p, _path + begin, i - begin);
                        p[i-begin] = '\0';
                }
        }

        // DINFO("_path=%s\n", _path);

#if 0
        YASSERT(_path[0] == '/');

        char *p = strchr(_path + 1, '/');
        if (p != NULL) {
                *p = '\0';
                strcpy(pool, _path + 1);
                strcpy(path2, p+1);
        } else {
                strcpy(pool, _path + 1);
                path2[0] = '\0';
        }
#endif

        return 0;
}

int path_head(const char *path, int sep, char *head, char *path2) {
        char _path[MAX_PATH_LEN];
        int begin = -1, i;
        int found = 0;

        path_normalize(path, _path);

        if (head)
                head[0] = '\0';

        sep = '/';
        size_t len = strlen(_path);
        for (i=0; i < len; i++) {
                if (_path[i] == sep) {
                        if (begin == -1)
                                continue;

                        strncpy(head, _path + begin, i - begin);
                        head[i - begin] = '\0';
                        strcpy(path2, _path + i);
                        found = 1;
                        break;
                } else {
                        if (begin == -1) {
                                begin = i;
                        }
                }
        }

        if (!found && begin != -1 && begin < i) {
                strncpy(head, _path + begin, i - begin);
                head[i - begin] = '\0';
                strcpy(path2, _path + i);
                found = 1;
        }

        return 0;
}

#define CHKID_POOL_SEPARATOR '/'

/**
 * split path to pool amd path.
 * eg:/pool/nbd/vol2 --> pool  /nbd/vol2
 *
 * @param path src
 * @param pool target
 * @param path2 target
 * @return 0
 */
int path_split(const char *path, char *pool, char *path2)
{
        int ret;
        char _path2[MAX_PATH_LEN];
        char *ptr;

        if (path[0] == '/') {
                ret = path_head(path, '/', pool, _path2);
                if (ret) {
                        GOTO(err_ret, ret);
                }

                if (!strlen(pool)) {
                        ret = EINVAL;
                        GOTO(err_ret, ret);
                }

                path_normalize(_path2, path2);
        } else {
                strcpy(path2, path);
                ptr = strrchr(path2, CHKID_POOL_SEPARATOR);
                if (ptr) {
                        strcpy(pool, ++ptr);
                }
        }

        return 0;
err_ret:
        return ret;
}

int path_access(const char *path)
{
        int ret;
        ret = access(path, F_OK);
        if (ret < 0) {
                ret = errno;
                DBUG("%s access ret %d\n", path, ret);
                if (ret == ENOENT)
                        goto err_ret;
                else
                        GOTO(err_ret, ret);
        }

        return 0;
err_ret:
        return ret;
}
