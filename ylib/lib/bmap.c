#include <time.h>
#include <limits.h>
#include <stdarg.h>
#include <errno.h>

#define DBG_SUBSYS S_LIBYLIB

#include "sysy_lib.h"
#include "bmap.h"
#include "dbg.h"

#define SETBIT(a, n) (a[n/CHAR_BIT] |= (1<<(n%CHAR_BIT)))
#define DELBIT(a, n) (a[n/CHAR_BIT] &= (~(1<<(n%CHAR_BIT))))
#define GETBIT(a, n) (a[n/CHAR_BIT] &  (1<<(n%CHAR_BIT)))

int bmap_create(bmap_t *bmap, uint32_t size)
{
        int ret;
        size_t len;

        if (size > UINT32_MAX) {
                ret = EINVAL;
                GOTO(err_ret, ret);
        }

        /* @init */
        len = size / CHAR_BIT + (size % CHAR_BIT ?  CHAR_BIT: 0);

        ret = ymalloc((void **)&bmap->bits, len);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        DINFO("len %llu, size %u\n", (LLU)len, size);

        _memset(bmap->bits, 0x0, len);

        bmap->nr_one = 0;
        bmap->len = len;
        bmap->size = size;
        bmap->idx = 0;

        return 0;
err_ret:
	return ret;
}

void bmap_destroy(bmap_t *bmap)
{
        if (bmap->bits) {
                yfree((void **)&bmap->bits);
                bmap->bits = NULL;
        }
}

int bmap_set(bmap_t *bmap, uint32_t off)
{
        DBUG("set %llu\n", (LLU)off);

        if (bmap_get(bmap, off))
                return EEXIST;
        SETBIT(bmap->bits, off%bmap->size);

        DBUG("bmap %p idx %llu value '%x'\n", bmap, (LLU)off, bmap->bits[(off%bmap->size) / CHAR_BIT]);

        bmap->nr_one++;
	return 0;
}

/** 
 * @retval 0
 * @retval 1
 */
int bmap_get(const bmap_t *bmap, uint32_t off)
{
        return GETBIT(bmap->bits, off%bmap->size);
}

int bmap_del(bmap_t *bmap, uint32_t off)
{
        if (likely(bmap_get(bmap, off))) {
                DELBIT(bmap->bits, off%bmap->size);
                bmap->nr_one--;
                return 0;
        } else
                return ENOENT;
}

int bmap_full(const bmap_t *bmap)
{
        return (bmap->size == bmap->nr_one) ? 1 : 0;
}

void bmap_load(bmap_t *bmap, char *opaque, int len)
{
        int i;

        if ((uint32_t)len > UINT32_MAX) {
                YASSERT(0);
        }

        bmap->bits = opaque;
        bmap->size = len * CHAR_BIT;
        bmap->nr_one = 0;
        bmap->len = len;

        bmap->idx = 0;
        for (i = 0; i < (int)bmap->size; i++) {
                if (bmap_get(bmap, i))
                        bmap->nr_one++;
        }
}

int bmap_get_empty(bmap_t *bmap)
{
        uint32_t idx, j, i;
        unsigned char c;

        if (bmap_full(bmap)) 
                return -1;

        //idx = _random() % bmap->size / CHAR_BIT;
        idx = bmap->idx / CHAR_BIT;

        for (i = 0; i < bmap->size / CHAR_BIT; i++) {
                c = bmap->bits[(i + idx) % (bmap->size / CHAR_BIT)];

                DBUG("bmap %p idx %llu value '%x'\n", bmap, (LLU)(i + idx) % bmap->size, c);

                if (c != 0xff) {
                        for (j = 0; j < CHAR_BIT; j++) {
                                if (((1 << j) & c) == 0) {
                                        bmap->idx = j + ((i + idx) % (bmap->size / CHAR_BIT)) * CHAR_BIT;
                                        DBUG("bmap %p empty %llu j %llu i %llu\n",
                                             bmap, (LLU)bmap->idx, (LLU)j, (LLU)i);
                                        YASSERT(bmap->idx < bmap->size);
                                        return bmap->idx;
                                }
                        }
                }
        }

        return -1;
}

int bmap_continuous(bmap_t *bmap, int start)
{
        int i;

        for (i = start; i < bmap->size; i++) {
                if (bmap_get(bmap, i) == 0)
                        break;
        }

        return i;
}

void bmap_clean(bmap_t *bmap)
{
        _memset(bmap->bits, 0x0, bmap->len);

        bmap->nr_one = 0;
}

int bmap_range_empty(bmap_t *bmap, uint32_t _from, uint32_t _to)
{
        uint32_t i, from, to;

        from = _from / CHAR_BIT;
        to = _to / CHAR_BIT;

        for (i = from; i < to; i++) {
                if (bmap->bits[i]) {
                        DBUG("from %llu to %llu not empty\n", (LLU)_from, (LLU)_to);
                        return 0;
                }
        }

        DBUG("from %llu to %llu is empty\n", (LLU)_from, (LLU)_to);

        return 1;
}
