#include "config.h"

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/timerfd.h>
#include <sys/eventfd.h>
#include <pthread.h>
#include <sys/eventfd.h>
#include <signal.h>

#define DBG_SUBSYS S_LIBYLIB

#include "sysy_lib.h"
#include "job_dock.h"
#include "configure.h"
#include "timer_worker.h"
#include "ylock.h"
#include "adt.h"
#include "tpool.h"
#include "dbg.h"

typedef struct {
        int fd;
        char name[MAX_NAME_LEN];
        timer_worker_exec_t exec;
        timer_worker_queue_t queue;
        void *ctx;
} event_t;

typedef struct {
        tpool_t tpool;
        int epoll_fd;
        event_t *array[0];
} timer_worker_t;

//extern uint64_t nofile_max;
static timer_worker_t *timer_worker;

//timer_worker_handler_t jobtracker;

#define EPOLL_TMO 30

static int __timer_worker_add(int fd)
{
        int ret;
        struct epoll_event ev;

        ev.data.fd = fd;
        ev.events = EPOLLIN | EPOLLET;
        ret = _epoll_ctl(timer_worker->epoll_fd, EPOLL_CTL_ADD, fd, &ev);
        if (ret == -1) {
                ret = errno;
                GOTO(err_ret, ret);
        }

        return 0;
err_ret:
        return ret;
}

inline static int __timer_worker_del(int fd)
{
        int ret;
        struct epoll_event ev;

        ev.data.fd = fd;
        ev.events = EPOLLIN | EPOLLET;
        ret = _epoll_ctl(timer_worker->epoll_fd, EPOLL_CTL_DEL, fd, &ev);
        if (ret == -1) {
                ret = errno;
                GOTO(err_ret, ret);
        }

        return 0;
err_ret:
        return ret;
}

static void *__timer_worker(void *_args)
{
        int ret, nfds, retry;
        struct epoll_event ev;
        event_t *event;
        uint64_t notify;

        (void) _args;

        while (1) {
                ret = tpool_wait(&timer_worker->tpool);
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                while (1) {
                        nfds = _epoll_wait(timer_worker->epoll_fd, &ev, 1, EPOLL_TMO * 1000);
                        if (nfds < 0) {
                                ret = -nfds;
                                GOTO(err_ret, ret);
                        }

                        if (nfds == 0)
                                continue;

                        YASSERT(nfds == 1);

                        event = timer_worker->array[ev.data.fd];

                        ret = __timer_worker_del(event->fd);
                        if (unlikely(ret))
                                GOTO(err_ret, ret);

                        ret = read(event->fd, &notify, sizeof(notify));
                        if (ret < 0) {
                                ret = errno;
                                GOTO(err_ret, ret);
                        }

                        retry = 0;
                retry:
                        ret = tpool_left(&timer_worker->tpool);
                        if (unlikely(ret)) {
                                DWARN("all thread busy, retry %u\n", retry);
                                sleep(1);
                                retry++;
                                goto retry;
                        }

                        event->exec(event->ctx);

                        ret = __timer_worker_add(event->fd);
                        if (unlikely(ret))
                                GOTO(err_ret, ret);

                        tpool_return(&timer_worker->tpool);
                        break;
                }
        }

        return NULL;
err_ret:
        YASSERT(0);
        return NULL;
}

int timer_worker_init(int threads)
{
        int ret, size;

        size = sizeof(timer_worker_t) + sizeof(event_t *) * nofile_max;
        ret = ymalloc((void **)&timer_worker, size);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        memset(timer_worker, 0x0, size);

        timer_worker->epoll_fd = epoll_create(nofile_max);
        if (timer_worker->epoll_fd == -1) {
                ret = errno;
                GOTO(err_ret, ret);
        }

        ret = tpool_init(&timer_worker->tpool, __timer_worker, timer_worker, "timer_worker", threads);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        return 0;
err_ret:
        return ret;
}

int timer_worker_create(timer_worker_handler_t *handler, const char *name,
                        timer_worker_exec_t exec, timer_worker_queue_t queue, void *ctx)
{
        int ret, fd;
        event_t *event;

        fd = timerfd_create(CLOCK_REALTIME, TFD_CLOEXEC | TFD_NONBLOCK);
        if (fd < 0) {
                ret = errno;
                GOTO(err_ret, ret);
        }

        YASSERT((uint64_t)fd < nofile_max);
        YASSERT(timer_worker->array[fd] == NULL);

        ret = ymalloc((void **)&event, sizeof(*event));
        if (unlikely(ret))
                GOTO(err_ret, ret);

        event->fd = fd;
        event->ctx = ctx;
        event->queue = queue;
        event->exec = exec;
        strcpy(event->name, name);
        timer_worker->array[fd] = event;

        tpool_increase(&timer_worker->tpool);

        ret = __timer_worker_add(fd);
        if (unlikely(ret))
                GOTO(err_free, ret);

        handler->fd = fd;

	return 0;
err_free:
        yfree((void **)&event);
err_ret:
        return ret;
}

int timer_worker_settime(const timer_worker_handler_t *handler, uint64_t _time)
{
        int ret;
        struct itimerspec new_value;

        new_value.it_interval.tv_nsec = 0;
        new_value.it_interval.tv_sec = 0;
        new_value.it_value.tv_sec = _time / 1000000;
        new_value.it_value.tv_nsec = _time % 1000000;

        ret = timerfd_settime(handler->fd, 0, &new_value, NULL);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        return 0;
err_ret:
        return ret;
}
