#ifndef __LONGTASK_H__
#define __LONGTASK_H__

#include "sysy_conf.h"

int longtask_init(void);
int longtask_insert(uint64_t *handler, int retval, const char *desc);
int longtask_update(uint64_t id, int retval, const char *desc);
int longtask_get(uint64_t id, int *retval, char *desc);
int longtask_drop(uint64_t id);

#endif
