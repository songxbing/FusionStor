#ifndef __YTIME_H__
#define __YTIME_H__

#define ytime_t uint64_t /*time in microsecond*/

extern uint64_t ytime_gettime(void);
int ytime_getntime(struct timespec *ntime);
void ytime_2ntime(ytime_t ytime, struct timespec *ntime);

#endif
