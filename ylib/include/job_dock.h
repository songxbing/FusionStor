#ifndef __JOB_DOCK_H__
#define __JOB_DOCK_H__

#include <sys/timeb.h>

#include "worker.h"

#define EXEC_DIRECT 1
#define EXEC_INDIRECT 0

typedef int (*net_cmp_func)(const void *, const void *);
typedef void (*net_reset_func)(void *);
typedef const char *(*net_print_func)(const void *);
typedef void (*net_notify_func)(const void *);
typedef int (*net_revoke_func)(job_t *);
typedef int (*remove_job_func)(job_t *job, sy_spinlock_t *lock);
typedef void (*job_timeout_handler_t)(job_t *job);

int  jobdock_init(net_print_func net_print, int allocate);
void jobdock_iterator(void);
uint64_t jobdock_load(void);
void jobdock_load_set(uint64_t load);
int jobdock_worker_create(worker_handler_t *handler, const char *name);

int  job_create(job_t **job, worker_handler_t *jobtracker, const char *name);
void job_destroy(job_t *job);
int job_context_create(job_t *job, size_t size);
int job_get_ret(job_t *job, int idx);
uint64_t job_timermark(job_t *job, const char *stage);
int job_exec(job_handler_t handler, int retval, int direct);
job_handler_t job_handler(job_t *job, int idx);
void job_sleep(job_t *job, suseconds_t usec);

#endif
