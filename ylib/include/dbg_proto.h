#ifndef __YLIB_DBG_PROTO_H__
#define __YLIB_DBG_PROTO_H__

/* debug subsystems (32 bits, non-overlapping) */
#define S_UNDEF                  0x00000000

#define S_LIBYLIB                0x00000001
#define S_LIBYNET                0x00000002
#define S_LIBCLUSTER             0x00000004
#define S_LIBSCHEDULE            0x00000008
#define S_LIBINTERFACE           0x00000010
#define S_LIBLSV                 0x00000020
#define S_LIBREPLICA             0x00000040
#define S_LIBCHUNK               0x00000080
#define S_LIBCONTROL             0x00000100
#define S_LIBSTORAGE             0x00000200
#define S_LIBTASK                0x00000400

#define YFS_DEBUG
#define D_MSG_ON

/* debug masks (32 bits, non-overlapping) */
#define D_BUG           0x00000001
#define D_INFO          0x00000002
#define D_WARNING       0x00000004
#define D_ERROR         0x00000008
#define D_FATAL         0x00000010

#ifndef DBG_SUBSYS
#define DBG_SUBSYS S_UNDEF
#endif
#endif
