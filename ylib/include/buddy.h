#ifndef __BUDDY_H__
#define __BUDDY_H__

typedef struct buddy {
        size_t mpage_count;
        size_t buddy_trees[0];
} buddy_t;

#define BUDDY_SIZE(count) (sizeof(size_t) + count * sizeof(size_t))

int buddy_init(void *buddy_addr, size_t page_num);

int buddy_alloc(void *buddy_addr, size_t size);
size_t buddy_free(void *buddy_addr, size_t offset);

#endif
