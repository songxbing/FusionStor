#ifndef __TIMER_WORKER_H__
#define __TIMER_WORKER_H__

#include <pthread.h>
#include <semaphore.h>

#include "list.h"
#include "job.h"
#include "analysis.h"
#include "ylock.h"

#define TIMER_WORKER_TYPE_TIMER 1
#define TIMER_WORKER_TYPE_SEM 2

typedef struct {
        int fd;
} timer_worker_handler_t;

typedef int (*timer_worker_exec_t)(void *);
typedef int (*timer_worker_queue_t)(void *, const void *);

//extern timer_worker_handler_t jobtracker;

int timer_worker_init(int thread);
int timer_worker_settime(const timer_worker_handler_t *handler, uint64_t _time);
int timer_worker_create(timer_worker_handler_t *handler, const char *name,
                        timer_worker_exec_t exec, timer_worker_queue_t queue, void *ctx);

#endif


