#ifndef __LOCK_TBA__H_
#define __LOCK_TBA__H_

#include "list.h"
#include "hash_table.h"

/**
 * @file Dynamic Lock Table
 *
 * @note 加锁/解锁务必要匹配
 */

typedef struct {
        hashtable_t ht;
        count_list_t free_list;
        char name[MAX_NAME_LEN];
        int inited;
} lock_table_t;

int ltable_init(lock_table_t *lt, const char *name);
int ltable_destroy(lock_table_t *lt);

int ltable_wrlock(lock_table_t *lt, uint64_t idx);
int ltable_rdlock(lock_table_t *lt, uint64_t idx);
int ltable_unlock(lock_table_t *lt, uint64_t idx);
int ltable_trywrlock(lock_table_t *lt, uint64_t idx);
int ltable_tryrdlock(lock_table_t *lt, uint64_t idx);

void * ltable_start_transaction(lock_table_t *lt);

void ltable_transaction_go(lock_table_t *lt, void *saction, uint64_t idx);

void ltable_revert_transaction(lock_table_t *lt, void *saction);

void ltable_stop_transaction(lock_table_t *lt, void *saction);

#endif
