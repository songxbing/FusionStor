#ifndef __BITMAP_H__
#define __BITMAP_H__

#include "sysy_conf.h"
#include "dbg.h"

int bh_init(void);
int bh_register(const char *name, func_t func, void *args, int step);

#endif
