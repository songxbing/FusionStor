#ifndef __POOL_LIST_H__
#define __POOL_LIST_H__

typedef struct {
        struct list_head hook;
        int fd;
        time_t create_time;
        char uuid[UUID_LEN];
} pool_list_t;

int pool_list_init();
int pool_list_newfd(int *_fd);
int pool_list_addfd(const char *uuid, int fd);
int pool_list_lspool(const char *uuid, uint64_t offset, void *de, int *delen);
int pool_list_closefd(const char *uuid);

#endif /* __POOL_LIST_H__ */
