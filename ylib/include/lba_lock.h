#ifndef __LBA_LOCK_H
#define __LBA_LOCK_H 1

int lba_lock_init(hashtable_t *ht);
int lba_lock_destroy(hashtable_t ht);
int lba_lock_wrlock(hashtable_t ht, uint64_t lba);
int lba_lock_rdlock(hashtable_t ht, uint64_t lba);
int lba_lock_unlock(hashtable_t ht, uint64_t lba);

#endif
