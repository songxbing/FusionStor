#ifndef __STRING_BUFFER_H
#define __STRING_BUFFER_H

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "configure.h"

typedef struct __string_buffer {
        char buf[MAX_BUF_LEN];
        int tail;

        void (*push)(struct __string_buffer *sbuf, const char *str);
        void (*clean)(struct __string_buffer *sbuf);
        void (*dump)(struct __string_buffer *sbuf);
} string_buffer;

static inline void __string_buffer_push(string_buffer *sbuf, const char *str) {
        int len = strlen(str);
        assert(len > 0);
        assert(sbuf->tail + 1 + len < MAX_BUF_LEN);

        if (sbuf->tail != 0) {
                sbuf->buf[sbuf->tail] = ',';
                sbuf->tail++;
        }

        strcpy(sbuf->buf + sbuf->tail, str);
        sbuf->tail += len;
}

static inline void __string_buffer_clean(string_buffer *sbuf) {
        memset(sbuf->buf, 0x0, MAX_BUF_LEN);
        sbuf->tail = 0;
}

static inline void __string_buffer_dump(string_buffer *sbuf) {
        printf("%s\n", sbuf->buf);
}

static inline void string_buffer_init(string_buffer *sbuf) {

        memset(sbuf->buf, 0x0, MAX_BUF_LEN);
        sbuf->tail = 0;

        sbuf->push = __string_buffer_push;
        sbuf->clean = __string_buffer_clean;
        sbuf->dump = __string_buffer_dump;
}


#endif