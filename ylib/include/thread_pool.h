#ifndef __THREAD_POOL_H
#define __THREAD_POOL_H

#include <pthread.h>

#include "list.h"

typedef void * (*thread_pool_fn)(void *arg);

typedef enum {
        __TH_STOPPED__,
        __TH_RUNNING__,
        __TH_STOP__,
        __TH_PAUSE__,
} thread_status_t;

typedef struct __tp_thread_t {
        struct list_head hook;

        thread_pool_fn fn;
        void *arg;
        int free_arg;
        int ret;

        void *pool;
        pthread_t th;
        thread_status_t status;

        int (*is_stop)(struct __tp_thread_t *self);
        int (*is_pause)(struct __tp_thread_t *self);
} tp_thread_t;

typedef struct __thread_pool_t {
        count_list_t threads;

        pthread_rwlock_t lock;
        int running;

        int (*add)(struct __thread_pool_t *pool, thread_pool_fn fn, void *arg, int free_arg);

        int (*start)(struct __thread_pool_t *pool);
        int (*stop)(struct __thread_pool_t *pool);
        int (*is_stop)(struct __thread_pool_t *pool, int *stop);
        int (*ensure_stop)(struct __thread_pool_t *pool);
        int (*change_running)(struct __thread_pool_t *pool, int n);
} thread_pool_t;

int thread_pool_init(thread_pool_t *pool);
int thread_pool_destroy(thread_pool_t *pool);

int thread_pool_join(thread_pool_t *pool);


// test

int thread_pool_test();

#endif
