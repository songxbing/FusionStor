#ifndef __SEQUENCE_H__
#define __SEQUENCE_H__

int sequence_get(uint64_t *sequence);
int sequence_init(const char *home);

#endif
