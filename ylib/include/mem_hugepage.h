#ifndef __MEM_HUGEPAGE_H_
#define __MEM_HUGEPAGE_H_


//#define MAX_IO_SIZE (1024 * 1024)  /* 512K bytes */
#define MEMPAGE_SIZE (2 * 1024 * 1024)

typedef struct {
        void *pool;
        void *ptr;
        uint64_t phyaddr;
        uint32_t size;
        int idx;

        void *arg;
        int (*cb)(void *arg);
} mem_handler_t;

/* global mem */

int global_mem_init(int daemon);

int global_private_mem_init();

void *core_private_mem_init(int cpu_id, int core_hash);
void *register_private_static_stor_area(void *private_addr, size_t size, int type);

void *get_static_type_addr(void *private_mem, int type);

void get_global_private_mem(void **addr, uint64_t *size);
void get_global_public_mem(void **addr, uint64_t *size);
// int mem_hugepage_init_prep();
size_t get_core_mempages_size();

int mem_hugepage_new(uint32_t size, mem_handler_t *mem_handler);

int mem_hugepage_ref(mem_handler_t *mem_handler);
int mem_hugepage_deref(mem_handler_t *mem_handler);

/* mempages */
void *mempages_alloc(void *private_addr, uint32_t size);
void mempages_free(void *private_addr, void *addr);

void *huge_tls_malloc(size_t sz);
void huge_tls_free(void *ptr);

// @deprecated

/* mem hugepage */
int mem_hugepage_init();
int mem_hugepage_private_init();  // used by core thread mbuffer_t

void *mem_hugepage_alloc(size_t size, unsigned long *paddr);
void mem_hugepage_free(void *addr);

unsigned long mem_vtophys(void *vaddr);
struct mempage_t *mem_search_hp(void *vaddr);

#endif
