#ifndef __YPAGE_H
#define __YPAGE_H 1

#include <stdint.h>

typedef struct __range_t {
        uint64_t off;
        uint32_t size;
        uint32_t ps;
} range_t;


/**
 * @brief 首尾对齐到ps边界
 *
 * @param ps
 * @param off1
 * @param size1
 * @param _off2
 * @param _size2
 */
static inline void range_align32(uint32_t ps, uint32_t off1, uint32_t size1, uint32_t *_off2, uint32_t *_size2) {
        uint32_t off2;
        uint32_t size2;

        off2 = off1;
        size2 = size1;

        if (off1 % ps) {
                off2 -= off1 % ps;
                size2 += off1 % ps;
        }

        if (size2 % ps) {
                size2 += ps - size2 % ps;
        }

        *_off2 = off2;
        *_size2 = size2;
}

static inline void range_align(uint32_t ps, uint64_t off1, uint32_t size1, uint64_t *_off2, uint32_t *_size2) {
        uint64_t off2;
        uint32_t size2;

        off2 = off1;
        size2 = size1;

        if (off1 % ps) {
                off2 -= off1 % ps;
                size2 += off1 % ps;
        }

        if (size2 % ps) {
                size2 += ps - size2 % ps;
        }

        *_off2 = off2;
        *_size2 = size2;
}

static inline void range_align2(const range_t *range, range_t *new_range) {
        range_align(range->ps, range->off, range->size, &new_range->off, &new_range->size);
}


#endif
