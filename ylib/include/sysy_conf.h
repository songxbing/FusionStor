#ifndef __SYSY_CONST_H__
#define __SYSY_CONST_H__

#include <sys/epoll.h>
#include <unistd.h>

#if 0
# define EPOLL_EDGE
#endif

#ifdef EPOLLRDHUP
# define HAVE_EPOLLRDHUP
#endif

#ifdef EPOLL_EDGE
# ifdef HAVE_EPOLLRDHUP
#  define Y_EPOLL_EVENTS_LISTEN (EPOLLIN | EPOLLERR | EPOLLRDHUP)
#  define Y_EPOLL_EVENTS        (Y_EPOLL_EVENTS_LISTEN | EPOLLET)
# else
#  define Y_EPOLL_EVENTS_LISTEN (EPOLLIN | EPOLLERR)
#  define Y_EPOLL_EVENTS        (Y_EPOLL_EVENTS_LISTEN | EPOLLET)
# endif
#else
# ifdef HAVE_EPOLLRDHUP
#  define Y_EPOLL_EVENTS_LISTEN (EPOLLIN | EPOLLERR | EPOLLRDHUP)
#  define Y_EPOLL_EVENTS        (Y_EPOLL_EVENTS_LISTEN)
# else
#  define Y_EPOLL_EVENTS_LISTEN (EPOLLIN | EPOLLERR)
#  define Y_EPOLL_EVENTS        (Y_EPOLL_EVENTS_LISTEN)
# endif
#endif

#define MAX_USERNAME_LEN        32

#define MIN_PASSWD_LEN          6
#define MAX_PASSWD_LEN          128

#define MAX_PATH_LEN            (1024 * 2)
#define MAX_NAME_LEN            (256)
#define MAX_INFO_LEN            (512)

#define BIG_BUF_LEN             (1024 * 512)

#define PAGE_SIZE               (4096)
#define LICH_BLOCK_SIZE         (512)
#define LICH_IO_ALIGN           PAGE_SIZE

#define RW_TIMEOUT       (20)

#define MAX_OPEN_FILE           100000

/**
 * Block-level checksum
 */
#define YFS_SEG_CHECKSUM        0

#define YFS_SEG_LEN             MAX_BUF_LEN

#define MAX_LINE_LEN            (1024 * 2)

#define BITS_PER_CHAR           8

#define SKIPLIST_MAX_LEVEL      24
#define SKIPLIST_CHKSIZE_DEF    1

#define DIR_SUB_MAX             1024LLU

#define MAX_RETRY               20

#define YFS_LINEAR_DATA_FLOW    0
#define CDS_DATA_CACHE          0

typedef uint32_t chk_off_t;
typedef uint32_t chk_size_t;

typedef unsigned long long LLU;

#define EXPIRED_TIME            60

#ifndef O_NOATIME
# define O_NOATIME              0
#endif

/* BOOL TYPE */

#ifndef BOOL
#define BOOL                    int
#endif

#ifndef TRUE
# define TRUE                   1
#endif

#ifndef FALSE
# define FALSE                  0
#endif

#define SLIST_GROUP 10
#define SLIST_CHUNK_SIZE 1

#define RETRY_ERROR(__ret__)                                            \
        (__ret__ == ETIME || __ret__ == ETIMEDOUT || __ret__ == ENODATA \
         || __ret__ == ENONET || __ret__ == ENOTCONN)

#define SHM_MAX                 (10 * 1024 * 1024)      /* 10M? */

#define EXT3_SUPER_MAGIC        0xEF53
#define EXT4_SUPER_MAGIC        0xEF53
#define REISERFS_SUPER_MAGIC    0x52654973
#define XFS_SUPER_MAGIC         0x58465342
#define JFS_SUPER_MAGIC         0x3153464a

/**
 * Magic in disk
 */
#define YFS_MAGIC               (0x6d12ecaf + 1)
#define META_VERSION0           (YFS_MAGIC + 11)
#define META_VERSION1           (META_VERSION0 + 1)
#define META_VERSION2           (META_VERSION0 + 2)     /* Haven't used yet */

#define LICH_STATUS_PRE          "status"

#define SYS_PAGE_SIZE           4096

#define STEP_MAX                100
#define QUEUE_MAX               1000

#define PIPE_SIZE (4096 * 16)

#define MAX_GROUP_VOLUMES_NUM   (128)
#define MAX_GROUP_NAME_LEN      (MAX_NAME_LEN * (MAX_GROUP_VOLUMES_NUM + 1))
#endif
