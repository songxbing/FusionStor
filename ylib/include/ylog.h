#ifndef __YLOG_H__
#define __YLOG_H__

#include <stdint.h>
#include <linux/limits.h>
#include <semaphore.h>

#include "ylock.h"
#include "sysy_conf.h"

typedef enum {
        YLOG_STDERR,
        YLOG_FILE,
        YLOG_SYSLOG,
} logmode_t;

typedef enum {
        YLOG_TYPE_STD, /*standard log optput*/
        YLOG_TYPE_PERF, /*performance log optput*/
        YLOG_TYPE_BALANCE, /*balance log type*/
        YLOG_TYPE_RAMDISK, /*ramdisk log type, record each io crc */
        YLOG_TYPE_MAX,
}logtype_t;

typedef struct {
        char file[MAX_PATH_LEN];
        int logfd;
        int count;
        int isparent;
        uint64_t size;
        time_t time;
        pthread_rwlock_t lock;
        sy_spinlock_t spinlock;
        logmode_t logmode;
} ylog_t;

extern ylog_t (*g_ylogs)[];

extern int dmsg_init();
extern int syslog_init();
extern int ylog_init(logmode_t, char file[][MAX_PATH_LEN]);
extern int ylog_destroy(logtype_t logtype);
extern int ylog_write(logtype_t logtype, const char *msg);
extern int ylog_reset(logtype_t logtype);
extern int ylog_set_parent(logtype_t logtype, int parent);

#define YROC_ROOT "/dev/shm/sysy/proc"
#define FILEMODE (S_IREAD | S_IWRITE | S_IRGRP | S_IROTH)

int yroc_create(const char *, int *);
int yroc_write(int, const void *, size_t);

#endif
