#ifndef __BMAP_H
#define __BMAP_H

#include <stdlib.h>
#include <sys/types.h>
#include <stdint.h>

typedef struct {
        // 1的个数
        uint32_t nr_one;
        // bit数
        uint32_t size;
        // 字符数
        uint32_t len;
        // 分配空位置的游标
        uint32_t idx;
        // 数据
        char *bits;
} bmap_t;

int bmap_create(bmap_t *bmap, uint32_t size);
void bmap_destroy(bmap_t *bmap);
void bmap_clean(bmap_t *bmap);

int bmap_set(bmap_t *bmap, uint32_t off);
int bmap_get(const bmap_t *bmap, uint32_t off);
int bmap_del(bmap_t *bmap, uint32_t off);

int bmap_get_empty(bmap_t *bmap);
int bmap_continuous(bmap_t *bmap, int start);

int bmap_full(const bmap_t *bmap);
int bmap_range_empty(bmap_t *bmap, uint32_t from, uint32_t to);

void bmap_load(bmap_t *bmap, char *opaque, int len);


#endif
