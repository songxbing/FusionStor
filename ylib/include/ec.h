#ifndef __EC_H__
#define __EC_H__

#include "buffer.h"
#include "sysy_lib.h"
#include "dbg.h"

//m = k + r
#define EC_MMAX 6
#define EC_KMAX 5

#define STRIP_BLOCK 4096
#define STRIP_ALIGN  4096
#define STRIP_MAX (LICH_CHUNK_SPLIT/STRIP_BLOCK)
#define STRIP_SIZE(ec) ((ec)->k * STRIP_BLOCK)

typedef enum {
    PLUGIN_NULL,
    PLGUIN_EC_ISA,
    PLGUIN_MAX
} ec_plugin_t;

typedef enum {
    TECH_NULL,
    TECH_ISA_SSE,
} ec_technique_t;

#pragma pack(8)
typedef struct {
        uint8_t m:5;
        uint8_t plugin:3;
        uint8_t k:5;
        uint8_t tech:3;
        uint32_t log_size:8;
        uint32_t data_size:24;
} ec_t;
#pragma pack()

#if ENABLE_EC
#define EC_ISEC(ec) ((ec) && (ec)->plugin == PLGUIN_EC_ISA)
#else
#define EC_ISEC(ec) ((ec) && 0)
#endif
#define EC_ISLOG(ec) ((ec) && (ec)->log_size != 0)


typedef struct {
        uint8_t idx;
        uint32_t offset;
        uint32_t count;
        buffer_t buf;
} ec_strip_t;

int ec_encode(char **data, char **coding, int blocksize, int m, int k);
int ec_decode(unsigned char *src_in_err, char **data, char **coding, int blocksize, int m, int k);

//uint8_t technique=reed_sol_van;
#endif
