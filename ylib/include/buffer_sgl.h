int mbuffer_is_aligned(buffer_t *buf, int align);
int mbuffer_trans_sgl(void *_sgl, int *sgl_count, seg_t *seg);  /*to remove coupling*/
int mbuffer_sg_trans_sgl(void *__sgl, int *sgl_count, seg_t *seg);