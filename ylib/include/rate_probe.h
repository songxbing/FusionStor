#ifndef __RATE_PROBE_H_
#define __RATE_PROBE_H_

#include <sys/types.h>

#include "ylock.h"

typedef struct {
        sy_rwlock_t lock;
        uint64_t rate;
        uint64_t tally;
        uint64_t unit;  //rate compute unit
        suseconds_t useconds;
} rate_probe_t;

int rate_probe_init(rate_probe_t* probe, uint64_t compute_unit);
int rate_probe_set_compute_uint(rate_probe_t *probe, uint64_t compute_unit);
int rate_probe_increase(rate_probe_t *probe, uint64_t n);
int rate_probe_detect(rate_probe_t *probe, uint64_t *rate);

#endif
