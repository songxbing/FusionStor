#ifndef __JOB_H__
#define __JOB_H__

#include <stdint.h>
#include <sys/types.h>
#include <stdint.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdlib.h>

#include "ytime.h"
#include "list.h"
#include "yid.h"
#include "ylock.h"
#include "buffer.h"
#include "sysy_conf.h"
#include "dbg.h"

typedef enum {
        //STATUS_NULL,
        STATUS_PREP,
        STATUS_WAIT,
        STATUS_RESUME,
        STATUS_DONE,
} job_status_t;

#pragma pack(8)

typedef struct {
        uint32_t jobid;
        uint16_t seq;
        uint16_t idx;
} job_handler_t;

#pragma pack()

/*network iocb*/
typedef struct {
        struct list_head hook;
        //nio_priority_t priority;
        int offset;
        buffer_t buf;
} niocb_t;

#define JOB_NAME_LEN 36
#define JOB_CONTEXT_LEN 4096

typedef struct __job {
        uint32_t status;
        uint16_t retry;
        void *context;
        void *worker;
        void (*state_machine)(struct __job *, int *keep);
        char name[JOB_NAME_LEN];
        char __context__[JOB_CONTEXT_LEN];

        //sem_t sem;
        uint16_t steps;
        struct timeval queue_time;
        struct {
                struct timeval create; /*create time*/
                struct timeval step; /*prev step time*/
        } timer;
} job_t;

static inline void job_setname(job_t *job, const char *name)
{
        if (job) {
                strncpy(job->name, name, JOB_NAME_LEN);
        }
}

#endif
