/*
 * =====================================================================================
 *
 *       Filename:  hostcache.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  04/11/2011 05:51:58 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *        Company:  
 *
 * =====================================================================================
 */
#ifndef HOST_CACHE_H_
#define HOST_CACHE_H_
#include "sysy_lib.h"
#include "dbg.h"

typedef struct hostentry_s {
        char *nm;
        int len;
        int lkn;
} hostentry_t;

typedef struct hostmcache_s {
        hashtable_t hashtb;
} hostmcache_t;

extern hostmcache_t hostcache;

int hostmcache_compare(const void *d1, const void *d2);
uint32_t hostmcache_hash(const void *d1);
int hostmcache_init(hostmcache_t *hostcache);
int hostmcache_insert(hostmcache_t *hostcache, hostentry_t *ent);
int hostmcache_delete(hostmcache_t *hostcache, hostentry_t *ent, hostentry_t **retval);
void hostmcache_find(hostmcache_t *hostcache, hostentry_t *ent, hostentry_t **retval);
int hostentry_destroy(hostentry_t *entry);
#endif
