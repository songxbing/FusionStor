#ifndef __STAT_CACHE_H__
#define __STAT_CACHE_H__

#include <rpc/rpc.h>
#include <stdint.h>

#include "fh.h"
#include "job.h"
#include "nfs_state_machine.h"

extern int stat_mcache_create(void);
extern int stat_mcache_get(uint32_t dev, uint64_t ino, struct stat *stbuf);
extern int stat_mcache_add(uint32_t dev, uint64_t ino, struct stat *stbuf);
extern int stat_mcache_drop(uint32_t dev, uint64_t ino);
extern int stat_mcache_fget(uint32_t dev, uint64_t ino, const char *__path,
                           struct stat *stbuf);
extern int stat_mcache_reload(uint32_t dev, uint64_t ino, const char *__path);

#endif
