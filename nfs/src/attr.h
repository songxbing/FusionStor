#ifndef _YNFS_ATTR_H__
#define _YNFS_ATTR_H__

#include <sys/types.h>
#include <sys/stat.h>
#include <rpc/rpc.h>
#include <unistd.h>

#include "fh.h"
#include "lichstor.h"
#include "nfs_state_machine.h"

extern int get_postopattr(uint32_t dev, uint64_t ino, const char *path,
                          post_op_attr *attr);
extern int get_preopattr(uint32_t dev, uint64_t ino, mode_t mode,
                         preop_attr *attr);
extern int sattr_tomode(mode_t *, sattr *);
extern int get_postopattr_stat(post_op_attr *, struct stat *);

void get_preopattr1(fileid_t *fileid, preop_attr *attr);
void get_postopattr1(fileid_t *fileid, post_op_attr *attr);

#endif
