#ifndef __YNFS_FH_H__
#define __YNFS_FH_H__

#include "nfs_state_machine.h"

#define FH_DIR 1

/* minimum length of complete filehandle */
#define MIN_FH_LEN 25

/* maximum depth of pathname described by filehandle */
#define MAX_FH_LEN (64 - MIN_FH_LEN)

#pragma pack(1)
typedef struct {
        uint64_t ino;
        uint32_t dev;
        uint32_t fh_mode; /*copy from stat.st_mode*/
        uint32_t root; /*mount root from volume*/
        uint32_t volid;
        unsigned char len;
        unsigned char inos[MAX_FH_LEN];
} ynfs_fh3_t;
#pragma pack()

#define FD_NONE (-1)    /* used for get_inogen */

extern int fh_comp_raw(ynfs_fh3_t *, const char *path, mode_t mode, int root);
extern int fh_valid(ynfs_fh3_t *);
extern int fh_extend(ynfs_fh3_t *, ynfs_fh3_t *, uint32_t dev, uint64_t ino, mode_t mode);
extern int fh_extend_post(postop_fh *, ynfs_fh3_t *, uint32_t dev, uint64_t ino, mode_t mode);
extern int fh_extend_type(postop_fh *, ynfs_fh3_t *, uint32_t dev, uint64_t ino,
                          const char *path, mode_t mode);
extern int fh_decomp_raw(char *path, ynfs_fh3_t *);

extern int nfh_valid(nfs_fh3 *);

#endif
