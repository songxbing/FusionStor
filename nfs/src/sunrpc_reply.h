#ifndef __SUNRPC_REPLY_H__
#define __SUNRPC_REPLY_H__

#include "job.h"
#include "xdr.h"

void sunrpc_reply_send(const sockid_t *sockid, uint32_t xid,
                       xdr_ret_t xdr_ret, void *_buf, int state);

#endif
