#ifndef __NFS_EVENTS_H__
#define __NFS_EVENTS_H__

#include "config.h"

#include "job.h"
#include "sunrpc_proto.h"

void mnt_event_handler(const sockid_t *sockid, sunrpc_request_t *req, buffer_t *_buf);
void nfs_event_handler(const sockid_t *sockid, sunrpc_request_t *req, buffer_t *_buf);
void acl_event_handler(const sockid_t *sockid, sunrpc_request_t *req, buffer_t *_buf);

#endif
