#include "config.h"

#include <time.h>
#include <unistd.h>
#include <errno.h>

#define DBG_SUBSYS S_LIBINTERFACE

#include "nfs_conf.h"
#include "ynet_rpc.h"
#include "dbg.h"

#define SLEEP_INTERVAL 5
#define LENSTATE 20
#define STATENUM 3

extern struct sockstate sock_state;

void *handler_ynfsstate(void *arg)
{
        int ret;
        (void) arg;
        char sstate[LENSTATE * STATENUM + 1];
        long long unsigned state[STATENUM];
        int logfd;
        time_t counter;

        ret = yroc_create(YNFS_STATE, &logfd);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        while (srv_running) {
                sleep(SLEEP_INTERVAL);
                counter = gettime();
                state[0] = sock_state.bytes_send;
                state[1] = sock_state.bytes_recv;
                state[2] = (LLU) counter;

                snprintf(sstate, LENSTATE*STATENUM+1, "%llu %llu %llu",
                                state[0], state[1], state[2]);

                if (yroc_write(logfd, (void *)sstate, strlen(sstate)) != 0)
                        GOTO(close_logfd, errno);
        }

close_logfd:
        close(logfd);
err_ret:
        return (void *) -1;
}
