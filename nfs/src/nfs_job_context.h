#ifndef __NFS_JOB_CONTEXT_H__
#define __NFS_JOB_CONTEXT_H__

#include "config.h"

#include "fh.h"
#include "lichstor.h"
#include "buffer.h"

#define	RQCRED_SIZE	400	/* this size is excessive */

typedef struct {
        nfs_arg_t arg;
        int retry;
        preop_attr attr;
        int eof; /*read eof*/
        fileid_t fileid;
        buffer_t buf;
} nfs_job_context_t;

/*************new struct************/

typedef struct {
        char path[MAX_PATH_LEN];
        ynfs_fh3_t fh;
        char blank[MAX_FH_LEN];
} nfs_job_context_meta_t;

typedef struct {
        char path[MAX_PATH_LEN];
        ynfs_fh3_t fh;
        char blank[MAX_FH_LEN];
} nfs_job_context_stat_t;

#endif
