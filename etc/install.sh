#!/bin/bash

CONF_DIR="/opt/fusionstack/lich/etc"
LICHCONF="lich.conf"
HOSTSCONF="hosts.conf"
CLUSTER="lich-shell.conf"
SSD_MODELS="ssd.models"

usage()
{
    echo "Usage: $0 [OPTIONS]"
    echo "-i install"
    echo "-e remove"

    exit 1
}

install()
{

    if [ -e $CONF_DIR ];
    then
        echo "$CONF_DIR exists, ingore"
    else
        mkdir -p $CONF_DIR

        cp $LICHCONF $CONF_DIR

        cp $HOSTSCONF $CONF_DIR

        cp $CLUSTER $CONF_DIR

        cp $SSD_MODELS $CONF_DIR
    fi
}

remove()
{
    echo "remove $CONF_DIR"
    rm -fr $CONF_DIR
}

if [ $# -lt 1 ]
then 
    usage
fi

while getopts ieh options
do
    case $options in
        i)
        echo "install conf"
        install
        ;;
        e)
        echo "remove conf"
        remove
        ;; 
        h)
        usage
        ;;
        \?)
        usage
        ;;
    esac
done
        
