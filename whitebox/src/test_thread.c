#include "thread_pool.h"
#include "string_buffer.h"
#include "sysutil.h"
#include "nodectl.h"


void test_string_buffer() {
    string_buffer sbuf;

    string_buffer_init(&sbuf);

    sbuf.push(&sbuf, "abc");
    sbuf.push(&sbuf, "def");
    sbuf.dump(&sbuf);

}

void test_get_text() {
    const char *path = "scale.conf";
    char val[MAX_PATH_LEN];

    int ret = _get_text(path, val, MAX_PATH_LEN);
    printf("ret %d\n", ret);

    ret = recovery_status_get_int("scale", 51);
    printf("ret %d\n", ret);
}


int main(int argc, char **argv) {

    thread_pool_test();
    test_string_buffer();
    test_get_text();

    return 0;
}
