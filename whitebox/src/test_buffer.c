#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>

#include "cmocka.h"
#include "cluster.h"
#include "buffer.h"

#include "lsv_conf.h"
#include "lsv_types.h"
#include "lsv_bitmap.h"


void test_buffer(void **state) {
        buffer_t buf;
        int len = 3;
        char dest[4096];

        mbuffer_init(&buf, 0);
        mbuffer_appendmem(&buf, "abc", 3);
        mbuffer_get(&buf, dest, len);

        assert_memory_equal("abc", dest, len);
}


int main(int argc, char **argv) {
        int ret;
        ret = env_init_simple("test");
        assert (ret == 0);

        const struct CMUnitTest tests[] = {
                cmocka_unit_test(test_buffer),
        };
        return cmocka_run_group_tests(tests, NULL, NULL);
}
