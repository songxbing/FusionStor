#include "config.h"

#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>

#include "cmocka.h"

#include "cluster.h"
#include "buffer.h"
#include "ypage.h"
#include "chunk.h"
#include "replica.h"

// #include "lsv_conf.h"
// #include "lsv_types.h"
// #include "lsv_bitmap.h"
// #include "lsv_volume_gc.h"

#include "lich_api.h"


#define K1 1024
#define M1 1048576


void test_base(void **state) {
        assert_int_equal(sizeof(chkinfo_t), 40);
        assert_int_equal(sizeof(reploc_t), 4);
        assert_int_equal(CHKINFO_SIZE(6), 64);

        assert_int_equal(sizeof(seg_t), 60);
        assert_int_equal(sizeof(mem_cache64_t), 64);

        assert_int_equal(sizeof(replica_srv_entry_t), 96);

        assert_int_equal(sizeof(table_proto_t), 0x160);  // 352
}

#if 0
void test_lsv(void **state) {
        assert_int_equal(LSV_VOLGC_TAIL_INIT, 65);

        assert_int_equal(LSV_MAX_VOLUME_SIZE, 256L*(1L<<40));
        assert_int_equal(LSV_MAX_CHUNK_NUM, 256L*(1L<<20));
        assert_int_equal(LSV_VOLGC_STORAGE_BITMAP_NUM, 64 * (1L << 20));
        assert_int_equal(LSV_VOLGC_STORAGE_BITMAP_DIRTY_BITMAP_NUM, 2 * (1L << 10));
}
#endif

void test_path(void **state) {
        char pool[MAX_PATH_LEN];
        char protocol[MAX_PATH_LEN];
        char vol[MAX_PATH_LEN];
        char snap[MAX_PATH_LEN];
        char path[MAX_PATH_LEN];

        path_decode("/p1/iscsi", pool, protocol, vol, snap);
        printf("pool %s protocol %s vol %s snap %s\n", pool, protocol, vol, snap);

        path_decode("/p1/iscsi/v1@snap1///abc", pool, protocol, vol, snap);
        printf("pool %s protocol %s vol %s snap %s\n", pool, protocol, vol, snap);

        char p[MAX_PATH_LEN];
        path_head("/p1/iscsi/v1@snap2", '/', pool, p);
        printf("pool %s path2 %s\n", pool, p);

        path_head(p, '/', pool, p);
        printf("pool %s path2 %s\n", pool, p);

        path_normalize("", path);
        printf("path normalize %s\n", path);
}

void test_ypage(void **state) {
        uint64_t off2;
        uint32_t size2;

        range_align(4, 0, 5, &off2, &size2);

        assert_int_equal(off2, 0);
        assert_int_equal(size2, 8);

        range_align(4, 1, 5, &off2, &size2);

        assert_int_equal(off2, 0);
        assert_int_equal(size2, 8);

        range_align(4, 1, 8, &off2, &size2);

        assert_int_equal(off2, 0);
        assert_int_equal(size2, 12);
}

#if 0
void test_unit(void **state) {

        assert_int_equal(sizeof(row2_bitmap_page_unit_t), 6);
        assert_int_equal(sizeof(row2_bitmap_unit_t), 2048);

        assert_int_equal(LSV_PAGE_SIZE, 4096);
        assert_int_equal(BITMAP_CHUNK_SIZE, M1);

        // cache
        lsv_bitmap_cache_unit_t cache_unit;
        printf("bitmap cache uinit %lu\n", sizeof(cache_unit));

        assert_int_equal(MAX_COUNT_CACHE, 512);

        assert_int_equal(sizeof(cache_unit.bitmap_cache), M1);
}
#endif


int main(int argc, char **argv) {
        int ret;
        ret = env_init_simple("test");
        assert (ret == 0);

        const struct CMUnitTest tests[] = {
                cmocka_unit_test(test_base),
                // cmocka_unit_test(test_lsv),
                // cmocka_unit_test(test_unit),
                cmocka_unit_test(test_ypage),
                cmocka_unit_test(test_path),
        };
        return cmocka_run_group_tests(tests, NULL, NULL);
}
