#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdio.h>
#include "cmocka.h"

#include "sysutil.h"
#include "token_bucket.h"
#include "ytime.h"


/* Ensure add() adds two integers correctly. */
static void test_add(void **state) {
        (void) state; /* unused */

        assert_int_equal(3+3, 6);
        assert_int_equal(3-3, 0);
}

static void test_gen(void **state) {
        generator_t gen;
        uint32_t i;

        chkid_generator_init(&gen, 8, 3);

        //chkid_generator(&gen, &i); assert_int_equal(i, 0);
        //chkid_generator(&gen, &i); assert_int_equal(i, 2);
        //chkid_generator(&gen, &i); assert_int_equal(i, 1);
        //chkid_generator(&gen, &i); assert_int_equal(i, 3);

        while (chkid_generator(&gen, &i) != ENOENT) {
                printf("i %d\n", i);
        }
}

static void test_token_bucket(void **state) {
        token_bucket_t bucket;
        int is_ready;

        token_bucket_init(&bucket, 0, 10, 10, 10);

        for (int i=0; i < 1000; i++) {
                while (1) {
                        token_bucket_consume(&bucket, 1, &is_ready);
                        if (!is_ready) {
                                // printf("wait %d\n", i);
                                sleep(0.001);
                                continue;
                        }
                        break;
                }

                printf("%jd consume %d\n", ytime_gettime(), i);
        }
}

int main(int argc, char **argv) {
        const struct CMUnitTest tests[] = {
                cmocka_unit_test(test_add),
                cmocka_unit_test(test_gen),
                cmocka_unit_test(test_token_bucket),
        };
        return cmocka_run_group_tests(tests, NULL, NULL);
}
