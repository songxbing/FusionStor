#include "config.h"

#include <stdio.h>
#include <dirent.h>
#include <sys/param.h>

#define DBG_SUBSYS S_LIBINTERFACE

#include "sysy_lib.h"
#include "dbg.h"
#include "token_bucket.h"

void test_token_bucket() {
    int ret, i;
    token_bucket_t bucket;

    ret = env_init_simple("bucket");
    if (ret)
        goto out;

    DINFO("hello, world\n");

    token_bucket_init(&bucket, 10, 10, 10);

    sleep(1);

    ret = token_bucket_consume(&bucket, 10);
    printf("ret %d tokens %0.2f\n\n", ret, bucket.tokens);

    ret = token_bucket_consume(&bucket, 1);
    printf("ret %d tokens %0.2f\n\n", ret, bucket.tokens);

    sleep(3);

    ret = token_bucket_consume(&bucket, 1);
    printf("ret %d tokens %0.2f\n\n", ret, bucket.tokens);

    for (i = 0; i < 100; i++) {
        sleep(1);
        ret = token_bucket_consume(&bucket, 1);
    }
out:
}

int main(int argc, char **argv) {
    test_token_bucket();
}
