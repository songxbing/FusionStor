/*###################################################################
  > File Name: whitebox/src/test_re.c
  > Author: Vurtune
  > Mail: vurtune@foxmail.com
  > Created Time: Tue 23 May 2017 07:20:22 PM PDT
###################################################################*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "sysy_conf.h"
#include "ynet_net.h"
#include "ynet_rpc.h"
#include "net_global.h"
#include "regex.h"
#include "fence.h"
#include "fnotify.h"
#include "dbg.h"

static int __valid_ip(const char *ipstr)
{
        int ret;
        regex_t reg;
        regmatch_t match;
        char errorbuf[MAX_BUF_LEN];
        const char *regstr = "^(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\\."
                "(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\."
                "(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\\."
                "(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)$";
        
        ret = regcomp(&reg, regstr, REG_EXTENDED);
        if (unlikely(ret)) {
                regerror(ret, &reg, errorbuf, MAX_BUF_LEN);
                DERROR("%s %d\n", errorbuf, ret);
                GOTO(err_ret, ret);
        }

        ret = regexec(&reg, ipstr, 1, &match, 0);
        if (unlikely(ret)) {
                if (ret == REG_NOMATCH) {
                        ret = ENOENT;
                        DINFO("key %s buf %s\n", regstr, ipstr);
                        GOTO(err_free,ret);
                } else {
                        regerror(ret, &reg, errorbuf, MAX_BUF_LEN);
                        DERROR("%s %d\n", errorbuf, ret);
                        GOTO(err_free, ret);
                }
        }

        regfree(&reg);

        return 0;
err_free:
        regfree(&reg);
err_ret:
        return ret;
}


int main(int argc, char *argv[])
{
        int count;
        char *iplist[1024];
        char tmp[1024];
        strcpy(tmp, argv[1]);
        count = 1024;
        _str_split(tmp, ',', iplist, &count);
        while(count--){
                __valid_ip(iplist[count]);
        }
}
