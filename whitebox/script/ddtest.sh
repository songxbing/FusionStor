#! /bin/bash

COUNT=$[$1*1024/4]
echo ${COUNT}

DISK=/dev/sdf

rm -f ./$1.img*

dd if=/dev/zero of=./$1.img bs=4096 count=$COUNT

dd if=/dev/zero of=$DISK bs=4096 count=$COUNT
dd if=$DISK of=./$1.img2 bs=4096 count=$COUNT

md5sum ./$1.img*
