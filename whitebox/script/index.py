#!/usr/bin/env python

import os
import sys
import struct
import sqlite3
import base64
from BitVector import BitVector
from optparse import OptionParser

DBPREFIX = '/opt/fusionstack/data/chunk/'

def db_iterater(dbid):
    conn = sqlite3.connect(DBPREFIX + "%d.db"%dbid)
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()

    try:
        cursor.execute('create index IdxRawParent on raw(parent)')
    except:
        print "dbid %d err"%dbid

    print "dbid %d"%dbid

    cursor.close()
    conn.close()

def main():
    for i in range(0, 10):
        db_iterater(i)

if __name__ == '__main__':
    main()
