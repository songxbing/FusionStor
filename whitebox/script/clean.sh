#!/usr/bin/env bash

set -x

HOST=t53

stop_and_clean_lich() {
    lich stop

    rm -rf /opt/fusionstack/log/lich.log
    rm -rf /opt/fusionstack/log/lich.log-*
    rm -rf /opt/fusionstack/core/*
    rm -rf /opt/fusionstack/data/*

    rm /opt/fusionstack/etc/cluster.conf
}

start_lich() {
    # create lich cluster

    # lich prep $HOST

    #sleep $1
    if [ -n "$(netstat -anop | grep 3260)" ]
    then
        sleep 60
    fi

    lich create $HOST

    # DISK

    lich.node --disk_list
    lich.node --disk_add --force all
}

stop_and_clean_lich
start_lich

# v1

lichbd pool create p1 -p iscsi
lichbd find / -r
