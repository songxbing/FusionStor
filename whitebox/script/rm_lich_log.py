#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import glob


def main(n):
    pattern = '/opt/fusionstack/log/lich.log-*'
    print 'rm %s, n=%d' % (pattern, n)
    files = glob.glob(pattern)
    files.sort()
    for f in files[:-n]:
        print 'rm file %s...' % f
        os.remove(f)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        n = int(sys.argv[1])
    else:
        n = 5
    main(n)
