#!/usr/bin/env bash

# set -x

SIZE=$1
SRC=$2
DEST=$3

COUNT=$((256 * $SIZE))

rm -rf ./$SIZE.*

# set -x

dd if=$SRC of=./$SIZE.img bs=4096 count=$COUNT
echo
dd if=./$SIZE.img of=$DEST bs=4096 count=$COUNT
echo
dd if=$DEST of=./$SIZE.img2 bs=4096 count=$COUNT
echo

md5sum ./$SIZE.*

