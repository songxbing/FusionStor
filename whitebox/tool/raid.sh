#!/usr/bin/env bash

set -x

MegaCli=/opt/MegaRAID/MegaCli/MegaCli64

JBOD_ENC=59
JBOD_MAX=23

OTHER_ENC=58
OTHER_MAX=7

$MegaCli -PDList -a0

$MegaCli -CfgClr -a0

$MegaCli -AdpSetProp -EnableJBOD 1 -a0

for i in $(seq 0 3)
do
    $MegaCli -PDMakeJBOD -PhysDrv[$JBOD_ENC:$i] -a0
done

for i in $(seq 4 $JBOD_MAX)
do
    $MegaCli -PDMakeGood -PhysDrv[$JBOD_ENC:$i] -Force -a0
    $MegaCli -CfgLdAdd -r0 [$JBOD_ENC:$i] WB RA Cached NoCachedBadBBU -a0
done

for i in $(seq 0 $OTHER_MAX)
do
    $MegaCli -PDMakeGood -PhysDrv[$OTHER_ENC:$i] -Force -a0
    $MegaCli -CfgLdAdd -r0 [$OTHER_ENC:$i] WB RA Cached NoCachedBadBBU -a0
done
