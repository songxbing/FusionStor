# USE valgrind

valgrind --tool=memcheck --leak-check=full --show-reachable=yes -v /opt/fusionstack/lich/sbin/lichd --home /opt/fusionstack/data -f > log.txt 2>&1

grep 'illegal\|inappropriate\|inadequate\|overlapping\|memory leak\|overlap\|invalid\|definitely' log.txt
