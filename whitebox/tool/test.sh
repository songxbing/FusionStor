#!/usr/bin/env bash

set -x

# lichbd pool create p1

lichbd vol create p1/iscsi/v1 --size 100Gi
lichbd vol create p1/iscsi/v2 --size 100Gi
lichbd vol create p1/iscsi/v3 --size 100Gi

# lichbd pool create p2 -p iscsi
# lichbd vol create p2/v1 --size 30G -F row2 -p iscsi --repnum 3
# lichbd vol create p2/v2 --size 30G -F row2 -p iscsi --repnum 3
# lichbd vol create p2/v3 --size 30G -F row2 -p iscsi --repnum 3
