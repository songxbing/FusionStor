#!/usr/bin/env bash

set -x

NODES="node64 node65 node66 node67"

for i in $NODES
do
	ssh root@$i "echo 4 > /dev/shm/lich4/msgctl/level"
done
