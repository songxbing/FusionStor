#!/usr/bin/env python

import os, sys


def main(fname):
    d = {}
    with open(fname) as f:
        lines = f.readlines()
        lines = [x.strip('\n') for x in lines]
        for line in lines:
            items = line.split(' ')
            items = [x for x in items if x]
            avg = items[len(items)-2]
            # print '%20s %s' % (avg, line)
            d[avg] = line

    for k, v in sorted(d.items(), key=lambda x: float(x[0]), reverse=True):
        print '%s' % v


if __name__ == '__main__':
    if len(sys.argv) > 1:
        fname = sys.argv[1]
    else:
        fname = 'perf.log'

    main(fname)
