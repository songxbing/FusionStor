#!/usr/bin/env bash

set -x

usage() {
    echo $0 pool diskid disksize
}

creatediskall() {
    nodes=`grep -v ^# /opt/fusionstack/etc/cluster.conf`
    script=`dirname $0`"/createdisk.sh"
    target="/tmp/createdisk.sh"
    lichbd pool create $1

    for i in $nodes;do
        scp $script $i:$target
        ssh $i $target $*
        ssh $i rm -rf $target
    done
}

main() {
    if [ $# != 3 ]; then
        usage;
    else
        creatediskall $*
    fi
}

main $*
