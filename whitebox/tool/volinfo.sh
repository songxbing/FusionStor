#!/usr/bin/env bash

#set -x

CHKID=$1

VOLPATH=$(lich.inspect --lookup $CHKID 2>/dev/null |grep '^path'|cut -d' ' -f3)

echo "== $CHKID, $VOLPATH"
lichbd vol info $VOLPATH
