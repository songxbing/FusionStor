#!/usr/bin/env bash

set -x

./clean.sh
sleep 60

./start.sh
sleep 20

./test.sh
