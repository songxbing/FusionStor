#!/usr/bin/env bash

# config network

# update yum.repo.d

PKG="vim git gcc cmake bison flex libuuid-devel libaio-devel openssl-devel sqlite-devel libcurl-devel hiredis-devel yajl-devel libpciaccess-devel rdma-core-devel numactl-devel python2-cryptography"
TOOL="smartmontools hdparm pciutils expect redhat-lsb ipython tree iscsi-initiator-utils python2-pip valgrind pdsh gdb"
PYTHON="python-paramiko"

# python
pip install --upgrade pip

for i in $PKG $TOOL $PYTHON
do
    yum install -y $i
done

# MetaRAID

# lich.conf

# centos 6 spesically
# config/include/configure.h:HAVE_STATIC_ASSERT

# yum install -y docker

# systemctl start docker
