#!/usr/bin/env bash

nodes=`grep -v ^# /opt/fusionstack/etc/cluster.conf`

for node in $nodes
do
    echo "===========`date`-$node========="
    ssh $node 'ls -l /opt/fusionstack/core/'
    ssh $node 'grep assert /opt/fusionstack/log/lich.log'
done
