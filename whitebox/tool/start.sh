#!/usr/bin/env bash

NODES="t151 t152 t153"

lich prep $NODES
lich create $NODES

lichbd pool create p1

for i in $NODES
do
    ssh root@$i lich.node --disk_add all --pool p1
done
