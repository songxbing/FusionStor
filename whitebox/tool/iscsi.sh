#!/usr/bin/env bash

set -x

HOST=127.0.0.1
VOL=v1

iscsiadm -m discovery -t sendtargets -p $HOST -l
# iscsiadm -m node -T iqn.2001-04-1234.com.meidisen:p1.$VOL -p $HOST:3260 -u
# iscsiadm -m node -T iqn.2001-04-1234.com.meidisen:p1.$VOL -p $HOST:3260 -l

sleep 3

ls -lh /dev/disk/by-path
