#!/usr/bin/env bash

set -x

nodes=`grep -v ^# /opt/fusionstack/etc/cluster.conf`

for i in $nodes
do
    echo $i
    ssh root@$i pkill -9 lichd

    ssh root@$i rm -rf /opt/fusionstack/etc/cluster.conf
    ssh root@$i rm -rf /opt/fusionstack/data/*
    ssh root@$i rm -rf /opt/fusionstack/core/*
    ssh root@$i rm -rf /opt/fusionstack/log/lich.log

    ssh root@$i rm -rf /dev/shm/lich4
done
