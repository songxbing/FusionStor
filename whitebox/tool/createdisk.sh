#!/usr/bin/env bash

set -x

usage() {
    echo uasage:
    echo $0 pool diskid disksize
}

disk_new() {
    tmp="/dev/shm/lich4_test.tmp"
    target="/opt/fusionstack/data/tmp/$disk.file"
    pool=$1
    disk=$2
    size=$3

    mkdir  /opt/fusionstack/data/tmp &>/dev/null
    bash -c "echo -n \"cluster=xxxx;node=lich_testx;type=new;disk=$disk;pool=$pool;cache=0;cached=0;\" > $tmp"
    bash -c "dd if=$tmp of=/opt/fusionstack/data/tmp/$disk.file bs=1 seek=1612 &>/dev/null"
    truncate  /opt/fusionstack/data/tmp/$disk.file --size $size
    ln -s /opt/fusionstack/data/tmp/$disk.file /opt/fusionstack/data/disk/disk/$disk.disk
}

disk_check() {
    ls -l /opt/fusionstack/data/disk/disk/ | grep $1.disk
    return $?
}

createdisk() {
    disk_check $2
    if [ $? == 0 ];then
        echo "diskid $1 already exists"
        exit $?
    else
        disk_new $*
    fi
}

main() {
    if [ $# != 3 ]; then
        usage;
    else
        createdisk $*
    fi
}

main $*
