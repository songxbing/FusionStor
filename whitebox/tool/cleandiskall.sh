#!/usr/bin/env bash

set -x

usage() {
    echo $0 diskid
}

removediskall() {
    nodes=`grep -v ^# /opt/fusionstack/etc/cluster.conf`
    disk="/opt/fusionstack/data/disk/disk/"$1.disk
    target="/opt/fusionstack/data/tmp/"$1.file

    for i in $nodes;do
        ssh $i rm -rf $disk
        ssh $i rm -rf $target
    done
}

main() {
    if [ $# != 1 ]; then
        usage;
    else
        removediskall $*
    fi
}

main $*
