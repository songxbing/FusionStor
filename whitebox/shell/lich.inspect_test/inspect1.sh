 #!/usr/bin/env bash
   
 set -x

 begin_test() {
     lichbd pool create p1 -p iscsi
     lichbd vol create p1/v1 -p iscsi -s 10Gi -F row2
 }

 test_allocate_and_move() {
     lich.inspect --allocate /iscsi/p1/v1 -p -f
     #    lich.inspect --move, -m /p1/v1 <nodename>
 }

 test_recover_and_scan() {
     lich.inspect --recover /iscsi/p1 --deep -v
     lich.inspect --scan /iscsi/p1 --deep -v
 }

 end_test() {
     #  iscsiadm -m node -u

     lichbd vol rm p1/v1 -p iscsi
     lichbd pool rm p1 -p iscsi
 }   

 begin_test
 test_allocate_and_move
 test_recover_and_scan
 end_test

