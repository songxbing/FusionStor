#/usr/bin/env bash
 set -x

 begin_test() {
     lichbd pool create p1 -p iscsi
     lichbd vol create p1/v1 -p iscsi -s 10Gi -F row2
 }

 
 test_tier_and_priority() {
     lich.inspect --tier /iscsi/p1 
     lich.inspect --tier /iscsi/p1/v1  
     lich.inspect --priority /iscsi/p1 0
     lich.inspect --priority /iscsi/p1/v1 0
 }

 test_localize() {
     lich.inspect --localize /iscsi/p1 0
     lich.inspect --localize /iscsi/p1/v1 0
     lich.inspect --localize /iscsi/p1
     lich.inspect --localize /iscsi/p1/v1     
 } 

 test_writeback() {
     lich.inspect --writeback /iscsi/p1 0
     lich.inspect --writeback /iscsi/p1/v1 0
     lich.inspect --writeback /iscsi/p1
 }

 test_multipath() {
     lich.inspect --multipath /iscsi/p1 0
     lich.inspect --multipath /iscsi/p1/v1 0
     lich.inspect --multipath /iscsi/p1
     lich.inspect --multipath /iscsi/p1/v1
 }

 end_test() { 
     lichbd vol rm p1/v1 -p iscsi
     lichbd pool rm p1 -p iscsi
 }

 begin_test
 test_tier_and_priority
 test_localize
 test_writeback
 test_multipath
 end_test
