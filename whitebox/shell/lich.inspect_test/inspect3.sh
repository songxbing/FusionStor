#/usr/bin/env bash

set -x

begin_test() {
    lichbd pool create p1 -p iscsi
    lichbd vol create p1/v1 -p iscsi -s 10Gi -F row2
    lichbd snap create p1/v1@snap1 -p iscsi
}

test_connection() {
    lich.inspect --connection /iscsi/p1/v1
    lich.inspect --connection /iscsi/p1/v1/snap1
}

test_paxosdump() {
    lich.inspect --paxosdump
}

test_md5sum() {
    lich.inspect --md5sum /iscsi/p1/vol1
}

test_dbdump_and_dbmode() {
    lich.inspect --dbdump all raw
    lich.inspect --dbmode all
}

#test_base64() {

#}

end_test() {
    lichbd vol rm p1/v1 -p iscsi
    lichbd pool rm p1 -p iscsi
}

begin_test
test_connection
test_paxosdump
test_md5sum
test_dbdump_and_dbmode
#test_base64
end_test
