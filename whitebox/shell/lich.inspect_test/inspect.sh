#!/usr/bin/env bash

set -x

begin_test() {
    lichbd pool create p1 -p iscsi
    lichbd vol create p1/v1 -p iscsi -s 10Gi -F row2
    lichbd snap create p1/v1@snap1 -p iscsi
}

test_list_and_stat_and_lookup() {
    lich.inspect --list /iscsi/p1
    lich.inspect --list /iscsi/p1/v1
    lich.inspect --stat /iscsi/p1/v1
    lich.inspect --lookup /iscsi
    lich.inspect --lookup /iscsi/p1
    lich.inspect --lookup /iscsi/p1/v1
}
#use the <path>


end_test() {
    #  iscsiadm -m node -u

    lichbd vol rm p1/v1 -p iscsi
    lichbd pool rm p1 -p iscsi
}

begin_test
test_list_and_stat_and_lookup
end_test

