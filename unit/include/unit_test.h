#ifndef __UNIT_TEST_H__
#define __UNIT_TEST_H__

int unit_test_buffer(const char *func);

int unit_test_ec(const char *func);

int unit_test_aio(const char *func);

int unit_test_wb(const char *func);

int unit_test_ramdisk(const char *func);

#endif /* __UNIT_TEST_H__ */
