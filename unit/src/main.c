/*
 * =====================================================================================
 *
 *       Filename:  unit/src/main.c
 *
 *    Description:  lich unit test 
 *
 *        Version:  1.0
 *        Created:  10/08/2017 16:18:25 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Gabe
 *        Company:  
 *
 * =====================================================================================
 */
#include "config.h"

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <dirent.h>
#include <ctype.h>

#define DBG_SUBSYS S_LIBINTERFACE

#include "configure.h"
#include "env.h"
#include "adt.h"
#include "net_table.h"
#include "lichstor.h"
#include "cluster.h"
#include "metadata.h"
#include "storage.h"
#include "volume.h"
#include "license.h"
#include "lichbd.h"
#include "net_global.h"
#include "unit_test.h"
#include "dbg.h"

typedef enum {
        OP_NULL,
        OP_TEST_BUFFER,
        OP_TEST_EC,
        OP_TEST_AIO,
        OP_TEST_WB,
        OP_TEST_RAMDISK,
} uint_test_op_t;

static void usage()
{
        fprintf(stderr, "\nusage:\n"
                "lich.unit_test --test_buffer\n"
                "lich.unit_test --test_ec\n"
                "lich.unit_test --test_aio\n"
                "lich.unit_test --test_wb\n"
                "lich.unit_test --test_ramdisk\n"
               );
}

int main(int argc, char *argv[])
{
        int ret, op = OP_NULL;
        char c_opt;
        const char *func;
        int verbose = 0;

        (void) verbose;
        dbg_info(0);

        while (srv_running) {
                int option_index = 0;

                static struct option long_options[] = {
                        { "test_buffer", required_argument, 0, 'b'},
                        { "test_ec", required_argument, 0, 'e'},
                        { "test_aio", no_argument, 0, 'a'},
                        { "test_wb", no_argument, 0, 'w'},
                        { "test_ramdisk", no_argument, 0, 'r'},
                        { "verbose", 0, 0, 'v' },
                        { "help",    0, 0, 'h' },
                        { 0, 0, 0, 0 },
                };

                c_opt = getopt_long(argc, argv, "b:e:awrvh", long_options, &option_index);
                if (c_opt == -1)
                        break;

                switch (c_opt) {
                case 0:
                        switch (option_index) {
                        default:
                                fprintf(stderr, "Hoops, wrong op got!\n");
                                YASSERT(0); 
                        }

                        break;
                case 'b':
                        op = OP_TEST_BUFFER;
                        func = optarg;
                        break;
                case 'e':
                        op = OP_TEST_EC;
                        func = optarg;
                        break;
                case 'a':
                        op = OP_TEST_AIO;
                        func = optarg;
                        break;
                case 'w':
                        op = OP_TEST_WB;
                        func = optarg;
                        break;
                case 'r':
                        op = OP_TEST_RAMDISK;
                        func = optarg;
                        break;
                case 'v':
                        verbose = 1;
                        break;
                case 'h':
                        usage();
                        EXIT(0);
                default:
                        usage();
                        EXIT(EINVAL);
                }
        }

        if (argc == 1) {
                usage();
                exit(EINVAL);
        }

        switch (op) {
        case OP_TEST_BUFFER:
                ret = lichbd_init("");
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                ret = unit_test_buffer(func);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
                break;
        case OP_TEST_EC:
                /*
                ret = unit_test_ec(func);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
                */
                break;
        case OP_TEST_AIO:
                ret = unit_test_aio(func);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
                break;
        case OP_TEST_WB:
                ret = unit_test_wb(func);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
                break;
        case OP_TEST_RAMDISK:
                ret = lichbd_init("");
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                ret = unit_test_ramdisk(func);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
                break;
        default:
                usage();
                exit(EINVAL);
        }

        return 0;
err_ret:
        fprintf(stderr, "error: %s\n", strerror(ret));
        EXIT(_errno(ret));
}
