#include "config.h"

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <dirent.h>
#include <ctype.h>
#include <libgen.h>

#define DBG_SUBSYS S_LIBINTERFACE

#include "configure.h"
#include "env.h"
#include "adt.h"
#include "dbg.h"
#include "buffer.h"
#include "../../storage/controller/ramdisk.h"

void __unit_test_ramdisk(const buffer_t *buf, uint64_t size, off_t offset)
{
        chkid_t chkid;

        memset(&chkid, 0x0, sizeof(chkid));
        ramdisk_crc_write(&chkid, buf, size, offset);
}

int unit_test_ramdisk(const char *func)
{
        (void) func;
        buffer_t buf;

        mbuffer_init(&buf, 0);
        mbuffer_appendzero(&buf, 4096);

        __unit_test_ramdisk(&buf, 4096, 0);
        __unit_test_ramdisk(&buf, 4096, 4096);
        __unit_test_ramdisk(&buf, 4096, 1048576);

        mbuffer_appendzero(&buf, 4096);

        __unit_test_ramdisk(&buf, 8192, 0);
        __unit_test_ramdisk(&buf, 8192, 4096);
        __unit_test_ramdisk(&buf, 8192, 1048576);

        mbuffer_appendzero(&buf, 512);

        __unit_test_ramdisk(&buf, 8704, 0);

        mbuffer_free(&buf);

        return 0;
}
