# LICH编码规范

## 设计原则

- 分离接口和实现，接口统一命名，保持相对稳定
- 分离机制和策略
- 管理好依赖关系
- 提高系统可见性，通过日志和命令，可以方便获取系统运行过程和状态信息

## 命名约定

## 结构

### 变量

- 尽量使用const
- magic value用宏定义，并保持有且只有一个定义

### 函数

- 函数要尽量方便测试
- 注意函数的通用性和粒度，方便重用
- 保持函数的对称性（在同一个层次上进行资源的分配和回收）
- 每个函数不可以超过50行
- 嵌套深度不可超过3层
- 函数参数不宜超过7个，如果参数过多，封装成struct进行传递
- 输入参数在左，输出参数在右
- 返回值风格统一，成功返回0，错误码从/usr/include/asm-generic/{errno-base.h, errno.h}取值 

### 编译优化

- unlikely分支预测
- O3编译

## 进程线程

## 同步机制

- 每个lock内的操作必须在有限的io内返回，有限是指，io数量不随着volume的大小而变化，一般不会超过10个

## 通信

## 内存管理

## IO主流程

- io主流程，只能使用plock，使用其他lock类型，必须向主管汇报
- io主流程不可使用malloc，只能使用mem_cache_calloc/mem_hugepage_new来申请内存。
  mem_cache_calloc使用过程要注意大小，使用static_assert来检查

## assert

## 日志

## 测试
