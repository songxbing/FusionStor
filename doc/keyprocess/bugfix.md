# 流程

以修复bugfix：7759为例

从testing生成新分支

    git checkout testing
    git pull

    git branch bugfix_7759
    git checkout bugfix_7759

    ... BUGFIX


推送到远端

    git push --set-upstream origin bugfix_7759

从远端拉分支到本地

    git checkout -b bugfix_7759 origin/bugfix_7759

合并testing:

    git checkout testing
    git pull

    git checkout bugfix_7759
    git merge testing

    ... TEST

合并到testing：

    git checkout testing
    git merge --no-ff bugfix_7759
    git push origin testing

删除bugfix分支

    git branch -D bugfix_7759
    git push origin :bugfix_7759
