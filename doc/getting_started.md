# 梦幻之旅

## 文档

学习计划：learning.md
架构文档：arch/
关键流程：keyprocess/
任务列表：todo/
特性文档：changelog/

## 安装

获取代码：git clone http://username:password@192.168.1.3/lich4.git

依赖
- lsb_release
- bison
- flex

进入lich4目录
- mkdir cb && cd cb
- cmake ..
- make
- make install

安装目录：/opt/fusionstack

## 部署

配置文件：/opt/fusionstack/etc/lich.conf
日志文件：/opt/fusionstack/log/lich.log
CORE文件：/opt/fusionstack/core/xxx

支持单节点模式：配置文件里指定solomode on;

准备集群：
- 准备ssh等环境：lich prep   v[1-3]
- 创建集群：     lich create v[1-3]
- 启动关闭集群： lich start | stop | restart

添加磁盘：

    lich.node --disk_add --force all
    lich.node --disk_list

## 测试

观察系统运行状态

命令行工具：
- lich
- lich.node
- lichbd
- lich.inspect
- /opt/fusionstack/lich/libexec/lich.admin

## 实现

    /opt/fusionstack/lich
    /opt/fusionstack/etc
    /opt/fusionstack/data
    /opt/fusionstack/log

    /dev/shm/lich4/hsm
    /dev/shm/lich4/clock
    /dev/shm/lich4/mapping
    /dev/shm/lich4/msgctl

    /dev/shm/lich4/nodectl/diskstat
    /dev/shm/lich4/nodectl/recovery
    /dev/shm/lich4/nodectl/balance
