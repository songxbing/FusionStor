# 目录结构说明


基础库lich4.so

- ylib: 通用c库，包括各种数据结构
- ynet: 通信层
- cluster: 集群和节点管理，选主过程，VIP等
- storage: controller, chunk, replica等，可以由stor_ctl.h跟进
- schedule：协程，core绑定

主进程：

- lichd：每个节点启动一个lichd进程，多个lichd进程构成集群，通过paxos算法选举出一个admin节点

应用协议：

- iscsi
- nbd
- lichbd
- nfs

工具：

- utils: 命令行工具，包括lichbd，lich.inspect, lich.admin等
- admin
  - lich: cluster.py
  - lich.node: node.py
