# 线程和定时器

可以获取主要的工作线程，分析每个线程的主要职责。

- pthread_create
- timer1_create

## nbd_srv.c

    __nbd_accept__
    __nbd_tcp_passive__
    __nbd_accept__
    __nbd_unix_passive__
    __nbd_service

## RAW

    ./ylib/lib/fnotify.c:        ret = pthread_create(&th, &ta, __fnotify_thr_fn, NULL);

    ./ylib/lib/analysis.c:        ret = pthread_create(&th, &ta, __worker, NULL);

    ./ylib/lib/mem_pool.c:        pthread_create(&td, NULL,mem_watch_dog , pool);
    ./ylib/lib/mem_pool.c:                pthread_create(&array[i], NULL,thread_fn ,NULL);

    ./ylib/lib/gettime.c:        ret = pthread_create(&th, &ta, __gettime_worker, NULL);

    ./ylib/lib/tpool.c:	ret = pthread_create(&th, &ta, tpool->worker,  (void *)tpool->context);

    ./ylib/lib/timer.c:                ret = pthread_create(&th, &ta, __timer_expire, (void *)group);

    ./ynet/sock/sdevent.c:        ret = pthread_create(&th, &ta, __sdevent_worker_writer, sdevent);

    ./ynet/net/main_loop.c:                ret = pthread_create(&th, &ta, __main_loop_worker, worker);

    ./ynet/net/offline_msg.c:        ret = pthread_create(&th, &ta, __offline_msg_worker, NULL);

    ./ynet/rpc/rpc_passive.c:        ret = pthread_create(&th, &ta, __rpc_accept__, arg);
    ./ynet/rpc/rpc_passive.c:        ret = pthread_create(&th, &ta, __rpc_accept_worker, &sem);

    ./ynet/rpc/rpc_table.c:                ret = pthread_create(&th, &ta, __rpc_table_scan, tmp);

    ./ynet/rpc/minirpc.c:        ret = pthread_create(&th, &ta, __minirpc_exec__, arg);
    ./ynet/rpc/minirpc.c:        ret = pthread_create(&th, &ta, __minirpc_acceptor_worker, NULL);

    ./storage/controller/castoff.c:        ret = pthread_create(&th, &ta, __castoff_thread_worker, seg);
    ./storage/controller/castoff.c:        ret = pthread_create(&th, &ta, __castoff_worker, (void *)idx);

    ./storage/controller/pool_ctl.c:        ret = pthread_create(&th, &ta, __readpool_watcher, NULL);

    ./storage/storage/md_map.c:        ret = pthread_create(&th, &ta, __md_map_load__, chkid);

    ./storage/storage/md.c:                ret = pthread_create(&th[i], &ta, __group_snapshot_lock_thread, &segs[i]);
    ./storage/storage/md.c:                ret = pthread_create(&th[i], &ta, __group_snapshot_unlock_thread, &segs[i]);
    ./storage/storage/md.c:                ret = pthread_create(&th[i], &ta, __group_snapshot_create_nolock_thread, &segs[i]);

    ./storage/storage/locator_rpc.c:        ret = pthread_create(&th, &ta, __locator_srv_worker, NULL);

    ./storage/task/balance.c:        ret = pthread_create(&th, &ta, __balance_thread_worker, seg);
    ./storage/task/balance.c:        ret = pthread_create(&th, &ta, __balance_worker, NULL);

    ./storage/task/recovery.c:        ret = pthread_create(&th, &ta, __recovery_thread, seg);
    ./storage/task/recovery.c:        ret = pthread_create(&th, &ta, __recovery_worker, NULL);

    ./storage/replica/replica_writeback.c:        ret = pthread_create(&th, &ta, __replica_writeback_flush_worker, NULL);

    ./storage/replica/clock.c:        ret = pthread_create(&th, &ta, __clock_recycle_worker, queue);
    ./storage/replica/clock.c:        ret = pthread_create(&th, &ta, __clock_worker, NULL);

    ./storage/replica/disk_mt.c:                ret = pthread_create(&th, &ta, __submit_mt_worker, NULL);

    ./storage/replica/writeback.c:        ret = pthread_create(&th, &ta, __writeback_md_worker, device);

    ./storage/replica/diskmd.c:        ret = pthread_create(&th, &ta, __diskmd_lost__, lost);
    ./storage/replica/diskmd.c:        ret = pthread_create(&th, &ta, __diskmd_probe, NULL);

    ./storage/chunk/chunk_cleanup.c:        ret = pthread_create(&th, &ta, __chunk_cleanup_worker, &ent->nid);

    ./schedule/vm.c:        ret = pthread_create(&th, &ta, __vm_worker, vm);

    ./schedule/core.c:        ret = pthread_create(&th, &ta, __core_worker, core);

    ./schedule/corenet_connect.c:        ret = pthread_create(&th, &ta, __corenet_accept__, ctx);
    ./schedule/corenet_connect.c:        ret = pthread_create(&th, &ta, __corenet_passive, NULL);

    ./schedule/aio.c:        ret = pthread_create(&th, &ta, __aio_worker, aio);

    ./cluster/msgqueue/mq_master.c:        ret = pthread_create(&th, &ta, __async_worker, NULL);

    ./cluster/nodepool/nodetable.c:        ret = pthread_create(&th, &ta, __nodelist_fd_watcher, &nodelist_fd);

    ./cluster/node/node.c:        ret = pthread_create(&th, &ta, __node_start, NULL);

    ./prof/prof_net.c:        ret = pthread_create(&th, &ta, __corenet_srv_wroker, session);
    ./prof/prof_net.c:        ret = pthread_create(&th, &ta, __schedule_srv_wroker, session);
    ./prof/prof_net.c:        ret = pthread_create(&th, &ta, __test_srv_wroker, session);
    ./prof/prof_net.c:        ret = pthread_create(&th, &ta, __prof_srv, NULL);
    ./prof/prof_net.c:                ret = pthread_create(&th, &ta, __test_cli_worker, arg);
    ./prof/prof_dio.c:        ret = pthread_create(&th, &ta, __prof_dio_accept, NULL);
    ./prof/prof_dio.c:        ret = pthread_create(&th, &ta, __prof_dio_cli_worker, arg);
    ./prof/prof_vm.c:        ret = pthread_create(&th, &ta, __prof_vm_accept, NULL);
    ./prof/prof_vm.c:                ret = pthread_create(&th, &ta, __prof_vm_cli_worker, arg);

    ./iscsi/src/iscsid.c:        ret = pthread_create(&th, &ta, __iscsi_accept_worker, conn);
    ./iscsi/src/iscsid.c:        ret = pthread_create(&th, &ta, __iscsi_accept, NULL);
    ./iscsi/src/iscsid.c:        ret = pthread_create(&th, &ta, __iscsi_service, NULL);

    ./nfs/src/sunrpc_passive.c:        ret = pthread_create(&th, &ta, __sunrpc_accept, NULL);
    ./nfs/src/mini_nlm.c:        ret = pthread_create(&th_cdsstate, &ta, handler_ynfsstate, NULL);
    ./nfs/src/mini_nlm.c:	pthread_create(&tid, NULL, grace_period_change, NULL);
    ./nfs/src/nfs_srv.c:        ret = pthread_create(&th, &ta, __nfs_service, NULL);

    ./lichbd/lichbd_rpc.c:        ret = pthread_create(&th, &ta, __lichbd_srv_accept, NULL);
    ./lichbd/lichbd_rpc.c:        ret = pthread_create(&th, &ta, __lichbd_srv_service, NULL);
    ./lichbd/lichbd_cache.c:        ret = pthread_create(&th, &ta, __lichbd_cache_worker, (void *)path);
    ./lichbd/lichbd.c:        ret = pthread_create(&th, &ta, __analysis_dump,  NULL);

    ./sheepdog/src/sheepdog.c:        ret = pthread_create(&th, &ta, __sheepdog_worker, se);
    ./sheepdog/src/sheepdog.c:        ret = pthread_create(&th, &ta, __sheepdog_worker, se);
    ./sheepdog/src/sheepdog.c:        ret = pthread_create(&th, &ta, __sheepdog_passive__, NULL);
    ./sheepdog/src/sheepdog.c:        ret = pthread_create(&th, &ta, __sheepdog_unix_passive__, NULL);
    ./sheepdog/src/sheepdog.c:        ret = pthread_create(&th, &ta, __sheepdog_service, NULL);

    ./utils/src/hierarch.c:        ret = pthread_create(&th, &ta, arg->ext.worker, arg);
    ./utils/src/utils.c:        ret = pthread_create(&th, &ta, arg->ext.worker, arg);
    ./utils/src/lich.prof.c:                ret = pthread_create(&th, &ta, __prof_net_worker, arg);
    ./utils/src/lich.prof.c:                ret = pthread_create(&th, &ta, __prof_direct_worker, arg);
    ./utils/src/lich.inspect.c:                ret = pthread_create(&ths[i], NULL, __lich_allocate_thread, &args[i]);
    ./utils/src/recovery.c:                ret = pthread_create(&th, &ta, __recovery_file_range, args);
    ./utils/src/copy.c:        ret = pthread_create(&th, &ta, arg->ext.worker, arg);
