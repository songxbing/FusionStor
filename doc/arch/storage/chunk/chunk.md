# 数据块CHUNK

卷被划分为固定大小的数据块，叫做CHUNK，采用副本或纠删码机制来保证数据安全。

## ID

chkinfo_t记录副本的实际存放位置，以节点nid来表示。chkinfo_t的磁盘预留空间是64B，
由此也限定了最大的副本数为6。

根据第一副本找到controller。

在节点内部，采用sqlite管理本地chunk数据。根据chkid可以得到磁盘ID和offset。
磁盘空间直接管理裸设备，随机分布。

chkinfo_t存在其父节点上，不存在该chunk本身的数据块上。

## chunk disk layout

五种chunk
- pool
- subpool
- vol
- subvol
- raw

统一采用table_proto_t管理五种chunk。table_proto_t的磁盘布局为：

info area | child bitmap | xattr bitmap | snap bitmap | item area
--------  | ------------ | ------------ | ----------- | ----------------
512 * 32  | 4K           | 2K           | 2K          | 1000K = 16000*64

其中：

    info area: 存fileinfo_t信息（pool, vol)，可以通过table_proto->getinfo获取，TABLE_PROTO_INFO_ATTR = 0
    child bitmap: item size (64B | 512B)
    xattr bitmap: item size (512B)
    snap bitmap: item size (512B)
    item area: child item = 500K(1/2)，xattr item = 250K(1/4), snap area = 250K(1/4)


pool: 无snap区，DIR_PROTO_ITEM_SIZE = 64, DIR_PROTO_ITEM_COUNT = 8000 (total 500K)

subpool: 无xattr和snap区，DIR_PROTO_EXTERN_ITEM_SIZE = 512, DIR_PROTO_EXTERN_ITEM_COUNT = 2000 (total 1000K)

vol: FILE_PROTO_ITEM_SIZE = 64, FILE_PROTO_ITEM_COUNT = 8000 (total 500K)

subvol: FILE_PROTO_EXTERN_ITEM_SIZE = 64, FILE_PROTO_EXTERN_ITEM_COUNT = 16000 (total 1000K)

raw: user data (1M?)


### 系统限制

- pool的子项：8000 * 2000
- 系统最大卷数：(8000 * 2000) ^ 2
- 最大卷大小：8000 * 16000 * 1M = 122T
- 单卷快照数： 结构性限制 (bitmap: 2048, item：250K/512 = 500, 实际限制：TABLE_PROPO_SNAP_MAX = 256)
- CLONE卷：无限制


## 强一致性担保

读写是强一致性。全部写入才会返回，读取的时候是任意一个。如果发生部分写入，是怎么处理的？
write all，read any。在any和all之间，有一个可变范围。


## 副本


## 纠删码

## 分配空间
