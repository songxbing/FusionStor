# 数据分布

管理物理资源：

> 没有物理存储池的概念，保护域的概念类似。若卷指定保护域，一个卷的数据只能出现在一个保护域。
>
> 故障域是指一个chunk的副本，分布在不同的保护域，确保任何一个故障域失效的情况下，依然存在可用副本。
>
> 混合存储的情况下，通常分为性能层和容量层，会影响到读写流程。

管理逻辑资源：

> pool是虚拟资源，类似于文件系统里的目录，或对象存储的容器。pool可以定义一定的属性，为其子节点所继承。如
> 保护域，副本数，目标分层。
>
> volume是主要的虚拟资源，服务于应用层。volume有元数据和数据，其中数据是指应用数据，类似于文件系统的文件概念。

元数据和数据

> 统一采用固定大小的chunk进行管理，分成多种chunk类型，见<chunk.md>。
>
> dispatch_newdisk返回副本的节点列表
> chunk_proto_create/replica_srv_create可以指定分层(-1: 自动分层, 0: SSD, 1: HDD）

数据分布算法

> 对比Ceph的CRUSH算法，Lich采用更为简单的算法。
> 1. 中心化，admin节点复制chunk副本位置的分配
> 2. chunk副本位置信息，持久化到chkinfo里，而不是通过计算得到
> 3. 目前的算法，没有充分考虑设备权重
> 4. 没有引入PG层，一个chunk会在整个保护域内/集群内分配副本数，区别于PG所蕴涵的copyset复制算法。
> 5. ceph的PG对应OSDs，lich的chunk对应node
>
> 用ceph的位置策略表达lich的副本位置计算过程(__diskmap_get_insite__)：
> 1. take(root) [optional]
> 2. select(1, site)
> 3. select(n, rack)
> 4. select(1, node)
> 5. select(1, disk)
> 6. emit
>
> 以上设计选择，会带来什么样的问题呢？

区分目标属性和实际属性

> 比如关于分层的属性：priority和tier。priority是目标分层，tier是当前的实际分层。
>
> 再如，fileinfo.repnum是卷的目标副本数，chkinfo.repnum是当前的实际副本数。
>
> 实际值和目标值的偏离，需要通过一定的过程进行校正。
