# 目标/设定副本数

元数据和数据管理采用不同的规则。

配置项：

- LICH_REPLICA_METADATA === 3, in configure.h
- gloconf.chunk_rep
- gloconf.fore_write_repnum

## meta

### location

    chkinfo_t


### init

pool

    pool:    LICH_REPLICA_METADATA
    subpool: LICH_REPLICA_METADATA

volume

    vol:    fileinfo
    subvol: fileinfo
    raw:    fileinfo

snapshot:

    vol:    fileinfo
    subvol: fileinfo
    raw:    fileinfo

## data

### location

    fileinfo_t

### init

    __md_create_root
        md_initattr(&setattr, __S_IFDIR, gloconf.chunk_rep);

### rule

    // run on client-side
    md_getattr(parent, &fileinfo);
    md_info2attr(&setattr, &fileinfo);
