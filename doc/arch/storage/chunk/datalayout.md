# 数据布局

ServerSAN导出三种实体：pool, volume, snapshot。 可以按状态机的方式去理解。

元数据和数据都采用1M的数据块进行存储。 1M数据块叫做chunk，又叫做table，如table_proto_t结构。

每个chunk有如下结构标示：

- chkid_t
- chkinfo_t：增加了副本信息，放置位置为nid，chkid到磁盘的位置通过节点本地的sqlite进行管理。

## pool

pool只有元数据，由两层chunk组成。

第一层：一个chunk，          chkid：（pool:id:0）
第二层：第一次chunk的子节点，chkid：（subpool:id:idx）


顶级pool chunk，不会用到快照区域。第二级chunk的数据区，由键值对构成，
每个512B，一分为二：（name，chkinfo）


## volume

第一层：一个chunk，          chkid：（vol:id:0）
第二层：第一次chunk的子节点，chkid：（subvol:id:idx）
第三层：第二层chunk的子节点，chkid：（raw:id:idx）

对同一个volume，具有相同的ID，不同层的chunk，type不同，idx按offset计算而来。

table1_t：卷的第一层
table2_t：卷的第二层

table_proto_t：1Mchunk的内存数据结构

## snapshot

同volume，COFW（copy on first write）方式，在发生copy的时候，性能有抖动。

目前实现为链式快照。
