# INFO

## 说明

scan阶段:
- chunk recovery lost    : scan阶段发现的丢失chunk数（所有副本不可见)
- chunk recovery offline : scan阶段发现的离线副本数
- chunk recovery total   : 当前要恢复的chunk数

recover阶段:
- chunk recovery success : 已经恢复成功的chunk数
- chunk recovery fail    : 恢复失败的chunk数

推导出的数据：
- chunk need recovery    : totol - (success + fail)

task status:
- chunk recovery scanning: 处于scanning状态的节点数
- chunk recovery running : 处于running状态的节点数
- chunk recovery waiting : 处于waiting状态的节点数

## 详解

[root@44node cb]# cat /dev/shm/lich4/nodectl/recovery/info

status:waiting
lastscan:1490854481

> scan
lost:0
offline:0
recovery:0

> recover
success:0
fail:0

speed:0

[root@44node cb]# lich.node --health --json|python -mjson.tool
{
"cachestat": {
"/dev/sda": {
"disk_cache": "disable"
},
"/dev/sdb": {
"disk_cache": "disable"
},
"/dev/sdc": {
"disk_cache": "disable"
}
},
"chunkstat": {

"status": "waiting",
"lastscan": "1490853882",

"lost": "0",
"offline": "0",
"recovery": "0",

"scan_fail": 0,

"speed": "0",
"success": "0"
"fail": "0",

},

"diskstat": {
"0": {
"disk": "0",
"online": "1",
"total": "1907728",
"used": "250"
},
"1": {
"disk": "1",
"online": "1",
"total": "2861584",
"used": "250"
}
},
"nodestat": {
"admin": "192.168.110.44",
"capacity": "5000986099712",
"cluster": "aaa",
"hostname": "192.168.110.44",
"load": "0.000000",
"metas": "192.168.110.44,192.168.110.168,192.168.110.31,",
"nid": "1",
"ready": "True",
"running": true,
"skiped": false,
"starting": false,
"status": "admin",
"used": "21999124480",
"writeable": "True"
}
}
