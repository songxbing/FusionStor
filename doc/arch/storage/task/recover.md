# 恢复过程

分两阶段：
- 扫描，确定需要恢复的chunk
- 恢复，多线程执行

从sqlite扫描chkid，逐个检查其repnum，是否与fileinfo一致，如果不一致，追加到一临时文件
- chkinfo.repnum是实际副本数（需要进一步查看各副本的真实状态）
- fileinfo.repnum是目标副本数


## 需求和设计

### 需求

控制系统后台数据恢复进程所占用的带宽和 IO 处理资源，提供多种数据恢复的优先级策略。

至少提供优先前端应用或优先数据修复这两种优先级策略。在优先前端应用的策略中，数据
修复仅在资源较空闲时进行；在优先数据修复策略中，后台恢复以最快速度完成，
但卷仍然在线保持可用，仅性能有所降低。

展示恢复进度，关键参数和资源消耗情况。

### 设计

控制参数：
- 线程数
- 线程处理完一个chunk后的sleep时间
- 数据恢复的最大带宽

怎么计算当前带宽?

### 问题

## 功能说明

### 配置

配置目录：/dev/shm/lich4/nodectl/recovery

- immediately      功能开关
- thread           工作线程数
- interval         时间间隔
- suspend
- qos_maxbandwidth 设置最大带宽
- qos_sleep	   设置sleep多少豪秒
- qos_cycle	   设置计算带宽的时间间隔
- info             输出结果

### 命令及触发方式

自动触发：Recover 每10分钟执行
手动触发：echo 1 > /dev/shm/lich4/nodectl/recovery/immediately


## 测试
