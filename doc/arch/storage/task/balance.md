# 数据平衡

## 功能需求

平衡pool内各节点的空间利用率。

存储空间管理涉及几个操作：
- 分配
- 回收
- 恢复（偏离目标副本数）
- 平衡（容量和负载）

面临的问题：
- distribute data and workload to efficiently utilize available resources
- maximize system performance
- 最小化数据迁移量
- facilitating system growth and managing hardware failures
- addition and removal of storage resources

## 基本过程

只是处理卷和snapshot的raw数据块。

在每个节点上运行，扫描sqlite，按pool进行分组，获得chkid，只处理本地的非删除卷。

约束条件：
- 通一个pool内的各节点，如磁盘利用率相差大于某个指定值，则进行数据平衡工作，把数据库的副本从高利用率的节点迁移至低利用率的节点
- 副本数必须满足故障域规则，故障域有两种粒度：rack和节点，按hostname自动识别
// - tier (priority)
// - localize
// - 单节点模式
// - QoS

### 相关任务

- 恢复任务具有更高的优先级：同一个节点上，如正处于恢复过程，不启动或暂停balance过程

### 异常

- 处在恢复中的状态
- 卷被删除
- pool被删除

## 配置/控制参数

控制参数：
- /opt/fusionstack/data/balance/interval
- /opt/fusionstack/data/balance/threshold  (磁盘利用率阈值百分比，默认为3)
- /opt/fusionstack/data/balance/fill_rate （节点的平衡带宽控制）
- /opt/fusionstack/data/balance/thread     (单池平衡的工作线程数，默认为10)

功能开关
- start: echo 1 > /dev/shm/lich4/nodectl/balance/runlevel
- stop:  echo 2 > /dev/shm/lich4/nodectl/balance/runlevel

## 工具

- 查看当前任务状态：lich balance_chunk
- 查看所有pool的所有chunk数据，判定是否违反故障域规则，是否有垃圾数据，副本数是否合规

## 优化

% - V3: 初次分配时，根据节点上磁盘容量的不同，可以设置不同的权重。
