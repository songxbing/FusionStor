# 本地磁盘管理

## 问题

标准过程：
- 加盘时，需要做些什么？lichd是如何感知到该事件的？
- lichd用什么结构来管理本地磁盘？
- 如何获取磁盘的容量和使用情况？
- HSM过程: tier和cache
- 磁盘空间如何被分配和回收？
- 读写流程？

故障处理：
- 拔盘后，马上插回去，系统行为是什么样的？
- 拔盘时，recover过程？

## 过程

diskmd_init

> __diskmd_disk_add
> __diskmd_disk_remove
> __diskmd_disk_null
> __diskmd_disk_full

怎么维护disk的序号？0-255
申请空间时，如何返回可用空间？

### 添加磁盘

- 测试速度，推导tier，写入相应文件(node.py)
- lichd通过inotify机制感知到事件，并做出相应处理(diskmd.c)

### 删除磁盘

- lichd通过inotify机制感知到事件，并做出相应处理(diskmd.c)

## 命令

    lich.node --disk_list
    lich.node --disk_add --force all

## 结构

    disk_t
    diskmd_t


每块磁盘看作连续1M的数据块构成的空间。

diskid就是从零开始的整数。

磁盘状态：
- __DISK_OFFLINE__
- __DISK_DELETING__

节点状态：
- __NODE_STAT_WRITEABLE__
- __NODE_STAT_DELETING__
- __NODE_STAT_READY__
- __NODE_STAT_OFFLINE__


节点平衡
磁盘平衡
卷平衡


## 配置

- data/disk/disk/X.disk（指向块设备）
- data/disk/tier/X.tier (分层)
- data/disk/bitmap/X.bitmap (可推导总量和使用量, 见__diskmd_dump）

/dev/shm/lich4: nodectl/diskstat/X.stat

    disk:0
    online:0
    used:1
    total:51200

    disk:1
    online:1
    used:11
    total:51200

    disk:2
    online:1
    used:9
    total:51200

    disk:3
    online:1
    used:1
    total:51200
