# SSD Cache

SSD Cache实现了写缓存，没有实现读缓存, 通过内存实现读缓存。

## 配置项及系统行为

相关控制参数：

- lich.conf/disk_keep: 10G （废弃）
- lich.conf/disk_cache: 10G
- 卷的xattr: writeback
- 卷的属性：priority (设定卷的目标存储分层）

disk_cache配置磁盘cache区，对新盘有效，若一个盘已经配置，就固定了，不再改变。
此配置对SSD和HDD都起作用，预留磁盘末尾的空间。

priority是手工分层机制，持久化到卷属性上。

tier是自动分层机制，默认行为是什么？ 优先落到SSD上，若SSD满，落到HDD上。

所以，分三种情况：
- 自动分层，priority == -1，优先落盘到SSD上，如果SSD满，落盘到HDD上
- priority == 0, 同自动分层机制
- priority == 1，落盘到HDD上（此时根据writeback的设置，决定是否走SSD cache）

## 写IO流程

以下两种情况，会落盘到HDD上：
- priority == 1
- SSD满

落盘到HDD的写IO，其行为受xattr.writeback影响，分两种情况：
- xattr.writeback == 1，写入ssd的cache区+内存后，返回。
- xattr.writeback == 0, 写入HDD。

落盘到SSD的写IO，不经过Cache, 走原来的IO路径。
- 自动分层，会优先落到SSD上
- priority == 0, 会导致落盘到SSD上

## 已知问题

- 原来的分层机制，默认情况下，会写满SSD，导致后续写HDD（先快后慢）。
- 异步线程的工作机制 (周期性执行，20min执行一次, 最后才会置换proirity == 0的数据）
- disk cache区开启后，就不能再改变，空间无法回收，且不能关闭（不可逆过程）

## 不确定的地方

- 考虑以下问题，初次分配dispatch_newdisk，等固定了分层后，后续的变更规则是什么？

## HSM

### replica hsm:

replica_hsm_init

> gloconf.hsm_interval
> gloconf.hsm_usleep
>
> /opt/fusionstack/data/chunk/[0-9].db

> 定时器，HSM_UPDATE_INTERVAL 30*60

edge

move down:
- tier == 0
- priority == -1
- 活跃的判定条件 (IO)
- 存活时间

move up:
- tier == 1
- priority == 0 | priority == -1
- 活跃的判定条件 (IO)
- 存活时间


### disk hsm:

disk_hsm_rebuild

> /dev/shm/lich4/hsm/[0-99].db

    CREATE TABLE chunk (key text primary key,
    read integer,
    write integer,
    priority integer
    tier integer,
    mtime integer,
    atime integer,
    );
