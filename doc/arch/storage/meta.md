# 元数据

meta管理采用对称的中心化架构。在节点中选举出一个admin节点，
管理全局状态和数据分配工作。当admin节点发生故障时，会发生
failover过程，选举出新的admin节点来。

选举算法采用类似paxos的算法。

各节点间通过RPC进行通信，所有lichd进程构成存储cluster。

lichd进程内嵌各种协议server端，如iscsi target。

## C/S架构

client端可以与每个节点进行通信，推荐采用VIP机制，简化连接管理。

## 元数据

pool，容器，记录子节点。二级chunk块形成树型寻址结构。

volume，逻辑地址空间，由固定大小的chunk构成。三级chunk块形成树型寻址结构。

volume，支持精简配置，支持快照。精简配置和快照，会造成卷的性能抖动。所以，引入LS结构。

- QoS
- localize

snapshot，可以看作特别的卷，COW机制，与所在卷共享数据。

## 接口

VIP:
