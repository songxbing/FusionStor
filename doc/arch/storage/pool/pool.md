# 池管理

##　描述

　　每个池单独的目录树
    如：
        lichfs --list / -p pool1
        lichfs --mkdir /iscsi -p pool1
        lichfs --touch /iscsi/vol1 -p pool1


　　池列表放在rootable上
    如：
        rootable/
        ├── misc
        │   └── fileid
        ├── node
        │   ├── node2
        │   ├── node3
        │   ├── node4
        │   └── node5
        ├── sysroot
        │   ├── pool1     存放pool1根目录id
        │   └── pool2     存放pool2根目录id
        └── system
            ├── rmsnap
            ├── rollback
            └── unlink

        问题1：如果system目录也存放到rootable上，那怎么根据目录下的chkid来找到对应的pool?
              或许在每个池的目录下创建一个/system目录更合适。只把lich_system_createtime放到rootable上。

        问题2:lookup怎么处理？每个池需要遍历一次?
        问题3:对于chunk的操作都需要加上池的标识。用id?还是直接使用池的名字？


    diskmap中添加pool层(目前disk层没有使用)。
    如：
        1.pool --> site --> rack --> node --> disk     在指定pool分配chunk时，找到该pool,pool以下的分配规不变



    节点磁盘目录结构
        /opt/fusionstack/data/disk/disk/:
        lrwxrwxrwx 1 root root   14  8月 25 19:30 0.disk -> ../pool/pool1/0.disk
        lrwxrwxrwx 1 root root   14  8月 25 19:30 1.disk -> ../pool/pool1/1.disk
        /opt/fusionstack/data/disk/pool/:
        lrwxrwxrwx 1 root root    9  8月 25 19:28 0.disk -> /dev/sda5
        lrwxrwxrwx 1 root root    9  8月 25 19:29 1.disk -> /dev/sda6


## 命令

    lich.node --pool_list        #显示所有池 查询rootable
    lich.node --pool_create      #创建池 同步到rootable
    lich.node --pool_remove      #删除池（只能删除空池）

    lich.node --disk_add dev1 dev2 ... --pool pool

