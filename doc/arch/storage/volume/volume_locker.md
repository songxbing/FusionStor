# 卷的互斥锁


## HCI场景

VM启动之前，检查所有的卷是否已锁定。
如果有卷被锁定，则不能正常启动。
如果没有锁定，则该VM加锁成功后成为卷锁的OWNER。

这样可以防止多个VM加载一个卷，造成数据损坏的严重后果。

在网络故障，VM需要failover的时候，由HCI调度程序处理

- VM自杀
- 在别的主机上启动


## 用例

lichbd lock   pool1/volume1 -p iscsi
lichbd unlock pool1/volume1 -p iscsi


## 设计

### LOCK

设置卷的xattr：__LOCK__ == 1

- 加锁成功：返回0
- 加锁失败： ../utils/src/lichbd_utils.c:2059 main WARNING: exit worker (1) Operation not permitted


### UNLOCK

删除卷的xattr：__LOCK__

- 解锁成功：返回0
- 解锁失败：../utils/src/lichbd_utils.c:2059 main WARNING: exit worker (126) Required key not available
