# 控制器

每个逻辑实体，包括pool和volume，上面的所有操作，都通过全局唯一的controller节点进行协调。

controller默认是第一层的那个数据块的第一个副本所在位置，两者保持一致。

lease管理

## pool和volume共同的地方

- pool可以看着二级Btree，volume可以看着三级Btree。
- 除了用于导航的信息（树的遍历），还有额外的信息；
- 都参与lease管理过程

## pool controller

pool_proto_t
- name_tab: 记录pool的子项name到chkinfo的映射
- id_tab: 记录pool的子项chkid到chkinfio的映射

那如何定位name到subpool？ 随机查找空缺位置。

加载时，加载全部subpool。

## volume controller

volume_proto_t


基于offset定位子项


## lease管理
