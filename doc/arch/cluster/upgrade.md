# 升级

## 原则

升级过程应遵循若干原则：
- 确认原则（成功，or失败）
- 支持UNDO
- 不要覆盖已有配置
- 最小化影响
- 方便定制
- 过程可视化，方便查看进度和状态

## 目标

- 考虑版本
- 支持逐节点进行升级
- 支持维护模式，维护模式期间，暂停recover和balance过程
- 支持有条件地配置重载reload，而不是都需要restart lichd进程
- 把升级过程分解为若干阶段，可以独立定义每个阶段的任务
- 支持UNDO

## 现状

步骤：
- 统一分发升级包
- 统一重启：lich restart

命令：

    lich        update <op> [src] --force
    lich.node --update [dest] [src] [backup]

    lich start   [host]
    lich stop    [host]
    lich restart [host]

    > op := lich | etc | qemu

i.e.

> lich update etc
> lich update lich lich-testing-v4.0_346-575ec-16113014-C7.tar.gz

问题:
- 以上过程会影响上层应用吗？怎么才能改进？
- 怎么执行UNDO？

## 任务

1. 实现对单个节点进行升级，而不影响上层应用
2. ...待定

## 相关

### 定义维护模式

    lich.node --skip [0 | 1]

    > 1: 跳过，不启动
    > 0: 不跳过
