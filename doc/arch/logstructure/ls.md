# Log Structured

## cache

## dedup & compress

以pool为单位，pool上设置开关。dedup和compress作为整体进行控制。

## row snapshot

## remote mirroring

基于操作日志commit log。远程由网关接收或拉取数据。

- RPO
- RTO

## 实现

volume (-> LS) -> chunk -> replica | EC

- LS数据格式？
- commit log在什么位置？
- dedup所需repo，保持shared data block？
- 如何维护逻辑上的offset？
