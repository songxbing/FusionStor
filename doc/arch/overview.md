# 理解ServerSAN的一个模型

ServerSAN导出三种实体：pool, volume, snapshot。可以按状态机的方式去理解。

- pool：容器，目录
- volume: 文件
- snapshot：COW, 与volume采用统一的存储结构，共享部分数据

存储空间管理的基本任务
- 分配 (dispath_newid/dispath_newinfo)
- 回收空间 GC (大变小）
- 平衡 balance
- 恢复 recover
- 一致性（副本/EC）

约束条件
- 保护域
- 故障域
- QoS

优化特性

- 安全是底线
  - checksum
  - RR
- 时间(最短路径）
  - Tier/Cache
  - localize
  - RAID (writeback/writethrough)
  - Disk Cache (on/off)
  - 分支预测
  - inline
  - kernel bypass
- 空间
  - TP
  - EC
  - dedup/compress

快照

- cg
- snaptree
- COW
- ROW

产品化

- 配置管理
  - VIP
  - 维护模式
  - SNMP
- 接口
  - protocol (iSCSI, NBD, LICHBD)
  - Cloud (OpenStack Cinder, VAAI)

网络

- TCP/IP
- IB
- FC

软件工程

- 可见性
- 可测试性
- 五化（简单化，标准化，自动化，智能化, 平台化）

其它：

- log structured
- multi-tenancy
- NVMe over Fabrics(RDMA)
- iSER
- RDMA/SPDK/DPDK
