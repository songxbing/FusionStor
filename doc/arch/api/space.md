# 空间管理

## dispatch_newdisk

申请新的副本位置，可以传入现有位置。

## md_chunk_move

设置副本的位置，可能涉及到数据迁移和复制。


## Volume

创建Volume时候，会生成一个chunk。随着数据的写入，会有更多的chunk生成。

chunk分为两类：元数据和用户数据。对每一类chunk，有不同的管理策略。
