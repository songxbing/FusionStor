# 基于skiplist的timer

ytimer_t

startTimer(interval, handler) -> timerId
stopTimer(timerId)
perTickBookKeeping()


问题：
- 选择什么样的数据结构，各种操作的计算复杂度是什么？
- 如何控制时间精度？


sem_timedwait(sem, abs_timeout)


有两种方式解锁：

- 插入时，执行sem_post(sem)
- 时间到，收到事件
