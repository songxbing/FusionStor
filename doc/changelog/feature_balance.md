# 变更日志

2017.03.20

- 数据恢复
- 数据平衡

## 平衡算法
本地卷：本地卷的定义是主副本为本节点的卷。
保护域：创建时指定保护域的卷移动会在指定保护域内，未指定的卷将依据VOL CHUNK 主副本所在保护域。
故障域：卷所在site中rack数量大于副本数的，故障域限制在rack级，不够的限制在node级。

每次balance每遍会扫描本地卷，观察节点之间的利用率，将不满足利用率差值条件的（默认百分之五）的CHUNK进行搬移，
直到节点间满足利用率，或者已经平衡完自己控制的卷的数据块。

每一遍又为若干步长，步长完成会重新观察节点间利用率，如若利用率满足条件会提前终止balance的过程。

扫描本地卷
依次处理每一个卷（获取保护域的节点列表）， 取出满足移动条件的chunk放入任务队列。
对任一数据块，处理其各个副本
- 跳过主副本
- 检查磁盘利用率条件
- 卷的chunk移动不能跨过保护域
- 同一chunk的不同副本不能在同一故障域（故障域的定义是根据根据卷的副本数和卷的保护域内的rack数决定的）

任务队列：
- 主线程负责生成JOB列表
- 工作线程进行处理

终止条件：
- 节点间已经满足数据平衡。
- 不能再找到需要平衡的数据块

## 文件接口
- /dev/shm/lich4/nodectl/balance/immediately  手动启动balance 过程
- /dev/shm/lich4/nodectl/balance/interval     自动启动interval的间隔时间
- /dev/shm/lich4/nodectl/balance/thread       banlance过程中的线程数
- /dev/shm/lich4/nodectl/balance/step         balance中的step大小（每一定step数的chunk重新观察节点）
- /dev/shm/lich4/nodectl/balance/info         balance过程中dump出的信息
- /dev/shm/lich4/nodectl/balance/fill_rate    balance 速率限制

                                              info 例：
                                              ```
                                              status:done   balance状态
                                              interval:28800  balance间隔 8小时
                                              step:100        step=100
                                              thread: 5       5线程
                                              fill_rate 30    填充率30M
                                              vol_count: 0    本地卷数量
                                              node_count:0    节点数量
                                              job_count:0     总的chunk移动数
                                              job_left:0      剩余需要移动的chunk
                                              ```
### 测试逻辑：
    平衡的过程会达到一个相对的平衡，如果节点较少，卷的数量很少的情况下，因为受主副本不动的限制，
    （主副本很可能在一个利用率最高的节点上而本节点的数据大部分都不能动），平衡效果也是受数据分布限制的，
    所以每个卷的controler做的只是把自己的本地卷能迁移的进行迁移，使数据分布趋于平衡。
    balance 平衡的过程中主要是，节点balance前后数据分布的对比、balance的终止条件（分布已经平衡、或者不
    能再找到需要平衡的数据块）、balance过程中的副本迁移规则（故障域、保护域）。
