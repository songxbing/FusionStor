# localize_qos

## 2017/5/31

## localize
目前涉及chunk r/w 过程 卷的controler会随着session迁移(stor_localize 会把卷的主副
本拉过来) ，对卷进行读写时先进行chunk_check， 如果不是本地化的， 会在controler 的
生成一个对应的task加入后台chunk_bh队列（chunk sync 和 chunk localize 共用chunk_bh
队列），chunk localize 目前对前端的读写性能产生的较大的影响。

### qos
目的通过限制后台localize的速率，减少对前端业务的影响（不涉及sync的过程）
1. 通过手动指定localize速率限制后台执行速度。
2. 当前速率达到限速时任务移到队尾，优先执行其他任务。
3. 当队列中没有其他类型任务则阻塞队列运行。
4. 当队列中拥有超过十万个任务时，新的localize 请求将被丢弃。

控制接口:
1. /dev/shm/lich4/nodectl/localize/fill_rate
  echo 100 > /dev/shm/lich4/nodectl/localize/fill_rate 限制后台localize 速率为 100

2. /dev/shm/lich4/nodectl/localize/info
  cat /dev/shm/lich4/nodectl/localize/info

```
  chunk_bh info:
  queue_count:2                                 总chunk _bh队列数量
  count:79242                                   总chunk_bh任务数量
  threads:2                                     总chun_bh执行线程数量
  fill_rate:500.000000                          localize限速
  rate:187                                      当前localize速率
  queue0:                                       下面是各个队列具体的任务数
  count:1314
  sync:0
  localize:1314
  queue1:
  count:77928
  sync:0
  localize:77928
```
