# 变更日志

2017.02.20
- 增加 ylog 多实例，将原来的analysis used类型的log分离出来。
- 增加 performance_analysis 等变量的控制接口,和fetch 接口
        lich.admin --setvariable <key> <value> [-n hostname]
        lich.admin --fetchvariable <key> [-n hostname]
