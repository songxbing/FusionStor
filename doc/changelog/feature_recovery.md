# 变更日志

2017.03.21

- 数据恢复

1. 算法
使用token_bucket 算法实现recovery速度恢复限制，主要思想是限
制最大的恢复速度，来调节恢复速度。

2. 每遍的处理过程：
- 扫描需要恢复的chunk个数，恢复在vol control 节点进行恢复，
- 启动工作线程，对需要恢复的chunk进行恢复。
  如果是多个线程同时恢复，每个线程每次恢复1个chunk的数据，所有线程
  并行执行恢复过程。

3. 环境参数配置说明
    配置目录：/dev/shm/lich4/nodectl/recovery
    - fill_rate        恢复速度最大阀值
    - immediately      功能开关
    - thread           工作线程数
    - interval         时间间隔
    - info             输出结果
   默认配置：thread=1，fill_rate=100, interval=600
   进程数最大配置：10
   参考配置：当thread设置为1，fill_rate 设置300
             当thread设置大于5，fill_rate 设置300到500
      
4. 观察恢复进度cat  /dev/shm/lich4/nodectl/recovery/info
   [root@node69 build]# cat  /dev/shm/lich4/nodectl/recovery/info
		status:running
		recovery:16555
		success:2456
		fail:0
		lost:0
		offline:16544
		speed:30
		lastscan:1489993156
    
    recovery=success+fail
    offline：需要恢复的raw chunk 个数
    speed：恢复速度
    
5. 数据完整性的验证。
   观察恢复之前数据的md5sum和恢复之后的md5sum值。
   
6. 测试逻辑
   1. 测试进程数大于10，查看进程是否会默认设置为10，查看接口：
      cat  /dev/shm/lich4/nodectl/recovery/thread      
      测试fill_rate异常参数，例如：-1,0,100,1000
   2. 设置vol为1，thread 进程数1、5、10、恢复速度阀值fill_rate设置为，100到500恢复速度，测试单卷和进程与恢复速度的关系。
   3. 设置vol为5，thread 进程数1、5、10，恢复速度阀值fill_rate设置为，100到500恢复速度，测试多卷和进程与恢复速度的关系。
   4. 设置vol大于10，thread 进程数1、5、10，恢复速度阀值fill_rate设置为，300到700恢复速度，测试多卷的最大恢复速度。
   5. 观察最大恢复速度，对系统性能的影响，可以查看top，内存消耗，对业务的影响。

