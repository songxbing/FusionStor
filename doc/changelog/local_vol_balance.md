# 本地卷平衡
### 2017/5/18

目前vol create 的时候通过diskmap， 随机分配了vol 副本数的节点， 这样卷少的情况下会出现节点上的本地卷分布不均的情况。

## 改造方案
1.  master 节点， 在当前nodetable （nodetable 中有当前集群下所有节点的状态，和diskmap）中引入节点本地卷 和 本地卷数量 。

2. 所有节点在 节点启动后 会向master 节点同步自己的本地卷（本地卷比较多会多次同步）， master crash会向新的节点同步。

3. 在目前的结构基础上， 在通过disppatch_new_disk时使用伪随机方式， 选出本地卷最少的节点作为controler， 再在controler所在site内 分配其余副本 。

4. 节点 添加 删除 卷 chunk_select的过程 都会通过 rpc 通知 master， 更新 master 端的计数 。

## 涉及文件
cluster/dispatch/dispatch.c dispatch_rpc.c dispatch_srv.c
cluster/nodepool/nodetable.c
storage/storage/local_vol.c

* 增加 rpc  dispatch_vol_notifiction
同步本地卷、 添加 删除 conterol 迁移通知 master。
* 修改 dispatch_newdisk 统一最后一个参数的 flag {balance， force， controller}
controller 被置位时会选出site 内本地卷最少的节点作为 第0副本位置 。
对应的  nodetable_get_insite
* 增加api __nodetable_get_local_vol_min_insite(const char *site_name, nid_t *nid) site 下 本地卷最少的节点
* __pool_proto_newinfo 增加一个参数为创建的类型 { pool， vol}

6. 测试逻辑
1. 通过命令创建若干卷，卷（控制器）会均匀分布在集群节点上。
2. 节点启动， 手动添加、删除卷，conterl发生迁移（如原control 断电断网），都会通过日志观察到
3. 目前所有节点24小时会向master 同步自己的状态 。

4. controler 随session 迁移的状况暂时未进行同步， 目前后台localize需要限速，限速完成后进行执行。
```
dispatch_srv_vol_notifiction INFO: node:site1.zone1.node2 vol notifiction:3 options:1
```
其中 option  0 是节点启动同步
						 1 是创建卷
						 2 是删除卷
