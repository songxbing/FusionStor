# 变更日志

2017.02.20

- lichfs和lich.snapshot功能合并到lichbd，lichfs和lich.snapshot保持向后兼容
- 改写admin/*下调用lichbd的地方，主要是参数格式，语义不变
- 新增功能以后在lichbd上做
- 后续接口变化更新命令版本号

## 发现的一些问题

- admin/*下的python代码，调用lich相关命令时，没有必要的封装，难以维护
- admin/*的实现，没有必要的封装和组织，难以维护和扩展
- 需要逐步重构

## NEW lichbd

lichbd (version 0.0.2)

Available commands:
  pool create <pool> [-A storage_area] [-p protocol]
                                               create pool
  pool rm <pool> [-p protocol]                 remove pool
  pool ls [pool] [-p protocol]                 list pools

  vol create <volume> --size <M> [-A storage_area] [-p protocol] [-T <0|1>]
                                               create an empty volume, unit(k|K, |m|M, g|G)
  vol rm <volume> [-p protocol]                remove volume
  vol ls [pool] [-p protocol]                  list volumes
  vol info <volume> [-p protocol]              show information about volume size, date, etc
  vol mv <src> <dest> [-p protocol]            move src volume to dest
  vol rename <src> <dest> [-p protocol]        rename src volume to dest
  vol resize <volume> --size <M> [-p protocol] resize the volume, unit(k|K, |m|M, g|G)
  vol copy <src> <dest> [-A storage_area] [-p protocol] [-T <0|1>]
                                               copy src volume to dest
  vol duplicate <src> <dest> [-A storage_area] [-p protocol]
                                               duplicate src volume to dest in the specified protection domain.
  vol migrate <volume> [-A storage_area] [-p protocol]
                                               migrate volume to specified protection domain.
  vol import <path> <volume> [-A storage_area] [-p protocol] [-t <1|0>] [-T <0|1>]
                                               import volume from file. '-' for stdin, -t thin -T tier
  vol export <volume> <path> [-p protocol]     export volume to file. '-' for stdout
  vol lock <volume> [-p protocol]              lock an volume
  vol unlock <volume> [-p protocol]            unlock an volume

  snap create <volume>@<snap> [-A storage_area] [-p protocol]
                                               create a snapshot
  snap rm <volume>@<snap> [-p protocol]        deletes a snapshot
  snap ls <volume> [-p protocol] [-a]          dump list of volume snapshots, -a will show all
  snap rollback <volume>@<snap> [-p protocol]  rollback volume to snapshot
  snap clone <volume>@<snap> <volume> [-A storage_area] [-p protocol] [-T <0|1>]
                                               clone a snapshot into a COW
  snap flat <volume> [-p protocol]             flat a volume 
  snap protect <volume>@<snap> [-p protocol]   prevent a snapshot from being deleted
  snap unprotect <volume>@<snap> [-p protocol] allow a snapshot to be deleted
  snap cat <volume>@<snap> [-p protocol]       cat a snapshot 
  snap diff <volume>@<snap src>~<snap dst> [idx|from~to] :<file name> [-p protocol]
                                               diff snap and file name 
  snap copy <volume>@<snap> [idx|from~to] :<file name> [-p protocol]
                                               copy snap to file name

  attr set <pool|volume> <key> <value> [-p protocol]
                                               set attr
  attr rm <pool|volume> <key> [-p protocol]    remove attr
  attr ls <pool|volume> [-p protocol]          list all attrs
  attr get <pool|volume> <key> [-p protocol]   get attr

  find <path> [key]                            find key from path, no key wile show all files in the path
  stat <path>                                  show the path state
  fsstat                                       show the storage system state
  liststoragearea                              list the storage area


## OLD lichbd

lichbd (version 0.0.1)

Available commands:
  mkpool <pool> [-A storage_area] [-p protocol]
                                               create pool <pool>
  rmpool <pool> [-p protocol]                  remove pool <pool>
  lspools [pool] [-p protocol]                 list pools

  create <image> --size <M> [-A storage_area] [-p protocol] [-T <0|1>]
                                               create an empty image,unit(k|K, |m|M, g|G)
  rm <image> [-p protocol]                     delete an image
  ls [pool] [-p protocol]                      list lichbd images
  info <image> [-p protocol]                   show information about image size, date, etc
  mv <src> <dest> [-p protocol]                move src image to dest
  rename <src> <dest> [-p protocol]            rename src image to dest
  resize <image> --size <M> [-p protocol]      resize the image,unit(k|K, |m|M, g|G)
  copy <src> <dest> [-A storage_area] [-p protocol] [-T <0|1>]
                                               copy src image to dest
  duplicate <src> <dest> [-A storage_area] [-p protocol]
                                               duplicate src image to dest in the specified protection domain.
  migrate <pool/image> [-A storage_area] [-p protocol]
                                               migrate image to specified protection domain.
  import <path> <image> [-A storage_area] [-p protocol] [-t <1|0>] [-T <0|1>]
                                               import image from file. '-' for stdin, -t thin -T tier
  export <image> <path> [-p protocol]          export image to file. '-' for stdout

  lock <image> [-p protocol]                   lock an image
  unlock <image> [-p protocol]                 unlock an image

  attrset <path> <key> <value>                 set attr <path>
  attrremove <path> <key>                      remove attr  <path> <key>
  attrget <path> <key>                         list pools
  attrlist <path>                              list attr <path>
  clone <pool/image>@<snap> <pool/dest-image> [-A storage_area] [-p protocol] [-T <0|1>]
                                               clone a snapshot into a COW

  snap create <image>@<snap> [-A storage_area] [-p protocol]
                                               create a snapshot
  snap remove <image>@<snap> [-p protocol]     deletes a snapshot
  snap ls <image> [-p protocol] [-a]           dump list of image snapshots, -a will show all
  snap cat <image>@<snap> [-p protocol]        cat a snapshot
  snap diff <image>@<snap src>~<snap dst> [idx|from~to] :<file name> [-p protocol]
                                               diff snap and file name
  snap copy <image>@<snap> [idx|from~to] :<file name> [-p protocol]
                                               copy snap to file name
  snap flat <image> [-p protocol]              flat a image
  snap rollback <image>@<snap> [-p protocol]   rollback image to snapshot
  snap protect <image>@<snap> [-p protocol]    prevent a snapshot from being deleted
  snap unprotect <image>@<snap> [-p protocol]  allow a snapshot to be deleted

