# 变更日志

2017.03.30

recovery:
- 检查可写节点个数，如果发生变化，退出当前任务，重新开始
- 检查EREMCHG事件，计入success，并退回消耗的token

balance:
- 跳过本地副本，而不一定是第一副本

引入vec_t:
