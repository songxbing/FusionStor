#!/bin/bash

DOC_DIR="/sysy/yfs/app/doc"
MANUAL="manual.pdf"

usage()
{
    echo "Usage: $0 [OPTIONS]"
    echo "-i install"
    echo "-e remove"

    exit 1
}

install()
{
    echo "create $DOC_DIR"
    mkdir -p $DOC_DIR
    echo "cp $MANUAL $DOC_DIR"
    cp $MANUAL $DOC_DIR
}

remove()
{
    echo "remove $DOC_DIR"
    rm -fr $DOC_DIR
}

if [ $# -lt 1 ]
then 
    usage
fi

while getopts ieh options
do
    case $options in
        i)
        echo "install doc"
        install
        ;;
        e)
        echo "remove doc"
        remove
        ;; 
        h)
        usage
        ;;
        \?)
        usage
        ;;
    esac
done
        
