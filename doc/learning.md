# 学习

## 理念

做中学，学中做
刻意练习

复杂的事情简单做
简单的事情重复做
重复的事情用心做

## 基础

### TOOL

开发工具
- markdown <http://wowubuntu.com/markdown/#autolink>
- git <https://github.com/geeeeeeeeek/git-recipes/wiki>
- cmake
- clion/vim
- gdb
- ldconfig
- ldd
- nm
- valgrind

测试工具
- fio

性能分析：
- iostat
- top
- systemtap
- perf

基本工具
- yum or apt-get
- ssh
- rsync
- wget
- tree
- find
- grep
- tcpdump

系统配置
- /etc/resolv.conf
- ~/.vimrc

编程语言
- C
- linux shell
- python
- lua

### 关键流程

- 部署和测试lich（单节点模式）
- 熟悉编码规范
- bugfix
- gdb调试（运行，或结合core file）
- 自测

## 进阶

### 理论

计算机体系结构：
- CPU
- Memory
- IO
- Network

存储：
- RAID

分布式系统：
- 通信: RPC
- 副本和一致性
- 容错
- 安全

### 开源项目

- open vstorage
- ceph
- sheepdog
- qemu
- fuse

### 产品

- VSAN
- Nutanix
- EMC ScaleIO
- Huawei FusionStorage
