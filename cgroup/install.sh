#!/bin/bash
DIST=$2/cgroup
ETC_DIR=$(echo $2 |sed 's;/lich/*$;;')/etc
mkdir -p $DIST
rm -rf $DIST/*
cp  cgroup.conf  $ETC_DIR
cp ./* $DIST/
rm -f $DIST/Makefile
rm -f $DIST/install.sh
rm -f $DIST/cgroup.conf
sed -i "s;/opt/fusionstack/etc;$ETC_DIR;"  $DIST/*
sed -i "s;/opt/fusionstack/lich/cgroup;$DIST;"  $DIST/*
