#!/bin/bash

touch /opt/fusionstack/lich/cgroup/hazpid
touch /opt/fusionstack/lich/cgroup/hosts
homedir=/opt/fusionstack/lich/cgroup


while [ 1 ]
do
	lockdev -l /dev/null
	if [ $? == 0 ];then
		break
	fi

	sleep 1
done

if [ ! -d "/cgroup/cpuset/qemu" ];then
	echo ERROR: not found /cgroup/cpuset/qemu
	exit 1
fi

if [ ! -d "/cgroup/memory/qemu" ];then
	echo ERROR: not found /cgroup/memory/qemu
	exit 1
fi

for p in $(cat $homedir/hosts)
do
	pid=$(echo $p |sed 's/-.*//')
	
	if [ ! -d "/proc/$pid" ];then
		sed -i "/$p/d" $homedir/hosts
		echo kvm pid $pid exit..
		continue
	fi	

	for tid in $(ls /proc/$pid/task)
	do
		if [ $(grep "^$tid$" /cgroup/{cpu,cpuset,memory}/qemu/tasks 2>/dev/null|wc -l) -lt 3 ];then
				echo ERROR:kvm pid $pid tid:$tid not found 
				sed -i "/$p/d" $homedir/hosts
		fi
	done

done

for proc in $(cat $homedir/hazpid)
do
		pid=$(echo $proc |sed 's/-.*//')
		if [ ! -d "/proc/$pid" ];then
			sed -i "/$pid/d" $homedir/hosts
			echo hazard pid $pid exit..
			continue
		fi	
		for tid in $(ls /proc/$pid/task)
		do
			if [ $(grep "^$tid$" /cgroup/{cpu,cpuset,memory}/qemu/tasks 2>/dev/null|wc -l) -lt 3 ];then
				echo ERROR:hazard pid $pid tid:$tid not found  
				sed -i "/$proc/d" $homedir/hazpid
			fi
		done	
done

lockdev -u /dev/null
for type in lich qemu
do
	filename=$type
	if [ "$filename" == "qemu" ];then
		filename=kvm
	fi
	. $homedir/${filename}.conf
	mem=$(cat /cgroup/memory/$type/memory.limit_in_bytes)
	if [ "$mem" != "$MEMLIMIT" ];then
		echo ERROR: $type memlimit set $mem config $MEMLIMIT; 
	else
		echo INFO: $type memlimit:$mem
	fi
	echo INFO: $type memory failcnt:$(cat /cgroup/memory/$type/memory.failcnt)
	echo INFO: $type memory swap used:$(cat /cgroup/memory/$type/memory.stat |awk '{if($1=="total_swap"){print $2}}')

	swap=$(cat /cgroup/memory/$type/memory.swappiness)
	if [ "$swap" != "$SWAP" ];then
		echo ERROR: $type SWAPPINESS set $swap config $SWAP; 
	else
		echo INFO: $type swappiness:$swap
	fi

	cpus=""
	cpuset=$(cat /cgroup/cpuset/$type/cpuset.cpus)
	for cpu in $(echo $cpuset | sed 's/,/ /g')
	do
        	if ! echo $cpu|grep '-' >/dev/null;then
                	continue
        	fi

		for c in $(seq $(echo $cpu|sed 's/-/ /'))
		do
			cpus="$c,$cpus"
		done
	
	
	done
	

	ncpu=$(echo $cpuset | awk -F, 'BEGIN{ORS=","}{for(n=1;n<=NF;n++)if($n!~/-/){print $n}}'|sed 's/\,$//')
	ncpu="$ncpu,$cpus"
	real_cpu=$(echo -ne $(echo $ncpu|sed 's/,/\\n/g')|sort -n)
	config_cpu=$(echo -ne $(echo $CPUSET|sed 's/,/\\n/g') |sort -n)
	config_cpu=$(echo $config_cpu|sed 's/ /,/')
        real_cpu=$(echo $real_cpu|sed 's/ /,/')

	if [ "$real_cpu" != "$config_cpu" ];then
		echo ERROR: $type cpu set $real_cpu config $config_cpu
	else
		echo INFO: $type cpuset:$real_cpu
	fi

done
echo 
echo ====================show the cgloop.sh log============================
echo 
cat $homedir/log

