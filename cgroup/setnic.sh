#!/bin/bash
##
##
LICH_CONF="/opt/fusionstack/lich/cgroup/lich.conf"
LICHD_CONF="/opt/fusionstack/etc/lich.conf"
KVM_CONF="/opt/fusionstack/lich/cgroup/kvm.conf"
lich_nic_cnt=0
kvm_nic_cnt=0

if ! test  -f "$LICHD_CONF" || ! test -f "$LICH_CONF" || ! test  -f "$KVM_CONF" ;then
	exit
fi

lich_cpu_list=$(sed -n 's/CPUSET=//p' $LICH_CONF |sed 's/,/ /g')
kvm_cpu_list=$(sed -n 's/CPUSET=//p' $KVM_CONF |sed 's/,/ /g')
lich_cpu_num=$(echo $lich_cpu_list|awk '{print NF}')

if [ $lich_cpu_num -lt 4 ];then
	
	exit
fi

kvm_cpu_num=$(echo $kvm_cpu_list|awk  '{print NF}')
cpu_index=0	
#echo_mask_to_irq()
#{
		
#}

next_cpu()
{
	proc=$1
	irq_num=$2
	cpu_list=$(eval echo \$${proc}_cpu_list)
	cpu_num=$(eval echo \$${proc}_cpu_num)
	set $cpu_list
	shift $cpu_index
	echo the cpu list  $*  irq $irq_num process $proc  cpuindex $cpu_index
	for cpu in $*
	do
		aff_val=`echo "obase=16;ibase=10;$((2#1<<$cpu))"|bc`
		echo "echo $aff_val  > /proc/irq/$irq_num/smp_affinity"
		echo $aff_val  > /proc/irq/$irq_num/smp_affinity
		break
	done
	
	cpu_index=$((cpu_index + 1))

	if [ $cpu_index -eq $cpu_num ];then
		cpu_index=0
	fi
}
set_nic_cpu_affinity()
{
	tp=$1
	shift
	
	for eth in $@
	do
		echo set process $tp nic $eth irq 
		irq=$(cat /proc/interrupts |grep "$eth" |grep -v grep |grep -v $0 |awk '{if($NF~/-/){print substr($1, 0, length($1) -1)}}')
		for ir in $irq
		do
			next_cpu $tp $ir
		done
	done	
	
				
}
set_kvm_cpu_affinity()
{
	cpu_index=0
	set_nic_cpu_affinity kvm $@
}
set_lich_cpu_affinity()
{
	cpu_index=0
	set_nic_cpu_affinity lich $@
}
if  test -f  $LICH_CONF && test -f $KVM_CONF ;then
	cd /sys/class/net
	for nic in $(ls)
	do
		if [ -L "$nic/device" ];then
			nic_ipinfo=$(ip addr list $nic |awk '{if(a==2){a=0;if($0~/inet /){print $2}};if(a==1){a++};if($0~/state UP/){a=1}}')
			if [ -z "$nic_ipinfo" ];then
				kvm_nic_cnt=$(($kvm_nic_cnt + 1))
				kvm_nic_list="$nic $kvm_nic_list"
				continue
			fi
			nic_ip=$(echo $nic_ipinfo|sed 's/\/.*//')
			nic_mask=$(echo $nic_ipinfo|sed 's/.*\///')
		#	echo nic mask is $nic $nic_mask $nic_ipinfo 
			if [ $nic_mask -ge 8 ];then	
				mask=255;
			else
				nmask=$(((2**$nic_mask) - 1))
				mask=$(($nmask <<(8 - $nic_mask)))
			fi
			for oct in $(echo $nic_ip |awk -F . '{for(i=1;i<=NF;i++){print $i}}')
			do
				newoct=$(($oct &  $mask))
				new_net="$new_net.$newoct"
				nic_mask=$(($nic_mask - 8))
				if [ $nic_mask -le 0 ];then
					mask=0
					continue
				fi
				if [ $nic_mask  -lt 8 ];then
					nmask=$(((2**$nic_mask) - 1))
					mask=$(($nmask <<(8 - $nic_mask)))
				fi
			done
		
			new_net=$(echo $new_net |sed 's/^.//')
		
			for lich_net in $(cat /opt/fusionstack/etc/lich.conf  |awk '{if(a==1){if($0~/[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*/){print $0};if($0~/\}/){exit}};if($0~/networks/){a=1}}' |sed -n 's/\/.*//p')
			do
				echo $new_net $lich_net
				if [ "$new_net" == "$lich_net" ];then
					lich_nic_list="$nic $lich_nic_list"
					lich_nic_cnt=$(($lich_nic_cnt + 1))
					found="ok"
					break
				fi
			done
			if [ -z "$found" ];then
				kvm_nic_cnt=$(($kvm_nic_cnt + 1))
				kvm_nic_list="$nic $kvm_nic_list"
			fi
			found=""
		fi
	done

	if [ "$lich_nic_cnt" -gt 0 ];then 
		echo ====================set lich nic ============================
		set_lich_cpu_affinity $lich_nic_list
		echo ====================set kvm nic ============================
		if [ "$kvm_nic_cnt" -gt 0 ];then
			set_kvm_cpu_affinity $kvm_nic_list
		fi	
	fi	
fi
