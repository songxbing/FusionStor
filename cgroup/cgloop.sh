#!/bin/bash

touch /opt/fusionstack/lich/cgroup/hosts
touch /opt/fusionstack/lich/cgroup/hazpid

homedir=/opt/fusionstack/lich/cgroup

while [ 1 ]
do
	lockdev -l /dev/null
	if [ $? == 0 ];then
		break
	fi

	sleep 1
done

for host in $(cat $homedir/hosts)
do
	pid=$(echo $host |sed 's/-.*$//')
	vncinfo=$(echo $host |sed 's/[^-]*-//')
	if [ -d "/proc/$pid" ];then
		vi=$(echo $(cat /proc/$pid/cmdline)  |sed -n 's/.*-vnc\([0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+:[0-9]\+\).*/\1/p')	
		if [ -z "$vi" ];then
			 vi=$(echo $(cat /proc/$p/cmdline)  |sed -n 's/.*-vnc:\([0-9]\+\).*/\1/p')
			 if [ -z "$vi" ];then
			 	continue
			 fi
		else	 
			vi=$(echo $vi|sed 's/:/-/')
		fi	 
		if [ "$vi" == "$vncinfo" ];then
			continue
		fi	
	fi

	$homedir/cgkvmctrl.sh delete $host 
	sed -i "/$host/d" $homedir/hosts
done


for haz in $(cat $homedir/hazpid)
do
	echo hazard $haz
	pid=$(echo $haz |awk -F- '{print $1}')																
	num=$(echo $haz |awk -F- '{print $2}')																
	name=$(echo $haz |awk -F- '{print $3}')
	exist=$(ps aux |grep "$pid"|grep -v grep |grep -v $0 |awk 'BEGIN{a="'$num'"}{alen=split($a, array, "/");if(array[alen] == "'$name'"){print $a}}') 
	if [ -z "$exist" ];then
		sed -i "/$haz/d" $homedir/hazpid
	fi
done

echo $(date) begin scan new qemu process >$homedir/log
echo  >> $homedir/log

for p in $(ps aux |grep qemu |grep -v grep |awk '{print $2}')
do
	
	vi=$(echo $(cat /proc/$p/cmdline)  |sed -n 's/.*-vnc\([0-9]\+\.[0-9]\+\.[0-9]\+\.[0-9]\+:[0-9]\+\).*/\1/p')
	if [ -z "$vi" ];then
		vi=$(echo $(cat /proc/$p/cmdline)  |sed -n 's/.*-vnc:\([0-9]\+\).*/\1/p')
		if [ -z "vi" ];then
			continue
		fi
	else
		vi=$(echo $vi|sed 's/:/-/')
	fi

	if [ -z "$vi" ];then
		continue
	fi

#	mem=$(echo $(cat /proc/$p/cmdline)  |sed -n 's/.*-m\([0-9]\+[m|M|k|K|g|G]\)\-.*/\1/p')
#	if [ -z "$mem" ];then
#		mem=$(echo $(cat /proc/$p/cmdline)  |sed -n 's/.*-m\([0-9]\+\)\-.*/\1/p')
#	fi
#
#	if [ -z "$mem" ];then
#		continue
#	fi
#
#	if echo $mem |grep -i g >/dev/null; then
#		g=$(echo $mem|sed 's/[g|G]$//')
#		mem=$(($g * 1024 * 1024 * 1024))
#	elif echo $mem |grep -i k >/dev/null;then
#		k=$(echo $mem|sed 's/[k|K]$//')	
#		mem=$(($k * 1024))
#	else
#		m=$(echo $mem|sed 's/[m|M]$//')
#		mem=$((m * 1024 * 1024))
#	fi
	
		
	hi=$p-$vi

	for h in $(cat $homedir/hosts)
	do
		if [ "$h" == "$hi" ];then
			f=ok;
			break
		fi
	done	
	if [ -z "$f" ];then
#		 ls /proc/$p/task/ -l |awk '{if($1~/^d/){print $NF}}' >  $homedir/ 
#	       	 mkdir  $homedir/$hi
#		 cp $homedir/kvm.conf $homedir/$hi/
#		 sed -i "s/MEMLIMIT=.*$/MEMLIMIT=$mem/" $homedir/$hi/kvm.conf
		 $homedir/cgkvmctrl.sh  create $hi  $homedir/kvm.conf 2>&1 >>$homedir/log
		 echo $hi >> $homedir/hosts
	else
		for tid in $(ls /proc/$p/task)
		do
			 if [ $(grep $tid /cgroup/{cpu,cpuset,memory}/qemu/tasks 2>/dev/null|wc -l) -lt 3 ];then
			 	 $homedir/cgkvmctrl.sh  create $hi  $homedir/kvm.conf 2>&1 >>$homedir/log
				 break
			 fi	 
		done
	fi
	f=""
done


echo $(date) begin to scan hazard process >> $homedir/log
echo  >>$homedir/log


f=""
for proc in $(cat $homedir/process.conf)
do
	for hinfo in $(ps aux |grep $proc |grep -v grep |grep -v $0 |awk  '{for(i=10;i<13; i++){alen=split($i, array, "/");if(array[alen] == "'$proc'"){print $2"-"i"-"array[alen]}}}')
	do
		for ha in $(cat $homedir/hazpid)
		do
			 if [ "$ha" == "$hinfo" ];then
	         		f=ok
	                	break
	         	 fi
		done

		if [ -z "$f" ];then
			$homedir/cgkvmctrl.sh create $hinfo $homedir/kvm.conf 2>&1 >>$homedir/log
			echo $hinfo >> $homedir/hazpid

		else
			pid=$(echo $hinfo |sed 's/-.*//')
			for tid in $(ls /proc/$pid/task)
			do
				if [ $(grep $tid /cgroup/{cpu,cpuset,memory}/qemu/tasks 2>/dev/null|wc -l)  -lt 3 ];then
					$homedir/cgkvmctrl.sh  create $hinfo  $homedir/kvm.conf 2>&1 >>$homedir/log
					break
				fi
			done	
		fi
		f=""
	done
done

lockdev -u /dev/null

echo $(date) begin to scan lich process >>$homedir/log
echo  >>$homedir/log

$homedir/cglichctrl.sh  create $homedir/lich.conf  2>&1 >>$homedir/log
