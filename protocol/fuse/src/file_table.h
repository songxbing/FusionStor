#ifndef __FILETABLE_H__
#define __FILETABLE_H__

int filetable_init();
int filetable_open(int *_fd, const fileid_t *_fid);
int filetable_get(int fd, fileid_t *_fid);
int filetable_close(int fd);


#endif
