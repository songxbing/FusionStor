#ifndef __SD_MAP_H__
#define __SD_MAP_H__

int sd_map_init();
int sd_map_open(int *_fd, const fileid_t *_fid);
int sd_map_get(int fd, fileid_t *_fid);
int sd_map_close(int fd);


#endif
