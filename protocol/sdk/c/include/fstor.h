#ifndef __FSTOR_H
#define __FSTOR_H

#define FSTOR_OPEN_FILEINFO 0x00000001
#define FSTOR_OPEN_STAT     0x00000002
#define FSTOR_OPEN_ALL      0x00000003

typedef struct {
        char name[MAX_NAME_LEN];
        int flags;
        fileid_t fileid;
        fileinfo_t fileinfo;
        struct stat stbuf;
} fstor_vol_open_ctx_t;

int fstor_vol_open(const char *name, int flags, fstor_vol_open_ctx_t *ctx);

#endif