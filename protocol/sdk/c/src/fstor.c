#include "config.h"

#include <stdint.h>

#include "lich_md.h"
#include "lichstor.h"
#include "dbg.h"

#include "fstor.h"

int fstor_vol_open(const char *name, int flags, fstor_vol_open_ctx_t *ctx) {
        int ret;

        ctx->flags = flags;

        ret = stor_lookup1("default", name, &ctx->fileid);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        if (flags & FSTOR_OPEN_FILEINFO) {
                ret = md_getattr(&ctx->fileid, &ctx->fileinfo);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        }

        if (flags & FSTOR_OPEN_STAT){
                ret = stor_getattr("default", &ctx->fileid, &ctx->stbuf);
                if (unlikely(ret))
                        GOTO(err_ret, ret);
        }

        return 0;
err_ret:
        return ret;

}
