local ffi = require('ffi')
local struct = require('struct')

require('cdef.base')

ffi.cdef[[

]]


local stor_rpc = require('storage.stor_rpc')


local C = ffi.load('lich4s')


local function init_lich()
	C.env_init_simple('lichbd')
	C.network_connect_master()
	C.stor_init(nil, -1)
end

function main()
	init_lich()
	stor_rpc.stor_rootid()
end

-- TODO for test
main()

return {
	C = C,

	init_lich = init_lich,
}
