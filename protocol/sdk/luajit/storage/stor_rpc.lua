local ffi = require('ffi')
local struct = require('struct')

require('cdef.base')

ffi.cdef[[

//

int stor_rootid(chkid_t *rootid);

// from lich_stor.h
int stor_lookup1(const char *path, fileid_t *id);
int stor_lookup(const fileid_t *parent, const char *name, fileid_t *id);

// from lich_md.h

// from stor_ctl.h
int stor_rpc_listpool_open(const nid_t *nid, const chkid_t *parent, const char *uuid);
int stor_rpc_listpool(const nid_t *nid, const fileid_t *parent, const char *uuid, uint64_t offset, void *de, int *delen);
int stor_rpc_listpool_close(const nid_t *nid, const chkid_t *parent, const char *uuid);


]]

local C = ffi.load('lich4s')

local function print_chkid(chkid)
    print ('chkid=', chkid.id, chkid.idx, chkid.type)
end


local function stor_rootid()
    local rootid = ffi.new('chkid_t')
    print ('--- stor_rootid')
    print(C.stor_rootid(rootid))
    print_chkid(rootid)
end

local function stor_lookup1()
	local path = ffi.new('char[?]', 7, '/iscsi')
	local fid = ffi.new('fileid_t')
	print(C.stor_lookup1(path, fid))
	print_chkid(fid)
end


return {
    C = C,

    stor_rootid = stor_rootid,
    stor_lookup1 = stor_lookup1,
}
