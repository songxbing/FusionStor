local ffi = require('ffi')
local struct = require('struct')

require('cdef.base')

ffi.cdef[[

int dispatch_sysstat(lich_stat_t *stat, int force);
int dispatch_netinfo(ynet_net_info_t *info, const nid_t *nid);

]]

local stor_rpc = require('storage.stor_rpc')

local C = ffi.load('lich4s')
