local ffi = require("ffi")

local ctest = ffi.load('liblich4s.so')

require("cdef.base")

print(ctest.env_init_simple("lichbd"))
print(ctest.network_connect_master())
print(ctest.stor_init(NULL, -1))

--[[==========================pool create============================
io.write("pool all path:")
local path = io.read()
local fid = ffi.new("fileid_t")
local name = ffi.new("char[?]", 256)

ctest.stor_splitpath(path, fid, name)

ctest.stor_mkpool_with_area(fid, name, "lich_system_null", NULL, NULL)
==========================volume create============================
io.write("volume all path:")
local path = io.read()
io.write("repnum:")
local repnum = io.read("*n")
local parent = ffi.new("fileid_t")
local fileid = ffi.new("fileid_t")
local name = ffi.new("char[?]", 256)

ctest.stor_splitpath(path, parent, name)

ctest.stor_mkvol_with_area(fileid, parent, name, 0, "lich_system_null",NULL, repnum)
==========================pool rm============================
io.write("pool all path:")
local path = io.read()
local fid = ffi.new("fileid_t")
local name = ffi.new("char[?]", 256)

ctest.stor_splitpath(path, fid, name)

ctest.stor_rmpool(fid, name);
==========================volume rm============================
io.write("volume all path:")
local path = io.read()
local parent = ffi.new("fileid_t")
local fid = ffi.new("fileid_t")
local name = ffi.new("char[?]", 256)

ctest.stor_splitpath(path, parent, name)
ctest.stor_lookup(parent, name, fid)

ctest.stor_rmvol(parent, name)
==========================volume mv/rename============================
io.write("from path:")
local from = io.read()
io.write("to path:")
local to = io.read()

local fromid = ffi.new("fileid_t")
local fromname = ffi.new("char[?]", 256)
ctest.stor_splitpath(from, fromid, fromname)
local toid = ffi.new("fileid_t")
local toname = ffi.new("char[?]", 256)
ctest.stor_splitpath(to, toid, toname)

ctest.stor_rename(fromid, fromname, toid, toname)
==========================volume resize============================
io.write("volume all path:")
local path = io.read()
io.write("size:")
local size = io.read("*n")
local fid = ffi.new("fileid_t")

ctest.stor_lookup1(path, fid)
ctest.stor_truncate(fid, size);
=============================balance===============================
local info = ffi.new("nodeinfo_t")
local node = ffi.new("b_node_info_t")
local node_list = ffi.new("b_node_list_t")
local uuid = ffi.new("char[256]", 0)
local _uuid = ffi.new("uuid_t")
local buf = ffi.new("char[4096]", 0)
local offset = ffi.new("uint64_t")
local buflen = ffi.new("int[1]")
ffi.C.uuid_generate(_uuid)
ffi.C.uuid_unparse(_uuid,uuid)
ffi.C.printf("%s\n", uuid)
print(ctest.cluster_listnode_open(uuid))

node_list.node_count = 0;
node_list.writeable_count = 0;

print(ctest.cluster_listnode(buf, buflen, uuid, offset));

buf = ffi.cast("char *",buf)
ffi.C.printf("%d\n", buflen[0])
ffi.C.printf("%d\n", offset)
ffi.C.printf("%s\n", buf)
=============================storageareas===========================
local count = ffi.new("int[1]",{4096})
local left
local buf = ffi.new("char[4096]", 0)
local site = ffi.new("char[4096]",0)
local ptr

ctest.dispatch_list_storage_area(buf, count)
ptr = buf
left = count - (ptr - buf)
left = ffi.cast("unsigned int", left)
ptr = ffi.cast("const void *", ptr)
site = ffi.cast("const void **",site)

ptr = ctest._opaque_decode(ptr, left, site, NULL, NULL);
print("==============")
site = ffi.cast("const char *",site[0])

ffi.C.printf("%s\n", site)]]--