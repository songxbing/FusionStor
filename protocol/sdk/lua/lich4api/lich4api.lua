#!/usr/bin/env lua

local lich = require("lich4api")

function print_object(x, depth)
    if not depth then depth = 0 end
    if type(x) == "table" then
        for k, v in pairs(x) do
            print(k)
            print_object(v, depth+1)
        end
    else
        print('\t', x)
    end
end

function domain()
    print("domain")
    local tab = lich.node_getinfo("v4")

    for k, v in pairs(tab) do
        print(k,v)
    end

end

function domain1()
    print("domain1")
    local tab = lich.admin_listnode()

    for k, v in ipairs(tab) do
        local tab1 = lich.node_getinfo(v)
        if tab1 then
            for k1, v1 in pairs(tab1) do
                print('\t', k1, v1)
            end
        else
            print('\ttab1 is nil')
        end
    end
end


function pool_create()
    lich.env_init_simple("lichbd")
    lich.network_connect_master()
    lich.stor_init("", -1)

    print()
    domain()
    print()
    domain1()

    --print_object(lich.net_gethostname())
    --print_object(lich.dispatch_sysstat())

    io.write("pool all path:")
    local path = io.read()
    io.write("pool name:")
    local name = io.read()
    --io.write("site_name:") --lich_system_null
    --local site_name = io.read()

    local fid = lich.stor_splitpath(path)
    local tab1 = lich.md_getattr(fid)
    print_object(tab1)
    local tab2 = lich.md_initattr(tab1)
    print_object(tab2)
    local tab = {attr = tab2, fid = fid, name = name, site_name = "lich_system_null"}
    lich.md_mkpool_with_area(tab)
end

function vol_create()
    lich.env_init_simple("lichbd")
    lich.network_connect_master()
    lich.stor_init("", -1)

    io.write("volume all path:")
    local path = io.read()
    io.write("volume name:")
    local name = io.read()
    io.write("repnum:")
    local repnum = io.read()
    --io.write("site_name:") --lich_system_null
    --local site_name = io.read()

    local fid = lich.stor_splitpath(path)
    local tab1 = lich.md_getattr(fid)
    print_object(tab1)
    local tab2 = lich.md_initattr(tab1)
    print_object(tab2)
    local tab = {attr = tab2, fid = fid, name = name, repnum = repnum, site_name = "lich_system_null"}
    lich.md_mkvol(tab)
end

function snap_create()
    lich.env_init_simple("lichbd")
    lich.network_connect_master()
    lich.stor_init("", -1)

    io.write("volume all path:")
    local path = io.read()
    io.write("snap name:")
    local name = io.read()
    --io.write("site_name:") --lich_system_null
    --local site_name = io.read()

    local fid = lich.stor_lookup1(path)
    local tab = {fid = fid, name = name, site_name = "lich_system_null"}
    lich.md_snapshot_create_with_area(tab)
end

-- entry point
snap_create()
