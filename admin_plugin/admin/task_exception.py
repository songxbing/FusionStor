#!/usr/bin/env python2
#coding:utf-8


class Duplicate(Exception):
    pass


class TaskNotFound(Exception):
    pass


class Failure(Exception):
    def __init__(self, progress):
        self.progress = progress