from PluginManager import PluginManager

class RAID_Iface(object):
    __metaclass__ = PluginManager

    def prepare():
        '''
        prepare raid cmd, raid list info, new disk list info
        '''
        pass

    def add_raid0(self, disk, force):
        '''
        make a new disk to raid0.

        disk: new disk inq
        force: if found some error, is execute raid_miss
        '''
        pass

    def del_raid0(self, dev, force):
        '''
        destroy a raid0 disk.

        dev: device name
        force: if found some error, is execute raid_miss
        '''
        pass

    def import_raid_foreign(self):
        '''
        import foreign configure disk to raid.
        '''
        pass

    def raid_cache_flush(self):
        '''
        flush raid cache.
        '''
        pass

    def get_raid_cache(self, dev):
        '''
        '''
        pass

    def set_raid_policy(self, dev, cache):
        '''
        '''
        pass

    def set_raid_ratio(self, dev, ratio=None):
        '''
        '''
        pass

    def del_raid_missing(self):
        '''
        cleanup raid missing
        '''
        pass

    def get_light_flash(self):
        '''
        get disk list which disk light up
        '''
        pass

    def set_light_flash(self, switch, dev):
        '''
        set disk list to  on/off
        '''
        pass

    def check_raid_cache(self, dev, cacheconf, setcache=True):
        '''
        check device cache is correct
        dev: device name
        cacheconf: from cache.conf like {'ssd': {'skip': False, 'raid_cache': 'disable', 'disk_cache': 'disable'},
                                         'hdd': {'skip': False, 'raid_cache': 'enable', 'disk_cache': 'disable'}}
        setcache: is need set to correct cache policy, else only print cache policy
        '''
        pass

    def set_raid_cache(self, dev, cache):
        '''
        set raid cache
        '''
        pass

    def get_dev_info(self, dev):
        '''
        get device info
        dev: device name
        return: {'adp_type': 'LSI', 'vds': '1', 'disk_info': {'TOSHIBA_AL13SEB600_DE09Z3G0A0QDFRD3': {'slot': '1', 'adp_type': 'LSI', 'stat': 'Online', 'adp': '0', 'encl': '32', 'devid': '1', 'inq': 'TOSHIBA_AL13SEB600_DE09Z3G0A0QDFRD3', 'foreign': 'None', 'enclp': '1', 'media_type': 'HDD', 'interface': 'SAS', 'adp_name': 'PERC H710P Mini', 'max_temp': '29', 'curr_temp': '29', 'size': '558.911GB'}}, 'raid': '0', 'adp': '0', 'cache_policy': 'WriteBack, ReadAhead, Cached, No Write Cache if Bad BBU', 'adp_memory': '1024MB', 'pds': '1', 'raid_cache': 'Enabled', 'disk_cache': 'Disabled', 'adp_name': 'PERC H710P Mini', 'bbu_info': {'status': 'None', 'auto_learn': 'Transparent', 'full': '518', 'temperature': '40', 'learn_next': '2018-08-10 21:15:24', 'learn_period': 90, 'replace': 'No', 'relative': '100', 'voltage': '3937', 'learn_cycle': 'No', 'bbu': 'Present', 'type': 'BBU', 'remaining': '517', 'low': 'No'}, 'vd': '0', 'disk': ['TOSHIBA_AL13SEB600_DE09Z3G0A0QDFRD3']}
        '''
        pass

    def get_disk_info(self, dev):
        '''
        get device hardware info
        dev: device name
        return: {'slot': '1', 'adp_type': 'LSI', 'stat': 'Online', 'adp': '0', 'encl': '32', 'devid': '1', 'inq': 'TOSHIBA_AL13SEB600_DE09Z3G0A0QDFRD3', 'foreign': 'None', 'enclp': '1', 'media_type': 'HDD', 'interface': 'SAS', 'adp_name': 'PERC H710P Mini', 'max_temp': '29', 'curr_temp': '29', 'size': '558.911GB'}
        '''
        pass

    def get_new_disk(self):
        '''
        get new disk in raid
        return: {'LSI': {'0': {'TOSHIBA_AL13SEB600_DE09Z3G0A0R1FRD3': {'slot': '2', 'adp_type': 'LSI', 'stat': 'Unconfigured(good)', 'adp': '0', 'encl': '32', 'devid': '2', 'inq': 'TOSHIBA_AL13SEB600_DE09Z3G0A0R1FRD3', 'foreign': 'None', 'enclp': '1', 'media_type': 'HDD', 'interface': 'SAS', 'adp_name': 'PERC H710P Mini', 'max_temp': '29', 'curr_temp': '29', 'size': '558.911GB'}}}}
        '''
        pass

    def get_dev_device_model(self, dev):
        '''
        get device type (HDD/SDD)
        dev: device name
        return: disk type
        '''
        pass

    def get_disk_rotation(self, dev):
        '''
        get device rotation
        dev: device name
        return: rotation
        '''
        pass

    def get_all_ldpdinfo(self):
        '''
        get all disk info
        '''
        pass
