#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os

from config import Config
from instence import Instence
from node import Node
from storage import Storage
from utils import Exp, _dmsg, _dwarn, _derror, _get_value, _str2dict, _exec_pipe, _exec_shell, _exec_big

types = ["meta", "node", "namespace", "bucket", "object", "raw"]

def chunkcheck(path, name):
    config = Config()
    storage = Storage(config)

    for root, dirs, files in os.walk(path):
        for f in files:
            fchunk = os.path.join(root, f)
            if "rootable" in fchunk:
                continue

            if fchunk.split(".")[-1] not in types:
                continue

            chkid = fchunk.split("/")[-1]
            try:
                locs = storage.location(chkid)
            except Exp, e:
                _derror("deleted; %s; %s; %s" % (chkid, fchunk, e.err))
                continue

            if name not in locs:
                _derror("locerror; %s; %s; %s; %s" % (chkid, fchunk, name, str(locs)))
                continue

            _dmsg("ok; %s" % (fchunk))

if __name__ == "__main__":
    node = Node()
    for instence in node.instences:
        path = instence.home
        name = instence.name
        chunkcheck(path, name)
