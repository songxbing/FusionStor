#!/usr/bin/env python2

import os
import errno
import platform
import sys

from utils import Exp, _dwarn, _derror, _exec_pipe, _exec_pipe1
from Iface import DISK_Iface
from PluginManager import PluginManager

class DISK:
    def __init__(self, config, disktool, raid, commoncache):
        self.disktool = disktool

        PluginManager.SetTools('config', config)
        PluginManager.SetTools('disktool', disktool)
        PluginManager.SetTools('raid', raid)
        PluginManager.SetTools('commoncache', commoncache)
        PluginManager.SetTools('disk', self)

        path = os.path.abspath(os.path.join(os.path.dirname(__file__), 'Plugins'))
        sys.path.append(path)

        PluginManager.SetPluginPath(os.path.join(os.path.dirname(__file__), 'Plugins'))
        PluginManager.LoadAllPlugin()

    def get_disk_pool(self, dev):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.get_disk_pool(dev)

    def get_disk_info(self, dev):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.get_disk_info(dev)
        else:
            return {}

    def get_disk_real(self, dev):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.get_disk_real(dev)
        else:
            return [dev]

    def get_disk_target(self, dev, disk_num, pool):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.get_disk_target(dev, disk_num, pool)
        else:
            return dev

    def get_new_disk(self, used_disk):
        disks = []
        plugins = DISK_Iface.GetPluginObject()
        for plugin in plugins:
            new_disk = plugin.get_new_disk(used_disk)
            if new_disk is not None:
                for new in new_disk:
                    if new not in disks:
                        disks.append(new)
        return disks

    def get_new_info(self, dev):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.get_new_info(dev)
        else:
            return {}

    def show_new_disk(self, dev, dev_info):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.show_new_disk(dev, dev_info)
        else:
            return "  %s %s" % (dev, disktype)

    def get_all_disk(self):
        disks = []
        used_disk = {}
        plugins = DISK_Iface.GetPluginObject()
        for plugin in plugins:
            new_disk = plugin.get_all_disk(used_disk)
            if new_disk is not None:
                for new in new_disk:
                    if new not in disks:
                        disks.append(new)
        return disks

    def show_disk_info(self, dev, dev_info):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.show_disk_info(dev, dev_info)
        else:
            return "  %s %s" % (dev, disktype)

    def disk_read(self, dev, offset, size):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.disk_read(dev, offset, size)
        else:
            return ""

    def disk_write(self, dev, buff, offset):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.disk_write(dev, buff, offset)

    def disk_load_valid(self, dev, clusteruuid, hostname):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.disk_load_valid(dev, clusteruuid, hostname)

    def disk_info_valid(self, dev, buff):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.disk_info_valid(dev, buff)

    def get_disk_speed(self, dev):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.get_disk_speed(dev)

    def get_disk_rotation(self, dev):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.get_disk_rotation(dev)

    def set_disk_label(self, dev, label):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.set_disk_label(dev, label)

    def set_disk_clusteruuid(self, dev, clusteruuid):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.set_disk_clusteruuid(dev, clusteruuid)

    def set_disk_nodename(self, dev, nodename):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.set_disk_nodename(dev, nodename)

    def disk_add_superblock(self, dev, disk_num, pool, cache, clusteruuid, hostname):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.disk_add_superblock(dev, disk_num, pool, cache, clusteruuid, hostname)

    def disk_del_superblock(self, dev):
        disktype = self.disktool.get_dev_type(dev)
        plugin = DISK_Iface.GetPluginObject("%s.%s" % (disktype, disktype))
        if plugin:
            return plugin.disk_del_superblock(dev)
