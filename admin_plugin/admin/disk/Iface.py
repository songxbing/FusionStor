from PluginManager import PluginManager

class DISK_Iface(object):
    __metaclass__ = PluginManager
    def __init__(self):
        self.__prepare__ = False

    def prepare(self):
        '''
        prepare disk list info, disk tools
        '''
        if self.__prepare__ == False:
            for tool in self.tools:
                setattr(self.__class__, tool, self.tools[tool])

        self.__prepare__ == True

    def get_disk_pool(self, dev):
        '''
        get disk pool name
        dev: device name
        return: pool name
        '''
        return "__UNKNOW_POOL__"

    def get_disk_info(self, dev):
        '''
        get disk pool name
        dev: device name
        return: disk information
        "dev_info": {
            "status": "running", 
            "curr_temp": "32", 
            "cache_mode": "writeback", 
            "fs": null, 
            "fault": false, 
            "mount": null, 
            "cache": "/dev/sdc", 
            "free": null, 
            "label": null, 
            "raidcard": "LSI",
            "interface": "SATA", 
            "media_type": "HDD", 
            "max_temp": "32", 
            "type": "RAID", 
            "size": "931.0GB"
        } 
        '''
        return {}

    def get_disk_real(self, dev):
        '''
        get disk real used device
        return: array of used device
        '''
        return [dev]

    def get_disk_target(self, dev, disk_num, pool):
        '''
        for disk add, get link target
        '''
        return dev

    def get_new_disk(self, used_disk):
        '''
        get new disk list, if found new disk, need add new disk into used before return, other type will not get this device.
        used_disk: alredy be used disk by (lich/sysdev)
        return: array
        '''
        return []

    def get_new_info(self, dev):
        '''
        get new disk info
        dev: device name
        return: disk information
        "dev_info": {
            "raidcard": "LSI",
            "interface": "SATA",
            "media_type": "HDD",
            "max_temp": "32",
            "type": "RAID",
            "size": "931.0GB"
        }
        '''
        return {}

    def show_new_disk(self, dev, dev_info):
        '''
        show new device info for lich.node --disk_list
        return: format show info
        '''
        return ""

    def get_all_disk(self, used_disk):
        '''
        get new disk list 
        used_disk: alredy be used disk by (lich/sysdev)
        return: array
        '''
        return []

    def show_disk_info(self, dev, dev_info):
        '''
        show device info for lich.node --disk_list
        return: format show info
        '''
        return ""

    def disk_read(self, dev, offset, size):
        '''
        read from disk
        offset: read offset
        size: read size
        return: read content
        '''
        return ""

    def disk_write(self, dev, buff, offset):
        '''
        write fuff to dev with offset
        '''
        pass

    def disk_load_valid(self, dev, clusteruuid, hostname):
        '''
        get meta info from disk like {'type': 'data', 'disk':'1', 'pool':'default'}
        '''
        pass

    def disk_info_valid(self, dev, buff):
        '''
        valid disk info is correct
        '''
        pass

    def get_disk_speed(self, dev):
        '''
        get disk speed
        '''
        return 0

    def get_disk_rotation(self, dev):
        return 0

    def set_disk_label(self, dev, label):
        pass

    def set_disk_clusteruuid(self, dev, clusteruuid):
        pass

    def set_disk_nodename(self, dev, nodename):
        pass

    def disk_add_superblock(self, dev, disk_num, pool, cache, clusteruuid, hostname):
        pass

    def disk_del_superblock(self, dev):
        pass
