#!/usr/bin/env python2

import platform
import errno
import re
import os
import string
import time
import sys

from utils import Exp, _dmsg, _dwarn, _exec_pipe
from Normal import Normal
from raid.raid import RAID


class RAID(Normal):
    def get_disk_info(self, dev):
        dev_info = Normal.get_disk_info(self, dev)

        raid_info = self.raid.raid_info(dev)
        if raid_info is not None:
            raid_disk_info = self.raid.dev_info(dev)
            if 'curr_temp' not in raid_disk_info:
                dev_info['curr_temp'] = '0'
            else:
                dev_info['curr_temp'] = raid_disk_info['curr_temp']
            if 'max_temp' not in raid_disk_info:
                dev_info['max_temp'] = '0'
            else:
                dev_info['max_temp'] = raid_disk_info['max_temp']
            dev_info['media_type'] = raid_disk_info['media_type']
            dev_info['interface'] = raid_disk_info['interface']

            dev_info['raid_info'] = raid_info

        return dev_info

    def show_disk_info(self, dev, dev_info):
        disk_info = Normal.show_disk_info(self, dev, dev_info)
        return "%s level:RAID%s %6s#%s:'%s' raid_cahce:'%s' disk_cache:%s" %(disk_info,
                dev_info['raid_info']['raid'],
                dev_info['raid_info']['adp_type'], dev_info['raid_info']['adp'], dev_info['raid_info']['adp_name'],
                dev_info['raid_info']['cache_policy'], dev_info['raid_info']['disk_cache'])

    def get_new_disk(self, used_disk):
        new_disk = []
        new_raid_disk = self.raid.disk_list()
        if new_raid_disk:
            for card, disks in new_raid_disk.iteritems():
                for adp, disk in disks.iteritems():
                    for new in disk:
                        new_disk.append('raid_inq:' + disk[new]['adp_type']  + ':' + new)
                        if '__newdev__' not in used_disk:
                            used_disk['__newdev__'] = {}
                        used_disk['__newdev__'][new] = new
        return new_disk

    def get_new_info(self, dev):
        return self.raid.disk_info(dev)

    def show_new_disk(self, dev, dev_info):
        return "  %s %6s#%s:'%s'" %(dev.split(':')[2],
                dev_info['adp_type'], dev_info['adp'], dev_info['adp_name'])

    def get_disk_rotation(self, dev):
        return self.raid.disk_rotation(dev)
