#!/usr/bin/env python2

import platform
import errno
import re
import os
import string
import time

from utils import Exp, _dmsg, _dwarn, _exec_pipe, _exec_system
from Normal import Normal


class NVMe(Normal):
    def get_disk_real(self, dev):
        pci = self.disktool.get_dev_pci(dev)
        return [dev, pci]
