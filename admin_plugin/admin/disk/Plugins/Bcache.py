#!/usr/bin/env python2

import platform
import errno
import re
import os
import string
import time

from utils import Exp, _dmsg, _dwarn, _exec_pipe
from Normal import Normal


class Bcache(Normal):
    def get_disk_info(self, dev):
        dev_info = Normal.get_disk_info(self, dev)
        disk_dev = self.commoncache.get_coredev_by_fastdev(dev, self.config.cache_type)

        cacheid, cachedev, cache_status, cache_mode = self.commoncache.get_cacheinfo_by_coredev(disk_dev, self.config.cache_type)
        dev_info['coredev'] = disk_dev
        if disk_dev is not None:
            dev_info['coredev_info'] = self.disk.get_disk_info(disk_dev)
        else:
            dev_info['coredev_info'] = {}
        dev_info['coredev_info']['cache'] = str(cachedev)
        dev_info['coredev_info']['cache_mode'] = cache_mode
        dev_info['coredev_info']['status'] = cache_status

        cacheid, cache_status, cache_mode = self.commoncache.get_cacheinfo_by_cachedev(cachedev, self.config.cache_type)
        dev_info['cachedev'] = cachedev
        if cachedev is not None:
            dev_info['cachedev_info'] = self.disk.get_disk_info(cachedev)
        else:
            dev_info['cachedev_info'] = {}
        dev_info['cachedev_info']['cache_status'] = cache_status
        dev_info['cachedev_info']['cache_mode'] = cache_mode
        dev_info['cachedev_info']['cacheid'] = cacheid

        # cache disk add disk_stat for UMP
        '''
        cache_num = 0
        for dev, v in all_disk_json.iteritems():
            if 'type' not in v:
                cache_num = 0
                break

            if v['type'] == 'cache':
                cache_num += 1

        if cache_num > 0:
            for dev, v in all_disk_json.iteritems():
                if v['type'] == 'cache':
                    v['dirty_data'] = 0

            for dev, v in all_disk_json.iteritems():
                if v['type'] == 'cache':
                    continue

                if 'cache' in v['dev_info']:
                    cachedev = v['dev_info']['cache']
                    if cachedev:
                        sdName = os.path.split(dev)[-1]
                        all_disk_json[cachedev]['dirty_data'] += Bcache.dirty_data(sdName)

            for dev, v in all_disk_json.iteritems():
                if v['type'] == 'cache':
                    all_disk_json[dev]['disk_stat'] = Bcache.disk_stat(v['dev_info']['size'], v['dirty_data'])
                    del v['dirty_data']
        '''

        return dev_info

    def get_disk_real(self, dev):
        disk_dev = self.commoncache.get_coredev_by_fastdev(dev, self.config.cache_type)
        cacheid, cachedev, cache_status, cache_mode = self.commoncache.get_cacheinfo_by_coredev(disk_dev, self.config.cache_type)
        return [disk_dev, cachedev]

    def show_disk_info(self, dev, dev_info):
        disk_info = Normal.show_disk_info(self, dev, dev_info)
        coredev_info = self.disk.show_disk_info(dev_info['coredev'], dev_info['coredev_info'])
        if dev_info['cachedev']:
            cachedev_info = self.disk.show_disk_info(dev_info['cachedev'], dev_info['cachedev_info'])
        else:
            cachedev_info = "Not found"
        return "%s\n    coredev:%s\n    cachedev:%s" % (disk_info, coredev_info, cachedev_info)
