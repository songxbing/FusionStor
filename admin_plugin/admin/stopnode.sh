#!/bin/bash
if [ "$1" = "" ];then
    echo "no input argument";exit 1;
fi

while getopts h: arg 
do 
    case $arg in 
    h) 
    CODEFILE=$OPTARG/log/stopnode_recode.log;
    LOGFILE=$OPTARG/log/stopnode.log;
    echo '' > $CODEFILE

    for index in {1..30}  
    do 
        echo ''> $LOGFILE
        $OPTARG/lich/admin/cluster.py health scan >> $LOGFILE 2>&1;
        recode=$?;
        echo $recode > $CODEFILE
        
        HEALTH_RESULT=`cat $LOGFILE |grep 'chunk need recovery\|chunk recovery lost\|chunk recovery fail'|awk -F: '{print $2}'`
        ISSUCC=1
        for x in $HEALTH_RESULT
        do
            if [ $x != 0 ];then
                ISSUCC=0
            fi
        done 
        if [ $ISSUCC == 1 ];then
            break
        fi
        sleep 10 
    done
    
    if [ $ISSUCC == 1 ];then
        $OPTARG/lich/admin/node.py --stop >> $LOGFILE 2>&1;
        skip_recode=$?;
        echo $skip_recode > $CODEFILE
        if [ "$skip_recode" != 0 ];then
        	echo -e "\033[1;31m skiped fail\033[0m"
    	exit $skip_recode
        fi
        echo -e "\033[1;32m stoped OK\033[0m"
    else
        echo '9005' > $CODEFILE
        echo -e "\033[1;31m stoped fail\033[0m"
        exit $recode
    fi
    ;; 
    \?)
    echo "unknown argument" ;
    exit 1;
    ;; 
    esac    
done 




