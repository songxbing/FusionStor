# deps:
# - LICH_HOME
# - PROJECT_HOME

set (LICH_INSTALL_PATH "${LICH_HOME}/lich")

# message("install ${PROJECT_HOME}")
# message("install ${LICH_INSTALL_PATH}")

# execute_process(COMMAND make -C include install)
execute_process(
	COMMAND ${PROJECT_HOME}/include/install.sh -i ${LICH_INSTALL_PATH}
	WORKING_DIRECTORY ${PROJECT_HOME}/include)

execute_process(
	COMMAND ${PROJECT_HOME}/admin/install.sh -i ${LICH_INSTALL_PATH}
	WORKING_DIRECTORY ${PROJECT_HOME}/admin)

execute_process(
	COMMAND ${PROJECT_HOME}/manage/install.sh -i ${LICH_INSTALL_PATH}
	WORKING_DIRECTORY ${PROJECT_HOME}/manage)

execute_process(
	COMMAND ${PROJECT_HOME}/remotecopy/install.sh -i ${LICH_INSTALL_PATH}
	WORKING_DIRECTORY ${PROJECT_HOME}/remotecopy)

execute_process(
	COMMAND ${PROJECT_HOME}/cgroup/install.sh -i ${LICH_INSTALL_PATH}
	WORKING_DIRECTORY ${PROJECT_HOME}/cgroup)

# create symlink in ${LICH/INSTALL_PATH}/bin

execute_process(COMMAND rm -rf ${LICH_INSTALL_PATH}/bin/lichbd)
execute_process(COMMAND rm -rf ${LICH_INSTALL_PATH}/bin/lich.inspect)
execute_process(COMMAND rm -rf ${LICH_INSTALL_PATH}/bin/lichfs)
execute_process(COMMAND rm -rf ${LICH_INSTALL_PATH}/bin/lich.snapshot)

execute_process(COMMAND ln -s ${LICH_INSTALL_PATH}/libexec/lichbd ${LICH_INSTALL_PATH}/bin/lichbd)
execute_process(COMMAND ln -s ${LICH_INSTALL_PATH}/libexec/lich.inspect ${LICH_INSTALL_PATH}/bin/lich.inspect)

execute_process(COMMAND ln -s ${LICH_INSTALL_PATH}/libexec/lichfs ${LICH_INSTALL_PATH}/bin/lichfs)
execute_process(COMMAND ln -s ${LICH_INSTALL_PATH}/libexec/lich.snapshot ${LICH_INSTALL_PATH}/bin/lich.snapshot)

# create symlink in /usr/local/bin

execute_process(COMMAND rm -rf /usr/local/bin/lich)
execute_process(COMMAND rm -rf /usr/local/bin/lich.node)
execute_process(COMMAND rm -rf /usr/local/bin/lichbd)
execute_process(COMMAND rm -rf /usr/local/bin/lich.inspect)
execute_process(COMMAND rm -rf /usr/local/bin/lichfs)
execute_process(COMMAND rm -rf /usr/local/bin/lich.snapshot)
execute_process(COMMAND rm -rf /usr/local/bin/lich.balance)

execute_process(COMMAND ln -s ${LICH_INSTALL_PATH}/bin/lich /usr/local/bin/lich)
execute_process(COMMAND ln -s ${LICH_INSTALL_PATH}/bin/lich.node /usr/local/bin/lich.node)
execute_process(COMMAND ln -s ${LICH_INSTALL_PATH}/bin/lichbd /usr/local/bin/lichbd)
execute_process(COMMAND ln -s ${LICH_INSTALL_PATH}/bin/lich.inspect /usr/local/bin/lich.inspect)
execute_process(COMMAND ln -s ${LICH_INSTALL_PATH}/bin/lichfs /usr/local/bin/lichfs)
execute_process(COMMAND ln -s ${LICH_INSTALL_PATH}/bin/lich.snapshot /usr/local/bin/lich.snapshot)
execute_process(COMMAND ln -s ${LICH_INSTALL_PATH}/bin/lich.balance /usr/local/bin/lich.balance)

# execute_process(COMMAND ldconfig)
