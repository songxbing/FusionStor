#include "config.h"

#define DBG_SUBSYS S_LIBYNET

#include "ynet_net.h"
#include "configure.h"
#include "net_global.h"
#include "job_dock.h"
#include "rpc_proto.h"
#include "rpc_table.h"
#include "ynet_rpc.h"
#include "../net/net_rpc.h"
#include "dbg.h"

int rpc_inited = 0;

int rpc_init(int daemon)
{
        int ret;

        DINFO("rpc init\n");
        ret = net_init(NULL, daemon);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        ng.rack[0] = '\0';

        ret = jobdock_init(netable_rname, ng.daemon);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        ret = rpc_table_init("default", &__rpc_table__, 1);
        if (unlikely(ret))
                GOTO(err_ret, ret);

#if 1
        ret = net_rpc_init();
        if (unlikely(ret))
                GOTO(err_ret, ret);
#endif

        rpc_inited = 1;

        return 0;
err_ret:
        return ret;
}

int rpc_destroy(void)
{
        int ret;

        DBUG("wait for net destroy...\n");

        rpc_inited = 0;

        ret = net_destroy();
        if (unlikely(ret))
                GOTO(err_ret, ret);

        DINFO("net destroyed\n");

        return 0;
err_ret:
        return ret;
}
