#include "config.h"

#include <sys/types.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdint.h>
#include <sched.h>
#include <pthread.h>
#include <poll.h>
#include <errno.h>

#define DBG_SUBSYS S_LIBYNET

#include "sysy_lib.h"
#include "ynet_sock.h"
#include "sdevent.h"
#include "dbg.h"

static int __sock_poll_sd1(const int *sd, int sd_count, short event, uint64_t usec, struct pollfd *pfd, int *retval)
{
        int ret, i;
        struct timeval oldtv, newtv;
        uint64_t left, used;

        YASSERT(usec < 100 * 1000 * 1000);

        if (usec < 1000 * 500) {
                DERROR("wait %llu\n", (LLU)usec);
        }

        ANALYSIS_BEGIN(0);

        for (i = 0; i < sd_count; i++) {
                pfd[i].fd = sd[i];
                pfd[i].events = event;
                pfd[i].revents = 0;
        }

        timerclear(&oldtv);
        ret = _gettimeofday(&oldtv, NULL);
        if (ret == -1) {
                ret = errno;
                GOTO(err_ret, ret);
        }

        left = usec;
        while (1) {
                ret = poll(pfd, sd_count, left / 1000);
                if (ret == -1) {
                        ret = errno;
                        if (ret == EINTR) {
                                timerclear(&newtv);
                                ret = _gettimeofday(&newtv, NULL);
                                if (ret == -1) {
                                        ret = errno;
                                        GOTO(err_ret, ret);
                                }

                                used = _time_used(&oldtv, &newtv);
                                if (used >= left) {
                                        ret = ETIME;
                                        GOTO(err_ret, ret);
                                } else {
                                        left -= used;
                                        continue;
                                }
                        }

                        GOTO(err_ret, ret);
                } else if (ret == 0) {
                        // TODO rep exception, or ARP cache?
                        ret = ETIME;
                        goto err_ret;
                } else {
                        *retval = ret;
                        break;
                }
        }

        ANALYSIS_END(0, IO_WARN, NULL);

        return 0;
err_ret:
        return ret;
}

typedef struct {
        const int *sd;
        int sd_count;
        short event;
        uint64_t usec;
        struct pollfd *pfd;
        int *retval;
        task_t task;
} sock_poll_context_t;

static void __sock_poll_async__(job_t *job, int *keep)
{
        int ret;
        sock_poll_context_t *context;

        *keep = 0;

        context = job->context;

        ret = __sock_poll_sd1(context->sd, context->sd_count,
                              context->event, context->usec, context->pfd,
                              context->retval);

        schedule_resume(&context->task, ret, NULL);
}

static int __sock_poll_async(const int *sd, int sd_count, short event, uint64_t usec,
                             struct pollfd *pfd, int *retval)
{
        int ret;
        job_t *job;
        sock_poll_context_t *context;

        ret = job_create(&job, &jobtracker, "poll");
        if (unlikely(ret))
                GOTO(err_ret, ret);

        ret = job_context_create(job, sizeof(*context));
        if (unlikely(ret))
                GOTO(err_job, ret);

        context = job->context;
        context->sd = sd;
        context->sd_count = sd_count;
        context->event = event;
        context->usec = usec;
        context->pfd = pfd;
        context->task = schedule_task_get();
        context->retval = retval;
        job->status = STATUS_PREP;
        job->state_machine = __sock_poll_async__;

        job_exec(job_handler(job, 0), 0, EXEC_INDIRECT);

        ret = schedule_yield("poll_async", NULL, NULL);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        return 0;
err_job:
        job_destroy(job);
err_ret:
        return ret;
}

int sock_poll_sd1(const int *sd, int sd_count, short event, uint64_t usec, struct pollfd *pfd, int *retval)
{
        int ret;

        YASSERT(!schedule_suspend());

        if (schedule_running()) {
                ret = __sock_poll_async(sd, sd_count, event, usec, pfd, retval);
                if (unlikely(ret))
                        goto err_ret;
        } else {
                ret = __sock_poll_sd1(sd, sd_count, event, usec, pfd, retval);
                if (unlikely(ret))
                        goto err_ret;
        }

        return 0;
err_ret:
        return ret;
}

int sock_poll_sd(int sd, uint64_t usec, short event)
{
        int ret, pcount;
        struct pollfd pfds[1];

        pcount = 1;
        ret = sock_poll_sd1(&sd, 1, event, usec, pfds, &pcount);
        if (unlikely(ret))
                goto err_ret;

        return 0;
err_ret:
        return ret;
}
