#include "config.h"

#include <stdint.h>
#include <stdio.h>
#include <stdarg.h>

#define DBG_SUBSYS S_LIBYNET

#include "net_table.h"
#include "net_global.h"
#include "pipe_pool.h"
#include "net_events.h"
#include "rpc_proto.h"
#include "sdevent.h"
#include "conn.h"
#include "configure.h"
#include "dbg.h"

net_global_t ng;

int net_init(net_proto_t *op, int daemon)
{
        int ret; 
	//int ksubversion;

        if (op)
                ng.op = *op;

        ng.op.head_len = sizeof(ynet_net_head_t);
        ng.op.writer = ng.op.writer ? ng.op.writer
                : net_events_handle_write;
        ng.op.reader = ng.op.reader ? ng.op.reader
                : net_events_handle_read;
        ng.op.pack_len = ng.op.pack_len ? ng.op.pack_len
                : rpc_pack_len;
        ng.op.pack_handler = ng.op.pack_handler ? ng.op.pack_handler
                : rpc_pack_handler;

        ret = hosts_init();
        if (unlikely(ret))
                GOTO(err_ret, ret);

        ret = netable_init(daemon);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        ret = conn_init();
        if (unlikely(ret))
                GOTO(err_ret, ret);
        
        return 0;
err_ret:
        return ret;
}

int net_destroy(void)
{
        sdevent_destroy();

#if 0
        ret = netable_destroy();
        if (unlikely(ret))
                GOTO(err_ret, ret);
#endif

        return 0;
}
