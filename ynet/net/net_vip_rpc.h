#ifndef __NET_VIP_RPC_H__
#define __NET_VIP_RPC_H__

#include "cluster.h"

int netvip_rpc_init(void);
int netvip_rpc_getip_byvip(const nid_t *nid, const uint32_t vip, char *value, int *valuelen);
int netvip_rpc_master_setvip(const nid_t *nid, const char *vip, int len);

#endif
