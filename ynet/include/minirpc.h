#ifndef __MINIRPC_H__
#define __MINIRPC_H__

typedef enum {
        MINIRPC_OP_NONE,
        MINIRPC_OP_PAXOS,
        MINIRPC_OP_NODE,
        MINIRPC_OP_END,
} minirpc_op_t;

typedef int (*minirpc_request_handler)(void *context, void *rep, uint32_t *replen,
                                       const void *req, uint32_t reqlen);

int minirpc_start();
int minirpc_init(const char *hostname, const char *port);

void minirpc_request_register(int op, minirpc_request_handler handler, void *context);

int minirpc_request_wait(int op, char *rep, int *replen, const char *req, int reqlen,
                         const char *name, int timeout);


#endif
