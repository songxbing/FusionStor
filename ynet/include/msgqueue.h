#ifndef __MSGQUEUE_H__
#define __MSGQUEUE_H__

#include "net_proto.h"

#define MSGQUEUE_SEG_LEN 167772160 //固定值，不能改变
#define MSGQUEUE_SEG_COUNT_MAX (256)

typedef struct {
        int fd;
        uint32_t woff;
        uint32_t roff;
} msgqueue_seg_t;

typedef struct {
        sy_rwlock_t rwlock;
        char home[MAX_PATH_LEN];
        size_t seg_len;
        uint32_t msg_size; //单个msg的长度
        uint32_t persistent;
        uint32_t idx;
        int lock_fd;
        msgqueue_seg_t seg[MSGQUEUE_SEG_COUNT_MAX];
} msgqueue_t;

typedef struct {
        uint32_t len;
        uint32_t crc;
        char buf[0];
} msgqueue_msg_t;

#define msg_for_each(msg, left)                                         \
        for (; left && (unsigned)left > msg->len;                       \
                     left -= (msg->len + sizeof(msgqueue_msg_t)),       \
                     msg = (void *)msg + msg->len + sizeof(msgqueue_msg_t))


int msgqueue_init(msgqueue_t *queue, const char *path, uint32_t msg_len,  uint64_t maxsize, int persistent);
void msgqueue_close(msgqueue_t *queue);

int msgqueue_load(msgqueue_t *queue, const char *path, uint32_t msg_len, uint64_t maxsize, int persistent);
int msgqueue_get(msgqueue_t *queue, void *msg, uint32_t len);
int msgqueue_empty(msgqueue_t *queue);

int msgqueue_push(msgqueue_t *queue, const void *_msg, uint32_t len);
int msgqueue_pop(msgqueue_t *queue, void *msg, uint32_t len);

#endif
