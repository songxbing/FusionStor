#ifndef __FENCE_H__
#define __FENCE_H__


#include <stdint.h>
#include <inttypes.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "buffer.h"
#include "net_proto.h"
#include "sdevent.h"
#include "yid.h"
#include "ylock.h"
#include "sysy_lib.h"
#include "ynet_conf.h"
#include "msgqueue.h"
#include "configure.h"
#include "job_dock.h"
#include "adt.h"
#include "../sock/ynet_sock.h"
#include "dbg.h"



#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <signal.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <net/if_arp.h>
#include <poll.h>
#include <sys/time.h>
#include <netinet/ip_icmp.h>

#define STR_LEN 128
#define MAX_NIC_NUM 500


struct niclist {
	struct list_head list;
	char name[16];

	unsigned long ipaddr;
	unsigned long netmask;
	unsigned long netaddr;

	int ifindex;
	int status;
};

struct  ipinfo {
	struct niclist *nic;
	struct in_addr src;
	struct in_addr dst;
	struct hostinfo *host;
};



struct hostinfo {
	struct list_head list;
	struct hostinfo *next, *prev;
	struct ipinfo intf[MAX_NIC_NUM];
	char hostname[STR_LEN];
	int cur_index;
	int nic_num;
	int link;
	int fd;
	int seq;
	int ident;
};

typedef struct {
	char host[LICH_HOSTNAME_MAX];
} __host_t;

int ping_get_ident (void); 
void del_all_nic(struct list_head *nlist);
void del_all_host(struct list_head *hlist);
int handle_icmp_packs(struct list_head *hlist, int total);
int get_nic_info(struct list_head *nlist);
struct niclist* get_src_ip(struct in_addr  dst_ip, struct in_addr * src_ip, struct list_head *nlist,char devname[]);
int fence_init(const char *home);
int fence_test(void);
int fence_test_sync(void);
int icmp_host(const char *addr);

#endif

