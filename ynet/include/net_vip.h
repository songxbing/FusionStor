#ifndef __NET_VIP_H__
#define __NET_VIP_H__

#include <stdint.h>
#include <inttypes.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <net/if_arp.h>
#include <poll.h>
#include <sys/time.h>

#include "net_global.h"
#include "net_tool.h"

int netvip_init();
int netvip_check();
int netvip_setvip();
int netvip_destroy();
int iser_netvip_is_vip(struct sockaddr_in *addr);
int iser_netvip_in_vipnet(struct sockaddr_in *addr);
int netvip_is_vip(int conn_fd);
int netvip_in_vipnet(int conn_fd);
int netvip_check_vip(const char *vip); 
int netvip_getip_byid(const nid_t *nid, uint32_t vip, char *value, int *valuelen);

#endif

