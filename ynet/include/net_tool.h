#ifndef __NET_TOOL_H__
#define __NET_TOOL_H__


#include <stdint.h>
#include <inttypes.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <net/if_arp.h>
#include <poll.h>
#include <sys/time.h>

#include "cluster.h"

int nettool_add_ip(const char *ip, const char *if_name);
int nettool_del_ip(const char *ip, const char *if_name);
static inline uint32_t nettool_netmask(int mask_int) {
    return htonl(~((1 << (32 - mask_int)) - 1));
}

int nettool_set_ip(const char *ip, const char *if_name);
int nettool_set_down(const char *if_name);

int nettool_get_ip_byif(char *ip, char *mask, const char *if_name);
int nettool_get_ip_byvip(const uint32_t vip, char *ip, int *iplen);
int nettool_get_if_bynet(char *if_name, const char *ip);
int nettool_get_if_byip(char *if_name, const char *ip);
int nettool_get_subif(char *sub_if_name, const char *if_name);
int nettool_get_mac(char *mac, const char *if_name);
int nettool_get_broadcast(char *brd, const char *if_name);

int nettool_is_up(const char *if_name);
int nettool_is_ip(const char *ip);
int nettool_is_if(const char *if_name);
int nettool_is_equality(const char *ip, struct sockaddr_in *addr);
int nettool_in_ipnet(const char *ip, struct sockaddr_in *addr);
int nettool_is_use(const char *ip);

#endif

