#ifndef __NET_PROTO_H__
#define __NET_PROTO_H__

#include <sys/epoll.h>
#include <uuid/uuid.h>

#include "sysy_conf.h"
#include "sysy_lib.h"
#include "yid.h"
#include "worker.h"
#include "job_dock.h"
#include "msgarray.h"
#include "schedule.h"
#include "job.h"
#include "dbg.h"

#define YNET_PORT_RANDOM 0

typedef int (*net_pack_handler)(const sockid_t *sockid, buffer_t *buf);
typedef int (*net_pack_len)(void *, uint32_t, int *msg_len, int *io_len);
typedef int (*net_event_write_handler)(struct epoll_event *ev, void *);
typedef int (*net_event_read_handler)(void *sock, void *ctx);
typedef int (*net_reset_handler)(const net_handle_t *nh, char *nodeid);
//typedef int (*net_request_handler)(job_t *);

#define net_request_handler func_t

typedef enum {
        MSG_NULL, //XXX:fix this type
        MSG_HEARTBEAT,
        MSG_CLUSTER,
        MSG_REPLICA,
        MSG_ROOTABLE,
        MSG_MSGQUEUE,
        MSG_DISPATCH,
        MSG_FS,
        MSG_CHUNK,
        MSG_PROF,
        MSG_LEASE,
        MSG_NETVIP,
        MSG_MAX,
} net_progtype_t;

typedef struct {
        net_request_handler handler;
        void *context;
} net_prog_t;

typedef struct {
        uint32_t op;
        char buf[0];
} rpc_msg_t;

typedef struct {
        uint32_t head_len; //length of proto head, not suitable for http
        net_pack_len      pack_len;          /*return the length of a pack */
        net_event_read_handler reader;
        net_event_write_handler writer;
        net_pack_handler  pack_handler;
} net_proto_t;

/*inited in net_lib.c*/

#endif
