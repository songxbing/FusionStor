#ifndef __LIBNETLINK_H__
#define __LIBNETLINK_H__ 1

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <asm/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>
#include <malloc.h>
#include <string.h>
#include <net/if.h>
#include <limits.h>
#include <errno.h>


#define PREFIXLEN_SPECIFIED 1

struct rtnl_handle
{
    int fd;
    struct sockaddr_nl local;
    struct sockaddr_nl peer;
    __u32 seq;
    __u32 dump;
};

typedef struct
{
    __u8 family;
    __u8 bytelen;
    __s16 bitlen;
    __u32 flags;
    __u32 data[8];
} inet_prefix;

int rtnl_open(struct rtnl_handle *rth);
int rtnl_talk(struct rtnl_handle *rtnl, struct nlmsghdr *n, pid_t peer,
        unsigned groups, struct nlmsghdr *answer);
void rtnl_close(struct rtnl_handle *rth);

int get_prefix(inet_prefix *dst, const char *ip, int family);
int addattr_l(struct nlmsghdr *n, int maxlen, int type, void *data, int alen);

#endif /* __LIBNETLINK_H__ */
