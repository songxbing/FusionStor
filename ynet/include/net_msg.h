#ifndef __NET_MSG_H__
#define __NET_MSG_H__

typedef struct {
        uuid_t uuid;
        uint32_t len;
        uint32_t crc;
        uint32_t retval;
        char buf[0];
} msg_head_t;

int net_sendmulti(const int *sd, int count, const char *req, int reqlen);
int net_recvmulti(const int *sd, int count, char *rep, int *replen, int timeout, uuid_t *uuid);

#endif
