#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os
import sys
import ConfigParser

from Umpweb import defs
from Umpweb.common import utils


ump_main_path = os.path.abspath(os.path.split(os.path.realpath(__file__))[0] + "")

code_config_path = '%s/Umpweb/etc/%s/%s' % (ump_main_path, defs.PRODUCT_NAME, defs.CONFIG_NAME)

class InstallManage(object):

    def __init__(self):
        self.install_path = defs.install_path
        self.etc_install_path = '%s/etc/%s' % (self.install_path, defs.PRODUCT_NAME)
        self.cfg_install_path = os.path.join(self.etc_install_path, defs.CONFIG_NAME)

    def ensure_install_path(self):
        if not os.path.exists(self.etc_install_path):
            os.makedirs(self.etc_install_path)

    def system_release_info(self):
        platform_cmd = '''python -c "import platform;print ','.join(platform.dist())"'''
        returncode, stdout, stderr = utils._exec_pipe(platform_cmd)
        (distro, release, codename) = stdout.split(',')
        return distro, release, codename

    def install_ump_config(self):
        """ copy codepath/etc/ui.conf to install_path/etc/product_name
        
        """
        if os.path.isfile(self.cfg_install_path):
            print '*'*10
            print 'WARN: %s is existed. use the old.' % self.cfg_install_path
            print '*'*10
        else:
            cp_config_cmd = "cp %s %s" % (code_config_path, self.etc_install_path)
            returncode, stdout, stderr = utils._exec_pipe(cp_config_cmd)
            if not returncode == 0:
                print >>sys.stderr, stderr
                sys.exit(1)
        
    def run(self):
        self.ensure_install_path()
        self.system_release_info()
        self.install_ump_config()
        print '=================='
        print >>sys.stdout, self.etc_install_path
        print '=================='
    

if __name__ == '__main__':
    install_manage = InstallManage()
    install_manage.run()

