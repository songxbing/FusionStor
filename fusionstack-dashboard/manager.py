#!/usr/bin/env python2
#-*- coding: utf-8 -*-
"""
Created on 20101101
"""

import os
import web
import Ump
import time
from Ump import ua_main
from Ump.websocket import pushToClient

app = ua_main.app
home = os.path.abspath(os.path.split(os.path.realpath(Ump.__file__))[0])

def start_proxy():
    os.system('ump-proxy-download --stop > /dev/null 2>&1')
    os.system('ump-proxy-shell --stop > /dev/null 2>&1')
    os.system('python %s/websocket/ws_server_daemon.py --stop  > /dev/null 2>&1'%home)
    code = os.system('python %s/websocket/ws_server_daemon.py --start'%home)
    code = os.system('python %s/bin/ump-proxy-download --start'%home)
        #raise Exception('ump-proxy-download 启动失败')
#    code = os.system('ump-proxy-shell --start')
#    if code != 0:
#        return ''

def send_message():
    time.sleep(2)
    try:
        pushToClient.sendmessage('start')
    except:pass


if __name__ == "__main__":
    start_proxy()
    ua_main.log_init()
    ua_main.log_customize()
    ua_main.db_api.websession_hard_delete()
    send_message()
    ua_main._key()
    app.add_processor(web.loadhook(ua_main.session_hook))
    app.add_processor(web.loadhook(ua_main.history_hook))
    app.run()
