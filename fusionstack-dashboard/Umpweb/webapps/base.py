#!/usr/bin/env python2
# -*- coding: utf-8 -*-


import datetime
from Umpweb.common import utils
from Umpweb.db import api as db_api


class ClusterLicenseBase(object):

    def cluster_license(self):
        clusters = db_api.cluster_get_all()
        for cluster in clusters:
            clusterhosts = db_api.host_get_with_cluster(cluster.id)

            cluster.hosts = clusterhosts

            cluster.register_date_show = cluster.register_date

            cluster= self._set_invalid_date_show(cluster)

            cluster = self._set_recommended_size(cluster)
            cluster = self._set_license_stat(cluster)

            cluster = self._set_free_license(cluster)

            if cluster.register_date_show:
                if isinstance(cluster.register_date_show, (str, unicode)):
                    cluster.register_date_at = datetime.datetime.strptime(cluster.register_date_show, '%Y-%m-%d %H:%M:%S').date()
                if isinstance(cluster.register_date_show, datetime.datetime):
                    cluster.register_date_at = cluster.register_date_show.date()
        return clusters[0]

    def _set_license_stat(self, cluster):
        if cluster.license_stat == 'permit':
            cluster.license_stat_show = '已授权'
        elif cluster.license_stat == 'invalid':
            cluster.license_stat_show = '授权无效'
        elif cluster.license_stat == 'free':
            cluster.license_stat_show = '试用期'
        elif cluster.license_stat == 'error':
            cluster.license_stat_show = '授权失败'
        elif cluster.license_stat == 'permanent':
            cluster.license_stat_show = '已授权'
            cluster.invalid_date_show = '无限期'
            cluster.invalid_date_at = '无限期'
        else:
            cluster.license_stat_show = '未识别'
            cluster.register_date_show = ''
            cluster.invalid_date_show = ''

        permit_hosts = [host for host in cluster.hosts if host.node_register_info in ['permit', 'permanent']]
        cluster.permit_hosts = permit_hosts

        cluster.invalid_hosts = [host for host in cluster.hosts if host.node_register_info in ['invalid']]

        overdate = [host.node_register_info for host in cluster.hosts if host.node_register_info == 'overdate']
        overquota = [host.node_register_info for host in cluster.hosts if host.node_register_info == 'overquota']

        if overdate != [] and overquota != []:
            cluster.license_stat_ = '已过期,容量超额'
        elif overdate != [] and cluster.license_stat == 'permanent':
            cluster.license_stat_ = '已过期'
        elif overquota != [] and not  cluster.license_capacity=='Infinite capacity':
            cluster.license_stat_ = '容量超额'
        elif cluster.license_stat == 'overquota':
            cluster.license_stat_ = '容量超额'
        else:pass

        return cluster

    def _set_invalid_date_show(self, cluster):
        if cluster.invalid_date:
            cluster.invalid_date_at = datetime.datetime.strptime(cluster.invalid_date, '%Y-%m-%d %H:%M:%S').date() 

        cluster.invalid_date_show = cluster.invalid_date 
        if cluster.license_stat == 'permanent':
            cluster.invalid_date_show = '无限期'
            cluster.invalid_date_at = '无限期'

        return cluster

    def _set_free_license(self, cluster):
        frees = [h.node_register_info for h in cluster.hosts if h.node_register_info == 'free']
        if frees != []:
            cluster.license_stat_ = '试用期'
        cluster.free_hosts = frees

        if len(frees) == len(cluster.hosts) and cluster.hosts:
            delta = datetime.timedelta(days=90)

            if cluster.invalid_date:
                invalid_date = datetime.datetime.strptime(cluster.invalid_date, '%Y-%m-%d %H:%M:%S')
                storage_create_date = invalid_date - delta
                cluster.register_date_show = storage_create_date 
        return cluster 

    def _set_recommended_size(self, cluster):
        recommended_size = 0

        license_need_total = 0
        for node in cluster.hosts:
            license_need_total += node.license_total

        cluster.already_upload_capacity = cluster.license_capacity
        has_license_capacity = 0
        if cluster.license_capacity is None:
            cluster.already_upload_capacity = 0
        elif cluster.license_capacity == 'Infinite capacity':
            has_license_capacity = license_need_total + 100
            cluster.already_upload_capacity = '无限容量'
        else:
            cluster.already_upload_capacity = cluster.license_capacity
            has_license_capacity = cluster.license_capacity.strip('G')

        recommended_size = license_need_total - int(has_license_capacity)
        recommended_size_show = "0T"
        if recommended_size > 0:
            recommended_size = int(utils.yy(license_need_total, 1024)) if recommended_size else license_need_total
            recommended_size_show = "%sT" % recommended_size
        

        cluster.recommended_size_show = recommended_size_show
        return cluster


