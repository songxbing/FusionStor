#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os
import web
import json
import traceback

from Umpweb.base import render_jinja
from Umpweb.db import api as db_api
from Umpweb.common import utils, config
from Umpweb.common.utils import login_required
from Umpweb.base import session, _, render_jinja

urls = (
        '/list',           'PoolList',
        '/grid',           'GridPool',
        '/create',         'PoolCreate',
        '/delete',         'PoolDelete',
        '/update',         'PoolUpdate',
)

app = web.application(urls, locals())
render = render_jinja('static/templates/pool', encoding='utf-8',)

class PoolList:
    @login_required
    def GET(self):
        web.header("Content-Type", "text/plain")
        return render.pool_list()


POOL_PRIORITY_MAP = {
    '0': '高速层',
    '1': '低速层',
    '-1': '自动分层'
}


class GridPool:
    @login_required
    def GET(self):
        web_input = web.input()

        filters = {}

        page, total, records, rows = \
                utils.grid_rows_query(db_api.pool_table, web_input, filters=filters)

        for pool in rows:
            poolvolumes = db_api.volume_get_with_poolid(pool.id)
            pool.volumes_count = len(poolvolumes)

            pool_pds = db_api.protection_domain_get_with_pool(pool.id)
            pool.show_protection_domains = utils.join_object_keys(pool_pds, 'name')

            pool.priority_show = POOL_PRIORITY_MAP.get(pool.priority, pool.priority)
            if pool.ec_data:
                pool.repnum = '--'
                pool.ec = "%s + %s" % ( pool.ec_data ,pool.ec_code)
            else:
                pool.ec = '--'


        cells = ['id','path', 'size', 'protocol', 'volumes_count', 'quota', 'repnum', 'ec', 'provisioning', 'ports', \
                'connections', 'parent', 'show_protection_domains', 'priority_show', 'username','created_at']
        rows_json = utils.grid_json(page, total, records, rows, 'id', cells)

        web.header("Content-Type", "application/json")
        return rows_json

class PoolCreate:

    def GET(self):
        webInput = web.input()
        reload(config)
        protocols = config.protocols
        cluster = db_api.cluster_get(1)
        if cluster.is_nfs:
            protocols.append('nfs')
    
        user_id = web.config._session.user.id
        user = db_api.user_get(user_id)
        
        protection_domains = db_api.protection_domain_get_all()
        return render.pool_create(
                    user=user,
                    protocols=protocols, 
                    protection_domains=protection_domains)

    def POST(self):
        web_input = web.input()
        values = dict(**web.input())
        web.header("Content-Type", "application/json")
        body = {"PoolCreate":{'params':values}}
        return utils.web_return(body)


class PoolUpdate:

    def GET(self):
        x = web.input()
        pool_id = x['pool_id']
        pool = db_api.pool_get(pool_id)
        protection_domains = db_api.protection_domain_get_all()
        pool_pds = db_api.protection_domain_get_with_pool(pool.id)
        pool_pd_ids = [x.id for x in pool_pds ]
        return render.pool_update(pool=pool, pool_pd_ids=pool_pd_ids,
                protection_domains=protection_domains, provisioning=pool.provisioning)

    def POST(self):
        web_input = web.input()
        values = dict(**web.input())
        web.header("Content-Type", "application/json")
        body = {"PoolUpdate":{'params':values}}
        return utils.web_return(body)


class PoolDelete:
    @login_required
    def GET(self):
        web.header("Content-Type", "application/json")
        values = dict(**web.input())
        body = {"PoolDelete":{'params':values}}
        return utils.web_return(body)
