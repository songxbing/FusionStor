#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os
import web
import json
import traceback

from Umpweb.base import render_jinja
from Umpweb.db import api as db_api
from Umpweb.common import utils

from Umpweb.base import session, _, render_jinja

render = render_jinja('static/templates/remotecopy', encoding='utf-8', )


class RemoteCopyList:
    def GET(self):
        web.header("Content-Type", "text/plain")
        return render.remotecopy_list()


class RemoteCopySettings:
    def GET(self):
        web.header("Content-Type", "text/plain")
        return render.remotecopy_settings()


class ClientList:
    def GET(self):
        web.header("Content-Type", "text/plain")
        return render.client_list()


class ClientCreate:
    def GET(self):
        web.header("Content-Type", "text/plain")
        rrservers = db_api.rrserver_get_all()
        volumes = db_api.get_list(db_api.volume_table)
        return render.rrclient_create(rrservers=rrservers, volumes=volumes)

    def POST(self):
        x = web.input()
        web.header("Content-Type", "application/json")
        values = dict(**web.input())
        body = {"RRClientCreate": {'params': values}}
        return utils.web_return(body)

class ClientUpdate:

    def GET(self):
        x = web.input()
        rrclient = db_api.rrclient_get(x.get('id'))
        rrservers = db_api.rrserver_get_all()
        user_id = web.config._session.user.id
        volumes = db_api.get_list(db_api.volume_table)
        return render.rrclient_update(rrclient=rrclient, rrservers=rrservers, volumes=volumes)

    def POST(self):
        web.header("Content-Type", "application/json")
        values = dict(**web.input())
        body = {"RRClientUpdate": {'params': values}}
        return utils.web_return(body)

class ClientDelete:
    def POST(self):
        web.header("Content-Type", "application/json")
        x = web.input()
        body = {"RRClientDelete": {'params': {'id': x.get('id').strip()}}}
        return utils.web_return(body)


class GridClient:
    def POST(self):
        web_input = web.input()
        filters = {}
        page, total, records, rows = \
            utils.grid_rows_query(db_api.rrclient_table, web_input, filters=filters)
    
        for c in rows:
            rrservers = db_api.rrserver_get_with_rrclient(c.id)
            c.sites = utils.join_object_keys(rrservers, 'address')

        cells = ['id', 'name', 'src_volume_path', 'dst_volume_path', 'sites',
                'interval', 'qos', 'user', 'created_at']

        rows_json = utils.grid_json(
            page, total, records, rows, 'id', cells)
        web.header("Content-Type", "application/json")
        return rows_json


class ServerList:
    def GET(self):
        web.header("Content-Type", "text/plain")
        return render.server_list()


class ServerCreate:
    def GET(self):
        web.header("Content-Type", "text/plain")
        rrservers = db_api.rrserver_get_all()
        return render.rrserver_create(rrservers=rrservers)

    def POST(self):
        x = web.input()
        web.header("Content-Type", "application/json")
        values = dict(**web.input())
        body = {"RRServerCreate": {'params': values}}
        return utils.web_return(body)


class ServerUpdate:

    def GET(self):
        x = web.input()
        rrserver = db_api.rrserver_get(x.get('id'))
        return render.rrserver_update(rrserver=rrserver)

    def POST(self):
        web.header("Content-Type", "application/json")
        values = dict(**web.input())
        body = {"RRServerUpdate": {'params': values}}
        return utils.web_return(body)


class ServerDelete:
    def POST(self):
        web.header("Content-Type", "application/json")
        x = web.input()
        body = {"RRServerDelete": {'params': {'id': x.get('id').strip()}}}
        return utils.web_return(body)


class GridServer:

    def POST(self):
        web_input = web.input()
        filters = {}
        page, total, records, rows = \
            utils.grid_rows_query(db_api.rrserver_table, web_input, filters=filters)

        cells = ['id', 'address', 'port', 'user', 'username', 'created_at']

        rows_json = utils.grid_json(
            page, total, records, rows, 'id', cells)
        web.header("Content-Type", "application/json")
        return rows_json

