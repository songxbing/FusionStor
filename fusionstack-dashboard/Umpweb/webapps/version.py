#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os
import sys
import web

from Umpweb.base import session, _, render_jinja


reload(sys)
sys.setdefaultencoding('utf-8')
render = render_jinja('static/templates/version', encoding='utf-8',)


class Upgrade:
    def GET(self):
        return render.upgrade()

class About:
    def GET(self):
        return render.about()
