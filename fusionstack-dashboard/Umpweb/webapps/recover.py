#!/usr/bin/env python2
#-*- coding: utf-8 -*-
############################
# THIS FILE IS USELESS
###############################
import os
import web
import json
import sys
import traceback

from Umpweb.common import utils
from Umpweb.base import session, _, render_jinja
from Umpweb.db import api as db_api
from sqlalchemy.orm.exc import NoResultFound

render = render_jinja('static/templates/recover', encoding='utf-8',)

policy_path = '/dev/shm/lich4/nodectl/recovery/qos_policy'
maxbw_path = '/dev/shm/lich4/nodectl/recovery/qos_maxbandwidth'

class RecoverQos:

    def GET(self):
        msg = None
        policy = None
        maxbw = None
        ips = []
        values = {}
        reply = {}

        web.header("Content-Type", "text/plain")
        x = web.input()
        print "get get xxxxxxxxxxxxxxx" , x

        clusters = db_api.cluster_get_all()
        for cluster in clusters:
            if cluster.name is None:
                msg = "集群 %s 不存在，请创建集群。"%(cluster.name)
            hosts = db_api.host_get_with_cluster(cluster.id)
            if not hosts:
                msg = "节点不存在，请添加节点。"

        for host in hosts:
            ips.append(host.ip)

        values['hosts'] = ips

        body = {'GetRecoverQos':{'params':values}}
        x = utils.web_return(body)
        x = json.loads(x)

        reply = x['records']
        policy = reply['policy']
        maxbw = reply['maxbw']

        policy = policy.strip()
        maxbw = maxbw.strip()

        return  render.recover_qos(policy=policy, maxbw=maxbw, msg=msg)

    def POST(self):
        ips = []
        values = {}
        x = web.input()
        print  "post ppppppppppppppppp xxxxxxxxxxxxxx" , x
        hosts = db_api.host_get_all()
        hosts = sorted(hosts, key=lambda x :x.id)        
        for host in hosts:
            ips.append(host.ip)

        values['qos_maxbw'] = x.get('qos_maxbw')
        values['qos_policy'] = x.get('qos_policy')
        values['hosts'] = ips

        web.header("Content-Type", "application/json")
        body = {"RecoverQos":{'params':values}}
        return utils.web_return(body)
