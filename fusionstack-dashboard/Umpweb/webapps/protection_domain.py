#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os
import web
import json

from sqlalchemy.orm.exc import NoResultFound
from Umpweb.base import render_jinja
from Umpweb.db import api as db_api
from Umpweb.common import utils
from Umpweb.common.utils import login_required
from Umpweb.base import session, _, render_jinja

urls = (
        '/list',    'ProtectionDomain',
        '/grid',    'GridProtectionDomain',
)
app = web.application(urls, locals())

render = render_jinja('static/templates/protection_domain', encoding='utf-8',)

class ProtectionDomain:
    @login_required
    def GET(self):
        web.header("Content-Type", "text/plain")
        return render.protection_domain()

class GridProtectionDomain:
    def POST(self):
        web_input = web.input()
        filters = {}
        page, total, records, rows = \
                utils.grid_rows_query(db_api.protection_domain_table, web_input, filters=filters)

        for protection_domain in rows:
            protection_domain.show_name = protection_domain.name 
            #if protection_domain.name == "default":
            #    protection_domain.show_name ="默认保护域"

        cells = ['id', 'show_name', 'created_at','id']

        rows_json = utils.grid_json(
                page, total, records, rows, 'id', cells)
        web.header("Content-Type", "application/json")
        return rows_json
