#!/usr/bin/env python2
#-*- coding: utf-8 -*-


import os
import re
import json
import time
import sys
import cgi
import web
import uuid
import httplib2
import httplib
import traceback
import subprocess
import datetime

from Umpweb.common import utils
from Umpweb.base import render_jinja
from Umpweb.db import api as db_api

render = render_jinja('static/templates/modal', encoding='utf-8',)

class ConfirmModal:
    def GET(self):
        return render.modal()

class AlertModal:
    def GET(self):
        return render.alert_modal()
