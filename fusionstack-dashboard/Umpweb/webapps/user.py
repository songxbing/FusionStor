#!/usr/bin/env python2
#-*- coding: utf-8 -*-
"""
Created on 20101015
Author: zkz
"""

from Umpweb.db import api as db_api
from Umpweb.common.utils import current_dir, login_required, grid_rows_query, grid_json

import web
from Umpweb.base import render_jinja

import os
import json
import sqlite3
import time
import hashlib
import re
import traceback
from Umpweb.base import session, _
from Umpweb.common import utils

render = render_jinja('static/templates/user', encoding='utf-8',)



def notfound():
    return web.notfound(render.notfound())

class Page():
    def GET(self):
        return render.page()

class GridUser():
    def GET(self):
        web_input = web.input()
        filters = {}
        page, total, records, rows = \
                utils.grid_rows_query(db_api.user_table, web_input, filters=filters)
        for user in rows:
            user_pds = db_api.protection_domain_get_with_user(user.id)
            user.show_protection_domains = utils.join_object_keys(user_pds, 'name')
            role = db_api.user_attrget_role(user.name)
            user.rolename = role.name

        cells = ['id', 'name', 'rolename', 'identity', 'repnum', 'quota', 'show_protection_domains', 'created_at']
        rows_json = grid_json(page, total, 
                   records, rows, 'id', cells)
        web.header("Content-Type", "application/json")
        return rows_json


def encode_passwd(plain_text):
    return hashlib.md5(plain_text.strip()).hexdigest()


class UserCreate():
    def GET(self):
        pds = db_api.protection_domain_get_all()
        return render.user_create_page(pds=pds)
    
    def POST(self):
        web.header("Content-Type", "application/json")
        values = dict(**web.input())
        print values
        body = {'UserCreate':{'params':values}}
        return utils.web_return(body)


class UserUpdate():
    def GET(self):
        x = web.input()
        user = db_api.user_get(x['user_id'])
        pds = db_api.protection_domain_get_all()
        user_pds = db_api.protection_domain_get_with_user(user.id)
        user_pd_ids = [x.id for x in user_pds ]
        return render.user_update(user=user, pds=pds, user_pd_ids=user_pd_ids)
    
    def POST(self):
        web_input = web.input()
        web.header("Content-Type", "application/json")
        values = dict(**web.input())
        body = {'UserUpdate':{'params':values}}
        return utils.web_return(body)


class UserList():
    def GET(self):
        users = db_api.user_get_all()
        return render.user_list(users=users)


class UserPassword1():
    def GET(self):
        x = web.input()
        user = db_api.user_get(x['user_id'])
        return render.user_password(user=user)


class UserPassword():
    def GET(self):
        x = web.input()
        user = db_api.user_get(x['user_id'])
        return render.user_edit(user=user)

    def POST(self):
        x = web.input()
        web.header("Content-Type", "application/json")
        values = dict(**web.input())
#       values = {'password':password,'new_password':newpasswd,'userid':userid}
        body = {'UserPassword':{'params':values}}
        return utils.web_return(body)


class Self_User_edit():
    def GET(self):
        x = web.input()
        user = db_api.user_get(x['user_id'])
        return render.user_edit(user=user)


class UserDelete():
    def GET(self):
        web.header("Content-Type", "application/json")
        values = dict(**web.input())
        body = {'UserDelete':{'params':values}}
        return utils.web_return(body)

