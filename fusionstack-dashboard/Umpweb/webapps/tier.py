#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os
import web
import json

from sqlalchemy.orm.exc import NoResultFound
from Umpweb.base import render_jinja
from Umpweb.db import api as db_api
from Umpweb.common import utils
from Umpweb.common.utils import login_required


render = render_jinja('static/templates/tier', encoding='utf-8',)

class TierList:
    @login_required
    def GET(self):
        web.header("Content-Type", "text/plain")
        return render.tier_list()
