#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os
import sys
import web
import Umpweb

from Umpweb.common import utils 
from Umpweb.base import session, _, render_jinja


reload(sys)
sys.setdefaultencoding('utf-8')
render = render_jinja('static/templates/snmp', encoding='utf-8',)


class Snmp:

    def GET(self):
        return render.snmp()
    
    def POST(self):
        web.header("Content-Type", "application/json")
        x = web.input()
        body = {'SetSnmpHost':{'params':dict(**x)}}
        return utils.web_return(body)


class SnmpHostDelete:

    def POST(self):
        web.header("Content-Type", "application/json")
        x = web.input()
        body = {'SnmpHostDelete':{'params':dict(**x)}}
        return utils.web_return(body)


class SnmpHosts:
    
    def GET(self):
        web.header("Content-Type", "application/json")
        x = web.input()
        body = {'GetSnmpHosts':{'params':dict(**x)}}
        x = utils.web_return(body)
        return  x


class SnmpHostTest:

    def POST(self):
        web.header("Content-Type", "application/json")
        x = web.input()
        body = {'SnmpHostTest':{'params':dict(**x)}}
        return utils.web_return(body)
