#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import os
import web
import json
import traceback

from Umpweb.base import render_jinja
from Umpweb.db import api as db_api
from Umpweb.common import utils, config
from Umpweb.common.utils import login_required
from Umpweb.base import session, _, render_jinja

urls = (
        '/mail',                'Mail',
        '/mail/create',         'MailCreate',
        '/mail/delete',         'MailDelete',
        '/mail/test',           'MailTest',
        '/support',             'Support',
        '/support/create',      'SupportCreate',
        '/support/delete',      'SupportDelete',
)

app = web.application(urls, locals())
render = render_jinja('static/templates/customer', encoding='utf-8',)


class Mail:
    @login_required
    def GET(self):
        web.header("Content-Type", "text/plain")
        mail = db_api.mail_get_first()
        return render.mail_setting(mail=mail)


# add by zhuwei
class RecMailCreate:
    def POST(self):
        web_input = web.input()
        values = dict(**web.input())
        web.header("Content-Type", "application/json")
        body = {"RecMailCreate":{'params':values}}
        print values
        return utils.web_return(body)


class MailCreate:

    def POST(self):
        web_input = web.input()
        values = dict(**web.input())
        web.header("Content-Type", "application/json")
        body = {"MailCreate":{'params':values}}
        print values
        return utils.web_return(body)


class MailDelete:

    def POST(self):
        web_input = web.input()
        values = dict(**web.input())
        web.header("Content-Type", "application/json")
        body = {"MailDelete":{'params':values}}
        return utils.web_return(body)


class MailTest:

    def POST(self):
        values = dict(**web.input())
        web.header("Content-Type", "application/json")
        body = {"MailTest":{'params':values}}
        return utils.web_return(body)


class Support:
    @login_required
    def GET(self):
        web.header("Content-Type", "text/plain")
        customer = db_api.customer_get_first()
        mail = db_api.mail_get_first()
        return render.support(customer=customer, mail=mail)


class SupportCreate:

    @login_required
    def GET(self):
        web.header("Content-Type", "text/plain")
        return render.support_create()

    def POST(self):
        web_input = web.input()
        values = dict(**web.input())
        web.header("Content-Type", "application/json")
        body = {"CustomerCreate":{'params':values}}
        return utils.web_return(body)


class SupportDelete:

    def POST(self):
        web_input = web.input()
        values = dict(**web.input())
        web.header("Content-Type", "application/json")
        body = {"CustomerDelete":{'params':values}}
        return utils.web_return(body)

if __name__ == '__main__':
    pass
