#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import sys
import traceback

class UmpException(Exception):
    """
    """
    pass

class LichFault(UmpException):
    pass


class LichLiscenFault(LichFault):
    pass


class AuthenticationFailed(UmpException):
    pass

class NoAvailableEdog(UmpException):
    pass

class EdogTempOffline(UmpException):
    pass

class EdogOffline(UmpException):
    pass

class EdogFault(UmpException):
    pass

class PermissionDenied(UmpException):
    pass

class QuotaError(UmpException):
    pass

class IpRangeError(UmpException):
    pass

class HostUnable(UmpException):
    pass

class HostNotEmpty(UmpException):
    pass

class HostnameDuplica(UmpException):
    pass

class ImageUnable(UmpException):
    pass

class InstanceMigrateError(UmpException):
    pass

class InstanceStopRequired(UmpException):
    pass

class ResourcesOver(UmpException):
    '''资源跨集群'''
    pass

class ResourcesNotFound(UmpException):
    pass

class ResourcesTypeError(UmpException):
    '''资源类型错误'''
    pass

class ResourcesInuse(UmpException):
    '''资源正在使用中'''
    pass

class ResourcesConditionDeficit(UmpException):
    '''资源条件不具备 '''
    pass

class OperateReject(UmpException):
    '''操作被拒绝'''
    pass

class UnknownParameter(UmpException):
    '''不能处理的参数'''
    pass

class ClusterDuplica(UmpException):
    pass

class ClusterDoubleIp(UmpException):
    pass

class DeleteError(UmpException):
    pass

class ClusterNotEmpty(UmpException):
    pass

class VolumeInuse(UmpException):
    pass

class VolumesTooMany(UmpException):
    pass

class VolumeLocked(UmpException):
    pass

class VolumeError(UmpException):
    pass

class VpoolNotEmpty(UmpException):
    pass

class VpoolUnable(UmpException):
    pass

class VpoolConditionDeficit(UmpException):
    pass


class Duplica(UmpException):
    pass



if __name__ == "__main__":
    pass
