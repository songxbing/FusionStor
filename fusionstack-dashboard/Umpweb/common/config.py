#!/usr/bin/env python2
#-*- coding: utf-8 -*-

import ConfigParser
import os
import uuid
import Umpweb
import logging

from utils import ensure_dir, home
from Umpweb import defs


LOG = logging.getLogger('Umpweb.common.config')


currpath = os.path.abspath(os.path.split(os.path.realpath(Umpweb.__file__))[0]  )
cfgInstallPath = os.path.abspath(os.path.split(os.path.realpath(Umpweb.__file__))[0] + "/../../../" )

cp = ConfigParser.ConfigParser()
cp_read = cp.read(defs.CONFIG_PATH)

class UmpSetting(object):
    def __init__(self, cp=None, filename=None):
        if cp: 
            self.cp = cp
        elif filename:
            cp = ConfigParser.ConfigParser()
            cp.read(filename)
            self.cp = cp

    def get(self, section='ui_main', key='', _default=None):
        try:
            res = self.cp.get(section, key)
        except Exception, e:
            res = _default
        return res 

_ump_setting = UmpSetting(cp=cp)


debug = cp.get('ui_main', 'debug').lower() == 'true'

main_log = cp.get('ui_main', 'main_log')

try:
    expired_time = int(cp.get('ui_main', 'expired_time'))
except Exception,e:
    expired_time = 10
    pass

is_showPool = cp.get('ui_main', 'is_showPool').lower() == 'true'
is_fusionstack_sso = _ump_setting.get(key='is_fusionstack_sso', _default=False)
is_fusionstack_sso = is_fusionstack_sso in ['on', 'true', 'True', True]

ensure_dir(main_log)
main_log_maxbytes = int(cp.get('ui_main', 'main_log_maxbytes'))
main_log_backupCount = int(cp.get('ui_main', 'main_log_backupCount'))

history_log = cp.get('ui_main', 'history_log')
ensure_dir(history_log)
history_log_maxbytes = int(cp.get('ui_main', 'history_log_maxbytes'))
history_log_backupCount = int(cp.get('ui_main', 'history_log_backupCount'))

ui_consumer_port = int(cp.get('ui_main', 'ui_consumer_port'))
protocols = [x.strip() for x in cp.get('ui_main', 'protocol').split(',')]

controller_server_ip = cp.get('ui_main', 'controller_server_ip')
controller_port = int(cp.get('ui_main', 'controller_port'))

if __name__ == '__main__':
    LOG.info('%s %s' % (main_log_maxbytes, main_log))
    LOG.info('%s' % (history_log_maxbytes))
