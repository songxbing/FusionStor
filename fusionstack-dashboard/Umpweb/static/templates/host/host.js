var HostModalDemo = angular.module('FusionStorUI.host', []);
HostModalDemo.controller('HostModalCtrl',['$scope','$modal', function($scope, $modal) {
    var modalInstance;

    $scope.open = function(url, isFirst) {  
        console.log(url);
        $scope.item = {};
        if (url.indexOf('add') > -1 && isFirst) {

            $scope.item.showConfigBtn = true;
        } else {
            $scope.item.showConfigBtn = false;
        }
        modalInstance = $modal.open({  
            backdrop: 'static',       
            keyboard: false,
            templateUrl : url,
            controller : HostModalInstanceCtrl,  
            resolve : { 
                item : function() {  
                    return $scope.item;  
                }  
            }  
        });  
    };  

    $scope.close = function() {
        modalInstance.dismiss('cancel');  
    }
}]); 
var HostModalInstanceCtrl = function($scope, $modalInstance, item, hostval) {  
    $scope.first = {};
    if (!hostval.first) {
        hostval.first = {};
    } else {
        $scope.first = hostval.first;
    }
    $scope.item = item;  
    $scope.selected = {  
        item : $scope.item 
    };  
    $scope.ok = function() {
        submit_host_add(hostval.first);
        $scope.item.showConfigBtn = false;
        hostval.first = {};
        $scope.first.nohosts = true;

    };
    $scope.toogle = function (e) {
        var $target = $(e.currentTarget);
        var ele = $target.children(".move");  
        if($target.hasClass("on")){  
            ele.animate({left: "0"}, 300);  
            $scope.first.nohosts = false;
        }else {
            ele.animate({left: '30px'}, 300);  
            $scope.first.nohosts = true;
        }
    }

    $scope.config = function() {  
        var flag = submit_host_config();
        if(flag) {
            hostval.first.vip = $scope.first.vip;
            hostval.first.NetWork = $scope.first.NetWork;
            hostval.first.NetMask = $scope.first.NetMask;
            if ($scope.first.nohosts === void 0) {
                $scope.first.nohosts = true;
            }
            hostval.first.nohosts = $scope.first.nohosts

            angular.element('#host_add').scope().open("/host/add", true);
            $modalInstance.dismiss('cancel');
        }
        
    };

    $scope.goto_config = function() {
        angular.element('#host_add').scope().open("/host/config");
        $modalInstance.dismiss('cancel');
    };
    
    $scope.cancel = function() {  
        $modalInstance.dismiss('cancel');  
    };  
};  
  
