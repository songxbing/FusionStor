'use strict';
var UserModalDemo = angular.module('FusionStorUI.user', []);
UserModalDemo.controller('UserModalCtrl', ['$scope', '$modal', '$http', function ($scope, $modal, $http) {
    var modalInstance;
    $scope.templateData = '';
    $scope.open = function (user_id, url) {
        $scope.item = {};
        $http.get(url).success(function (data) {
            $scope.templateData = data;
            modalInstance = $modal.open({
                template: $scope.templateData,
                controller: UserModalInstanceCtrl,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    item: function () {
                        return $scope.item;
                    }
                }
            });
            modalInstance.opened.then(function () {// 模态窗口打开之后执行的函数
            });
            modalInstance.result.then(function (result) {
            }, function (reason) {
            });
        });
    };
    $scope.close = function () {
        modalInstance.dismiss('cancel');
    }
}]);
var UserModalInstanceCtrl = function ($scope, $modalInstance, item) {
    $scope.item = item;
    $scope.selected = {
        item: $scope.item
    };
    $scope.ok = function (user_id) {
        var bValid = true;
        var passwd = $("#user_password_modal").find("#passwd");
        var new_passwd = $("#user_password_modal").find("#new_passwd");
        var re_passwd = $("#user_password_modal").find("#re_passwd");
        bValid = bValid && checkLength_pwd(passwd, "密码由3至16位字符构成", 3, 16, "密码");
        bValid = bValid && checkLength_pwd(new_passwd, "新密码由3至16位字符构成", 3, 16, "新密码");
        bValid = bValid && checkPasswd(new_passwd, re_passwd, '两次输入密码不一致。');
        if (bValid) {
            if (enable_submit) {
                enable_submit = false;
                var $submitBtn = $('.modal-footer').find('.btn-blue');
                $submitBtn.html('<i class="fa fa-spinner fa-spin" style="margin:3px 6px;"></i>');
                $('.rtn-tip').slideUp('fast');
                $.post('/user_password', {
                        'password': $.trim(passwd.val()),
                        'new_password': $.trim(new_passwd.val()),
                        'id': user_id,
                    },
                    function (data) {
                        if (data.reply.is_success) {
                            $('#resp_tips').text('密码修改成功').css("display", "block")
                            $modalInstance.close($scope.selected.item);
                        } else {
                            var error = data.reply.error;
                            $('.rtn-tip').text(error).slideDown('fast');
                            $submitBtn.html(SelfTranslate("[[ 'label ok'|translate ]]"));
                            enable_submit = true;
                        }
                    }
                );
            }
        }

    };
    $scope.user_create = function () {
        var modal_selector = $("#user_create_modal")
        var name = modal_selector.find("#user_name");
        var passwd = modal_selector.find("#add_passwd");
        var re_passwd = modal_selector.find("#re_add_passwd");
        var identity = modal_selector.find("input[name='identity']:checked").val();
        var quota = modal_selector.find("#user_quota");
        var repnum = modal_selector.find("#user_repnum");
        var user_pds = modal_selector.find(".user_pds");
        var pd_ids = $scope.get_pd_ids(user_pds)

        var bValid = true;
        bValid = bValid && checkLength(name, "用户名", 3, 32);
        bValid = bValid && checkRegexp(name, /^[0-9a-z-]+$/, "用户名由小写英文字母、数字、中划线(英文)构成。");
        bValid = bValid && checkLength_pwd(passwd, "密码由3至16位字符构成", 3, 16, '密码');
        bValid = bValid && checkPasswd(passwd, re_passwd, '两次密码输入不一致。');

        var post_data = {
            'identity': identity,
            'name': $.trim(name.val()),
            'password': $.trim(passwd.val()),
            'repnum': repnum.attr('realvalue'),
            'quota': $.trim(quota.val()),
            'protection_domain_ids': pd_ids
        }

        if (!is_fusionnas) {
            var chap_name = modal_selector.find("#chap_name");
            var chap_password = modal_selector.find("#chap_password");
            bValid = bValid && checkCHAP(chap_name, chap_password);
            post_data['chap_name'] = $.trim(chap_name.val())
            post_data['chap_password'] = $.trim(chap_password.val())
        }

        if ($.trim(quota.val()).length > 0) {
            bValid = bValid && checkRegexp_num(quota, /^[0-9]+$/i, "配额必须为大于0的整数");
        }
        if (bValid) {
            if (enable_submit) {
                enable_submit = false;
                var $submitBtn = $('.modal-footer').find('.btn-blue');
                $submitBtn.html('<i class="fa fa-spinner fa-spin" style="margin:3px 6px;"></i>');
                $('.rtn-tip').slideUp('fast');
                $.post('/user_create', post_data,
                    function (data) {
                        if (data.reply.is_success) {
                            $('#user_grid_table').trigger("reloadGrid");
                            $modalInstance.close($scope.selected.item);
                        } else {
                            var error = data.reply.error;
                            $('.rtn-tip').text(error).slideDown('fast');
                            $submitBtn.html(SelfTranslate("[[ 'label ok'|translate ]]"));
                            enable_submit = true;
                        }
                    });
            }
        }
    };
    $scope.get_pd_ids = function (selector) {
        var pd_ids = ''
        selector.each(function () {
            if ($(this).attr('checked') == true || $(this).attr('checked') == "checked") {
                pd_ids += $(this).val() + ",";
            }
        });
        return pd_ids;
    };

    $scope.update = function (user_id) {
        var modal_selector = $("#user_update_modal")
        var quota = modal_selector.find("#user_quota");
        var repnum = modal_selector.find("#user_repnum");
        var user_pds = modal_selector.find(".user_pds");
        var pd_ids = $scope.get_pd_ids(user_pds);
        var bValid = true;
        if ($.trim(quota.val()).length > 0) {
            bValid = bValid && checkRegexp_num(quota, /^[0-9]+$/i, "配额必须为大于0的整数");
        }

        var post_data = {
            'repnum': repnum.attr('realvalue'),
            'quota': $.trim(quota.val()),
            'id': user_id,
            'protection_domain_ids': pd_ids,
        }
        if (!is_fusionnas) {
            var chap_name = modal_selector.find("#chap_name");
            var chap_password = modal_selector.find("#chap_password");
            bValid = bValid && checkCHAP(chap_name, chap_password);
            post_data['chap_name'] = $.trim(chap_name.val())
            post_data['chap_password'] = $.trim(chap_password.val())
        }

        if (bValid) {
            if (enable_submit) {
                enable_submit = false;
                var $submitBtn = $('.modal-footer').find('.btn-blue');
                $submitBtn.html('<i class="fa fa-spinner fa-spin" style="margin:3px 6px;"></i>');
                $('.rtn-tip').slideUp('fast');
                $.post('/user_update', post_data,
                    function (data) {
                        if (data.reply.is_success) {
                            $('#user_grid_table').trigger("reloadGrid");
                            $modalInstance.close($scope.selected.item);
                        } else {
                            var error = data.reply.error;
                            $('.rtn-tip').text(error).slideDown('fast');
                            $submitBtn.html(SelfTranslate("[[ 'label ok'|translate ]]"));
                            enable_submit = true
                        }
                    }
                );
            }
        }
    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};

