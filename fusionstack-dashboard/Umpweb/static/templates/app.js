//=======================================================================
    var fusionstorApp = angular.module('FusionStorApp', [
        'ngSanitize',
        'ui.bootstrap', 
        'pascalprecht.translate',
        "FusionStorUI.user",
        "FusionStorUI.host",
        "FusionStorUI.disk",
        "FusionStorUI.QoS",
        "FusionStorUI.volume",
        "FusionStorUI.oplog",
        "FusionStorUI.modal",
        "FusionStorUI.snapshot",
        "FusionStorUI.cgsnapshot",
        "FusionStorUI.vgroup",
        "FusionStorUI.isolation",
        "FusionStorUI.pool",
        "FusionStorUI.RRServer",
        "FusionStorUI.RRClient",
        "FusionStorUI.import",
        "FusionStorUI.sysconfig",
        "FusionStorUI.opconfig",
        "FusionStorUI.task",
        "FusionStorUI.report",
        "FusionStorUI.mail",
        "FusionStorUI.folder",
        "FusionStorUI.license",
        "FusionStorUI.about"
    ]);
    fusionstorApp.config(['$interpolateProvider', '$translateProvider', function($interpolateProvider, $translateProvider) {
        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
        var lang = window.localStorage.lang||'zh_CN';
        $translateProvider.preferredLanguage(lang);
        $translateProvider.useStaticFilesLoader({
            prefix: '/static/i18n/',
            suffix: '.json'
        });
    }]);
    fusionstorApp.controller('LanguageSwitchingCtrl', ['$scope', '$translate', '$http', '$compile', function ($scope, $translate, $http, $compile) {
        $scope.switching = function(lang){
            window.localStorage.lang = lang;
            $scope.cur_lang = $translate.use(lang);
        };
        $scope.cur_lang = $translate.use('zh_CN');
        $scope.cur_lang = 'test';
        $scope.firstName = "John";

        $scope.translate = function(key){
            return $translate.instant(key)
        };
    
        $scope.loadGlobal = function(box, url) {
            $http.get(url).success(function (response) {
                $compile(box.html(response))($scope);
            });
        };
        
        $scope.showSubMenu = '';
        $scope.openSubMenu = function(name) {
          if (name == $scope.showSubMenu) {
            $scope.showSubMenu = null;
            return;
          }   
          $scope.showSubMenu = name;
        }
    }]);
    fusionstorApp.directive('loadA', function ($http, $compile) {
        return {
            link: function ($scope, element, attrs) {
    
                $scope.loadContentpanel = function($event) {
                    var url = $event;
                    if (url != '/summary/fusionnas' && url != '/summary/fusionstor') {
                        url = ($("#"+$event.currentTarget.id).attr('realvalue'));
                    }
                    $http.get(url).success(function (response) {
                        $compile($('.contentpanel').html(response))($scope);
                        
                    });
                    $("#interval_tip").addClass(url);
                }

                if (is_fusionnas){
                    $scope.loadContentpanel('/summary/fusionnas');
                } else {
                    $scope.loadContentpanel('/summary/fusionstor');
                }
            }
        }
    });
    fusionstorApp.directive('loadSummary', function ($http, $compile) {
        return {
            link: function ($scope, element, attrs) {
    
                $scope.loadSummary = function(boxClassName, url) {
                    $http.get(url).success(function (response) {
                        $compile($('.'+boxClassName).html(response))($scope);
                    });
                }
//                $scope.loadSummary("box5", "/volume");
                $scope.loadSummary("box5", "/summary/storage");
                $scope.loadSummary("box6", "/summary/rebuild");
                $scope.loadSummary("box8", "/summary/memory");
                $scope.loadSummary("box10", "/summary/io");
                $scope.loadSummary("box11", "/summary/iopss");
                $scope.loadSummary("box7", "/summary/iops");
                //$scope.loadSummary("box9", "/cpu");
            }
        }
    });
    fusionstorApp.service('hostval', function() {
        var obj = {};
        return obj;
    });
    function loadGlobal(obj, url) {
        var appElement = document.querySelector('[ng-controller=LanguageSwitchingCtrl]');
        var $scope = angular.element(appElement).scope(); 
        $scope.loadGlobal(obj, url);
    }

    function bootstrapApplication() {
        angular.element(document).ready(function() {
            angular.bootstrap(document, ["FusionStorApp"]);
        }); 
    }
    
//=======================================================================

