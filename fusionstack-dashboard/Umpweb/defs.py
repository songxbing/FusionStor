#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os
import Umpweb

def get_install_dir():
    return os.path.abspath(os.path.split(os.path.realpath(Umpweb.__file__))[0] + "/../../..")

CODE_MAIN_PATH = os.path.abspath(os.path.split(os.path.realpath(Umpweb.__file__))[0] + "/../..")

#install_path /opt/fusionstack
install_path = get_install_dir()


LOCAL_HOST = ['0.0.0.0', 'localhost', '127.0.0.1']
LOG_FOLER = os.path.join(install_path, 'log')

UMP_HOME = os.path.dirname(os.path.realpath(Umpweb.__file__))

UMP_NAME = 'fusionstack-dashboard'
ETC_NAME = 'etc'

#fusionstor or fusionnas
PRODUCT_NAME = os.path.basename(CODE_MAIN_PATH)

ETC_DIR_PATH = os.path.join(install_path, ETC_NAME, PRODUCT_NAME)

DB_CFG = 'db.cfg'
DB_CFG_PATH = os.path.join(ETC_DIR_PATH, DB_CFG)

#/opt/fusionstack/log/product
UMP_LOG_FOLER = os.path.join(LOG_FOLER, PRODUCT_NAME)

CONFIG_NAME = 'ui.conf'
CONFIG_PATH = os.path.join(ETC_DIR_PATH, CONFIG_NAME)

UMP_TREE = "%s/%s" % (PRODUCT_NAME, 'fusionstack-dashboard/Umpweb')
UMP_PATH = os.path.join(install_path, UMP_TREE)

is_fusionnas = PRODUCT_NAME == 'fusionnas'
DASHBOARD_PID_FILE = '/var/run/%s_dashboard_server.pid' % (PRODUCT_NAME)

if is_fusionnas:
    UI_PORT = 9090
else:
    UI_PORT = 8080

if __name__ == '__main__':
    print CODE_MAIN_PATH
    print LOG_FOLER
    print PRODUCT_NAME
    print CONFIG_PATH
