#!/usr/bin/env python2
#-*- coding: utf-8 -*-


"""
Created on 20160328
"""

from Umpweb.webapps import (
    summary, 
    protection_domain,
    host,
    disk,

    pool,
    volume,
    folder,
    
    access_control,

    schedule,
    report,
    task,
    alert,
    customer,
    license,
)

urls = (
        '/cluster_create_page',             'Umpweb.webapps.cluster.ClusterCreate',
        '/cluster_import_page',             'Umpweb.webapps.cluster.ClusterImport',
        '/cluster_page',                    'Umpweb.webapps.cluster.ClusterPage',
        '/cluster_list',                    'Umpweb.webapps.cluster.ClusterList',
        '/grid_cluster',                    'Umpweb.webapps.cluster.GridCluster',
        '/cluster',                         'Umpweb.webapps.cluster.Cluster',
        '/(\d+)/get_iops_day',              'Umpweb.webapps.cluster.GetIopsDay',
        '/(\d+)/get_iops_week',             'Umpweb.webapps.cluster.Get_iops_week',
        '/(\d+)/get_iops_month',            'Umpweb.webapps.cluster.Get_iops_month',
        '/(\d+)/get_read_day',              'Umpweb.webapps.cluster.Get_read',
        '/(\d+)/get_read_week',             'Umpweb.webapps.cluster.Get_read_week',
        '/(\d+)/get_read_month',            'Umpweb.webapps.cluster.Get_read_month',
        '/(\d+)/get_write_day',             'Umpweb.webapps.cluster.Get_write',
        '/(\d+)/get_write_week',            'Umpweb.webapps.cluster.Get_write_week',
        '/(\d+)/get_write_month',           'Umpweb.webapps.cluster.Get_write_month',
        '/(\d+)/get_swallow_spit_day',      'Umpweb.webapps.cluster.Get_swallow_spit',
        '/(\d+)/get_swallow_spit_week',     'Umpweb.webapps.cluster.Get_swallow_spit_week',
        '/(\d+)/get_swallow_spit_month',    'Umpweb.webapps.cluster.Get_swallow_spit_month',
        '/(\d+)/get_swallow_day',           'Umpweb.webapps.cluster.Get_swallow',
        '/(\d+)/get_swallow_week',          'Umpweb.webapps.cluster.Get_swallow_week',
        '/(\d+)/get_swallow_month',         'Umpweb.webapps.cluster.Get_swallow_month',
        '/(\d+)/get_spit_day',              'Umpweb.webapps.cluster.Get_spit',
        '/(\d+)/get_spit_month',            'Umpweb.webapps.cluster.Get_spit_month',
        '/(\d+)/get_spit_week',             'Umpweb.webapps.cluster.Get_spit_week',
        '/(\d+)/get_iops_val_day',          'Umpweb.webapps.cluster.Get_iops_val',
        '/(\d+)/get_iops_val_week',         'Umpweb.webapps.cluster.Get_iops_val_week',
        '/(\d+)/get_iops_val_month',        'Umpweb.webapps.cluster.Get_iops_val_month',

        '/(\d+)/get_latency',               'Umpweb.webapps.cluster.GetLatency',
        '/(\d+)/get_read_latency',          'Umpweb.webapps.cluster.GetReadLatency',
        '/(\d+)/get_write_latency',         'Umpweb.webapps.cluster.GetWriteLatency',


        '/pd',                              protection_domain.app,  
        '/host',                            host.app,
        '/pool',                            pool.app,
        '/volume',                          volume.app,
        '/access_control',                  access_control.app,
        '/task',                            task.app,
        '/schedule',                        schedule.app,
        '/report',                          report.app,
        '/alert',                           alert.app,
        '/disk',                            disk.app,
        '/customer',                        customer.app,
        '/folder',                          folder.app,
        '/summary',                         summary.app,

        '/license',                         license.app,
        '/tier_list',                       'Umpweb.webapps.tier.TierList',


        '/qos_list',                        'Umpweb.webapps.qos.QOSList',
        '/grid_qos',                        'Umpweb.webapps.qos.GridQOS',
        '/qos_create',                      'Umpweb.webapps.qos.QOSCreate',
        '/qos_update',                      'Umpweb.webapps.qos.QOSUpdate',
        '/qos_delete',                      'Umpweb.webapps.qos.QOSDelete',

        '/recover_qos',                     'Umpweb.webapps.recover.RecoverQos',

        '/snapshot',                        'Umpweb.webapps.snapshot.Snapshot',
        '/snapshot_list',                   'Umpweb.webapps.snapshot.SnapshotList',
        '/grid_snapshot',                   'Umpweb.webapps.snapshot.GridSnapshot',
        '/snapshot_create',                 'Umpweb.webapps.snapshot.SnapshotCreate',
        '/snapshot_update',                 'Umpweb.webapps.snapshot.SnapshotUpdate',
        '/snapshot_rollback',               'Umpweb.webapps.snapshot.SnapshotRollback',
        '/snapshot_delete',                 'Umpweb.webapps.snapshot.SnapshotDelete',
        '/snapshot_clone',                  'Umpweb.webapps.snapshot.SnapshotClone',
        '/snapshot_task',                   'Umpweb.webapps.snapshot.SnapshotTask',
        '/get_snapshots',                   'Umpweb.webapps.snapshot.GetSnapshots',

        '/vgroup_list',		                'Umpweb.webapps.vgroup.VGroupList',
        '/grid_vgroup',                     'Umpweb.webapps.vgroup.GridVGroup',
        '/grid_volume_for_cgsnap',          'Umpweb.webapps.vgroup.GridVolumeCGSnap',
        '/vgroup_create',                   'Umpweb.webapps.vgroup.VGroupCreate',
        '/vgroup_update',                   'Umpweb.webapps.vgroup.VGroupUpdate',
        '/vgroup_delete',                   'Umpweb.webapps.vgroup.VGroupDelete',

        '/cgsnapshot_list',                  'Umpweb.webapps.cgsnapshot.CGSnapshotList',
        '/grid_cgsnapshot',                  'Umpweb.webapps.cgsnapshot.GridCGSnapshot',
        '/cgsnapshot_create',                'Umpweb.webapps.cgsnapshot.CGSnapshotCreate',
        '/cgsnapshot_update',                'Umpweb.webapps.cgsnapshot.CGSnapshotUpdate',
        '/cgsnapshot_rollback',              'Umpweb.webapps.cgsnapshot.CGSnapshotRollback',
        '/cgsnapshot_delete',                'Umpweb.webapps.cgsnapshot.CGSnapshotDelete',
        '/cgsnapshot_clone',                 'Umpweb.webapps.cgsnapshot.CGSnapshotClone',
        '/cgsnapshot_task',                  'Umpweb.webapps.cgsnapshot.CGSnapshotTask',
        
        '/cgroup_snapshot_create',          'Umpweb.webapps.cgroup.CgroupSnapCreate',

        '/grid_snapshot_policy',            'Umpweb.webapps.snapshot.GridSnapshotPolicy',
        '/snapshot_policy_list',            'Umpweb.webapps.snapshot.SnapshotPolicyList',
        '/snapshot_policy_create',          'Umpweb.webapps.snapshot.SnapshotPolicyCreate',
        '/snapshot_policy_delete',          'Umpweb.webapps.snapshot.SnapshotPolicyDelete',
        '/snapshot_consistency',            'Umpweb.webapps.snapshot.SnapshotConsistency',

        '/remotecopy_list',                 'Umpweb.webapps.remotecopy.RemoteCopyList',
        '/remotecopy_settings',             'Umpweb.webapps.remotecopy.RemoteCopySettings',

        '/rrclient_list',                   'Umpweb.webapps.remotecopy.ClientList',
        '/grid_rrclient',                   'Umpweb.webapps.remotecopy.GridClient',
        '/rrclient_create',                 'Umpweb.webapps.remotecopy.ClientCreate',
        '/rrclient_update',                 'Umpweb.webapps.remotecopy.ClientUpdate',
        '/rrclient_delete',                 'Umpweb.webapps.remotecopy.ClientDelete',

        '/rrserver_list',                   'Umpweb.webapps.remotecopy.ServerList',
        '/grid_rrserver',                   'Umpweb.webapps.remotecopy.GridServer',
        '/rrserver_create',                 'Umpweb.webapps.remotecopy.ServerCreate',
        '/rrserver_update',                 'Umpweb.webapps.remotecopy.ServerUpdate',
        '/rrserver_delete',                 'Umpweb.webapps.remotecopy.ServerDelete',


        '/login',                           'Umpweb.index.Login',
        '/logout',                          'Umpweb.index.Logout',
        '/is_login',                        'Umpweb.index.IsLogin',
        '/logout_callback',                 'Umpweb.index.Logout_callback',
        '/home',                            'Umpweb.index.Home',
        '/',                                'Umpweb.index.ValidLogin',
        '',                                 'Umpweb.index.ValidLogin',
        '/valid_session_uuid',              'Umpweb.index.ValidSessionUuid',
        '/favicon.ico',                     'Umpweb.index.Favicon',
        '/switch_language',                 'Umpweb.index.SwitchLanguage',
        '/session_timeout',                 'Umpweb.index.SessionTimeout',

        '/import',                          'Umpweb.webapps.import.Import',
        '/snmp',                            'Umpweb.webapps.snmp.Snmp',
        '/snmp_hosts',                      'Umpweb.webapps.snmp.SnmpHosts',
        '/snmp_host_delete',                'Umpweb.webapps.snmp.SnmpHostDelete',
        '/snmp_host_test',                  'Umpweb.webapps.snmp.SnmpHostTest',


        '/config_export',                   'Umpweb.webapps.import.Export',
        '/config_import',                   'Umpweb.webapps.import.Import',
        '/user_list',                       'Umpweb.webapps.user.UserList',
        '/user_list',                       'Umpweb.webapps.user.UserList',
        '/grid_user',                       'Umpweb.webapps.user.GridUser',
        '/user_password',                   'Umpweb.webapps.user.UserPassword',
        '/user_password1',                  'Umpweb.webapps.user.UserPassword1',
        '/user_create',                     'Umpweb.webapps.user.UserCreate',
        '/user_delete',                     'Umpweb.webapps.user.UserDelete', 
        '/user_update',                     'Umpweb.webapps.user.UserUpdate', 

        '/config',                          'Umpweb.webapps.config.Config',
        '/sysconfig_list',                  'Umpweb.webapps.config.SysconfigList',
        '/sysconfig_update',                'Umpweb.webapps.config.SysconfigUpdate',
        '/grid_sysconfig',                  'Umpweb.webapps.config.GridSysconfig',
        '/set_expired_time',                'Umpweb.webapps.config.SetExpiredTime',

        '/report_ump_list',                 'Umpweb.webapps.umpconfig.SysconfigForUMPList',

        '/sysconfig_for_ump_list',          'Umpweb.webapps.umpconfig.SysconfigForUMPList',
        '/sysconfig_for_ump_create',        'Umpweb.webapps.umpconfig.SysconfigForUMPCreate',
        '/grid_sysconfig_for_ump',          'Umpweb.webapps.umpconfig.GridSysconfigForUMP',
        '/opconfig_update',                 'Umpweb.webapps.umpconfig.SysconfigForUMPUpdate',

        '/oplog_list',                      'Umpweb.webapps.oplog.EventList',
        '/grid_oplog',                      'Umpweb.webapps.oplog.GridEvent',
        '/oplog_delete',                    'Umpweb.webapps.oplog.EventDelete',
        '/oplog_export',                    'Umpweb.webapps.oplog.EventExport',

        '/confirm_modal',                   'Umpweb.webapps.modal.ConfirmModal',
        '/alert_modal',                     'Umpweb.webapps.modal.AlertModal',
        '/upgrade',                         'Umpweb.webapps.version.Upgrade',
        '/about',                           'Umpweb.webapps.version.About',

        )
