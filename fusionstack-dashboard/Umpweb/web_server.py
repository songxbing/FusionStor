#!/usr/bin/env python 
# -*- coding:utf-8 -*-

import sys, os
import getopt
from daemon import Daemon
from Umpweb import web_main
from Umpweb.common import log
from Umpweb import defs


class WebServerDaemon(Daemon):
    def __init__(self, port=None):
        self.pidfile = defs.DASHBOARD_PID_FILE
        self.name = 'ump_dashboard_server'
        self.prog_name = 'web_server'
        super(WebServerDaemon, self).__init__(self.pidfile, name=self.name, prog_name=self.prog_name, port=port)


    def run(self):
        web_main.main(self.port)

    def test(self):
        web_main.main(self.port, is_WLOG=False)

def usage():
    print ("usage:")
    name = 'ump-dashboard-server'
    print (name + " --start")
    print (name + " --stop")
    print (name + " --status")
    print (name + " --test")
    print (name)
    exit(1)

def main():
    op = None 
    ext = None
    port = None
    try:
        opts, args = getopt.getopt(
                sys.argv[1:],
            'h', ['start', 'stop', 'help', 'test','status','port=']
                )
        if not opts:
            usage()
    except getopt.GetoptError, err:
        usage()

    if len(sys.argv) > 2 :
        port = sys.argv[-1]
        assert isinstance(port, int) or port.isdigit(), "port is require Integer"
    webserver_daemon = WebServerDaemon(port)

    for o, a in opts:
        if o in ('--help'):
            usage()
            exit(0)
        elif ( o == '--port'):
            port = a
        elif (o == '--start'):
            op = o
            webserver_daemon.start()
            print ('%s is running' %(webserver_daemon.name))
        elif (o == '--stop'):
            op = o
            res = webserver_daemon.stop()
            if res:
                print ('%s is stopped' % (webserver_daemon.name))
        elif (o == '--test'):
            webserver_daemon.test()
        elif (o == '--status'):
            is_run, pid = webserver_daemon.status()
            if is_run:
                print ('%s is running,pid is %s' %(webserver_daemon.name,pid))
            else:
                print ('%s is stopped' % (webserver_daemon.name))
        else:
            assert False, 'oops, unhandled option: %s, -h for help' % o
            exit(1)

if __name__ == '__main__':
    if (len(sys.argv) == 1):
        usage()
    else:
        main()

