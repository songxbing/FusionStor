# 变更日志

## 20170919 row2_v0.0.15

变更项：
- 提取bitmap cache max到配置文件
- 支持resize(在线扩容)
- 优化lock table，采用free list

解决BUG：
- recover造成性能抖动
- vol md5sum

TODO:
- diskmap
- rebalance
- iscsi write same
- vol load
- vol rm (too slow)
- chunk alloc
- chunk discard
- memory leak

## 20170914 row2_v0.0.11

变更项：
- 性能优化：tier
- 性能优化：mbuffer

解决BUG：
- lich.inspect md5
- alloc chunk时失败：部分成功，部分失败导致元数据不一致
- disable disk probe
- IO error时，不再YASSERT，直接返回错误码
- 修复bitmap cache错误情况下，没有unlock导致timeout

## 20170907 row2_v0.0.9

变更项：
- 性能优化：优化chunk清零
- 性能优化：优化写时首尾页对齐

解决BUG：
- 创建存在的快照，有错误提示
- flat后，清除lich.inspect的source和snap
- IO error时，不再YASSERT，直接返回错误码

## 20170907 row2_v0.0.7

变更项：
- lichbd vol duplicate可用
- 删除卷时，如当前有连接，不允许删除，且提示

解决BUG：
- 改进bitmap并发控制机制，有望解决unlock不配对等问题

## 20170906 row2_v0.0.6

变更项：
- 完成snap delete 

解决BUG：
- 修改__table1_chunk_check

## 20170905

申请提测最新dev_cmnet（commit：961b40eab589d9cfb8a1a2e56e65fc5c2fc8f976）, 变更日志：

- 合并redis cache代码，默认没有启用，但是增加了依赖性：hiredis-devel，每个节点都需按照此开发包：yum install -y hiredis-devel
- 合并卷qos代码，采用新的qos算法
- 合并testing_volsize代码，支持最大卷256TB，变更之前支持的最大卷是32TB；

解决BUG：
- row2 删除卷，空间不回收；
- row2 删除卷时，flush会进入死循环，导致任务超时，产生core；
