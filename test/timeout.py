#!/usr/bin/env python
# -*- coding:utf-8 -*-

import signal
import time

def alarm_handler(signum, frame):
    raise AssertionError

class timeout():
    def __init__(self, second):
        self.second = second

    def __enter__(self):
        signal.signal(signal.SIGALRM, alarm_handler)
        signal.alarm(self.second)
        return self

    def __exit__(self, typ, val, tb):
        signal.alarm(0)

def test():
    count = 0
    with timeout(10):
        try:
            while True:
                print count
                count += 1
                time.sleep(1)
        except AssertionError:
            print 'timeout...'

if __name__ == '__main__':
    test()
