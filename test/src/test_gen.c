#include <sys/poll.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/mman.h>

#define DBG_SUBSYS S_LIBYLIB

#include "ynet_rpc.h"
#include "../../ynet/sock/sock_tcp.h"
#include "dbg.h"

int test_chkid_generator(int chknum)
{
        int ret;
        generator_t gen;
        uint32_t *array, i;

        chkid_generator_init(&gen, chknum, FILE_PROTO_EXTERN_ITEM_COUNT);

        ret = ymalloc((void **)&array, sizeof(*array) * chknum);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        memset(array, 0x0, sizeof(*array) * chknum);

        while (!chkid_generator(&gen, &i)) {
                YASSERT(array[i] == 0);
                array[i] = -1;
        } 

        for (i = 0; i < chknum; i++) {
                YASSERT(array[i] == -1);
        }
        
        yfree((void **)&array);
        
        return 0;
err_ret:
        return ret;
}

int main(int argc, char *argv[])
{
        int chknum;
        
        chknum = atoi(argv[1]);

        DINFO("chknum %u\n", chknum);

        test_chkid_generator(chknum);

        return 0;
}
