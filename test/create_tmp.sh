truncate $2 --size 0
for ((i=0; i<$1; ++i)); do
    line=$(printf "%05d abcdefghijklmnopqrstuvwsyzabcdefghijklmnopqrstuvwsyzabcdefghijklmnopqrstuvwsyzabcdefghijklmnopqrstuvwsyzabcdefghijklmnopqrstuvwsyz\n" $i)
    echo $line >> $2
done
