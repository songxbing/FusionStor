#!/usr/bin/env python2
import os
import sys
import errno

from testsnapshot import testsnapshot

sys.path.insert(0, "../lich/admin/")

from lich import Lich
from utils import _print1, _print2, _get_value, Exp, _dwarn, _dmsg, _derror

def test_create(lich, name, retval):
    _print1("test create...")

    lich.create(name)

    _print1("test create ok")

def test_clone(lich, name, retval):
    _print1("test clone...")

    lich.create(_from, _to)

    _print1("test clone ok")

def test_list_insert(lich, retval):
    _print1("test list insert...")

    for path in lich.list.keys():
        _print1("test list insert at %s ..." %(path))
        lich.list_insert(path)

    _print1("test list insert ok")

def test_list_check(lich, retval):
    _print1("test list check...")

    for path in lich.list.keys():
        _print1("test list check at %s ..." %(path))

        lich.list_check(path)

    _print1("test list check ok")

def main():
    _print1('begin test')
    lich = Lich(20)

    lich.fail_simulate()

    #test_create(lich, "/ns1", 0)
    #test_create(lich, "/ns1", errno.EEXIST)

    try:
        test_list_insert(lich, 0)
        test_list_check(lich, 0)

        testsnapshot(5)
    except Exp, e:
        lich.destroy()
        _derror("test fail:%s, %s ret %d\n" %(e.err, e.out, e.errno))
        raise Exception(e)

    lich.destroy()

if __name__ == '__main__':
    main()
