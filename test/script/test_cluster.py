#!/usr/bin/env python2
import os
import sys
import errno
import uuid
import getopt
import subprocess
import time

admin = os.path.abspath(os.path.split(os.path.realpath(__file__))[0] + '/../lich/admin')
sys.path.insert(0, admin)

from utils import  _get_value, Exp, _dwarn, _dmsg, _derror
from config import Config
from fail import Fail
from node import Node
from cluster import Cluster

config = Config()
disk = config.home + '/disk/'

#os.system('pkill -9 python2')
os.system('pkill -9 lich')
os.system('rm -rf ' + config.home + '/disk/*/*')
os.system('rm -rf ' + config.home + '/etc/cluster.conf')
os.system('rm -rf ' + config.home + '/log/*')

cluster = Cluster()
cluster.init()

#os.system('mkdir -p /dev/shm/lich4/msgctl/')
#os.system('echo 1 > /dev/shm/lich4/msgctl/backtrace')
