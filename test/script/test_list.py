#!/usr/bin/env python2
import os
import sys
import errno
import uuid
import getopt
import subprocess
import time

admin = os.path.abspath(os.path.split(os.path.realpath(__file__))[0] + '/../lich/admin')
sys.path.insert(0, admin)

from utils import  _get_value, Exp, _dwarn, _dmsg, _derror
from config import Config
from fail import Fail

def test_exec(cmd):
    p = subprocess.Popen(cmd, shell=True)
    try:
        ret = p.wait()
        if (ret == 0):
            res = (p.communicate()[0])
            return
        else:
            print ("exec cmd: " + cmd)
            msg = "exec fail:" + cmd
            raise Exp(ret, msg)
    except KeyboardInterrupt as err:
        _dwarn("interupted")
        p.kill()
        exit(errno.EINTR)

class Test_list:
    def __init__(self, parent, count):
        self.config = Config()
        self.bin = self.config.home + "/lich/libexec"
        self.count = count
        self.parent = parent
        self.dict = {}
        for i in range(self.count):
            self.dict[str(uuid.uuid1())] = str(uuid.uuid1())

    def create(self):
        idx = 0
        total = len(self.dict.items())
        for (k, v) in self.dict.items():
            idx += 1
            cmd_create = self.cmd_create(k, v)
            cmd_remove = self.cmd_remove(k, v)

            retry = 0
            while (1):
                print (cmd_create + '[%d/%d]' % (idx, total) + " retry %d" % (retry))
                try:
                    test_exec(cmd_create)
                except Exp, e:
                    if (e.errno == errno.EIO or retry > 300):
                        raise

                    if (e.errno == errno.EEXIST):
                        if (retry == 0):
                            raise Exp(errno.EEXIST, "create fail")
                        else:
                            try:
                                test_exec(cmd_remove)
                            except Exp, e:
                                pass

                    time.sleep(1)
                    retry = retry + 1
                    continue
                break
            
    def check(self):
        idx = 0
        total = len(self.dict.items())
        for (k, v) in self.dict.items():
            idx += 1
            cmd_check = self.cmd_check(k, v)

            retry = 0
            while (1):
                print (cmd_check + '[%d/%d]' % (idx, total) + " retry %d" % (retry))
                try:
                    test_exec(cmd_check)
                except Exp, e:
                    if (e.errno == errno.EIO or retry > 300):
                        raise

                    time.sleep(1)
                    retry = retry + 1
                    continue
                break

    def update(self):
        if (self.updateable == False):
            return

        idx = 0
        total = len(self.dict.items())
        for (k, v) in self.dict.items():
            idx += 1
            self.dict[k] = v + str(uuid.uuid1())

        for (k, v) in self.dict.items():
            cmd_update = self.cmd_update(k, v)

            retry = 0
            while (1):
                print (cmd_update + '[%d/%d]' % (idx, total) + " retry %d" % (retry))
                try:
                    test_exec(cmd_update)
                except Exp, e:
                    if (e.errno == errno.EIO or retry > 300):
                        raise

                    time.sleep(1)
                    retry = retry + 1
                    continue
                break

def test_create(level):
    if (level == 1):
        target = '/' + str(uuid.uuid1())
    elif (level == 2):
        target = '/' + str(uuid.uuid1()) + '/' + str(uuid.uuid1())
    else:
        raise Exp(errno.EINVAL, "not support")

    config = Config()
    cmd = config.home + "/lich/libexec/lich --create %s" % (target);
    try:
        test_exec(cmd)
    except Exp, e:
        if (e.errno == errno.EEXIST):
            pass
        else:
            _derror(e.err)
            exit(e.errno)

    return target


class Dir_test(Test_list):
    def __init__(self, parent, count):
        Test_list.__init__(self, parent, count)
        self.updateable = False
    def cmd_create(self, key, value):
        return "%s/lich --create %s/%s" % (self.bin, self.parent, key)

    def cmd_check(self, key, value):
        return "%s/lich --stat %s/%s > /dev/null " % (self.bin, self.parent, key)

    def cmd_remove(self, key, value):
        return "%s/lich --remove %s/%s" % (self.bin, self.parent, key)

class File_test(Test_list):
    def __init__(self, parent, count):
        Test_list.__init__(self, parent, count)
        self.updateable = True
    def cmd_create(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        return "echo %s > %s && %s/lich --clone :%s %s/%s" % (value, tmp, self.bin, tmp, self.parent, key)

    def cmd_check(self, key, value):
        return "got=`%s/lich --cat %s/%s` && need='%s' && if [[ $need != $got ]]; then echo need: $need got: $got && exit 5; fi" % (self.bin, self.parent, key, value)

    def cmd_remove(self, key, value):
        return "%s/lich --remove %s/%s" % (self.bin, self.parent, key)

    def cmd_update(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        return "echo %s > %s && %s/lich --replace --clone :%s %s/%s" % (value, tmp, self.bin, tmp, self.parent, key)

class Attr_test(Test_list):
    def __init__(self, parent, count):
        Test_list.__init__(self, parent, count)
        self.updateable = True
    def cmd_create(self, key, value):
        return "%s/lich.attr --set %s --value %s %s" % (self.bin, key, value, self.parent)

    def cmd_check(self, key, value):
        return "got=`%s/lich.attr --get %s %s` && need='%s' && if [[ $need != $got ]]; then echo need: $need got: $got && exit 5; fi" % (self.bin, key, self.parent, value)

    def cmd_remove(self, key, value):
        return "%s/lich.attr --remove %s %s" % (self.bin, key, self.parent)

    def cmd_update(self, key, value):
        return "%s/lich.attr --set %s --value %s %s" % (self.bin, key, value, self.parent)

def usage():
    print ("usage:")
    print (sys.argv[0] + " --length [length] --type [dir,file,attr]")

def main():
    try:
        opts, args = getopt.getopt(
                sys.argv[1:], 
                'hv', ['length=', 'type=']
                )
    except getopt.GetoptError, err:
        print str(err)
        usage()
        exit(errno.EINVAL)

    t = 'all'
    for o, a in opts:
        if o in ('--help'):
            usage()
            exit(0)
        elif o in ('-v', '--verbose'):
            verbose = 1
        elif o in ('--length'):
            length = int(a)
        elif o in ('--type'):
            t = a
        else:
            assert False, 'oops, unhandled option: %s, -h for help' % o
            exit(1)

    test = []
    if (t == 'dir'):
        test.append(Dir_test('', length))
        target = test_create(1)
        test.append(Dir_test(target, length))
    elif (t == 'file'):
        target = test_create(2)
        test.append(File_test(target, length))
    elif (t == 'attr'):
        target = test_create(2)
        test.append(Attr_test(target, length))
    elif (t == 'all'):
        test.append(Dir_test('', length))
        target = test_create(1)
        test.append(Dir_test(target, length))

        target = test_create(2)
        test.append(File_test(target, length))

        target = test_create(2)
        test.append(Attr_test(target, length))
    else:
        assert False, 'oops, unhandled option: %s, -h for help' % t
        exit(1)

    fail = Fail(1000)
    fail.start()

    for i in test:
        try:
            i.create()
        except Exp, e:
            fail.stop()
            _derror(e.err)
            os.system('kill -9 ' + str(os.getpid()))

    for i in test:
        try:
            i.update()
        except Exp, e:
            fail.stop()
            _derror(e.err)
            os.system('kill -9 ' + str(os.getpid()))

    for i in test:
        try:
            i.check()
        except Exp, e:
            fail.stop()
            _derror(e.err)
            os.system('kill -9 ' + str(os.getpid()))

    fail.stop()

if __name__ == '__main__':
    if (len(sys.argv) == 1):
        usage()
    else:
        main()
