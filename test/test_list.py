#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import os
import sys
import errno
import uuid
import getopt
import subprocess
import time
import hashlib
import random
import string
from optparse import OptionParser

admin = os.path.abspath(os.path.split(os.path.realpath(__file__))[0] + '/../admin')
sys.path.insert(0, admin)

from utils import  (
    _get_value, Exp, _dwarn, _dmsg, _derror,
    _exec_pipe, _exec_pipe1, _exec_system,
    _get_value, _set_value, _str2dict)
from global_variable import dock_list
from timeout import timeout
from fail import Fail, fail_exit, VALGRIND_CMD, VALGRIND_KEYWORD


config_variables = {}
TIMEOUT = 0
__pool_idx__ = 0

VALGRIND = False
VALGRIND_ADD_LIST = ['lichfs', 'lich.admin', 'lich.snapshot', 'lichbd', 'lich.inspect']

def _get_configdump():
    global dock_list

    if len(dock_list):
        docker = dock_list[0]
    else:
        _derror('invalid dock_list')
        sys.exit(errno.EINVAL)

    cmd = 'docker exec %s /opt/fusionstack/lich/libexec/lich.admin --configdump' % docker

    try:
        (res, err) = _exec_pipe1(cmd.split(' '), 0, False)
    except Exp, e:
        if e.out:
            _derror(str(e.out))
        if e.err:
            _derror(str(e.err))
        exit(e.errno)

    d = _str2dict(res[:-1])
    return d


ec = None

def test_exec(cmd):
    print(cmd)
    p = subprocess.Popen(cmd, shell=True)
    try:
        ret = p.wait()
        if (ret == 0):
            res = (p.communicate()[0])
            return
        else:
            res = (p.communicate()[0])
            print ("exec cmd: " + cmd)
            print ("error: (%s)" % (str(res)))
            msg = "exec fail:\n" + cmd
            raise Exp(ret, msg)
    except KeyboardInterrupt as err:
        _dwarn("interupted")
        p.kill()
        exit(errno.EINTR)


def test_exec_cmd(cmd):
    test_exec(cmd)


def docker_create_pool(name):
    global __pool_idx__

    idx = __pool_idx__ % 256
    __pool_idx__ = __pool_idx__ + 1
    _set_value("/tmp/pool_idx", str(__pool_idx__))
    tmp = '/dev/shm/lich4_test.tmp'
    for i in dock_list:
        s = str(uuid.uuid1())
        cmd = 'mkdir  /opt/fusionstack/data/tmp &>/dev/null'
        _exec_system(dock_cmd(i, cmd))
        cmd = 'bash -c "echo -n \'cluster=xxxx;node=lich_testx;type=new;disk=1;pool=%s;cache=0;cached=0;\' > %s"' % (name, tmp)
        _exec_system(dock_cmd(i, cmd))
        cmd = 'bash -c "dd if=%s of=/opt/fusionstack/data/tmp/%u.file bs=1 seek=1612 &>/dev/null"' % (tmp, idx)
        _exec_system(dock_cmd(i, cmd))
        cmd = 'truncate  /opt/fusionstack/data/tmp/%u.file --size 10G' % (idx)
        _exec_system(dock_cmd(i, cmd))
        cmd = 'ln -s /opt/fusionstack/data/tmp/%u.file /opt/fusionstack/data/disk/disk/%u.disk' % (idx, idx)
        _exec_system(dock_cmd(i, cmd))


def md5(value):
    m = hashlib.md5()
    m.update(value)
    return m.hexdigest()

def cmdcat(cmd_spec, sep='&&'):
    n = len(cmd_spec)
    assert n > 0

    cmd = ''
    for i, v in enumerate(cmd_spec):
        if i == n - 1:
            cmd += v
        else:
            cmd += '%s %s ' % (v, sep)
    return cmd


def is_lich_cmd(cmd):
    lich_cmd = VALGRIND_ADD_LIST
    for x in lich_cmd:
        if x in cmd:
            return True
    return False

def vg_cmd(cmd):
    global VALGRIND
    if VALGRIND and is_lich_cmd(cmd):
        cmd = VALGRIND_CMD + cmd
    return cmd

# docker cmd
def dock_cmd(docker, cmd_spec, sep='&&'):
    if isinstance(cmd_spec, list):
        cmd_spec = ['docker exec %s %s' % (docker, vg_cmd(x)) for x in cmd_spec]
        cmd = cmdcat(cmd_spec, sep)
    elif isinstance(cmd_spec, str):
        cmd = 'docker exec %s %s' % (docker, vg_cmd(cmd_spec))
    else:
        assert 0

    print '== dock_cmd', cmd
    return cmd

# got cmd

# def gcmd00(value1, value2):
#     return "got='%s' && need='%s' && if [ $need != $got ]; then echo need: $need got: $got && exit 5; fi" % (value1, value2)
#
#
# def gcmd01(value, cmd):
#     return "got='%s' && need=`%s` && if [ $need != $got ]; then echo need: $need got: $got && exit 5; fi" % (value, cmd)


def got_cmd10(cmd, value):
    return """got=`%s` && need='%s' && if [ "$need" != "$got" ]; then echo need: "$need" got: "$got" && exit 5; fi""" % (cmd, value)


def got_cmd11(cmd1, cmd2):
    return """got=`%s` && need=`%s` && if [ "$need" != "$got" ]; then echo need: "$need" got: "$got" && exit 5; fi""" % (cmd1, cmd2)


def err_cmd10(cmd, value):
    # res = "Operation not permitted"
    return """`%s` && got='0' || got=$? && need='%s' && if [ "$need" != "$got" ]; then echo need: "$need" got: "$got" && exit 5; fi""" % (cmd, value)


class Test_list:
    def __init__(self, docker, parent, count, uniq, pool):
        self.prefix = "docker exec "+ docker
        self.docker = docker
        self.parent = parent
        self.count = count

        self.dict = {}
        for i in range(self.count):
            self.dict[str(uuid.uuid1())] = str(uuid.uuid1())

        self.uniq = uniq
        self.pool = pool

        self.updateable = False
        self.removeable = False

    def _run_cmds(self, cmd_gen):
        idx = 0
        total = len(self.dict)
        for k, v in self.dict.iteritems():
            idx += 1
            cmd = cmd_gen(k, v)
            print (cmd + '[%d/%d]' % (idx, total))
            try:
                test_exec(cmd)
            except Exp, e:
                raise

    def create(self):
        return self._run_cmds(self.cmd_create)

    def remove(self):
        if (self.removeable == False):
            return

        return self._run_cmds(self.cmd_remove)

    def check(self):
        return self._run_cmds(self.cmd_check)

    def update(self):
        if (self.updateable == False):
            return

        for k, v in self.dict.iteritems():
            self.dict[k] = v + str(uuid.uuid1())

        return self._run_cmds(self.cmd_update)


def test_mkpool(docker, target, repnum):
    docker_create_pool(target)
    with timeout(TIMEOUT):
        try:
            cmd = "/opt/fusionstack/lich/libexec/lichbd pool create %s" % (target);
            test_exec_cmd(dock_cmd(docker, cmd))
        except AssertionError:
            raise Exp(errno.ETIMEDOUT, 'pool create timeout')

    with timeout(TIMEOUT):
        try:
            cmd = "/opt/fusionstack/lich/libexec/lichbd attr set %s lich_system_repnum %d" % (target, repnum)
            test_exec_cmd(dock_cmd(docker, cmd))
        except AssertionError:
            raise Exp(errno.ETIMEDOUT, 'set attr timeout')

    return target


def test_mkvol(docker, target):
    with timeout(TIMEOUT):
        try:
            tmp = '/dev/shm/lich4_test.tmp'
            cmd1 = """bash -c 'echo -n "%s" > %s'""" % ("     ", tmp)
            cmd2 = "lichbd vol import %s %s" % (tmp, target)
            cmd = dock_cmd(docker, [cmd1, cmd2])
            test_exec_cmd(cmd)
        except AssertionError:
            raise Exp(errno.ETIMEDOUT, 'vol import timeout')

    return target


def test_setattr(docker, target, key, value, pool=None):
    with timeout(TIMEOUT):
        try:
            cmd = "/opt/fusionstack/lich/libexec/lichfs --attrset %s %s %s" % (target, key, value);
            if pool is not None:
                cmd += " --pool %s" % (pool);
            test_exec_cmd(dock_cmd(docker, cmd))
        except AssertionError:
            raise Exp(errno.ETIMEDOUT, 'lichfs set attr timeout')

    return target


def test_create_pool(docker, target):
    with timeout(TIMEOUT):
        try:
            cmd = "/opt/fusionstack/lich/libexec/lich.admin --poolcreate %s" % (target);
            test_exec_cmd(dock_cmd(docker, cmd))
        except AssertionError:
            raise Exp(errno.ETIMEDOUT, 'lich.admin pool create timeout')

    return target


class Pool_test(Test_list):
    def __init__(self, docker, parent, count, uniq=None, pool=None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)
        self.removeable = False
        _derror("pool remove disabled")

    def cmd_create(self, key, value):
        docker_create_pool(key)
        _dmsg("create pool %s %s" % (key, value))
        cmd = "lichbd pool create %s" %(key)
        return dock_cmd(self.docker, cmd)

    def cmd_check(self, key, value):
        cmd = "lichbd pool ls | grep %s > /dev/null " % (key)
        return dock_cmd(self.docker, cmd)

    def cmd_remove(self, key, value):
        cmd = "lichbd pool rm %s" % (key)
        return dock_cmd(self.docker, cmd)


class Vol_test(Test_list):
    def __init__(self, docker, parent, count, uniq=None, pool=None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)
        self.updateable = True

    def cmd_create(self, key, value):
        print "+++++++value:%s" %value
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 = """bash -c "echo -n '%s' > %s" """ % (value, tmp)
        cmd2 = "lichbd vol import %s %s/iscsi/%s" % (tmp, self.parent, key)
        return dock_cmd(self.docker, [cmd1, cmd2])

    def cmd_update(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 = """bash -c "echo -n '%s' > %s" """ % (value, tmp)
        cmd2 = "lichbd vol import %s %s/iscsi/%s" % (tmp, self.parent, key)
        return dock_cmd(self.docker, [cmd1, cmd2])

    def cmd_check(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 = 'rm -rf %s' % tmp
        cmd2 = 'lichbd vol export %s/iscsi/%s %s' % (self.parent, key, tmp)
        cmd3 = 'cat %s' % tmp
        return got_cmd10(dock_cmd(self.docker, [cmd1, cmd2, cmd3]), value)

    def cmd_remove(self, key, value):
        cmd = "lichbd rm %s/iscsi/%s" % (self.parent, key)
        return dock_cmd(self.docker, cmd)


#you can adjust it according to your docker virtual machine memory size, volume_proto table2 array need memory
VOL_RW_TEST_MAX_SIZE = 1*1024*1024*1024

class Vol_rw_test(Test_list):
    def __init__(self, docker, parent, count, uniq=None, pool=None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)
        self.updateable = True

    def cmd_create(self, key, value):
        cmd = "lichbd vol create %s/iscsi/%s --size %dB" % (self.parent, key, VOL_RW_TEST_MAX_SIZE)
        return dock_cmd(self.docker, cmd)

    def cmd_update(self, key, value):
        off = uuid.UUID(key).int % (VOL_RW_TEST_MAX_SIZE - len(value))
        cmd = "lichbd write %s/iscsi/%s '%s' -o %d" % (self.parent, key, value, off)
        return dock_cmd(self.docker, cmd)

    def cmd_check(self, key, value):
        off = uuid.UUID(key).int % (VOL_RW_TEST_MAX_SIZE - len(value))
        cmd = "lichbd cat %s/iscsi/%s -o %d -l %d" % (self.parent, key, off, len(value))
        return got_cmd10(dock_cmd(self.docker, cmd), value)

    def cmd_remove(self, key, value):
        cmd = "lichbd rm %s/iscsi/%s" % (self.parent, key)
        return dock_cmd(self.docker, cmd)

class Vol_md5sum_test(Test_list):
    def __init__(self, docker, parent, count, uniq=None, pool=None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)
        self.updateable = True

    def cmd_create(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 = """bash -c "echo -n '%s' > %s" """ % (value, tmp)
        cmd2 = "lichbd vol import %s %s/iscsi/%s" % (tmp, self.parent, key)
        return dock_cmd(self.docker, [cmd1, cmd2])

    def cmd_update(self, key, value):
        return self.cmd_create(key, value)

    def cmd_check(self, key, value):
        if 0:
            tmp = '/dev/shm/lich4_test.tmp'
            cmd1 = "lichbd vol md5sum %s/iscsi/%s | grep '[0-9a-f]\{32\}'" % (self.parent, key)
            cmd2 = 'rm -rf %s' % tmp
            cmd3 = 'lichbd vol export %s/iscsi/%s %s' % (self.parent, key, tmp)
            cmd4 = 'md5sum %s | cut -d " " -f1' % tmp
            return got_cmd11(dock_cmd(self.docker, cmd1), dock_cmd(self.docker, [cmd2, cmd3, cmd4]))
        else:
            cmd = "lichbd vol md5sum %s/iscsi/%s | grep '[0-9a-f]\{32\}'" % (self.parent, key)
            src_md5 = md5(value)
            return got_cmd10(dock_cmd(self.docker, cmd), src_md5)

    def cmd_remove(self, key, value):
        cmd = "lichbd rm %s" % (self.parent)
        return dock_cmd(self.docker, cmd)


class Vol_resize_test(Test_list):
    def __init__(self, docker, parent, count, uniq=None, option=None, pool=None, reduce = None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)
        self.option = option
        self.reduce = reduce

    def cmd_create(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 = """bash -c "echo -n '%s' > %s" """ % (value, tmp)
        cmd2 = "lichbd vol import %s %s/iscsi/%s" % (tmp, self.parent, key)
        cmd3 = "lichbd vol info %s/iscsi/%s" % (self.parent, key)
        cmd4 = "lichbd vol resize %s/iscsi/%s -s 1g" %(self.parent, key)
        cmd5 = "lichbd vol info %s/iscsi/%s" % (self.parent, key)
        return dock_cmd(self.docker, [cmd1, cmd2, cmd3, cmd4, cmd5])

    def cmd_check(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 ='rm -rf %s' %(tmp)
        cmd2 = "lichbd vol export %s/iscsi/%s %s" % (self.parent, key, tmp)
        cmd3 = "cat %s" %(tmp)
        cmd4 = "lichbd vol resize %s/iscsi/%s -s 512b " %(self.parent, key)

        if self.reduce:
            s = err_cmd10(dock_cmd(self.docker, [cmd4]), '1')
        else:
            s = got_cmd10(dock_cmd(self.docker, [cmd1, cmd2,cmd3]), value)
        return s

    def cmd_remove(self, key, value):
        cmd = "lichbd rm %s/iscsi/%s" % (self.parent, key)
        return dock_cmd(self.docker, cmd)

    def cmd_update(self, key, value):
        return self.cmd_create(key, value)


class Vol_cp_dup_mig_test(Test_list):
    def __init__(self, docker, parent, count, uniq=None, option=None, repeat = None, pool=None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)
        self.option = option
        self.repeat = repeat
        #self.updateable = True
        self.removeable = True

    def cmd_create(self, key, value):
        print "++++++++++opt:%s" % self.option
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 = """bash -c "echo -n '%s' > %s" """ % (value, tmp)
        cmd2 = "lichbd vol import %s %s/iscsi/%s" % (tmp, self.parent, key)
        cmd3 = "lichbd vol %s %s/iscsi/%s %s/iscsi/%s-1" % (self.option, self.parent, key, self.parent, key)
        cmd4 = "lichbd vol ls %s/iscsi" % self.parent
        return dock_cmd(self.docker, [cmd1, cmd2, cmd3, cmd4])

    def cmd_update(self, key, value):
        return self.cmd_create(key, value)

    def cmd_check(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 ='rm -rf %s' %(tmp)
        cmd2 = "lichbd vol export %s/iscsi/%s-1  %s" % (self.parent, key, tmp)
        cmd3 = "cat %s" %(tmp)
        cmd4 = "lichbd vol %s %s/iscsi/%s-1 %s/iscsi/%s-1" % (self.option, self.parent, key, self.parent, key)
        if self.repeat:
            s = err_cmd10(dock_cmd(self.docker, cmd4), '17')
        else:
            s = got_cmd10(dock_cmd(self.docker, [cmd1, cmd2, cmd3]), value)
        return s

    def cmd_remove(self, key, value):
        cmd = "lichbd vol rm %s/iscsi/%s-1" %(self.parent, key)
        return dock_cmd(self.docker, cmd)

class Vol_cp_dup_mig_test_1(Test_list):
    def __init__(self, docker, parent, count, uniq=None, option=None, pool=None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)
        self.option = option
        self.removeable = True

    def cmd_create(self, key, value):
        print "++++++++++opt:%s" % self.option
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 = """bash -c "echo -n '%s' > %s" """ % (value, tmp)
        cmd2 = "lichbd vol import %s %s/iscsi/%s" % (tmp, self.parent, key)
        cmd3 = "lichbd vol %s %s/iscsi/%s %s/nfs/%s" % (self.option, self.parent, key, self.parent, key)
        cmd4 = "lichbd vol ls %s/nfs" % (self.parent)
        return dock_cmd(self.docker, [cmd1, cmd2, cmd3, cmd4])

    def cmd_check(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 ='rm -rf %s' %(tmp)
        cmd2 = "lichbd vol export %s/nfs/%s  %s" % (self.parent, key, tmp)
        cmd3 = "cat %s" %(tmp)
        return got_cmd10(dock_cmd(self.docker, [cmd1, cmd2, cmd3]), value)

    def cmd_update(self, key, value):
        return self.cmd_create(key, value)

    def cmd_remove(self, key, value):
        cmd = "lichbd vol rm %s/nfs/%s" %(self.parent, key)
        return dock_cmd(self.docker, cmd)


class Vol_cp_dup_mig_test_2(Test_list):
    def __init__(self, docker, parent, count, uniq=None, option=None, error = None, pool=None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)
        self.option = option
        self.error = error
        self.removeable = True

    def cmd_create(self, key, value):
        print "value:%s" %value
        print "++++++++++opt:%s" % self.option
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 = """bash -c "echo -n '%s' > %s" """ % (value, tmp)
        cmd2 = "lichbd vol import %s volume-%s/iscsi/%s" % (tmp, self.uniq, self.uniq)
        cmd3 = "lichbd vol %s volume-%s/iscsi/%s volume-1-%s/iscsi/%s" % (self.option, self.uniq, self.uniq, self.uniq, key)
        cmd4 = "lichbd vol ls volume-1-%s/iscsi" % (self.uniq)
        return dock_cmd(self.docker, [cmd1, cmd2, cmd3, cmd4])

    def cmd_update(self, key, value):
        return self.cmd_create(key, value)

    def cmd_check(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 = "lichbd snap create volume-1-%s/iscsi/%s@snap-migrate-test" %(self.uniq, key)
        cmd2 = "lichbd vol %s volume-1-%s/iscsi/%s %s/iscsi/%s" % (self.option, self.uniq, key, self.parent, key)
        cmd3 ='rm -rf %s' %(tmp)
        cmd4 = "lichbd vol export volume-1-%s/iscsi/%s %s" % (self.uniq, key, tmp)
        cmd5 = "cat %s" %(tmp)
        if self.error:
            cmd = err_cmd10(dock_cmd(self.docker, [cmd1, cmd2]), '1')
            return cmd
        else:
            return got_cmd10(dock_cmd(self.docker, [cmd3, cmd4, cmd5]), value)

    def cmd_remove(self, key, value):
        if self.error:
            cmd1 = "lichbd snap rm volume-1-%s/iscsi/%s@snap-migrate-test" %(self.uniq, key)
            cmd2 = "lichbd vol rm volume-1-%s/iscsi/%s" %(self.uniq, key)
            return dock_cmd(self.docker, [cmd1, cmd2])
        else:
            cmd = "lichbd vol rm volume-1-%s/iscsi/%s" %(self.uniq, key)
        return dock_cmd(self.docker, cmd)


class Allocate_test(Test_list):
    def __init__(self, docker, parent, count, uniq=None, pool=None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)
        self.updateable = False

    def cmd_create(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 = "lichbd vol create %s/iscsi/%s --size 32M" % (self.parent, key)
        cmd2 = "lichbd vol allocate %s/iscsi/%s -p 1000" % (self.parent, key)
        return dock_cmd(self.docker, [cmd1, cmd2])

    def cmd_check(self, key, value):
        cmd1 = "%s lichbd vol info %s/iscsi/%s -C allocate | grep chknum | awk -F': ' '{print $2}'" % (self.prefix, self.parent, key)
        cmd2 = "%s lichbd vol info %s/iscsi/%s -C full | grep allocate | awk -F': ' '{print $2}'" % (self.prefix, self.parent, key)
        return got_cmd11(cmd1, cmd2)

    def cmd_remove(self, key, value):
        cmd = "lichbd rm %s/iscsi/%s" % (self.parent, key)
        return dock_cmd(self.docker, cmd)

    def cmd_update(self, key, value):
        return ""


class Attr_test(Test_list):
    def __init__(self, docker, parent, count, uniq=None, pool=None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)
        self.updateable = True

    def cmd_create(self, key, value):
        cmd = "lichbd attr set %s %s %s" % (self.parent, key, value)
        return dock_cmd(self.docker, cmd)

    def cmd_check(self, key, value):
        cmd = "lichbd attr get %s %s" % (self.parent, key)
        return got_cmd10(dock_cmd(self.docker, cmd), value)

    def cmd_remove(self, key, value):
        cmd = "lichbd attr rm %s %s" % (self.parent, key)
        return dock_cmd(self.docker, cmd)

    def cmd_update(self, key, value):
        cmd = "lichbd attr set %s %s %s" % (self.parent, key, value)
        return dock_cmd(self.docker, cmd)


class Snap_create_test(Test_list):
    def __init__(self, docker, parent, count, uniq=None, pool=None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)

    def cmd_create(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 = """bash -c 'echo -n %s > %s' """ % (value, tmp)
        cmd2 = "lichbd vol import %s %s" % (tmp, self.parent)
        cmd3 = "lichbd snap create %s@%s" % (self.parent, key)
        return dock_cmd(self.docker, [cmd1, cmd2, cmd3])

    def cmd_check(self, key, value):
        cmd = "lichbd snap cat %s@%s" % (self.parent, key)
        return got_cmd10(dock_cmd(self.docker, cmd), value)

    def cmd_remove(self, key, value):
        cmd = "lichbd snap rm %s@%s" % (self.parent, key)
        return dock_cmd(self.docker, cmd)


class Snap_protect_test(Test_list):
    def __init__(self, docker, parent, count, uniq=None, pool=None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)
        self.removeable = True

    def cmd_create(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 = """bash -c 'echo -n %s > %s' """ % (value, tmp)
        cmd2 = "lichbd vol import %s %s" % (tmp, self.parent)
        cmd3 = "lichbd snap create %s@%s" % (self.parent, key)
        return dock_cmd(self.docker, [cmd1, cmd2, cmd3])

    def cmd_check(self, key, value):
        cmd1 = "lichbd snap protect %s@%s" % (self.parent, key)
        cmd2 = "lichbd snap rm %s@%s" %(self.parent, key)
        return err_cmd10(dock_cmd(self.docker, [cmd1, cmd2]), '1')

    def cmd_remove(self, key, value):
        cmd1 = "lichbd snap unprotect %s@%s" %(self.parent, key)
        cmd2 = "lichbd snap rm %s@%s" %(self.parent, key)
        return dock_cmd(self.docker, [cmd1, cmd2])


class Snap_rollback_test(Test_list):
    def __init__(self, docker, parent, count, reverse=True, uniq=None, pool=None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)
        self.reverse = reverse

    def check(self):
        with timeout(TIMEOUT):
            try:
                cmd = "%s lichbd snap ls %s" % (self.prefix, self.parent)
                res = _exec_pipe(cmd.split(' '), retry = 10, timeout=60)
            except AssertionError:
                raise Exp(errno.ETIMEDOUT, 'lichbd snap ls timeout')
        '''
        skip header [snapshot] [parent] ...
        '''
        lst = res.split('\n')[1:-1]
        if self.reverse:
            lst.reverse()

        idx = 0
        total = len(lst)
        for k in lst:
            idx += 1
            cmd_check = self.cmd_check(k, self.dict[k])

            print (cmd_check + ' [%d/%d]' % (idx, total))
            try:
                test_exec(cmd_check)
            except Exp, e:
                raise

    def cmd_create(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 = """bash -c 'echo -n %s > %s'""" %(value, tmp)
        cmd2 = "lichbd vol import %s %s" % (tmp, self.parent)
        cmd3 = "lichbd snap create %s@%s" % (self.parent, key)
        return dock_cmd(self.docker, [cmd1, cmd2 ,cmd3])

    def cmd_check(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 = "%s lichbd snap rollback %s@%s" % (self.prefix, self.parent, key)
        cmd = "%s lichbd vol export %s %s" % (self.prefix, self.parent, tmp)
        cmd += " && %s cat %s" % (self.prefix, tmp)
        return cmdcat([cmd1, got_cmd10(cmd, value)])

    def cmd_remove(self, key, value):
        cmd = "%s lichbd snap rm %s@%s" % (self.prefix, self.parent, key)
        return cmd


class Snap_remove_test(Test_list):
    def __init__(self, docker, parent, count, uniq=None, pool=None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)

    def cmd_create(self, key, value):
        cmd1 = "%s/lichfs --write %s %s" % (self.prefix, self.parent, value)
        cmd2 = "%s/lich.snapshot --create %s@%s" % (self.prefix, self.parent, key)
        if self.pool is not None:
            cmd1 += " --pool %s" % (self.pool)
            cmd2 += " --pool %s" % (self.pool)
        return "%s && %s" % (cmd1, cmd2)

    def cmd_check(self, key, value):
        cmd1 = "%s/lich.snapshot --cat %s@%s" % (self.prefix, self.parent, key)
        cmd2 = "%s/lich.snapshot --remove %s@%s" % (self.prefix, self.parent, key)
        if self.pool is not None:
            cmd1 += " --pool %s" % (self.pool)
            cmd2 += " --pool %s" % (self.pool)
        return cmdcat([got_cmd10(cmd1, value), cmd2])

    def cmd_remove(self, key, value):
        return ""


class Snap_clone_test(Test_list):
    def __init__(self, docker, parent, count, uniq=None, pool=None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)
        #self.value = str(uuid.uuid1());
        #self.snap = str(uuid.uuid1());

    def cmd_create(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        cmd1 = """bash -c 'echo -n %s > %s'""" % (value, tmp)
        cmd2 = "lichbd vol import %s %s" % (tmp, self.parent)
        cmd3 = "lichbd snap create %s@%s" % (self.parent, key)
        cmd4 = "lichbd snap clone %s@%s clone-%s/iscsi/%s" % (self.parent, key, self.uniq, key)
        return dock_cmd(self.docker, [cmd1, cmd2, cmd3, cmd4])

    def cmd_check(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        cmd = "%s lichbd vol export clone-%s/iscsi/%s %s" % (self.prefix, self.uniq, key, tmp)
        cmd += " && %s cat %s" % (self.prefix, tmp)
        return got_cmd10(cmd, value)

    def cmd_remove(self, key, value):
        cmd1 = "lichbd snap rm %s@%s" % (self.parent, key)
        cmd2 = "lichbd vol rm  clone-%s/iscsi/%s" % (self.uniq, key)
        return docker_cmd(docker, [cmd1, cmd2])


class Snap_flat_test(Test_list):
    def __init__(self, docker, parent, count, uniq=None, pool=None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)

    def cmd_create(self, key, value):
        tmp = '/dev/shm/lich4_test.tmp'
        cmd = """%s bash -c 'echo -n %s > %s'""" % (self.prefix, value, tmp)
        cmd += " && %s lichbd vol import %s %s" % (self.prefix, tmp, self.parent)
        cmd += " && %s lichbd snap create %s@%s" % (self.prefix, self.parent, key)
        cmd += " && %s lichbd snap clone %s@%s flat-%s/iscsi/%s" % (self.prefix, self.parent, key, self.uniq, key)
        return cmd

    def cmd_check(self, key, value):
        cmd1 = "%s lichbd snap flat flat-%s/iscsi/%s" % (self.prefix, self.uniq, key)
        tmp = '/dev/shm/lich4_test.tmp'
        cmd = "%s lichbd vol export flat-%s/iscsi/%s %s" % (self.prefix, self.uniq, key, tmp)
        cmd += " && %s cat %s" % (self.prefix, tmp)
        return cmdcat([cmd1, got_cmd10(cmd, value)])

    def cmd_remove(self, key, value):
        cmd = "%s lichbd snap rm %s@%s" % (self.prefix, self.parent, key)
        cmd += " && %s lichbd vol rm  clone-%s/iscsi/%s" % (self.prefix, self.uniq, key)
        return cmd


class Iscsi_test(Test_list):
    def __init__(self, docker, parent, count, uniq=None, pool=None):
        Test_list.__init__(self, docker, parent, count, uniq, pool)
        self.iqn = config_variables['iscsi.iqn']
        self.vol_name = "testiscsi"
        self.target_name = "%s:%s.%s" % (self.iqn, self.parent, self.vol_name)
        self.vol_size = 32 * 1024 * 1024
        self.file_size = 1024 * 1024
        self.server_ip = self._get_server_ip()

    def _get_server_ip(self):
        ip = _exec_pipe(['docker', 'inspect', "--format='{{.NetworkSettings.IPAddress}}'", self.docker], 0, False)[:-1]
        return ip.strip('\'')

    def _get_dev_name(self):
        dev_list = []
        path = '/dev/disk/by-path'
        found = 0

        try:
            dev_list = os.listdir(path)
        except Exception as e:
            raise Exp(e.errno, "list %s failed" % (path))

        file_count = len(dev_list)
        if not file_count:
            raise Exp(errno.ENOENT, "empty directory")

        for dev in dev_list:
            if self.target_name in dev:
                found = 1
                dev_name = os.path.join(path, dev)

        if found:
            return dev_name
        return None

    def _write_file(self, data, offset):
        hash1 = hashlib.md5()
        writehash = 0

        with open(self.dev_name, "w") as fp:
            fp.seek(offset, 0)
            fp.write(data)
            fp.flush()
            hash1.update(data)

            writehash = hash1.hexdigest()
        return writehash

    def _read_file(self, offset):
        hash2 = hashlib.md5()
        readhash = 0

        with open(self.dev_name, "r") as fp:
            left = self.file_size
            step = 1024*1024
            fp.seek(offset, 0)

            while left:
                toread = left if left < step else step
                line = fp.read(toread)
                hash2.update(line)
                left -= len(line)

            readhash = hash2.hexdigest()
        return readhash

    def create(self):
        cmd = "%s lichbd vol create %s/iscsi/%s --size 32M" % (self.prefix, self.parent, self.vol_name)
        _exec_pipe(cmd.split(' '), retry = 10, timeout=TIMEOUT)

        cmd = "iscsiadm -m discovery -t sendtargets -p %s" % (self.server_ip)
        _exec_pipe(cmd.split(' '), retry = 10, timeout=TIMEOUT)

        cmd = "iscsiadm -m node -T %s -l" % (self.target_name)
        _exec_pipe(cmd.split(' '), retry = 10, timeout=TIMEOUT)

        self.dev_name = self._get_dev_name()

    def check(self):
        data = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(self.file_size))
        writehash = 0
        readhash = 0

        if self.dev_name is None:
            raise Exp(errno.ENOENT, "No Such device")

        for i in range(self.count):
            offset = random.randint(0, self.vol_size - self.file_size)
            with timeout(TIMEOUT):
                try:
                    writehash = self._write_file(data, offset)
                except AssertionError:
                    raise Exp(errno.ETIME, "write timeout")

            with timeout(TIMEOUT):
                try:
                    readhash = self._read_file(offset)
                except AssertionError:
                    raise Exp(errno.ETIME, "read timeout")

            if writehash == readhash:
                print "Data Is Consistent"
                return
            else:
                print "Data Is Inconsistent"
                raise Exp(errno.EIO, "Data Inconsistency")

    def remove(self):
        cmd = "iscsiadm -m node --logoutall=all"
        _exec_pipe(cmd.split(' '), retry = 10, timeout=TIMEOUT)

        cmd = "iscsiadm -m node -o delete"
        _exec_pipe(cmd.split(' '), retry = 10, timeout=TIMEOUT)

        cmd = "%s lichbd vol rm %s/iscsi/%s" % (self.prefix, self.parent, self.vol_name)
        _exec_pipe(cmd.split(' '), retry = 10, timeout=TIMEOUT)

        cmd = "%s lichbd pool rm %s" % (self.prefix, self.parent)
        _exec_pipe(cmd.split(' '), retry = 10, timeout=TIMEOUT)

# DRIVER
#################################################################


def test_pool(test, docker, length, uniq=None, pool=None):
    test.append(Pool_test(docker, "", length, uniq, pool))


def test_vol(test, docker, length, repnum, uniq=None, pool=None):
    target = test_mkpool(docker, "volume-%s" % uniq, repnum)

    test.append(Vol_test(docker, target, length))
    test.append(Vol_rw_test(docker, target, length))

    # test.append(Vol_resize_test(docker, target, length, uniq, reduce=False))

    # test.append(Vol_resize_test(docker, target, length, uniq, reduce=True))

    opt = ['copy', 'duplicate', 'migrate']

    # test.append(Vol_cp_dup_mig_test(docker, target, length, uniq, opt[0], repeat = False))
    # test.append(Vol_cp_dup_mig_test(docker, target, length, uniq, opt[1], repeat = False))
    # test.append(Vol_cp_dup_mig_test(docker, target, length, uniq, opt[2], repeat = False))

    #同名迁移复制
    #test.append(Vol_cp_dup_mig_test(docker, target, length, uniq, opt[0], repeat = True))
    # test.append(Vol_cp_dup_mig_test(docker, target, length, uniq, opt[1], repeat = True))
    # test.append(Vol_cp_dup_mig_test(docker, target, length, uniq, opt[2], repeat = True))

    #跨pool下目录
    # test.append(Vol_cp_dup_mig_test_1(docker, target, length, uniq, opt[0]))
    # test.append(Vol_cp_dup_mig_test_1(docker, target, length, uniq, opt[1]))
    # test.append(Vol_cp_dup_mig_test_1(docker, target, length, uniq, opt[2]))

    #跨pool
    # target = test_mkpool(docker, "volume-1-%s" % uniq, repnum)

    # test.append(Vol_cp_dup_mig_test_2(docker, target, length, uniq, opt[0], error = False))

    # test.append(Vol_cp_dup_mig_test_2(docker, target, length, uniq, opt[1], error = False))

    # test.append(Vol_cp_dup_mig_test_2(docker, target, length, uniq, opt[2], error = False))

    # target = test_mkpool(docker, "volume-2-%s" % uniq, repnum)

    # test.append(Vol_cp_dup_mig_test_2(docker, target, length, uniq, opt[2], error = True))

    # test.append(Vol_md5sum_test(docker, target, length))


def test_allocate(test, docker, length, repnum, uniq=None, pool=None):
    target = test_mkpool(docker, "allocate-%s" % uniq, repnum)
    test.append(Allocate_test(docker, target, length))


def test_attr(test, docker, length, repnum, uniq = None, pool=None):
    target = test_mkpool(docker, "attr-%s" % uniq, repnum)
    test.append(Attr_test(docker, target, length, uniq, pool))

    target = test_mkvol(docker, "attr-%s/iscsi/vol" % (uniq))
    test.append(Attr_test(docker, target, length, uniq, pool))


def test_snap(test, docker, length, repnum, uniq=None, pool=None):
    target = test_mkpool(docker, "snap-%s" % uniq, repnum)

    target = test_mkvol(docker, "snap-%s/iscsi/test-snap-create" % (uniq))
    test.append(Snap_create_test(docker, target, length, uniq, pool))

    #test.append(Snap_protect_test(docker, target, length, uniq, pool))

    target = test_mkvol(docker, "snap-%s/iscsi/testsnap-rollback" % (uniq))
    test.append(Snap_rollback_test(docker, target, length, True, uniq, pool))

    target = test_mkvol(docker, "snap-%s/iscsi/test-snap-rollbackorder" % (uniq))
    test.append(Snap_rollback_test(docker, target, length, False, uniq, pool))

    target = test_mkpool(docker, "clone-%s" % uniq, repnum)
    target = test_mkvol(docker, "snap-%s/iscsi/test-clone" % (uniq))
    test.append(Snap_clone_test(docker, target, length, uniq, pool))

    target = test_mkpool(docker, "flat-%s" % uniq, repnum)
    target = test_mkvol(docker, "snap-%s/iscsi/test-flat" % (uniq))
    test.append(Snap_flat_test(docker, target, length, uniq, pool))


def test_iscsi(test, docker, length, repnum, uniq=None, pool=None):
    target = test_mkpool(docker, "iscsi-%s" % uniq, repnum)
    test.append(Iscsi_test(docker, target, length, uniq, pool))


def test_all(test, docker, length, repnum, uniq, pool=None):
    #test_allocate(test, docker, length, repnum, uniq, pool)
    test_pool(test, docker, length, uniq, pool)
    test_vol(test, docker, length, repnum, uniq, pool)
    test_attr(test, docker, length, repnum, uniq, pool)
    test_snap(test, docker, length, repnum, uniq, pool)
    #test_iscsi(test, docker, length, repnum, uniq, pool)


def main():
    parser = OptionParser()
    parser.add_option('-t', '--type',    action='store', type='string', default='all', dest='type',    help='type')
    parser.add_option('-l', '--length',  action='store', type='int',    default=5,     dest='length',  help='length')
    parser.add_option('-v', '--verbose', action='store_true',           default=False, dest='verbose', help='verbose')
    parser.add_option('-g', '--valgrind',  action='store', type='int',    default=0,     dest='valgrind',  help='valgrind')

    (options, args) = parser.parse_args()
    # print options, args

    t = options.type
    length = options.length
    global VALGRIND
    VALGRIND = options.valgrind

    if len(dock_list):
        docker = dock_list[0]
    else:
        print 'invalid dock_list'
        sys.exit(errno.EINVAL)

    #test_create_pool(docker, "default");

    global __pool_idx__
    __pool_idx__ = int(_get_value("/tmp/pool_idx"))
    _dmsg("pool idx %u\n" % ( __pool_idx__))

    test = []
    if (t == 'dir'):
        uniq = str(uuid.uuid1())
        test_pool(test, docker, length, 2, uniq)

        uniq = str(uuid.uuid1())
        test_pool(test, docker, length, 3, uniq, "pool1")

    elif (t == 'file'):
        uniq = str(uuid.uuid1())
        test_vol(test, docker, length, 2, uniq)

        uniq = str(uuid.uuid1())
        test_vol(test, docker, length, 3, uniq, "pool1")

    elif (t == 'attr'):
        uniq = str(uuid.uuid1())
        test_attr(test, docker, length, 2, uniq)
        uniq = str(uuid.uuid1())
        test_attr(test, docker, length, 3, uniq, "pool1")

    elif (t == 'snap'):
        uniq = str(uuid.uuid1())
        test_snap(test, docker, length, 2, uniq)
        uniq = str(uuid.uuid1())
        test_snap(test, docker, length, 3, uniq, "pool1")

    elif (t == 'iscsi'):
        uniq = str(uuid.uuid1())
        test_iscsi(test, docker, length, 2, uniq, "pool1")

    elif (t == 'all'):
        uniq = str(uuid.uuid1())
        test_all(test, docker, length, 2, uniq)

        uniq = str(uuid.uuid1())
        test_all(test, docker, length, 3, uniq)
        #test_all(test, docker, length, uniq, "pool1")

    else:
        assert False, 'oops, unhandled option: %s, -h for help' % t
        exit(1)

#    fail = Fail(1000)
#    fail.start()

    for m in ['create', 'update', 'check', 'remove']:
        for i in test:
            with timeout(TIMEOUT):
                try:
                    getattr(i, m)()
                    # i.create()
                except AssertionError:
                    fail_exit("%s %s fail"% (i.parent, m))
                except Exp, e:
                    fail_exit(e.err)

    #fail.stop()

def usage():
    print "use -h, --help to show help message"

if __name__ == '__main__':
    config_variables = _get_configdump()
    #TIMEOUT = int(config_variables['globals.rpc_timeout']) * 4
    TIMEOUT = 1000

    if (len(sys.argv) == 1):
        usage()
    else:
        main()
