#!/bin/bash

usage()
{
    echo "Usage: $0 [OPTIONS]"
    echo "-i install [path]"
    echo "-e remove [path]"

    exit 1
}

install()
{
    DIST=$1/manage
    LIC=$DIST/license

    rm -rf $DIST
    mkdir -p $LIC

    gcc -w  -o  license_dispatch license_dispatch.c license.c -lcrypto -lssl -lm -D_GNU_SOURCE -D__USE_XOPEN
    gcc -w  -o  license_check license_check.c license.c -lcrypto -lssl -lm -D_GNU_SOURCE -D__USE_XOPEN

    cp license_dispatch $DIST
    rm -rf license_dispatch
    cp *.py $DIST
    chmod a+x $DIST/*
}

remove()
{
    echo "do nothing"
}

if [ $# -lt 1 ]
then
    usage
fi

while getopts ieh options
do
    case $options in
        i)
        echo "install scripts $2"
        install $2
        ;;
        e)
        echo "remove scripts"
        remove
        ;;
        h)
        usage
        ;;
        \?)
        usage
        ;;
    esac
done
