#ifndef LICENSE_H
#define LICENSE_H

#define likely(x)       __builtin_expect(!!(x), 1)
#define unlikely(x)     __builtin_expect(!!(x), 0)

#define FREE_LICENSE (60 * 60 * 24 * 30 * 3)
#define MAX_BUF_LEN             (1024 * 64)
#define MAX_LINE_LEN            (1024 * 2)
#define MAX_PATH_LEN            (1024 * 4)
#define MAX_NAME_LEN            (256)
#define MAX_INFO_LEN            (512)
#define MAXSIZE 256

#define LICENSE_RECYCLE "dddee400ae8c4ca3b76e4a9a4eecaf3d"
#define LICENSE_PATH "/opt/fusionstack/license"
#define TMP_PATH "/tmp/host_sniffer"
#define DIS_PATH "/tmp"

#define DISPATCH_TIME  (7 * 24 * 60 * 60)
#define MONTH_SEC  (30 * 24 * 60 * 60)

#include "list.h"

# define GOTO(label, ret)                                                       \
        do {                                                                    \
                /*printf("%s:%d %s Process leaving via %s (%d)%s\n",       \
                        __FILE__, __LINE__, __FUNCTION__,                       \
                        #label, ret, strerror(ret));*/                            \
                goto label;                                                     \
        } while (0)

#define EXIT(__ret__)                                                           \
        do {   									\
                exit(__ret__);                                                  \
        } while (0)

struct list_node {
        struct list_head hook;
        char fmd5[33];
};

struct Grant {
       int exist_vaild_file;
       int capacity_grant_count;
};

typedef enum {
        LIMIT_TIME_SEC,
        LIMIT_TIME_DAY,
        LIMIT_TIME_MON,
} limit_time_t;

static inline int list_exists(struct list_head *head, char *fmd5)
{
        struct list_node *node;

        list_for_each_entry(node, head, hook) {
                if (!strcmp(node->fmd5, fmd5))
                    return 1;
        }

        return 0;
}

extern int license_decrypt_data(const char *encrypt, char *data, int len);
extern int license_encrypt_data(const char *data, char *encrypt, int len);

extern int license_encrypt(int fd, const char *mac_file, int month);
extern int license_encrypt_m(const char *host_name, const char *mac_file, const char *odir, limit_time_t time_type, int limit_time, int capacity);
extern int license_decrypt(int fd, time_t *time);
extern int license_get_capacity(char *path, long *capacity);
extern int license_get_time(char *path, time_t *time);
extern int license_set_ctime(char *path, time_t ctime);
extern int license_get_ctime(char *path, time_t *ctime);
extern int license_check(const char *path, long *permit_capacity, time_t *limit);
extern int license_sniffer(const char *path);

#endif /* LICENSE_H */
