#ifndef __HOSTMAP_H
#define __HOSTMAP_H

#include <stdbool.h>
#include "chunk.h"
#include "lichstor.h"

int hostmap_create(const char *initiator, const char *pool, const char *path);
int hostmap_rm(const char *initiator, const char *pool, const char *path);
int hostmap_rm_by_fileid(const char *initiator, const char *fileid);
int unmap_hosts_by_fileid(const char *fileid);
int hostmap_is_mapping(const char *initiator, const fileid_t *fileid, int *is_mapping);

int hostmap_rm_all(const char *initiator);
int hostmap_list(const char *initiator, int output_format);
int hostmap_get(const char *initiator, char *user, char *pass);

int list_volume_mapped_hosts(const char *volume);
int get_volume_mapped_hosts(const char *fileid, char *hosts[], int *count);
int hostmap_find_volume(const char *initiator, const char *fileid, int *success);
int hostmap_inherit(const char *fpool, const char *fpath,
                const char *tpool, const char *tpath);


#endif
