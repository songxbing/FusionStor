#ifndef __OFFLINE_MSG__
#define __OFFLINE_MSG__

int offline_msg_create(const nid_t *nid, volid_t *volid);
int offline_msg_rm(const nid_t *nid, volid_t *volid);
int offline_msg_iterator(const nid_t *nid, func_int1_t func, void *arg);


#endif
