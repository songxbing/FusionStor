#ifndef __CLUSTER_RPC_H__
#define __CLUSTER_RPC_H__

#include "sysy_conf.h"

int cluster_rpc_analysis(const nid_t *nid, const str_t *key, str_t *value);
int cluster_rpc_ping(const nid_t *nid, int timeout);
int cluster_rpc_getinfo(const nid_t *nid, const nid_t *peer, ynet_net_info_t *info);

#endif
