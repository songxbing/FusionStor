#ifndef __NODEID__
#define __NODEID__

int nodeid_init();
int nodeid_newid(int16_t *id, const char *name);
int nodeid_drop(int16_t id);
int nodeid_used(int16_t id);
void nodeid_destroy();

#endif
