#ifndef __DISKMAP_H__
#define __DISKMAP_H__

#include "config.h"
#include "cluster.h"

typedef struct {
        char pool[MAX_NAME_LEN];
        int writeable_rack;
        int writeable_node;
} brief_info_t;

#if 0
static inline int chkinfo_rack_check(const chkinfo_t *chkinfo)
{
        int ret, rack_count, i;
        char rack_array[MAX_NAME_LEN][LICH_REPLICA_MAX];
        
        rack_count = 0;
        for(i = 0; i < chkinfo->repnum; i++) {
                ret = __nid2_rackadd(rack_array, &rack_count, &chkinfo->diskid[i].id);
                if (ret) {
                        if (ret == EEXIST) {
                                continue;
                        } else
                                GOTO(err_ret, ret);
                }
        }

        //YASSERT(chkinfo->repnum == rack_count);
        YASSERT(rack_count >= 2);
        
        return 0;
err_ret:
        return ret;
}
#endif

/* for node(not use) */
int diskmap_init(void);
void diskmap_destroy(void);

void diskmap_add(const char *name, const nid_t *nid);
void diskmap_del(const char *name);

int diskmap_get(const char *pool, diskid_t *diskid, int repnum,
                const nid_t *skip, int skip_count, int flag);

int diskmap_total(uint32_t *site, uint32_t *rack, uint32_t *node, uint32_t *disk);
int diskmap_online(uint32_t *site, uint32_t *rack, uint32_t *node, uint32_t *disk);
int diskmap_check_insite(const char *site_name);
int diskmap_list_insite(void *buf, int *count);

/* for pool */
int poolmap_init(void);
int poolmap_destroy(void);

int poolmap_insert(const char *name, const char *pool, const nid_t *nid);
int poolmap_remove(const char *name, const char *pool);
int poolmap_brief_info(int *count, brief_info_t **infos);

int poolmap_dump(const char *pool);


#endif
