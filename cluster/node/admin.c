#include "config.h"

#include <sys/types.h>
#include <regex.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <signal.h>
#include <sys/vfs.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/statvfs.h>

#define DBG_SUBSYS S_LIBCLUSTER

#include "sysy_lib.h"
#include "env.h"
#include "get_version.h"
#include "configure.h"
#include "lichconf.h"
#include "cluster.h"
#include "job_dock.h"
#include "minirpc.h"
#include "rpc_proto.h"
#include "net_global.h"
#include "node.h"
#include "ylog.h"
#include "dbg.h"

typedef enum {
        ADMIN_GETINFO,
        ADMIN_JOIN,
} admin_op_t;

typedef struct {
        uint32_t op;
} getinfo_req_t;

static int __admin_getinfo(void *_rep, uint32_t *replen,
                       const void *_req)
{
        const getinfo_req_t *req = _req;
        nodeinfo2_t *rep = _rep;

        DINFO("getinfo\n");

        (void)replen;
        (void) rep;
        (void) req;

        return 0;
}

int admin_handler(void *context, void *rep, uint32_t *replen,
                      const void *req, uint32_t reqlen)
{
        int ret;
        const uint32_t *op = req;

        (void) context;
        (void) reqlen;

        switch (*op) {
        case ADMIN_GETINFO:
                ret = __admin_getinfo(rep, replen, req);
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                break;
        default:
                ret = EINVAL;
                DWARN("invalid op %d\n", *op);
                GOTO(err_ret, ret);
        }

        return 0;
err_ret:
        return ret;
}

#if 0
int admin_getinfo(nodeinfo2_t *info, const char *dist)
{
        int ret, replen;
        char addr[MAX_NAME_LEN], port[MAX_NAME_LEN];
        getinfo_req_t req;

        __split(addr, port, dist);

        ret = minirpc_request_wait(MINIRPC_OP_ADMIN, (void *)info, &replen,
                                   (void *)&req, sizeof(req), addr, port, 10);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        return 0;
err_ret:
        return ret;
}
#endif
