#ifndef __LICH_ADMIN_H__
#define __LICH_ADMIN_H__

int admin_handler(void *context, void *rep, uint32_t *replen,
                  const void *req, uint32_t reqlen);

int admin_getinfo(nodeinfo2_t *info, const char *dist);

#endif
