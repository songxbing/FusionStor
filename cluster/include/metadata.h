#ifndef __METADATA_H__
#define __METADATA_H__

#include "cluster.h"
#include "sysy_lib.h"

static inline void metadata_check_diskid(const diskid_t *diskid, int count)
{
        int i;

        if (count == 1)
                return;

        for (i = 1; i < count; i++) {
                YASSERT(nid_cmp(&diskid[i] , &diskid[0]));
        }

        if (count > 2)
                metadata_check_diskid(&diskid[1], count - 1);
}

int nodetable_init(void);
void nodetable_destroy(void);

void nodetable_dump();
int nodetable_count(int *count);

time_t nodetable_last_reset();
int nodetable_standalone(void);

int nodetable_list_open(const char *uuid);
int nodetable_list(char *buf, int *len, const char *uuid, uint64_t _offset);
int nodetable_list_close(const char *uuid);

int nodetable_update(const char *name, const char *dfinfo, const nodestat_t *stat);
int nodetable_update_local_vols(const char *name, const local_vol_t *local_vols, int options);

// int nodetable_check_deleting(const char *name, int *deleting);
// int nodetable_add(const char *name, const nid_t *nid);
int nodetable_new(const char *name, nid_t *nid);
int nodetable_del(const char *name);

int nodetable_get(const char *name, nodestat_t *stat);
int nodetable_offline(const char *name);
// int nodetable_check_deleting(const char *name, int *deleting);

int nodetable_writeable(const nid_t *diskid, int count);

int nodetable_newdisk(const char *pool, diskid_t *diskid, int repnum,
                         const nid_t *skip, int skip_count, int flag);

#endif
