#ifndef __DISPATCH_H__
#define __DISPATCH_H__

/* srv.c: server side */
int dispatch_srv_init(void);
void dispatch_srv_destroy(void);

int dispatch_srv_newid(chkid_t *chkid);
int dispatch_srv_newdisk(const char *pool, diskid_t *diskid, int repnum,
                         const nid_t *skip, int skip_count, int flag);

int dispatch_srv_netinfo(ynet_net_info_t *info, const nid_t *nid);
int dispatch_srv_writeable(const nid_t *diskid, int count);
int dispatch_srv_sysstat(lich_stat_t *stat, uint32_t force);
int dispatch_srv_pooldump(const char *pool);

int dispatch_srv_heartbeat(const char *name, const char *dfinfo, const nodestat_t *stat, const char *status,
                           uint32_t *admin_uptime);

int dispatch_srv_addnode(const char *name, const nid_t *nid);
int dispatch_srv_delnode(const char *name);

// site === storage ared === protection domain
int dispatch_srv_check_storage_area(const char *site_name);
int dispatch_srv_list_storage_area(char *buf, int *count);

/* rpc.c: client side */
int dispatch_rpc_init(void);
void dispatch_rpc_destroy(void);

int dispatch_rpc_newid(const nid_t *nid, chkid_t *chkid);
int dispatch_rpc_newdisk(const nid_t *nid, const char *pool, diskid_t *diskid, uint32_t repnum,
                         const nid_t *_skip, uint32_t skip_count, int flag);

int dispatch_rpc_writeable(const nid_t *nid, const nid_t *diskid, int count);
int dispatch_rpc_netinfo(const nid_t *master, ynet_net_info_t *info, const nid_t *nid);
int dispatch_rpc_sysstat(const nid_t *master, lich_stat_t *stat, uint32_t force);
int dispatch_rpc_pooldump(const nid_t *master, const char *pool);

int dispatch_rpc_heartbeat(const nid_t *master, const char *name, const char *dfinfo,
                           const nodestat_t *stat, const char *status, uint32_t *admin_uptime);

int dispatch_srv_vol_notifiction(const char *name, const local_vol_t *vols, uint32_t options);
int dispatch_rpc_vol_notifiction(const nid_t *nid, const char *name, const local_vol_t *vols, uint32_t options);

int dispatch_rpc_addnode(const nid_t *master, const char *name, const nid_t *nid);
int dispatch_rpc_delnode(const nid_t *master, const char *name);

int dispatch_rpc_check_storage_area(const nid_t *master, const char *site_name);
int dispatch_rpc_list_storage_area(const nid_t *master, void *buf, int *count);

/*scan.c*/
int dispatch_scan_init(void);
int dispatch_srv_scan(const char *tash, int parallels);
int dispatch_srv_scan_status(scan_status_t *status);

/* module init */
int dispatch_init(void);
void dispatch_destroy(void);

#endif
