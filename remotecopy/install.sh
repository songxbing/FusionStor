#!/bin/bash

usage()
{
    echo "Usage: $0 [OPTIONS]"
    echo "-i install [path]"
    echo "-e remove [path]"

    exit 1
}

install()
{
    DIST=$1/remotecopy

    rm -rf $DIST
    mkdir -p $DIST
    cp *.py $DIST
    cp lichrcd* $DIST
    cp remotecopy.conf.example $DIST

    chmod a+x $DIST/*.py
    chmod a+x $DIST/lichrcd*
    gcc get_version.c -o $DIST/get_version
}

remove()
{
    echo "do nothing"
}

if [ $# -lt 1 ]
then
    usage
fi

while getopts ieh options
do
    case $options in
        i)
        echo "install scripts $2"
        install $2
        ;;
        e)
        echo "remove scripts"
        remove
        ;;
        h)
        usage
        ;;
        \?)
        usage
        ;;
    esac
done
