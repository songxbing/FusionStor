import rpyc

class RpycRpc:
    def __init__(self, uuid, name, conf, server, port):
        self.uuid = uuid
        self.name = name
        self.conf = conf
        self.server = server
        self.port = port

    def connect(self):
        self.c = rpyc.connect(self.server, self.port)

    def close(self):
        self.c.close()

    def volume_reset(self):
        return self.c.root.volume_reset(self.uuid, self.name)

    def volume_negotiation(self, chknum):
        vol_name = self.conf['volume'][0][1]
        return self.c.root.volume_negotiation(self.uuid, self.name, vol_name, chknum)

    def volume_finish(self, neg):
        vol_name = self.conf['volume'][0][1]
        return self.c.root.volume_finish(self.uuid, self.name, vol_name, neg)

    def volume_left(self, neg):
        return self.c.root.volume_left(self.uuid, self.name, neg)

    def chunk_negotiation(self, i, l):
        return self.c.root.chunk_negotiation(self.uuid, self.name, i, l)

    def chunk_sync(self, i, l, chunk):
        vol_name = self.conf['volume'][0][1]
        return self.c.root.chunk_sync(self.uuid, self.name, vol_name, i, l, chunk)
