import zerorpc

class ZeroRpcRpc:
    def __init__(self, name, conf):
        self.name = name
        self.conf = conf

    def connect(self):
        self.c = zerorpc.Client()
        self.c.connect("tcp://%s:%s" % (self.conf['server'], self.conf['port']))

    def close(self):
        self.c.close()

    def sync_all_check(self, list):
        vol_name = self.conf['volume'][0][1]
        return self.c.sync_all_check(self.conf['uuid'], self.name, vol_name, list)

    def sync_all_prep(self, last):
        vol_name = self.conf['volume'][0][1]
        return self.c.sync_all_prep(self.conf['uuid'], self.name, vol_name, last)

    def sync_all_finish(self, last, chknum):
        vol_name = self.conf['volume'][0][1]
        return self.c.sync_all_finish(self.conf['uuid'], self.name, vol_name, last, chknum)

    def sync_increment_prep(self, list):
        vol_name = self.conf['volume'][0][1]
        return self.c.sync_increment_prep(self.conf['uuid'], self.name, vol_name, list)

    def sync_increment_finish(self, last):
        vol_name = self.conf['volume'][0][1]
        return self.c.sync_increment_finish(self.conf['uuid'], self.name, vol_name, last, self.conf['keep'])

    def sync_chunk(self, last, i, chunk):
        vol_name = self.conf['volume'][0][1]
        return self.c.sync_chunk(self.conf['uuid'], self.name, vol_name, last, i, chunk)

    def sync_break_check(self, list):
        vol_name = self.conf['volume'][0][1]
        return self.c.sync_break_check(self.conf['uuid'], self.name, vol_name, list)

    def get_snapshot_last(self):
        vol_name = self.conf['volume'][0][1]
        return self.c.get_snapshot_last(self.conf['uuid'], self.name, vol_name)
