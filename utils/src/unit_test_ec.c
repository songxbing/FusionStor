#include "config.h"

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <dirent.h>
#include <ctype.h>
#include <libgen.h>

#define DBG_SUBSYS S_LIBINTERFACE

#include "configure.h"
#include "env.h"
#include "adt.h"
#include "net_table.h"
#include "lichstor.h"
#include "cluster.h"
#include "metadata.h"
#include "lich_md.h"
#include "storage.h"
#include "volume.h"
#include "lichbd.h"
#include "license.h"
#include "md_map.h"
#include "net_global.h"
#include "utils.h"
#include "dbg.h"

#define STRIP_BLOCK 4096
#define STRIP_ALIGN  4096

int print_buf(char *buf)
{
        int i;

        printf("buf:%p start==========\n", buf);
        for (i = 0; i < STRIP_BLOCK; i++) {
                if (i%1024 == 0 && i!=0)
                        printf("\n");
                printf("%c", buf[i]);
        }
        printf("buf:%p end==========\n", buf);

        return 0;
}

int unit_test_ec()
{
        int i, ret;
        char *buffs[3] = {0};
        char *buf;
        unsigned char src_in_err[3] = {0};

        for (i = 0; i < 3; i++) {
                ret = posix_memalign((void **)&buf, STRIP_ALIGN, STRIP_BLOCK);
                if (ret)
                        GOTO(err_free, ret);

                buffs[i] = buf;
        }

        for (i = 0; i < STRIP_BLOCK; i++) {
                buffs[0][i] = 'a';
                buffs[1][i] = 'b';
        }

        ret = ec_encode(&buffs[0], &buffs[2], STRIP_BLOCK, 3, 2);
        if (ret)
                GOTO(err_free, ret);

        printf("=====encode ===========\n");
        print_buf(buffs[0]);
        print_buf(buffs[1]);
        print_buf(buffs[2]);

        for (i = 2048; i < STRIP_BLOCK; i++) {
                buffs[0][i] = 'c';
        }

        src_in_err[1] = 1;

        ret = ec_decode(src_in_err, &buffs[0], &buffs[2], STRIP_BLOCK, 3, 2);
        if (ret)
                GOTO(err_free, ret);

        printf("=====decode ===========\n");
        print_buf(buffs[0]);
        print_buf(buffs[1]);
        print_buf(buffs[2]);

        for (i = 0; i < 3; i++) {
                if (buffs[i])
                        free(buffs[i]);
        }

        return 0;
err_free:
        for (i = 0; i < 3; i++) {
                if (buffs[i])
                        free(buffs[i]);
        }
        return ret;
}
