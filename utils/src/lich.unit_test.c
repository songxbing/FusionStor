#include "config.h"

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <dirent.h>
#include <ctype.h>

#define DBG_SUBSYS S_LIBINTERFACE

#include "configure.h"
#include "env.h"
#include "adt.h"
#include "net_table.h"
#include "lichstor.h"
#include "cluster.h"
#include "metadata.h"
#include "storage.h"
#include "volume.h"
#include "license.h"
#include "lichbd.h"
#include "net_global.h"
#include "unit_test.h"
#include "dbg.h"

typedef enum {
        OP_NULL,
        OP_TEST_BUFFER,
        OP_TEST_EC,
} uint_test_op_t;

static void usage()
{
        fprintf(stderr, "\nusage:\n"
                "lich.unit_test --test_buffer\n"
                "lich.unit_test --test_ec\n"
               );
}

int main(int argc, char *argv[])
{
        int ret, op = OP_NULL;
        char c_opt;
        int verbose = 0;

        (void) verbose;
        dbg_info(0);

        while (srv_running) {
                int option_index = 0;

                static struct option long_options[] = {
                        { "test_buffer", no_argument, 0, 'b'},
                        { "test_ec", no_argument, 0, 'e'},
                        { "verbose", 0, 0, 'v' },
                        { "help",    0, 0, 'h' },
                        { 0, 0, 0, 0 },
                };

                c_opt = getopt_long(argc, argv, "b:vh", long_options, &option_index);
                if (c_opt == -1)
                        break;

                switch (c_opt) {
                case 0:
                        switch (option_index) {
                        default:
                                fprintf(stderr, "Hoops, wrong op got!\n");
                                YASSERT(0); 
                        }

                        break;
                case 'b':
                        op = OP_TEST_BUFFER;
                        break;
                case 'e':
                        op = OP_TEST_EC;
                        break;
                case 'v':
                        verbose = 1;
                        break;
                case 'h':
                        usage();
                        EXIT(0);
                default:
                        usage();
                        EXIT(EINVAL);
                }
        }

        if (argc == 1) {
                usage();
                exit(EINVAL);
        }

        ret = lichbd_init("");
        if (unlikely(ret))
                GOTO(err_ret, ret);

        switch (op) {
        case OP_TEST_BUFFER:
                ret = unit_test_buffer();
                if (unlikely(ret))
                        GOTO(err_ret, ret);
                break;
        case OP_TEST_EC:
                ret = unit_test_ec();
                if (unlikely(ret))
                        GOTO(err_ret, ret);
                break;
        default:
                usage();
                exit(EINVAL);
        }

        return 0;
err_ret:
        fprintf(stderr, "error: %s\n", strerror(ret));
        EXIT(_errno(ret));
}
