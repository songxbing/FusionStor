#include "config.h"

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <dirent.h>
#include <ctype.h>

#define DBG_SUBSYS S_LIBINTERFACE

#include "configure.h"
#include "env.h"
#include "adt.h"
#include "net_table.h"
#include "lichstor.h"
#include "system.h"
#include "cluster.h"
#include "metadata.h"
#include "lich_md.h"
#include "storage.h"
#include "volume.h"
#include "license.h"
#include "md_map.h"
#include "net_global.h"
#include "utils.h"
#include "dbg.h"

#define MULTITHREADING          1
#define COPY_THREAD_MAX         10
#define COPY_SIZE_MIN           32      /* 32M */

typedef struct {
        int fd;
        fileid_t fileid;
        fileid_t snapid;
        void * (*worker)(void *_arg);
} arg_ext_t;

typedef struct {
        sem_t sem;
        int ret;
        arg_ext_t ext;
        off_t offset;
        size_t size;
} arg_t;

typedef enum {
        OP_NULL,
        OP_CREATE,
        OP_LIST,
        OP_CAT,
        OP_COPY,
        OP_ROLLBACK,
        OP_REMOVE,
        OP_CLONE,
        OP_FLAT,
        OP_PROTECT,
        OP_UNPROTECT,
        OP_DIFF,
        OP_UPDATE,
        OP_VERSION,
} snap_op_t;

static void usage()
{
        fprintf(stderr, "\nusage:\n"
                "lich.snapshot --create <file name>@<snap name>\n"
                "lich.snapshot --remove <file name>@<snap name>\n"
                "lich.snapshot --rollback <file name>@<snap name>\n"
                "lich.snapshot --protect <file name>@<snap name> [0|1]\n"
                "lich.snapshot --clone <file name>@<snap name> <file name> [-p 0|1]\n"
                "lich.snapshot --flat <file name>\n"
                "lich.snapshot --list <file name> [-a] [-v] [-n]\n"
                "lich.snapshot --cat <file name>@<snap name>\n"
                "lich.snapshot --copy <file name>@<snap name> [idx|from~to] :<file name>\n"
                "lich.snapshot --diff <file name>@<snap src>~<snap dst> [idx|from~to] :<file name>\n"
                "\n"
                "lich.snapshot --update\n"
                "lich.snapshot --version\n"
               );
}

int main(int argc, char *argv[])
{
        int ret, op = OP_NULL, force = 0, tmp;
        char c_opt;
        int verbose = 0, all = 0, normal = 0, priority = -1, protect = 0;
        const char *name = NULL, *to = NULL, *snap = NULL, *idx = NULL;
        uint32_t version;

        dbg_info(0);

        (void) verbose;

        while (srv_running) {
                int option_index = 0;

                static struct option long_options[] = {
                        { "cat", required_argument, 0, 0},
                        { "protect", required_argument, 0, 0},
                        { "unprotect", required_argument, 0, 0},
                        { "copy", required_argument, 0, 0},
                        { "diff", required_argument, 0, 0},
                        { "flat", required_argument, 0, 0},
                        { "force", no_argument, 0, 0},
                        { "update", no_argument, 0, 0},
                        { "version", no_argument, 0, 0},
                        { "create", required_argument, 0, 'c'},
                        { "list", required_argument, 0, 'l'},
                        { "destroy", required_argument, 0, 'd'},
                        { "rollback", required_argument, 0, 'r'},
                        { "remove", required_argument, 0, 'm'},
                        { "clone", required_argument, 0, 'o'},
                        { "send", required_argument, 0, 's'},
                        { "recv", required_argument, 0, 'e'},
                        { "all", 0, 0, 'a'},
                        { "normal", 0, 0, 'n'},
                        { "verbose", 0, 0, 'v' },
                        { "help",    0, 0, 'h' },
                        { 0, 0, 0, 0 },
                };

                c_opt = getopt_long(argc, argv, "c:l:d:r:s:e:anp:vh", long_options, &option_index);
                if (c_opt == -1)
                        break;

                switch (c_opt) {
                case 0:
                        switch (option_index) {
                        case 0:
                                op = OP_CAT;
                                name = optarg;
                                break;
                        case 1:
                                op = OP_PROTECT;
                                name = optarg;
                                break;
                        case 2:
                                op = OP_UNPROTECT;
                                name = optarg;
                                break;
                        case 3:
                                op = OP_COPY;
                                name = optarg;
                                if (argc == 4) {
                                        to = argv[3];
                                } else if (argc == 5) {
                                        idx = argv[3];
                                        to = argv[4];
                                } else {
                                        usage();
                                        EXIT(EINVAL);
                                }
                                break;
                        case 4:
                                op = OP_DIFF;
                                name = optarg;
                                if (argc == 4) {
                                        to = argv[3];
                                } else if (argc == 5) {
                                        idx = argv[3];
                                        to = argv[4];
                                } else {
                                        usage();
                                        EXIT(EINVAL);
                                }
                                break;
                        case 5:
                                op = OP_FLAT;
                                name = optarg;
                                break;
                        case 6:
                                force = 1;
                                break;
                        case 7:
                                op = OP_UPDATE;
                                break;
                        case 8:
                                op = OP_VERSION;
                                break;
                        default:
                                fprintf(stderr, "Hoops, wrong op got!\n");
                                YASSERT(0); 
                        }

                        break;
                case 'c':
                        op = OP_CREATE;
                        name = optarg;
                        break;
                case 'l':
                        op = OP_LIST;
                        name = optarg;
                        break;
                case 'r':
                        op = OP_ROLLBACK;
                        name = optarg;
                        break;
                case 'o':
                        op = OP_CLONE;
                        name = optarg;
                        to = argv[3];
                        break;
                case 'm':
                        op = OP_REMOVE;
                        name = optarg;
                        break;
                case 'a':
                        all = 1;
                        break;
                case 'p':
                        tmp = atoi(optarg);
                        if (tmp == 0 || tmp == 1) {
                                priority = tmp;
                                protect = tmp;
                        }
                        break;
                case 'v':
                        verbose = 1;
                        break;
                case 'n':
                        normal = 1;
                        break;
                case 'h':
                        usage();
                        EXIT(0);
                default:
                        usage();
                        EXIT(EINVAL);
                }
        }

#if 1
        if (argc == 1) {
                usage();
                exit(EINVAL);
        }
#endif

        ret = env_init_simple("lich");
        if (unlikely(ret))
                GOTO(err_ret, ret);

        ret = network_connect_master();
        if (unlikely(ret)) {
            GOTO(err_ret, ret);
        }

        ret = stor_init(NULL, -1);
        if (unlikely(ret))
                GOTO(err_ret, ret);

        ret = storage_license_check();
        if (unlikely(ret))
                GOTO(err_ret, ret);

        if (op == OP_CREATE || op == OP_CAT || op == OP_COPY || op == OP_REMOVE ||
                        op == OP_ROLLBACK || op == OP_CLONE || op == OP_PROTECT) {
                snap = strchr(name, '@');
                if (!snap) {
                        fprintf(stderr, "snapshot name not specify\n");
                        ret = EINVAL;
                        GOTO(err_ret, ret);
                }

                if (!strncmp(snap + 1, LICH_SYSTEM_ATTR_PREFIX, strlen(LICH_SYSTEM_ATTR_PREFIX))) {
                        fprintf(stderr, "snapshot should not start with %s\n", LICH_SYSTEM_ATTR_PREFIX);
                        ret = EINVAL;
                        GOTO(err_ret, ret);
                }
        }

        switch (op) {
        case OP_CREATE:
                ret = utils_snapshot_create(name, force, protect, "");
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                break;
        case OP_LIST:
                ret = utils_snapshot_list(name, all, normal, verbose);
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                break;
        case OP_CAT:
                ret = utils_snapshot_cat(name);
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                break;
        case OP_COPY:
                ret = utils_snapshot_copy(name, to, idx);
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                break;
        case OP_DIFF:
                ret = utils_snapshot_diff(name, to, idx);
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                break;
        case OP_ROLLBACK:
                ret = utils_snapshot_rollback(name);
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                break;
        case OP_REMOVE:
                ret = utils_snapshot_remove(name, force);
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                break;
        case OP_CLONE:
                ret = utils_snapshot_clone(name, to, priority, "");
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                break;
        case OP_FLAT:
                ret = utils_snapshot_flat(name);
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                break;
        case OP_PROTECT:
                if (argc == 4) {
                        int num;
                        num = atoi(argv[3]);
                        ret = utils_snapshot_protect_set(name, num);
                        if (unlikely(ret))
                                GOTO(err_ret, ret);
                } else if (argc == 3) {
                        ret = utils_snapshot_protect_get(name);
                        if (unlikely(ret))
                                GOTO(err_ret, ret);
                } else {
                        ret = EINVAL;
                        GOTO(err_ret, ret);
                }

                break;
        case OP_UPDATE:
                ret = utils_snapshot_update();
                if (unlikely(ret))
                        GOTO(err_ret, ret);

                break;
        case OP_UNPROTECT:
                usage();
                exit(EINVAL);

                break;
        default:
                usage();
                exit(EINVAL);
        }

        return 0;
err_ret:
        EXIT(_errno(ret));
}
