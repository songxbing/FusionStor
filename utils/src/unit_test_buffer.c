#include "config.h"

#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <dirent.h>
#include <ctype.h>
#include <libgen.h>

#define DBG_SUBSYS S_LIBINTERFACE

#include "configure.h"
#include "env.h"
#include "adt.h"
#include "net_table.h"
#include "lichstor.h"
#include "cluster.h"
#include "metadata.h"
#include "lich_md.h"
#include "storage.h"
#include "volume.h"
#include "lichbd.h"
#include "license.h"
#include "md_map.h"
#include "net_global.h"
#include "utils.h"
#include "dbg.h"

/*need change #define BUFFER_SEG_MAX (512 * 1024) to #define BUFFER_SEG_MAX (16) in ylib/lib/buffer.c*/
void __test_buffer_write(int get_off, int get_size)
{
        int i;
        char tmp[49], out[49];
        buffer_t buf;

        mbuffer_init(&buf, 0);

        memset(tmp, 0x0, 49);
        memset(out, 0x0, 49);

        for (i = 0; i < 48; i++) {
                memset(tmp + i, '0' + (i % 8), 1);
        }
        printf("tmp1:");
        for (i = 0; i < 48; i++) {
                if (i % 16 == 0) {
                        printf(" ");
                }
                printf("%c", tmp[i]);
        }
        printf("\n");

        mbuffer_copy(&buf, tmp, 48);
        mbuffer_read(&buf, out + get_off, get_off, get_size);

        printf("out1:");
        for (i = 0; i < 48; i++) {
                if (i % 16 == 0) {
                        printf(" ");
                }
                if (out[i] == 0)
                        printf(" ");
                else
                        printf("%c", out[i]);
        }
        printf("\n");

        for (i = get_off; i < get_off + get_size; i++) {
                out[i] = '9';
        }

        printf("out2:");
        for (i = 0; i < 48; i++) {
                if (i % 16 == 0) {
                        printf(" ");
                }
                if (out[i] == 0)
                        printf(" ");
                else
                        printf("%c", out[i]);
        }
        printf("\n");

        mbuffer_write(&buf, out + get_off, get_off, get_size);
        mbuffer_get(&buf, out, buf.len);

        printf("out3:");
        for (i = 0; i < 48; i++) {
                if (i % 16 == 0) {
                        printf(" ");
                }
                if (out[i] == 0)
                        printf(" ");
                else
                        printf("%c", out[i]);
        }
        printf("\n\n");
}

void test_buffer_write()
{
        __test_buffer_write(0, 8);
        __test_buffer_write(4, 8);
        __test_buffer_write(8, 8);
        __test_buffer_write(0, 16);

        __test_buffer_write(16, 8);
        __test_buffer_write(20, 8);
        __test_buffer_write(24, 8);
        __test_buffer_write(16, 16);

        __test_buffer_write(32, 8);
        __test_buffer_write(36, 8);
        __test_buffer_write(40, 8);
        __test_buffer_write(32, 16);

        __test_buffer_write(0, 24);
        __test_buffer_write(4, 24);
        __test_buffer_write(8, 24);

        __test_buffer_write(12, 24);
        __test_buffer_write(16, 24);
        __test_buffer_write(20, 24);
        __test_buffer_write(24, 24);

        __test_buffer_write(0, 48);
}

int unit_test_buffer()
{
        test_buffer_write();
        return 0;
}
