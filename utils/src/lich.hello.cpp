#include "config.h"

#include <iostream>

#define DBG_SUBSYS S_LIBINTERFACE

class A {
private:
        int a;

public:
        void set_a(int a) {
                this->a = a;
        }

        int get_a() const {
                return this->a;
        }
};

int main(int argc, char **argv)
{
        std::cout << "Hello, C++" << std::endl;

        A a;
        a.set_a(100);
        std::cout << "a = " << a.get_a() << std::endl;
        return 0;
}
