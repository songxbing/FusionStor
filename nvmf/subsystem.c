/*-
 *   BSD LICENSE
 *
 *   Copyright (c) Intel Corporation.
 *   All rights reserved.
 *
 *   Redistribution and use in source and binary forms, with or without
 *   modification, are permitted provided that the following conditions
 *   are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *     * Neither the name of Intel Corporation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *   A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *   OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *   DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *   THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *   OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <ctype.h>
#include <assert.h>

#include "nvmf_internal.h"
#include "session.h"
#include "subsystem.h"
#include "transport.h"
#include "nvmf_spec.h"

#include "cpuset.h"
#include "core.h"
#include "list.h"
#include "lich_id.h"

LIST_HEAD(g_subsystems);
static uint64_t g_discovery_genctr = 0;
static struct spdk_nvmf_discovery_log_page *g_discovery_log_page = NULL;
static size_t g_discovery_log_page_size = 0;

bool lich_nvmf_subsystem_exists(const char *subnqn)
{
        struct list_head                *pos, *tmp;
	struct lich_nvmf_subsystem	*subsystem;

	if (!subnqn) {
		return false;
	}

        list_for_each_safe(pos, tmp, &g_subsystems) {
                subsystem = list_entry(pos, struct lich_nvmf_subsystem, entries);
		if (strcmp(subnqn, subsystem->subnqn) == 0) {
			return true;
		}
	}

	return false;
}

struct lich_nvmf_subsystem *nvmf_find_subsystem(const char *subnqn)
{
	struct lich_nvmf_subsystem	*subsystem;
        struct list_head                *pos, *tmp;

	if (!subnqn) {
		return NULL;
	}

        list_for_each_safe(pos, tmp, &g_subsystems) {
                subsystem = list_entry(pos, struct lich_nvmf_subsystem, entries);
		if (strcmp(subnqn, subsystem->subnqn) == 0) {
			return subsystem;
		}
	}

	return NULL;
}

struct lich_nvmf_subsystem *lich_nvmf_find_subsystem_with_cntlid(uint16_t cntlid)
{
        struct lich_nvmf_subsystem      *subsystem;
        struct lich_nvmf_session        *session;
        struct list_head                *pos, *tmp;
        struct list_head                *pos1, *tmp1;

        list_for_each_safe(pos, tmp, &g_subsystems) {
                subsystem = list_entry(pos, struct lich_nvmf_subsystem, entries);
                list_for_each_safe(pos1, tmp1, &subsystem->sessions) {
                        session = list_entry(pos1, struct lich_nvmf_session, link);
                        if (session->id == cntlid) {
                                return subsystem;
                        }
                }
        }

        return NULL;
}

struct lich_nvmf_subsystem *nvmf_get_subsystem(int core_hash)
{
        struct list_head                *pos, *tmp;
	struct lich_nvmf_subsystem	*subsystem;

        list_for_each_safe(pos, tmp, &g_subsystems) {
                subsystem = list_entry(pos, struct lich_nvmf_subsystem, entries);
		if (core_hash == subsystem->lcore) {
			return subsystem;
		}
	}

	return NULL;
}

bool lich_nvmf_subsystem_host_allowed(struct lich_nvmf_subsystem *subsystem, const char *hostnqn)
{
	struct lich_nvmf_host *host;
        struct list_head      *pos, *tmp;

	if (!hostnqn) {
		return false;
	}

	if (subsystem->num_hosts == 0) {
		/* No hosts means any host can connect */
		return true;
	}

        list_for_each_safe(pos, tmp, &subsystem->hosts) {
                host = list_entry(pos, struct lich_nvmf_host, link);
		if (strcmp(hostnqn, host->nqn) == 0) {
			return true;
		}
	}

	return false;
}

void lich_nvmf_subsystem_poll(struct lich_nvmf_subsystem *subsystem)
{
	struct lich_nvmf_session *session;
        struct list_head         *pos, *tmp;

        list_for_each_safe(pos, tmp, &subsystem->sessions) {
                session = list_entry(pos, struct lich_nvmf_session, link);

                /* For each connection in the session, check for completions */
                lich_nvmf_session_poll(session);
        }
}

static bool lich_nvmf_valid_nqn(const char *nqn)
{
	size_t len;

	len = strlen(nqn);
	if (len >= SPDK_NVMF_NQN_MAX_LEN) {
		DERROR("Invalid NQN \"%s\": length %zu > max %d\n", nqn, len, SPDK_NVMF_NQN_MAX_LEN - 1);
		return false;
	}

	if (strncmp(nqn, "nqn.", 4) != 0) {
		DERROR("Invalid NQN \"%s\": NQN must begin with \"nqn.\".\n", nqn);
		return false;
	}

	/* yyyy-mm. */
	if (!(isdigit(nqn[4]) && isdigit(nqn[5]) && isdigit(nqn[6]) && isdigit(nqn[7]) &&
	      nqn[8] == '-' && isdigit(nqn[9]) && isdigit(nqn[10]) && nqn[11] == '.')) {
		DERROR("Invalid date code in NQN \"%s\"\n", nqn);
		return false;
	}

	return true;
}

struct lich_nvmf_subsystem *lich_nvmf_create_subsystem(int core_hash, const char *nqn,
			   enum spdk_nvmf_subtype type,
			   enum lich_nvmf_subsystem_mode mode,
			   void *cb_ctx,
			   lich_nvmf_subsystem_connect_fn connect_cb,
			   lich_nvmf_subsystem_disconnect_fn disconnect_cb)
{
	struct lich_nvmf_subsystem	*subsystem;

	if (!lich_nvmf_valid_nqn(nqn)) {
		return NULL;
	}

	subsystem = calloc(1, sizeof(struct lich_nvmf_subsystem));
	if (subsystem == NULL) {
		return NULL;
	}

	subsystem->lcore = core_hash;
	subsystem->subtype = type;
	subsystem->mode = mode;
	subsystem->cb_ctx = cb_ctx;
	subsystem->connect_cb = connect_cb;
	subsystem->disconnect_cb = disconnect_cb;
	snprintf(subsystem->subnqn, sizeof(subsystem->subnqn), "%s", nqn);
        INIT_LIST_HEAD(&subsystem->listen_addrs);
        INIT_LIST_HEAD(&subsystem->hosts);
        INIT_LIST_HEAD(&subsystem->sessions);

	if (mode == NVMF_SUBSYSTEM_MODE_DIRECT) {
		//subsystem->ops = &lich_nvmf_direct_ctrlr_ops;
		subsystem->ops = NULL;
	} else {
		subsystem->ops = &lich_nvmf_virtual_ctrlr_ops;
	}

        list_add_tail(&subsystem->entries, &g_subsystems);
	g_discovery_genctr++;

	return subsystem;
}

void lich_nvmf_delete_subsystem(struct lich_nvmf_subsystem *subsystem)
{
	struct lich_nvmf_listen_addr	*listen_addr;
	struct lich_nvmf_host		*host;
	struct lich_nvmf_session	*session;
        struct list_head                *pos, *tmp;

	if (!subsystem) {
		return;
	}

	DWARN("subsystem %p is removed\n", subsystem);

	list_for_each_safe(pos, tmp, &subsystem->listen_addrs) {
                listen_addr = list_entry(pos, struct lich_nvmf_listen_addr, link);
                list_del(&listen_addr->link);
		//free(listen_addr->traddr);
		free(listen_addr->trsvcid);
		free(listen_addr->trname);
		free(listen_addr);
		subsystem->num_listen_addrs--;
	}

        list_for_each_safe(pos, tmp, &subsystem->hosts) {
                host = list_entry(pos, struct lich_nvmf_host, link);
                list_del(&host->link);
		free(host->nqn);
		free(host);
		subsystem->num_hosts--;
	}

	list_for_each_safe(pos, tmp, &subsystem->listen_addrs) {
                session = list_entry(pos, struct lich_nvmf_session, link);
		lich_nvmf_session_destruct(session);
	}

	if (subsystem->ops->detach) {
		subsystem->ops->detach(subsystem);
	}

        list_del(&subsystem->entries);
	g_discovery_genctr--;

	free(subsystem);
}

int lich_nvmf_subsystem_add_listener(struct lich_nvmf_subsystem *subsystem,
				 char *trname, u_long traddr, char *trsvcid)
{
	struct lich_nvmf_listen_addr *listen_addr = NULL;
	const struct lich_nvmf_transport *transport;
	int ret;

	transport = lich_nvmf_transport_get(trname);
	if (!transport) {
                ret = -1;
                GOTO(err_ret, ret);
	}

	listen_addr = calloc(1, sizeof(*listen_addr));
	if (!listen_addr) {
                ret = -1;
                GOTO(err_ret, ret);
		//return -1;
	}

	listen_addr->traddr.s_addr = traddr;
        /**
	if (!listen_addr->traddr) {
		free(listen_addr);
                ret = -1;
                GOTO(err_ret, ret);
	}
        **/

	listen_addr->trsvcid = strdup(trsvcid);
	if (!listen_addr->trsvcid) {
		//free(listen_addr->traddr);
		free(listen_addr);
                ret = -1;
                GOTO(err_ret, ret);
	}

	listen_addr->trname = strdup(trname);
	if (!listen_addr->trname) {
		//free(listen_addr->traddr);
		free(listen_addr->trsvcid);
		free(listen_addr);
                ret = -1;
                GOTO(err_ret, ret);
	}

	ret = transport->listen_addr_add(listen_addr);
	if (ret < 0) {
                DERROR("Unable to listen on address '%s'\n", inet_ntoa(listen_addr->traddr));
                ret = -1;
                GOTO(err_ret, ret);
	}

	list_add_tail(&listen_addr->link, &subsystem->listen_addrs);
	subsystem->num_listen_addrs++;
	g_discovery_genctr++;

	return 0;

err_ret:
	if(listen_addr) {
		if(listen_addr->trsvcid)
			free(listen_addr->trsvcid);
		if(listen_addr->trname)
			free(listen_addr->trname);

		free(listen_addr);
	}
        return -1;
}

int lich_nvmf_subsystem_add_host(struct lich_nvmf_subsystem *subsystem, char *host_nqn)
{
	struct lich_nvmf_host *host;

	host = calloc(1, sizeof(*host));
	if (!host) {
		return -1;
	}
	host->nqn = strdup(host_nqn);
	if (!host->nqn) {
		free(host);
		return -1;
	}

        list_add(&host->link, &subsystem->hosts);
	subsystem->num_hosts++;
	g_discovery_genctr++;

	return 0;
}

/**
int nvmf_subsystem_add_ctrlr(struct lich_nvmf_subsystem *subsystem,
			 struct spdk_nvme_ctrlr *ctrlr, const struct spdk_pci_addr *pci_addr)
{
	subsystem->dev.direct.ctrlr = ctrlr;
	subsystem->dev.direct.pci_addr = *pci_addr;
	// Assume that all I/O will be handled on one thread for now
	subsystem->dev.direct.io_qpair = spdk_nvme_ctrlr_alloc_io_qpair(ctrlr, 0);
	if (subsystem->dev.direct.io_qpair == NULL) {
		DERROR("spdk_nvme_ctrlr_alloc_io_qpair() failed\n");
		return -1;
	}
	return 0;
}
**/

static void nvmf_update_discovery_log(void)
{
	uint64_t numrec = 0;
	struct lich_nvmf_subsystem *subsystem;
	struct lich_nvmf_listen_addr *listen_addr;
	struct spdk_nvmf_discovery_log_page_entry *entry;
	const struct lich_nvmf_transport *transport;
	struct spdk_nvmf_discovery_log_page *disc_log;
	size_t cur_size;
        struct list_head *pos, *tmp;
        struct list_head *listen_pos, *listen_tmp;


	cur_size = sizeof(struct spdk_nvmf_discovery_log_page);
	disc_log = calloc(1, cur_size);
	if (disc_log == NULL) {
		DERROR("Discovery log page memory allocation error\n");
		return;
	}

        list_for_each_safe(pos, tmp, &g_subsystems) {
                subsystem = list_entry(pos, struct lich_nvmf_subsystem, entries);
		if (subsystem->subtype == SPDK_NVMF_SUBTYPE_DISCOVERY) {
			continue;
		}

                nvmf_ns_t *ns = NULL;
                ns = core_map_get_ns(subsystem->lcore);
                if (ns != NULL) {
                        DWARN("======= subsystem nqn:%s, core[%d] nsCount:%lu.\n",
                                        subsystem->subnqn, subsystem->lcore, ns->count);
                        lich_nvmf_subsystem_add_ns(subsystem, ns);
                } else {
                        DWARN("======= subsystem nqn:%s core[%d] don't has volume.\n",
                                        subsystem->subnqn, subsystem->lcore);
                        continue;
                }

                list_for_each_safe(listen_pos, listen_tmp, &subsystem->listen_addrs) {
                        listen_addr = list_entry(listen_pos, struct lich_nvmf_listen_addr, link);
			size_t new_size = cur_size + sizeof(*entry);
			void *new_log_page = realloc(disc_log, new_size);

			if (new_log_page == NULL) {
				DERROR("Discovery log page memory allocation error\n");
				break;
			}

			disc_log = new_log_page;
			cur_size = new_size;

			entry = &disc_log->entries[numrec];
			memset(entry, 0, sizeof(*entry));
			entry->portid = numrec;
			entry->cntlid = 0xffff;
			entry->asqsz = g_nvmf_tgt.max_queue_depth;
			entry->subtype = subsystem->subtype;
			snprintf((char *)entry->subnqn, sizeof(entry->subnqn), "%s", (char *)subsystem->subnqn);

			transport = lich_nvmf_transport_get(listen_addr->trname);
			assert(transport != NULL);

			transport->listen_addr_discover(listen_addr, entry);

			numrec++;
		}
	}

	disc_log->numrec = numrec;
	disc_log->genctr = g_discovery_genctr;

	free(g_discovery_log_page);

	g_discovery_log_page = disc_log;
	g_discovery_log_page_size = cur_size;
}

void lich_nvmf_get_discovery_log_page(void *buffer, uint64_t offset, uint32_t length)
{
	size_t copy_len = 0;
	size_t zero_len = length;

#if 0
	if (g_discovery_log_page == NULL ||
	    g_discovery_log_page->genctr != g_discovery_genctr) {
		nvmf_update_discovery_log();
	}
#else
        /**
         * 每个core对应一个subsystem,当本core上没有
         * volume时,discovery不显示该core对应的subsystem
         */
        nvmf_update_discovery_log();
#endif

	/* Copy the valid part of the discovery log page, if any */
	if (g_discovery_log_page && offset < g_discovery_log_page_size) {
		copy_len = nvmf_min(g_discovery_log_page_size - offset, length);
		zero_len -= copy_len;
		memcpy(buffer, (char *)g_discovery_log_page + offset, copy_len);
	}

	/* Zero out the rest of the buffer */
	if (zero_len) {
		memset((char *)buffer + copy_len, 0, zero_len);
	}

	/* We should have copied or zeroed every byte of the output buffer. */
	assert(copy_len + zero_len == length);
}

int lich_nvmf_subsystem_add_ns(struct lich_nvmf_subsystem *subsystem, nvmf_ns_t *ns)
{
	assert(subsystem->mode == NVMF_SUBSYSTEM_MODE_VIRTUAL);

	subsystem->dev.ns_list = ns->info;
	subsystem->dev.ns_count = ns->count;
	return 0;
}

int lich_nvmf_subsystem_set_sn(struct lich_nvmf_subsystem *subsystem, const char *sn)
{
	if (subsystem->mode != NVMF_SUBSYSTEM_MODE_VIRTUAL) {
		return -1;
	}

	snprintf(subsystem->dev.sn, sizeof(subsystem->dev.sn), "%s", sn);

	return 0;
}

const char * lich_nvmf_subsystem_get_nqn(struct lich_nvmf_subsystem *subsystem)
{
	return subsystem->subnqn;
}

/* Workaround for astyle formatting bug */
typedef enum spdk_nvmf_subtype nvmf_subtype_t;

nvmf_subtype_t lich_nvmf_subsystem_get_type(struct lich_nvmf_subsystem *subsystem)
{
	return subsystem->subtype;
}

/* Workaround for astyle formatting bug */
typedef enum lich_nvmf_subsystem_mode nvmf_mode_t;

nvmf_mode_t lich_nvmf_subsystem_get_mode(struct lich_nvmf_subsystem *subsystem)
{
	return subsystem->mode;
}
