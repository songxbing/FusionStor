# nvme-cli
NVM-Express user space tooling for Linux.


Two installation methods:

[1]. Source code installation

    # git clone https://github.com/linux-nvme/nvme-cli.git

    # cd nvme-cli && make && make install

[2]. Distro Support

### Centos

    # yum install -y nvme-cli.x86_64

### Fedora

nvme-cli is available in Fedora 23 and up.  Install it with your favorite
package manager.  For example:

    $ sudo dnf install nvme-cli

### Ubuntu

nvme-cli is supported in the Universe package sources for Xenial for
many architectures. For a complete list try running:
  ```
  rmadison nvme-cli
   nvme-cli | 0.3-1 | xenial/universe | source, amd64, arm64, armhf, i386, powerpc, ppc64el, s390x
  ```
A Debian based package for nvme-cli is currently maintained as a
Ubuntu PPA. Right now there is support for Trusty, Vivid and Wiley. To
install nvme-cli using this approach please perform the following
steps:
   1. Add the sbates PPA to your sources. One way to do this is to run
   ```
   sudo add-apt-repository ppa:sbates
   ```
   2. Perform an update of your repository list:
   ```
   sudo apt-get update
   ```
   3. Get nvme-cli!
   ```
   sudo apt-get install nvme-cli
   ```
   4. Test the code.
   ```
   sudo nvme list
   ```
   In the case of no NVMe devices you will see
   ```
   No NVMe devices detected.
   ```
   otherwise you will see information about each NVMe device installed
   in the system.



### If using MLNX_OFED, make sure to install driver with the --add-kernel-support and --with-nvmf flags.

    #./mlnxofedinstall --add-kernel-support --with-nvmf

On the target server, load nvmet and nvmet-rdma kernel modules.

    # modprobe nvmet

    # modprobe nvmet-rdma

    # modprobe nvme-rdma

On the client server, load nvme-rdma kernel module.

    # rmmod nvme

    # modprobe nvme-rdma



If not sure how to use, find the top-level documentation with:

    # man nvme

Or find a short summary with:

    # nvme help

### For example:

Discover available subsystems on NVMF target:

    # nvme discover -t rdma -a 192.168.16.122 -s 10060

    Discovery Log Number of Records 1, Generation counter 1

    =====Discovery Log Entry 0======

    trtype:  rdma

    adrfam:  ipv4

    subtype: nvme subsystem

    treq:    not specified

    portid:  1

    trsvcid: 4420

     
    subnqn:  nvme-subsystem-name

    traddr:  1.1.1.1

     
    rdma_prtype: not specified

    rdma_qptype: connected

    rdma_cms:    rdma-cm

    rdma_pkey: 0x0000

Connect to the discovered subsystems using the command:

    # nvme connect -t rdma -n nvme-subsystem-name -a 1.1.1.1 -s 4420

List nvme device info:

    # nvme list

    Node             SN                   Model                                    Namespace Usage                      Format           FW Rev  
    ---------------- -------------------- ---------------------------------------- --------- -------------------------- ---------------- --------
    /dev/nvme0n1                          LICH Volume Controller                   1          16.11  GB /  16.11  GB    512   B +  0 B   FFFFFFFF
    /dev/nvme1n1                          LICH Volume Controller                   1           5.37  GB /   5.37  GB    512   B +  0 B   FFFFFFFF
    /dev/nvme1n2                          LICH Volume Controller                   2          16.11  GB /  16.11  GB    512   B +  0 B   FFFFFFFF

In order to disconnect from the target run the nvme disconnect command:

    # nvme disconnect -d /dev/nvme0n1
