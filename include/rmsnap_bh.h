#ifndef __RMSNAP_BH_H__
#define __RMSNAP_BH_H__

int rmsnap_bh_root(const char *pool, fileid_t *rootid);
int rmsnap_bh_create(const char *pool, const fileid_t *fileid, const char *snap);
int rmsnap_bh_remove(const char *pool, const fileid_t *fileid, const char *snap);
int rmsnap_bh_init();
int rmsnap_bh_start();
void rmsnap_bh_stop();

// void rmsnap_bh_file(int signo);

#endif
