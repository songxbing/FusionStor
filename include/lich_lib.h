#ifndef __LICH_LIB_H__
#define __LICH_LIB_H__

int path_decode(const char *path, char *pool, char *protocol, char *vol, char *snap);
int path_head(const char *path, int sep, char *head, char **path2);
int path_split(const char *path, char *pool, char *path2);

#endif /* __LICH_LIB_H__ */
