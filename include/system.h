#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#include "sysy_conf.h"
#include "list.h"
#include "adt.h"

typedef struct {
        struct list_head hook;
        char pool[MAX_NAME_LEN];
} pool_name_t;

int system_init();

int system_set_createtime();
int system_get_createtime(time_t *createtime);

int system_pool_iterator(func_int1_t func, void *arg);
int system_pool_iterator1(func_int1_t func, void *arg);
int system_pool_count(int *count);

int system_pool_list(struct list_head *list);
int system_pool_list_exists(struct list_head *list, const char *name);

int system_pool_find(const char *pool, int *_find);
int system_pool_add(const char *name);
int system_pool_del(const char *name);
int system_get_pool_info(char *name, void *chkinfo);

int system_get_version(uint32_t *version);
int system_set_version(uint32_t version);
int system_tryset_version();

int etcd_task_iterator(const char *name, func_int1_t func, void *arg);

#endif
