#ifndef __CONSTDEF_H
#define __CONSTDEF_H

#define BYTES_PER_KB 1024
#define BYTES_PER_MB 1048576

#endif
