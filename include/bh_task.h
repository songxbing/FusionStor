#ifndef __BH_TASK_H
#define __BH_TASK_H

#include "worker.h"
#include "lich_id.h"

/**
 * 冲突的后台任务:
 * 1.　删除卷 :  与任何后台任务冲突
 * 2.  flat　: 与回滚冲突吗?, rmvol
 * 3.  rollback : flat?, rmvol, rmsnap
 * 4.　rmsnap : rollback, rmvol
 */

typedef enum {
        BH_TASK_FREE    = 0,
        BH_TASK_RUNNING = 1,
} bh_task_status_t;

typedef int (*bh_task_func)(const chkid_t *chkid, const char *name);

typedef struct {
        char name[MAX_NAME_LEN];
        char root[MAX_NAME_LEN];
        worker_handler_t worker_handler;
        uint64_t interval;
        bh_task_func func;
        int inited;
} bh_task_t;

// extern bh_task_t volume_cleanup_bh_task;
// extern bh_task_t rollback_bh_task;
// extern bh_task_t flat_bh_task;

/*
int rmvol_bh_start();
int rollback_bh_start();
int flat_bh_start();
int bh_task_start(bh_task_t *task);

*/

int bh_task_create(const char *pool, const char *root, const fileid_t *fileid);
int bh_task_remove(const char *pool, const char *root, const fileid_t *fileid);

// int bh_task_root(const char *pool, const char *root, fileid_t *rootid);

// void rollback_bh_file(int signo);

int etcd_task_start();
void etcd_task_stop();

int bh_task_start();
void bh_task_stop();


#endif
