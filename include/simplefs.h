#ifndef __SIMPLEFS_H__
#define __SIMPLEFS_H__

#include <sys/statvfs.h>

#include "storage.h"
#include "lichstor.h"

int pfs_init();
int pfs_open(const char *path, int flag, mode_t mode);
int pfs_create(const char *path, mode_t mode);
int pfs_pwrite(int fd, const void *buf, size_t size, off_t off);
int pfs_pwrite_with_path(const char *path, const void *buf, size_t size, off_t off);
int pfs_pread(int fd, void *buf, size_t size, off_t off);
int pfs_pread_with_path(const char *path, void *buf, size_t size, off_t off);
int pfs_close(int fd);
int pfs_getattr(const char *path, struct stat *stbuf);
int pfs_readdir(const char *path, off_t offset, void **_de, int *_delen);
int pfs_mkdir(const char *path, mode_t mode);
int pfs_unlink(const char *path);
int pfs_rmdir(const char *path);
int pfs_chmod(const char *path, mode_t mode);
int pfs_chown(const char *path, uid_t uid, gid_t gid);
int pfs_truncate(const char *path, off_t size);
int pfs_statfs(const char *path, struct statvfs *stbuf);
int pfs_utimens(const char *path, const time_t atime, const time_t mtime, const time_t ctime);


int pfs_access(const char *path, int mask);
int pfs_readlink(const char *path, char *buf, size_t size);
int pfs_mknod(const char *path, mode_t mode, dev_t rdev);
int pfs_symlink(const char *from, const char *to);
int pfs_rename(const char *from, const char *to);
int pfs_link(const char *from, const char *to);
#endif
