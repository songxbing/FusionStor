#ifndef __RMVOL_BH_H__
#define __RMVOL_BH_H__

int rmvol_bh_root(const char *pool, fileid_t *rootid);
int rmvol_bh_init();
int rmvol_bh_start();
void rmvol_bh_file(int signo);

#endif
