#ifndef __PROF_H__
#define __PROF_H__

int prof_rpc_net(const diskid_t *nid, int send, int recv);
int prof_rpc_direct(int sd, int sendsize, int recvsize);
int prof_net(const char *srv, int threads, int runtime);
int prof_vm(const char *srv, int threads, int runtime);
int prof_dio(const char *srv, int threads, int runtime);
int prof_lichbd(int threads, int size, int runtime, char mode, const char *volume);
int prof_vm_init();
int prof_rpc_init();
int prof_net_init();
int prof_dio_init();

#endif
