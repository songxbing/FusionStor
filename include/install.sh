#!/bin/bash

usage()
{
    echo "Usage: $0 [OPTIONS]"
    echo "-i install [path]"
    echo "-e remove [path]"

    exit 1
}

install()
{
    DIST=$1/include

    rm -rf $DIST
    mkdir -p $DIST

    cp lichbd.h $DIST
    cp lich_id.h $DIST
    cp lichstor.h $DIST
    cp lich_lib.h $DIST
}

remove()
{
    echo "do nothing"
}

if [ $# -lt 1 ]
then
    usage
fi

while getopts ieh options
do
    case $options in
        i)
        echo "install scripts $2"
        install $2
        ;;
        e)
        echo "remove scripts"
        remove
        ;;
        h)
        usage
        ;;
        \?)
        usage
        ;;
    esac
done
