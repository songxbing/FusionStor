#ifndef __FS_PROTO__
#define __FS_PROTO__

#include "sysy_conf.h"
//#include <rpc/types.h>
//@RFC1813

#include "ec.h"

#if LSV
#define SET_PHYSICAL_SIZE 0x00000001
#define SET_LOGICAL_SIZE  0x00000002
#endif

typedef enum {
        __NOT_SET_SIZE = 0,
        __SET_PHYSICAL_SIZE,
        __SET_LOGICAL_SIZE,
} __size_how;

typedef struct {
        // = 0x01: set physical size
        // = 0x02: set logical size
        __size_how set_it;
        uint64_t size;
} __set_size;

typedef enum {
        __DONT_CHANGE = 0,
        __SET_TO_SERVER_TIME,
        __SET_TO_CLIENT_TIME,
} __time_how;

typedef struct {
        uint32_t seconds;
        uint32_t nseconds;
} __time;

typedef struct {
        __time_how set_it;
        __time time;
} __set_time;

typedef struct {
        int set_it;
        uint32_t val;
} __set_u32;

typedef __set_u32 __set_mode;
typedef __set_u32 __set_uid;
typedef __set_u32 __set_gid;
typedef __set_u32 __set_replica;
typedef __set_u32 __set_clone;
typedef __set_u32 __set_protect;
typedef __set_u32 __set_priority;
typedef __set_u32 __set_localize;
typedef __set_u32 __set_multpath;
typedef __set_u32 __set_writeback;
typedef __set_u32 __set_readraw;
typedef __set_u32 __set_del;

typedef struct {
        int set_it;
        ec_t ec;
} __set_ec;

#if LSV
typedef __set_u32 __set_lsv;

typedef struct {
        int set_it;
        uint64_t source;
        char snap[128];
} __set_lsv_clone;

#endif

typedef struct {
        __set_mode mode;
        __set_uid uid;
        __set_gid gid;
        __set_size size;
        __set_time atime;
        __set_time btime;
        __set_time ctime;
        __set_time mtime;
        __set_replica replica;
        __set_clone clone;
        __set_protect protect;
        __set_priority priority;
        __set_localize localize;
        __set_multpath multpath;
        __set_writeback writeback;
        __set_readraw readraw;
        __set_del deleting;
        __set_ec ec;
#if LSV
        __set_lsv lsv;
        __set_lsv_clone lsv_clone;
#endif
} setattr_t;

#endif
