#ifndef __LICHBD_CACHE_H__
#define __LICHBD_CACHE_H__

#include "config.h"
#include "buffer.h"
#include "sysy_lib.h"

typedef struct {
        //int fd;
        uint64_t hit;
        char cached;
        char loading;
        uint64_t clock0;
        uint64_t clock1;
        time_t begin;
        time_t last_update;
        struct list_head wlist;
} cache_chunk_t;

typedef struct {
        char name[MAX_PATH_LEN];
        int loading;
        uint64_t edge;
        time_t last_check;
        time_t last_load;
        uint64_t count;
        cache_chunk_t *chunk;
} lichbd_cache_t;

int lichbd_cache_init(const char *path);
int lichbd_cache_create(lichbd_cache_t **_cache, const char *name, uint64_t size);
void lichbd_cache_check(lichbd_cache_t *cache);
int lichbd_cache_write(lichbd_cache_t *cache, const buffer_t *buf, size_t size, off_t offset);
int lichbd_cache_read(lichbd_cache_t *cache, buffer_t *buf, size_t size, off_t offset);
void lichbd_cache_destroy(lichbd_cache_t *cache);

#endif
