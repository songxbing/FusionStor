#ifndef __TRANSACTION_H
#define __TRANSACTION_H

#include "lich_id.h"

#define TRANS_REDO_MAX 100

typedef enum {
        TRANS_CREATE_SNAPSHOT = 1,
} trans_op_t;

typedef struct {
        trans_op_t op;
        uint32_t redo; //redo次数，不能超过TRANS_REDO_MAX
        chkid_t chkid;
} trans_base_t;

typedef struct {
        trans_base_t base;
        char key[MAX_NAME_LEN];
        int is_new;
        char buf[0];
} trans_context_t;

typedef struct {
        trans_base_t base;
        char snap[MAX_NAME_LEN];
        char site[MAX_NAME_LEN];
        int priority;

        // context
        uint64_t snap_from;
        uint64_t snap_version;
} trans_create_snapshot_t;

int trans_begin(trans_context_t **ctx, const void *new_trans, size_t len);
int trans_commit(trans_context_t *ctx);
int trans_abort(const char *key);

int trans_redo(void *_volume_proto);
int trans_is_done(void *_volume_proto);


#endif
