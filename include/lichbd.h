#ifndef __LICHBD_H__
#define __LICHBD_H__

/*copy from ceph/librbd.h*/

#include <netinet/in.h>
#include <linux/types.h>
#include <string.h>
#include <lich_id.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

typedef struct {
        fileid_t fileid;
        void *vm;
} lichbd_ioctx_t;

typedef  chkid_t lichbd_image_t;
typedef void (*lichbd_func_t)(void *arg, void *retval);

void lichbd_write_async(const char *pool, const lichbd_image_t *id, const char *buf, size_t size,
                        off_t offset, lichbd_func_t func, void *arg);
void lichbd_read_async(const char *pool, const lichbd_image_t *id, char *buf, size_t size,
                       off_t offset, lichbd_func_t func, void *arg);
int lichbd_open(const char *pool, const char *name, lichbd_image_t *image);
int lichbd_create(const char *pool, const char *name, lichbd_image_t *image);
int lichbd_truncate(const char *pool, const lichbd_image_t *image, uint64_t size);
int lichbd_lookup(const lichbd_image_t *image, char *pool);
int lichbd_stat(const char *pool, const lichbd_image_t *image, struct stat *stbuf);
int lichbd_localize(const char *pool, const lichbd_image_t *image);
int lichbd_init(const char *root);
int lichbd_init_new(void);

void lichbd_write1(lichbd_ioctx_t *ioctx, const char *buf, size_t size, off_t offset, int flag,
                  lichbd_func_t func, void *arg);
void lichbd_read1(lichbd_ioctx_t *ioctx, char *buf, size_t size, off_t offset, int localize,
                 lichbd_func_t func, void *arg);

int lichbd_connect(const char *pool, const char *name, lichbd_ioctx_t *ioctx, int flag);
void lichbd_disconnect(lichbd_ioctx_t *ioctx);

int lichbd_pread1(lichbd_ioctx_t *ioctx, char *buf, size_t size, off_t offset, int localize);
int lichbd_pwrite1(lichbd_ioctx_t *ioctx, const char *buf, size_t size, off_t offset, int flag);

int lichbd_snap_flat(lichbd_ioctx_t *ioctx, int idx);
int lichbd_snap_pread(lichbd_ioctx_t *ioctx, const fileid_t *snapid, char *buf, size_t size, off_t offset);
int lichbd_snap_diff(lichbd_ioctx_t *ioctx, const fileid_t *snapsrc,
        const fileid_t *snapdst, char *buf, size_t size, off_t offset);

#endif
