#ifndef __LICHBD_RPC__
#define __LICHBD_RPC__

#include "vm.h"
#include "lichbd.h"

typedef struct {
        sockid_t sockid;
        void *cache;
        char pool[MAX_NAME_LEN];
        char path[0];
} rpc_vm_ctx_t;

int lichbd_srv_init();

int lichbd_rpc_init();
int lichbd_rpc_connect(sockid_t *sockid, const char *pool, const char *path);

int lichbd_rpc_read(buffer_t *_buf, size_t size, off_t offset, int localize);
int lichbd_rpc_write(const buffer_t *_buf, size_t size, off_t offset, int flag);

int lichbd_rpc_snap_flat(int idx);
int lichbd_rpc_snap_read(const fileid_t *snapid, buffer_t *_buf, size_t size, off_t offset);
int lichbd_rpc_snap_diff(const fileid_t *snapsrc,
        const fileid_t *snapdst, buffer_t *_buf, size_t size, off_t offset);

int lichbd_rpc_check();

int lichbd_rpc_reply(int *count);

#endif
