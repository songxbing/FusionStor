#ifndef __LICHBD_IO_H__
#define __LICHBD_IO_H__

#include "lichbd.h"

void lichbd_write(lichbd_ioctx_t *ioctx, const buffer_t *buf, size_t size, off_t offset, int flag,
                  lichbd_func_t func, void *arg);
void lichbd_read(lichbd_ioctx_t *ioctx, buffer_t *buf, size_t size, off_t offset, int localize,
                 lichbd_func_t func, void *arg);

int lichbd_pread(lichbd_ioctx_t *ioctx, buffer_t *buf, size_t size, off_t offset, int localize);
int lichbd_pwrite(lichbd_ioctx_t *ioctx, const buffer_t *buf, size_t size, off_t offset, int flag);

#endif
