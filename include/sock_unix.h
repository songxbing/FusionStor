#ifndef __SOCK_UNIX__
#define __SOCK_UNIX__

int sock_unix_listen(const char *path, int *_sd, struct sockaddr_un *addr);
int sock_unix_connect(const char *path, int *_sd, struct sockaddr_un *addr);
int sock_unix_tuning(int sd);

#endif
