#ifndef __FSROOT__
#define __FSROOT__

/**
 * @file /下所有pool的一级目录，组织成list的形式, 映射到其fileid.
 *
 * 如：
 * - /pool1/iscsi
 * - /pool1/unlink
 * - /pool1/rmsnap
 * - /pool1/rollback
 * - /pool1/flat
 */

typedef struct {
        struct list_head hook;
        char pool[MAX_NAME_LEN];
        char name[MAX_NAME_LEN];
        int inited;
        fileid_t rootid;
} root_entry_t;

typedef struct {
        sy_rwlock_t lock;
        struct list_head list;
} root_t;

int stor_root_init();

int stor_root(const char *pool, const char *name, fileid_t *rootid);

/**
 * @brief delete pool
 *
 * @param pool
 * @return
 */
int stor_root_del(const char *pool);

#endif
