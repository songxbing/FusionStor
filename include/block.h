#ifndef __BLOCK_H__
#define __BLOCK_H__

#include "yid.h"
#include "net_table.h"
#include "env.h"
#include "fileinfo.h"
#include "fs_proto.h"

int block_init();

int block_removebyid(const chkid_t *id);
int block_truncate(const fileid_t *id, uint64_t size);

int block_pwrite(const fileid_t *id, const char *_buf, size_t size, off_t offset);
int block_pread(const fileid_t *oid, char *_buf, int size, off_t offset);

int block_xattr_get(const nid_t *_nid, const chkid_t *chkid, const char *key,
                   char *value, int *valuelen);
int block_xattr_set(const nid_t *_nid, const chkid_t *chkid, const char *key,
                   char *value, int valuelen);
int block_xattr_get_extend(const nid_t *_nid, const chkid_t *chkid, const char *key,
                   char *name, char *password);
int block_reset(const char *pool, const chkid_t *id);
int block_getattr(const char *pool, const chkid_t *id, struct stat *stbuf);

int block_listpool_open(const fileid_t *fileid, const char *uuid);
int block_listpool(const fileid_t *fileid, const char *uuid, uint64_t offset, void *_de, int *_delen);
int block_listpool_close(const fileid_t *fileid, const char *uuid);

int block_lookup(const char *pool, const fileid_t *parent, const char *name, fileid_t *fileid);
int block_root(const char *pool, fileid_t *rootid);
int block_preload(const fileid_t *oid, int flag);

int block_check_ready(const fileid_t *oid); //volume  is ready, loading snapshots.
int block_reload(const fileid_t *oid);

/**
 * @brief for lich.inspect --connection
 *
 * @param fileid
 * @param addr
 * @return
 */
int block_connect(const char *pool, const volid_t *volid, const char *_addr, int port, const char *context);
int block_disconnect(const char *pool, const fileid_t *fileid, const char *addr, int port, const char *context);
//not use, replace with md_connection
int block_connection(const chkid_t *chkid, void *list, int *count);

int block_mkpool(const fileid_t *parent, const char *name,
                const setattr_t *_setattr, fileid_t *_fileid);
int block_rmdir(const char *pool, const fileid_t *parent, const char *name);

int block_create(const char *pool, const fileid_t *parent, const char *name, const setattr_t *_setattr,
                fileid_t *fid, uint64_t size);
int block_unlink(const fileid_t *parent, const char *name);

int block_lookup1(const char *pool, const char *path, fileid_t *id);

#endif
