#ifndef __LAYOUT_H__
#define __LAYOUT_H__

#include <limits.h>

/**
 * chunk distribution graph by Gabe:
 *
 *     pool:pool_name
 *     path:/protocol/volume
 *
 *     etcd:/lich4/storage/pool_name --> (chkinfo) --.
 *                 .---------------------------------|-----------------------------.
 *                 | (node1)                         | (node2)                     | (node3)
 *  +-----------------------------+  +-----------------------------+  +-----------------------------+
 *  | info area | bit map | items |  | info area | bit map | items |  | info area | bit map | items |    /  --> pool.x.0
 *  |           |         | | | | |  |           |         | | | | |  |           |         | | | | |
 *  +-----------------------------+  +-----------------------------+  +-----------------------------+
 *                         | (chkinfo)
 *              .----------|-----------------------------------------------------------.
 *              | (node1)  `-----------------------. (node2)                          | (node3)
 *  +-----------------------------+  +-----------------------------+  +-----------------------------+
 *  | info area | bit map | items |  | info area | bit map | items |  | info area | bit map | items |    --> subpool.x.0
 *  |           |         | | | | |  |           |         | | | | |  |           |         | | | | |
 *  +-----------------------------+  +-----------------------------+  +-----------------------------+
 *                         | (chkinfo)
 *               .---------|------------------------------------------------------------.
 *               | (node1) `------------------------. (node2)                          | (node3)
 *  +-----------------------------+  +-----------------------------+  +-----------------------------+
 *  | info area | bit map | items |  | info area | bit map | items |  | info area | bit map | items |    /protocol/  --> pool.xx.0
 *  |           |         | | | | |  |           |         | | | | |  |           |         | | | | |
 *  +-----------------------------+  +-----------------------------+  +-----------------------------+
 *                         | (chkinfo)
 *               .---------|------------------------------------------------------------.
 *               | (node1) `------------------------. (node2)                          | (node3)
 *  +-----------------------------+  +-----------------------------+  +-----------------------------+
 *  | info area | bit map | items |  | info area | bit map | items |  | info area | bit map | items |    --> subpool.xx.0
 *  |           |         | | | | |  |           |         | | | | |  |           |         | | | | |
 *  +-----------------------------+  +-----------------------------+  +-----------------------------+
 *                         | (chkinfo)
 *               .---------|------------------------------------------------------------.
 *               | (node1) `------------------------. (node2)                          | (node3)
 *  +-----------------------------+  +-----------------------------+  +-----------------------------+
 *  | info area | bit map | items |  | info area | bit map | items |  | info area | bit map | items |    /protocol/volume  --> vol.x.0
 *  |           |         | | | | |  |           |         | | | | |  |           |         | | | | |
 *  +-----------------------------+  +-----------------------------+  +-----------------------------+
 *                         | (chkinfo)
 *               .---------|------------------------------------------------------------.
 *               | (node1) `------------------------. (node2)                          | (node3)
 *  +-----------------------------+  +-----------------------------+  +-----------------------------+
 *  | info area | bit map | items |  | info area | bit map | items |  | info area | bit map | items |    --> subvol.x.0
 *  |           |         | | | | |  |           |         | | | | |  |           |         | | | | |
 *  +-----------------------------+  +-----------------------------+  +-----------------------------+
 *                         | (chkinfo)
 *               .---------`-------------------------.
 *               | (node1)                           | (node2)
 *  +-----------------------------+  +-----------------------------+
 *  |           user data         |  |           user data         |  raw.x.0
 *  |                             |  |                             |
 *  +-----------------------------+  +-----------------------------+
 */

/*
  table layout by Gabe
  --------------------------------------------------------------------------------------------------------------------------------------------
  |     info area      |               bitmap area size (8192)                    |                 item area size (1024000)                 |
  | (512 * 32 = 16384) |                                                          |                 item (16000 * 64 == 1024000)             |
  |                    |----------------------------------------------------------|----------------------------------------------------------|
  |                    |          map area          |  xmap area   |  snap area   |            ITEM            |    XATTR     |     SNAP     |
  |                    |         item (8000)        |  item (4000) |  item (4000) |         item (8000)        |  item (500)  |  item (500)  |
  |                    |     8000 * 1bit < 4096     |4000*1bit<2048|4000*1bit<2048|     8000 * 64 = 512000     |500*512=256000|500*512=256000|
  --------------------------------------------------------------------------------------------------------------------------------------------
*/

/*layout size*/
#define TABLE_PROTO_ITEM_SIZE 512

#define TABLE_PROTO_INFO_AREA (16384)
#define TABLE_PROTO_MAP_ALL  (8192)
#define TABLE_PROTO_MAP_AREA  (TABLE_PROTO_MAP_ALL / 2) /* 4096 */
#define TABLE_PROTO_XMAP_AREA (TABLE_PROTO_MAP_ALL / 4) /* 2048 */
#define TABLE_PROTO_SMAP_AREA (TABLE_PROTO_MAP_ALL / 4) /* 2048*/
#define TABLE_PROTO_ITEM_ALL  (LICH_CHUNK_SPLIT - TABLE_PROTO_INFO_AREA - TABLE_PROTO_MAP_ALL) /* 1024000 */
#define TABLE_PROTO_ITEM_AREA  (TABLE_PROTO_ITEM_ALL / 2) /* 512000 */
#define TABLE_PROTO_XATTR_AREA (TABLE_PROTO_ITEM_ALL / 4) /* 256000 */
#define TABLE_PROTO_SNAP_AREA  (TABLE_PROTO_ITEM_ALL / 4) /* 256000 */

/*layout offset*/
#define TABLE_PROTO_INFO (0)
#define TABLE_PROTO_MAP (TABLE_PROTO_INFO + TABLE_PROTO_INFO_AREA)
#define TABLE_PROTO_XMAP (TABLE_PROTO_MAP + TABLE_PROTO_MAP_AREA)
#define TABLE_PROTO_SMAP (TABLE_PROTO_XMAP + TABLE_PROTO_XMAP_AREA)
#define TABLE_PROTO_ITEM (TABLE_PROTO_SMAP + TABLE_PROTO_SMAP_AREA )
#define TABLE_PROTO_XATTR (TABLE_PROTO_ITEM +  TABLE_PROTO_ITEM_AREA)
#define TABLE_PROTO_SNAP (TABLE_PROTO_XATTR +  TABLE_PROTO_XATTR_AREA)

#define TABLE_PRORO_XATTR_ITEM_SIZE TABLE_PROTO_ITEM_SIZE /* 512 */
#define TABLE_PRORO_XATTR_ITEM_TOTAL (TABLE_PROTO_XATTR_AREA / TABLE_PRORO_XATTR_ITEM_SIZE) /* 256000 / 512 = 500 */
#define TABLE_PRORO_XATTR_ITEM_COUNT (TABLE_PRORO_XATTR_ITEM_TOTAL / CHAR_BIT * CHAR_BIT)     /* round numbers */

#define TABLE_PRORO_SNAP_ITEM_SIZE TABLE_PROTO_ITEM_SIZE /* 512 */
#define TABLE_PRORO_SNAP_ITEM_TOTAL (TABLE_PROTO_SNAP_AREA / TABLE_PRORO_SNAP_ITEM_SIZE) /* 256000 / 512 = 500 */
#define TABLE_PRORO_SNAP_ITEM_COUNT (TABLE_PRORO_SNAP_ITEM_TOTAL / CHAR_BIT * CHAR_BIT)     /* round numbers */


#define TABLE_PROTO_INFO_MAX (TABLE_PROTO_INFO_AREA / TABLE_PROTO_ITEM_SIZE) /* 16384 / 512 = 32 */

#define TABLE_PROTO_HEAD (TABLE_PROTO_INFO_AREA + TABLE_PROTO_MAP_ALL) /* 16384 + 8192 = 24576 */

/*

dir table layout
  -----------------------------------------------------------------------------------------------------------------------------------------------------
  | info area, size(512) | extern area, size (5632), record (10 * 512byte < 5632) | map area size (2048), record (2000 * 1bit < 2048) | dir data | ...
  -----------------------------------------------------------------------------------------------------------------------------------------------------

*/

#define DIR_PROTO_MAP_AREA (2048)
#define DIR_PROTO_INFO_AREA (512)
#define DIR_PROTO_EXTERN_AREA (5632)
#define DIR_PROTO_REC_SIZE (128)
#define DIR_PROTO_REC_COUNT (2000)
#define DIR_PROTO_EXTERN_COUNT (10)
#define DIR_PROTO_MAP_SIZE (DIR_PROTO_REC_COUNT / CHAR_BIT)

// pool chunk layout, use 512K (map area)
#define DIR_PROTO_ITEM_SIZE (512 / CHAR_BIT)
#define DIR_PROTO_ITEM_COUNT (2000 * CHAR_BIT / 2)

// subpool chunk layout, use 1000K
#define DIR_PROTO_EXTERN_ITEM_SIZE (512)
#define DIR_PROTO_NAME_SIZE (DIR_PROTO_EXTERN_ITEM_SIZE / 4)
#define DIR_PROTO_EXTERN_ITEM_COUNT (2000)

// volume chunk layout
#define FILE_PROTO_ITEM_SIZE         (TABLE_PROTO_ITEM_SIZE / CHAR_BIT) /* 512 / 8 = 64 */
#define FILE_PROTO_ITEM_COUNT        (TABLE_PROTO_ITEM_AREA / FILE_PROTO_ITEM_SIZE) /* 512000 / 64 = 8000 */

#define FILE_PROTO_EXTERN_ITEM_SIZE  (TABLE_PROTO_ITEM_SIZE / CHAR_BIT) /* 512 / 8 = 64 */
#define FILE_PROTO_EXTERN_ITEM_COUNT (TABLE_PROTO_ITEM_ALL / FILE_PROTO_EXTERN_ITEM_SIZE) /* 1024000 / 64 = 16000 */
#define FILE_PROTO_EXTERN_ITEM_BIT   (FILE_PROTO_EXTERN_ITEM_COUNT / CHAR_BIT) /* 16000 / 8 = 2000 */


#endif
