#ifndef __LICH_ID_H__
#define __LICH_ID_H__

#include <stdint.h>

#pragma pack(8)

typedef struct {
        uint64_t id;
        uint32_t type;
        uint32_t idx;
} chkid_t;

#pragma pack()

typedef chkid_t fileid_t;
typedef chkid_t volid_t;
typedef chkid_t poolid_t;

typedef struct __task_t {
        int16_t taskid;
        int16_t scheduleid;
        uint32_t fingerprint;
        //void *schedule;
} task_t;

#endif
