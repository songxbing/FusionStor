#ifndef __NODECTL_H__
#define __NODECTL_H__

#include <sys/epoll.h>
#include <semaphore.h>
#include <libaio.h> 
#include <pthread.h>

#include "fnotify.h"
#include "ylock.h"

typedef struct __nodectl_file_t {
        char path[MAX_PATH_LEN];
        int magic;

        // ops
        int (*set)(struct __nodectl_file_t *nf, const char *value);

        int (*get)(struct __nodectl_file_t *nf, char *value, const char *_default);
        int (*get_int)(struct __nodectl_file_t *nf, int _default);
        uint64_t (*get_long)(struct __nodectl_file_t *nf, uint64_t _default);

        // NOT use rename method
        int (*set2)(struct __nodectl_file_t *nf, const char *value);

        void (*unlink)(struct __nodectl_file_t *nf);

        int (*_register)(struct __nodectl_file_t *nf, const char  *_default,
                         fnotify_callback mod_callback,
                         fnotify_callback del_callback,
                         void *context);
        int (*unregister)(struct __nodectl_file_t *nf);
} nodectl_file_t;

int nodectl_file_init(nodectl_file_t *nf, const char *prefix, const char *key);
int opt_data_init(nodectl_file_t *nf, const char *key);
int opt_nodectl_init(nodectl_file_t *nf, const char *key);

typedef struct __nodectl_option_t {
        sy_rwlock_t lock;
        int started;

        char prefix[MAX_PATH_LEN];
        char key[MAX_PATH_LEN];

        nodectl_file_t nf;
        fnotify_callback mod_cb;

        char dv[MAX_NAME_LEN];
        int type;
        int value;
        int _min;
        int _max;

        // ops
        int (*start)(struct __nodectl_option_t *opt, const char *key, const char *dv, fnotify_callback mod_cb);
} nodectl_option_t;

// int nodectl_option_start(nodectl_option_t *opt, const char *key, const char *dv);
int nodectl_option_init(nodectl_option_t *opt, const char *prefix);

int opt_data_init2(nodectl_option_t *opt);
int opt_nodectl_init2(nodectl_option_t *opt);

int nodectl_init();
int diskctl_init();

// from nodectl.c
int nodectl_get(const char *key, char *value, const char *_default);
int nodectl_get_int(const char *key, int _default);

int nodectl_set(const char *key, const char *value);

// NOT use rename method
int nodectl_set2(const char *key, const char *value);

void nodectl_unlink(const char *key);

int nodectl_register(const char *key, const char  *_default, fnotify_callback mod_callback,
                     fnotify_callback del_callback, void *context);
int nodectl_unregister(const char *key);

int opt_config_register(const char *key, const char  *_default, fnotify_callback mod_callback,
                        fnotify_callback del_callback, void *context);

#endif
