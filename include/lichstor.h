#ifndef __LICHFS_H__
#define __LICHFS_H__

#include <sys/statvfs.h>

//#include "storage.h"
#include "fs_proto.h"
#include "fileinfo.h"
#include "yid.h"
#include "ec.h"

#define FAKE_BLOCK 4096
#define MAX_READDIR_ENTRIES 100

typedef struct __vol_param {
        char name[MAX_NAME_LEN];
        char site[MAX_NAME_LEN];
        int volume_format;
        uint64_t size;
        uint32_t repnum;
        int priority;
        setattr_t *setattr;
} vol_param_t;

static inline void vol_param_init(vol_param_t *param) {
        memset(param, 0, sizeof(vol_param_t));
        param->priority = -1;
        param->size = 0;
}

typedef struct __snap_clone_param {
        char vol_name[MAX_NAME_LEN];
        char snap_name[MAX_NAME_LEN];
        char to_vol_name[MAX_NAME_LEN];
        char site[MAX_NAME_LEN];
        int priority;
        //
        fileid_t vol_fileid;
        fileinfo_t vol_fileinfo;
        int volume_format;
        //
        fileid_t snap_fileid;
        fileinfo_t snap_fileinfo;

        //
        struct stat stbuf;
} snap_clone_param_t;

static inline void snap_clone_param_init(snap_clone_param_t *param) {
        memset(param, 0, sizeof(snap_clone_param_t));
        param->priority = -1;
}

typedef struct __snap_protect_param {
        char snap_name[MAX_NAME_LEN];
        int on;
} snap_protect_param_t;

static inline void snap_protect_param_init(snap_protect_param_t *param) {
        memset(param, 0, sizeof(snap_protect_param_t));
        param->on = -1;
}

// -- context
int stor_init(const char *home, uint64_t  max_object);
void stor_destroy();

// -- common
int stor_lookup(const char *pool, const fileid_t *parent, const char *name, fileid_t *id);
int stor_lookup1(const char *pool, const char *path, fileid_t *id);

// include snapshot
int stor_lookup1x(const char *pool, const char *path, fileid_t *id);

int stor_splitpath(const char *pool, const char *_path, fileid_t *parent, char *name);

int stor_getpool(const fileid_t *id, char *pool);
int stor_reset(const char *pool, const chkid_t *id);
int stor_getattr(const char *pool, const fileid_t *id, struct stat *stbuf);
int stor_getsnapversion(const char *pool, const fileid_t *id, uint64_t *snap_version);

// -- pool
int stor_listpool_open(const fileid_t *id, const char *uuid);
int stor_listpool(const fileid_t *fileid, const char *uuid, uint64_t offset, void **_de, int *_delen);
int stor_listpool_close(const fileid_t *id, const char *uuid);
int stor_listpoolplus(const fileid_t *id, off_t offset, void **de, int *delen);

int stor_mkpool(const fileid_t *parent, const char *name, const setattr_t *_setattr, fileid_t *_id);
int stor_mkpool_with_area(const fileid_t *parent, const char *name, ec_t *ec,
                const char *site_name, const setattr_t *_setattr, fileid_t *_fileid);
int stor_rmpool(const char *pool, const fileid_t *parent, const char *name);

// -- volume
int stor_mkvol_with_area(fileid_t *_fileid, const fileid_t *parent, vol_param_t *param);
int stor_mkvol(const fileid_t *parent, const char *name, const setattr_t *_setattr, fileid_t *_id);
int stor_mkvolfrom(const fileid_t *rootid, const char *path, fileid_t *id);
int stor_mkvol_sync2master(const char *name, const chkid_t *chkid);

int stor_rmvol(const fileid_t *parent, const char *name, int force);
int stor_rmvol_sync2master(const char *name, const chkid_t *chkid);

int stor_chmod(const char *pool, const fileid_t *id, mode_t mode);
int stor_chown(const char *pool, const fileid_t *id, uid_t owner, gid_t group);
int stor_truncate(const char *pool, const fileid_t *id, uint64_t length);
int stor_rename(const char *pool, const fileid_t *fromdir, const char *fromname,
                const fileid_t *todir, const char *toname);
int stor_utime(const char *pool, const fileid_t *id, uint32_t atime, uint32_t mtime);

int stor_openfrom(const fileid_t *rootid, const char *path, fileid_t *id);
int stor_set_repnum(const char *pool, const fileid_t *id, int repnum);
int stor_localize(const char *pool, const fileid_t *fileid);

int stor_statvfs(const char *pool, const fileid_t *id, struct statvfs *vfs);
int stor_isempty(const fileid_t *fileid, int *empty);
int stor_get_location(const char *pool, const fileid_t *fileid, char *buf, diskid_t *_diskid);

int stor_stat(const char *pool, const fileid_t *id, char *buf, int verbose);
int stor_stat_file(const char *pool, const fileid_t *id);
int stor_location(const char *pool, const fileid_t *id, nid_t *nid);

// IO
int stor_write(const char *pool, const fileid_t *id, const buffer_t *_buf, size_t size, off_t offset);
int stor_read(const char *pool, const fileid_t *id, buffer_t *_buf, size_t _size, off_t offset);

int stor_write_withnid(const char *pool, const fileid_t *id, const buffer_t *_buf, size_t size, off_t offset, nid_t *nid);
int stor_read_withnid(const char *pool, const fileid_t *id, buffer_t *_buf, size_t _size, off_t offset, nid_t *nid);

int stor_write_remote(const volid_t *volid, const buffer_t *buf, size_t size, off_t offset);
int stor_read_remote(const volid_t *volid, const buffer_t *buf, size_t size, off_t offset);

int stor_pwrite(const char *pool, const fileid_t *id, const char *_buf, size_t size, off_t offset);
int stor_pread(const char *pool, const fileid_t *id, char *_buf, size_t size, off_t offset);

//unmap.
int stor_unmap(const char *pool, const fileid_t *id, size_t _size, off_t offset);

// -- snapshot
int stor_snapshot_create(const fileid_t *parent, const char *name, int p, const char *_site);
int stor_snapshot_listopen(const fileid_t *id, const char *uuid);
int stor_snapshot_list(const fileid_t *id, const char *uuid, uint64_t offset, void *de, int *delen);
int stor_snapshot_listclose(const fileid_t *id, const char *uuid);
int stor_snapshot_read(const char *pool, const fileid_t *parent, const fileid_t *fileid,
                         buffer_t *_buf, size_t _size, off_t offset);
int stor_snapshot_diff(const char *pool, const fileid_t *parent, const fileid_t *snapsrc,
        const fileid_t *snapdst, buffer_t *_buf, size_t _size, off_t offset);
int stor_snapshot_rollback(const fileid_t *parent, const char *);
int stor_snapshot_flat(const fileid_t *fileid, const int idx, int force);
int stor_snapshot_remove(const char *pool, const fileid_t *parent, const char *, int force);
int stor_snapshot_protect(const char *pool, const fileid_t *parent, const char *name, snap_protect_param_t on);

int stor_snapshot_updateparent(const fileid_t *parent, const char *name, uint64_t from);
int stor_snapshot_setfrom(const fileid_t *fileid, uint64_t from);
int stor_snapshot_check(const fileid_t *, const char *name);

int stor_group_snapshot_create(const fileid_t *fids, const char *name, int count, const char **_site);

int stor_iterator(func_int1_t func1);

#endif
