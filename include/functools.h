#ifndef __LICH_FUNCTOOLS_H
#define __LICH_FUNCTOOLS_H

#include "yid.h"
#include "dbg.h"


typedef int (*squeue_cmp_func)(const void *, const void *);
typedef uint32_t (*squeue_hash_func)(const void *);


static inline uint32_t chkid_hash_func(const void *chkid) {
        return ((chkid_t *)chkid)->id;
}

static inline int chkid_cmp_fuc(const void *a, const void *b) {
        const chkid_t *key = a;
        // TODO used-defined structure which include chkid_t as a field
        const chkid_t *val = b;

        DBUG("chkid "CHKID_FORMAT" --- "CHKID_FORMAT"\n", CHKID_ARG(key),
                CHKID_ARG(val));

        return !chkid_cmp(key, val);
}



#endif
