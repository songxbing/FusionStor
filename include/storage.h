#ifndef __STORAGE_H__
#define __STORAGE_H__

#include "yid.h"
#include "net_table.h"
#include "metadata.h"
#include "env.h"
#include "cluster.h"
#define LICH_ATTR_PROTOCOL_ISCSI    0x00000001
#define LICH_ATTR_PROTOCOL_NFSv3    0x00000002
#define OBJECT_OFFLINE              SHM_ROOT"/offline/"

#define OINFO_SIZE(__oinfo__) (sizeof(oinfo_t) + __oinfo__->buflen)

int print_chunk_loctaion(const fileid_t *oid, char *buf);


static inline void print_err(const char *name, int arg)
{
        if (name)
                fprintf(stderr, "\x1b[1;31minvalid char '%c' in %s, only 'a-z', '0-9', "
                        "'-', ':' is allowed\x1b[0m\n", arg, name);
}

static inline int get_tgt_len(const char *path)
{
        const char *idx, *tmp;
        int cnt = 0;

        tmp = path;
        if (*tmp == '/') {
                tmp++;
        }

        idx = strchr(tmp, '/');
        if (idx != NULL) {
                /* namespace */
                while (tmp != idx) {
                        cnt++;
                        tmp++;
                }

                tmp++;
                idx = strchr(tmp, '/');
                if (idx != NULL) {
                        /* target */
                        while (tmp != idx) {
                                cnt++;
                                tmp++;
                        }
                        cnt++;
                } else {
                        /* target */
                        while (*tmp) {
                                cnt++;
                                tmp++;
                        }
                        cnt++;
                }
        } else {
                /* namespace */
                while (*tmp) {
                        cnt++;
                        tmp++;
                }
        }

        return cnt;
}

static inline int is_valid_char(char ch)
{
        return ((ch >= 'a' && ch <= 'z') ||
                (ch >= '0' && ch <= '9') ||
                (ch == '-') ||
                (ch == ':'));
}

static inline int is_valid_name(const char *name, const char *type)
{
        const char *idx, *tmp;

        tmp = name;

        idx = strchr(tmp, '/');
        if (idx != NULL) {
                print_err(type, *idx);
                return 0;
        }

        while (*tmp) {
            if (!is_valid_char(*tmp)) {
                print_err(type, *tmp);
                return 0;
            }
            tmp++;
        }

        return 1;
}

#endif
