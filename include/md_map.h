#ifndef __MD_MAP__
#define __MD_MAP__

/**
 * 维护控制器到节点的映射关系
 */
int md_map_init();

/**
 * @brief 根据cache和broadcast过程定位pool或volume controller所在节点
 *
 * @note controller发生切换时，需要处理EREMCHG，主动失效缓存项，
 * 且中间过程不能屏蔽该错误码
 *
 * @param chkid
 * @param nid
 * @return
 */
int md_map_getsrv(const chkid_t *chkid, nid_t *nid);

int md_map_update(const chkid_t *chkid, const nid_t *nid);

int md_map_drop(const chkid_t *chkid, const nid_t *nid);

#endif
