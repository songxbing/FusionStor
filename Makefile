PLATFORM=`uname -m`
#PLATFORM_OS=`lsb_release -d |awk '{print $$2}'`
#OS_VERSION=`lsb_release -r | awk -F'.' '{print $$1}' |awk '{print $$2}'`
AUSERS=`grep "^mds:" /etc/passwd > /dev/null`
hostname = `hostname`

RELEASE_PATH="/opt/fusionstack"

all: version
	make -C build confl.c
	make -C build confy.c
	make -C build liblich4.la -j10
	make -C build -j10

install: version
	make -C build  confl.c
	make -C build  confy.c
	make -C build liblich4.la -j10
	make -C build -j10 install

	if [ -f $(RELEASE_PATH)/etc/lich.conf ]; then \
		sed -i "s:#home XXX:home "$(RELEASE_PATH)":g" "$(RELEASE_PATH)/etc/lich.conf"; \
	fi

	make -C include install
	make -C admin
	make -C manage
	make -C remotecopy
	make -C cgroup
	rm -rf $(RELEASE_PATH)/lich/bin/lichfs
	rm -rf $(RELEASE_PATH)/lich/bin/lich.snapshot
	rm -rf $(RELEASE_PATH)/lich/bin/lich.inspect
	rm -rf $(RELEASE_PATH)/lich/bin/lichbd
	ln -s $(RELEASE_PATH)/lich/libexec/lichfs $(RELEASE_PATH)/lich/bin/lichfs
	ln -s $(RELEASE_PATH)/lich/libexec/lich.snapshot $(RELEASE_PATH)/lich/bin/lich.snapshot
	ln -s $(RELEASE_PATH)/lich/libexec/lich.inspect $(RELEASE_PATH)/lich/bin/lich.inspect
	ln -s $(RELEASE_PATH)/lich/libexec/lichbd $(RELEASE_PATH)/lich/bin/lichbd

version:
	./tools/get_version.sh

autogen:
	./tools/autogen.sh

conf:
#	sed -i 's:__PREFIX__:$(RELEASE_PATH)/lich/admin:g' admin/Makefile
	echo -ne "install:\n	./install.sh -i $(RELEASE_PATH)/lich\nuninstall:\n	./install.sh -e $(RELEASE_PATH)/lich\n " > cgroup/Makefile 
	echo -ne "install:\n	./install.sh -i $(RELEASE_PATH)/lich\nuninstall:\n	./install.sh -e $(RELEASE_PATH)/lich\n " > admin/Makefile 
	echo -ne "install:\n	./install.sh -i $(RELEASE_PATH)/lich\nuninstall:\n	./install.sh -e $(RELEASE_PATH)/lich\n " > manage/Makefile
	echo -ne "install:\n	./install.sh -i $(RELEASE_PATH)/lich\nuninstall:\n	./install.sh -e $(RELEASE_PATH)/lich\n " > remotecopy/Makefile
	#CFLAGS="-g" ./tools/lichconf.sh "--prefix=$(RELEASE_PATH)/lich" "sysconfdir=$(RELEASE_PATH)/etc" ;
	CFLAGS="-O3 -g -Werror=return-type " ./tools/lichconf.sh "--prefix=$(RELEASE_PATH)/lich" "sysconfdir=$(RELEASE_PATH)/etc" ;

#unittest:
#	cd chunk && make test

clean:
	make -C build clean
	make -C include clean

distclean:
	make -C build distclean
	make -C include distclean

tar:
	tools/app_tar.sh "$(RELEASE_PATH)/lich"
