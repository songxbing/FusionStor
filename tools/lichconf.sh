#!/bin/sh

#set -xv

PLATFORM=`uname -m`

if [ $PLATFORM = 'x86_64' ];then
    CONF_ARGS='--with-pic'
else
    CFLAGS='-D_FILE_OFFSET_BITS=64'
fi

#PREFIX='/opt/fusionstack/lich'

echo "0:" $CONF_ARGS
echo "1:" $1
echo "2:" $2
echo "3:" $3

if [ -x ccache ]; then
       CC="ccache gcc $1" CFLAGS="$CFLAGS" ./configure $CONF_ARGS $1 $2
else 
       CC="gcc $1" CFLAGS="$CFLAGS" ./configure $CONF_ARGS $1 $2
fi
