#!/bin/bash

COMMIT=`git log |sed -n '/commit/p'|sed -n '1p'|awk '{print $2}' | cut -c -5`
BRANCH=`git branch|sed -n '/\*/p' |awk '{print $2}' `
Vnum=`cat v_num`
echo $Vnum
NOW=`date --rfc-3339=seconds | sed 's/\://g' | sed 's/\-//g' | sed 's/\ //g' | sed 's/\+.*//g'`
Time=`date +%y%m%d%H`
lich=`pwd | cut -d '/' -f 3`

if `lsb_release -a|grep -q 'CentOS Linux release 7'`;then
sys="C7"
echo $sys
elif `lsb_release -a|grep -q 'CentOS release 6'`;then
sys="C6"
echo $sys
elif `lsb_release -a|grep -q Ubuntu`;then
sys="U"
echo $sys
fi

DIST=/tmp/lich-$BRANCH-${Vnum}-$COMMIT-$Time-$sys.tar.gz
ETC=/tmp/lich-etc-$BRANCH-${Vnum}-$COMMIT-$Time-$sys.tar.gz
LICENSE=/tmp/lich-license-$BRANCH-${Vnum}-$COMMIT-$Time-$sys.tar.gz
LIC_MNG=/tmp/lich-license-manage-$BRANCH-${Vnum}-$COMMIT-$Time-$sys.tar.gz
REMOTECOPY=/tmp/lich-remotecopy-$BRANCH-${Vnum}-$COMMIT-$Time-$sys.tar.gz
TMP=`mktemp -d`

#NOW=$YEAR$MONTH$DAY$TIME

rm -rf $DIST
rm -rf $ETC
rm -rf $LICENSE
rm -rf $LIC_MNG
rm -rf $REMOTECOPY

cp $1/libexec/lich.license* tools/license* $TMP && rm $1/libexec/lich.license_gen && cd $TMP && tar czvf $LICENSE * > /dev/null
rm -rf $TMP && mkdir -p $TMP/remotecopy && mkdir $TMP/remotecopy/etc
cp -r $1/admin $TMP/remotecopy && mv $1/remotecopy $TMP/remotecopy/src && mv $TMP/remotecopy/src/remotecopy.conf.example $TMP/remotecopy/etc
cd $TMP && tar czvf $REMOTECOPY * > /dev/null && rm -rf $TMP
cd $1 && tar czvf $LIC_MNG manage > /dev/null && rm -rf manage
cd $1 && tar czvf  $DIST * > /dev/null
cd $1/../etc && tar czvf  $ETC * --exclude cluster.conf > /dev/null

rm -f /var/www/html/$lich/$BRANCH/latest/*
mkdir -p /var/www/html/$lich/$BRANCH/latest
cp $DIST /var/www/html/$lich/$BRANCH/latest
cp $ETC  /var/www/html/$lich/$BRANCH/latest
cp $LICENSE /var/www/html/$lich/$BRANCH/latest
cp $LIC_MNG /var/www/html/$lich/$BRANCH/latest
cp $REMOTECOPY /var/www/html/$lich/$BRANCH/latest
# mv $CGRP /var/www/html/$lich/$BRANCH/latest
mkdir -p /var/www/html/$lich/$BRANCH/$NOW
mv $DIST /var/www/html/$lich/$BRANCH/$NOW
mv $ETC /var/www/html/$lich/$BRANCH/$NOW
mv $LICENSE /var/www/html/$lich/$BRANCH/$NOW
mv $LIC_MNG /var/www/html/$lich/$BRANCH/$NOW
mv $REMOTECOPY /var/www/html/$lich/$BRANCH/$NOW
#mv $CGRP  /var/www/html/$lich/$BRANCH/$NOW

echo /var/www/html/$lich/$BRANCH/$NOW
echo /var/www/html/$lich/$BRANCH/latest
PACKAGE=$(ls /var/www/html/$lich/$BRANCH/$NOW | grep lich-$BRANCH)
for i in `ifconfig | grep 'inet '  | grep -v 'inet .*127' | awk '{if (index($2, ":")) {split($2, array, ":"); printf array[2]"\n"} else {print $2}}'`; do
    echo http://$i/$lich/$BRANCH/$NOW/$PACKAGE;
    echo http://$i/$lich/$BRANCH/latest/$PACKAGE;
done

LICH_DIR=$(pwd)

exit

if [ ! -d /root/rpmbuild ];then
	exit
fi

cd /root/rpmbuild/SPECS
SPEC_MOD="/root/rpmbuild/SPECS/qemu-kvm.spec.mod"
SPEC="/root/rpmbuild/SPECS/qemu-kvm.spec"
rm -f $SPEC
cp $SPEC_MOD $SPEC
sed -i "/^%define pkgrelease/s/$/\.$COMMIT/" $SPEC

rpmbuild -ba  $SPEC  &2>1 1>/var/log/rpmbuild.log
while [ 1 ]
do
	file1=$(ls /root/rpmbuild/RPMS/x86_64/qemu-img* )
	file=$(ls /root/rpmbuild/RPMS/x86_64/qemu-kvm-0*)
	if [ -n  "$file" -a -n "$file1" ];then
		mv $file /var/www/html/$lich/$BRANCH/$NOW
		mv $file /var/www/html/$lich/$BRANCH/latest

		mv $file1 /var/www/html/$lich/$BRANCH/$NOW
		mv $file1 /var/www/html/$lich/$BRANCH/latest
		break
	fi
	sleep 5
done
