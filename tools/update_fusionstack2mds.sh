
if [ $# -lt 1 ]; then
    echo "Usage: $0 lich.tar.gz"
    exit 1;
fi

nodes=`/opt/fusionstack/lich/libexec/lich.admin --list`

#1.  
    lich.cluster stop

#2.  
    #pdsh -w node[2-4]
    #mv /opt/fusionstack /opt/mds 

    for node in $nodes
    do
        echo "$node mv /opt/fusionstack /opt/mds"
        ssh $node 'if [ ! -d /opt/mds ];then mkdir /opt/mds; fi'
        ssh $node 'if [ -d /opt/fusionstack ];then mv -f /opt/fusionstack/* /opt/mds; rm -rf /opt/fusionstack/; fi'
    done

#3.  
    sed -i 's/home \/opt\/fusionstack;/home \/opt\/mds;/g' /opt/mds/etc/lich.conf
    tar -xzvf $1 -C /opt/mds/lich
    /opt/mds/lich/bin/lich.cluster update lich --force
    /opt/mds/lich/bin/lich.cluster update etc --force

#4.
    #pdsh -w node[2-4]
    #rm -rf /usr/bin/lich*
    #ln -s  /opt/mds/lich/bin/lich /usr/bin/lich  
    #ln -s /opt/mds/lich/bin/lich.cleancore /usr/bin/lich.cleancore  
    #ln -s  /opt/mds/lich/bin/lich.cleanlog /usr/bin/lich.cleanlog 
    #ln -s /opt/mds/lich/bin/lich.cluster  /usr/bin/lich.cluster  
    #ln -s /opt/mds/lich/bin/lich.inspect  /usr/bin/lich.inspect  
    #ln -s /opt/mds/lich/bin/lich.node               /usr/bin/lich.node  
    #ln -s   /opt/mds/lich/bin/lich.snapshot       /usr/bin/lich.snapshot
    #ln -s  /opt/mds/lich/bin/lich.tgt         /usr/bin/lich.tgt 

    #echo '/opt/mds/lich/lib' > /etc/ld.so.conf.d/lich.conf
    #ldconfig

    for node in $nodes
    do
        echo "$node ldconfig"
        ssh $node 'rm -rf /usr/bin/lich*'
        ssh $node 'ln -s  /opt/mds/lich/bin/lich /usr/bin/lich'
        ssh $node 'ln -s /opt/mds/lich/bin/lich.cleancore /usr/bin/lich.cleancore'
        ssh $node 'ln -s  /opt/mds/lich/bin/lich.cleanlog /usr/bin/lich.cleanlog '
        ssh $node 'ln -s /opt/mds/lich/bin/lich.cluster  /usr/bin/lich.cluster  '
        ssh $node 'ln -s /opt/mds/lich/bin/lich.inspect  /usr/bin/lich.inspect  '
        ssh $node 'ln -s /opt/mds/lich/bin/lich.node               /usr/bin/lich.node'  
        ssh $node 'ln -s   /opt/mds/lich/bin/lich.snapshot       /usr/bin/lich.snapshot'
        ssh $node 'ln -s  /opt/mds/lich/bin/lich.tgt         /usr/bin/lich.tgt '

        ssh $node 'echo "/opt/mds/lich/lib" > /etc/ld.so.conf.d/lich.conf'
        ssh $node 'ldconfig'
    done

#5.
    lich.cluster --start
