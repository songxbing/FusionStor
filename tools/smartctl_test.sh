#!/bin/bash

# This script is to ###


#############################################

	Devlist=` more /proc/partitions | grep "sd.*[^1-9]$" |sed -n "1p" |awk '{print $4}'` #可以去除sda1这样的盘符
	Devnum=`/opt/MegaRAID/MegaCli/MegaCli64 -PDList -aall |grep  "Device Id" |wc -l`
	Device_id=`/opt/MegaRAID/MegaCli/MegaCli64 -PDList -aall |grep  "Device Id" |awk '{print $3}'` 
	date_day=`date`
	echo ${date_day} -------------------------------------------------->> /opt/fusionstack/log/smartctl_log
    printf "%-5s %-24s %-5s %-7s %-6s %-7s\n" rev Serial_Number 5_RSC 197_CPS 198_OU health_state >> /opt/fusionstack/log/smartctl_log
	for((i=1; i<=$Devnum; i++))
	do
		Device_id_number=`echo $Device_id | gawk '{print $'$i'}'`		
		Serial_Number=`smartctl -i -d  megaraid,$Device_id_number /dev/$Devlist |grep Serial |awk '{print $3}'`
        smartctl_A_info=`smartctl -A -d  megaraid,$Device_id_number /dev/$Devlist`
        health="GOOD"

	    Reallocated_Sector_Ct=`echo "$smartctl_A_info" |grep Reallocated_Sector_Ct |awk '{print $10}'`
        if (( $Reallocated_Sector_Ct > 500 ));then
             health="WARING"
        fi

        Current_Pending_Sector=`echo "$smartctl_A_info" |grep Current_Pending_Sector |awk '{print $10}'` 
        if (( $Current_Pending_Sector > 500 ));then
            health="WARING"
        fi

        Offline_Uncorrectable=`echo "$smartctl_A_info" |grep Offline_Uncorrectable |awk '{print $10}'`
        if [[ $Offline_Uncorrectable == "" ]];then
            Offline_Uncorrectable="NS"
        else
            if (( $Offline_Uncorrectable > 500 ));then
                health="WARING"
            fi
        fi

        Reallocated_Sector_Ct_val=`echo "$smartctl_A_info" |grep Reallocated_Sector_Ct |awk '{print $4}'`
        Reallocated_Sector_Ct_thresh=`echo "$smartctl_A_info" |grep Reallocated_Sector_Ct |awk '{print $6}'`
        if [[ $Reallocated_Sector_Ct_thresh >  $Reallocated_Sector_Ct_val ]];then
            health="BAD"
        fi

        printf "%-5s %-24s %-5s %-7s %-6s %-7s\n" ${Device_id_number} ${Serial_Number} ${Reallocated_Sector_Ct} ${Current_Pending_Sector} ${Offline_Uncorrectable} ${health}  >> /opt/fusionstack/log/smartctl_log
        
	done
  date_day=`date`
	echo ${date_day} -------------------------------------------------->> /opt/fusionstack/log/smartctl_log

	

