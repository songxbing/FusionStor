#!/bin/bash

FUNC_BASE=./tools/get_version.base
FUNC_FILE=include/get_version.h

MANAGE_DIR=manage/
REMOTECOPY_DIR=remotecopy/

rm -f $FUNC_FILE
cat $FUNC_BASE | sed -n '1,5p' >> $FUNC_FILE

echo "#define LVERSION \"`git log | head -n 5 |grep -i ^commit | head -n 1 |awk '{print $2}'`\"" >> $FUNC_FILE
echo >> $FUNC_FILE

echo "#define YVERSION \\" >> $FUNC_FILE
echo "\"Version:     `cat ./v_num|cut -d '_' -f 1`\\">> $FUNC_FILE
echo "\nInternalID:  `cat ./v_num|cut -d '_' -f 2`\\">> $FUNC_FILE
echo "\nBuildId:     `git log | head -n 5 |grep -i ^commit | head -n 1 |awk '{print $2}'`\\" >> $FUNC_FILE
#echo "\n`git show |grep  ^Date:`\\" >> $FUNC_FILE
echo "\nDate:        `git log | head -n 5 |grep ^Date: | head -n 1|cut -c 9-`\\" >> $FUNC_FILE
echo "\nBranch:      lich/`git branch |grep '^\*' |awk '{print $2}'`\\" >> $FUNC_FILE
echo "\nSystem:      `lsb_release -i | awk '{print $3}'` `uname -ir`\\" >> $FUNC_FILE
echo "\nGlibc:       `/lib64/libc.so.6 | head -n 1`\"" >> $FUNC_FILE
echo >> $FUNC_FILE
cat $FUNC_BASE | sed -n '6,12p'>> $FUNC_FILE

cp $FUNC_FILE $MANAGE_DIR
cp $FUNC_FILE $REMOTECOPY_DIR
