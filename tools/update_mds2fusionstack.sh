
if [ $# -lt 1 ]; then
    echo "Usage: $0 lich.tar.gz"
    exit 1;
fi

nodes=`/opt/mds/lich/libexec/lich.admin --list`

#1.  
    lich.cluster --stop

#2.  
    #pdsh -w node[2-4]
    #mv /opt/mds /opt/fusionstack 

    for node in $nodes
    do
        echo "$node mv /opt/mds /opt/fusionstack"
        ssh $node 'if [ ! -d /opt/fusionstack ];then mkdir /opt/fusionstack; fi'
        ssh $node 'if [ -d /opt/mds ];then mv -f /opt/mds/* /opt/fusionstack; rm -rf /opt/mds/; fi'
    done

#3.  
    sed -i 's/home \/opt\/mds;/home \/opt\/fusionstack;/g' /opt/fusionstack/etc/lich.conf
    tar -xzvf $1 -C /opt/fusionstack/lich
    /opt/fusionstack/lich/bin/lich.cluster --update lich --force
    /opt/fusionstack/lich/bin/lich.cluster --update etc --force

#4.
    #pdsh -w node[2-4]
    #rm -rf /usr/bin/lich*
    #ln -s  /opt/fusionstack/lich/bin/lich /usr/bin/lich  
    #ln -s /opt/fusionstack/lich/bin/lich.cleancore /usr/bin/lich.cleancore  
    #ln -s  /opt/fusionstack/lich/bin/lich.cleanlog /usr/bin/lich.cleanlog 
    #ln -s /opt/fusionstack/lich/bin/lich.cluster  /usr/bin/lich.cluster  
    #ln -s /opt/fusionstack/lich/bin/lich.inspect  /usr/bin/lich.inspect  
    #ln -s /opt/fusionstack/lich/bin/lich.node               /usr/bin/lich.node  
    #ln -s   /opt/fusionstack/lich/bin/lich.snapshot       /usr/bin/lich.snapshot
    #ln -s  /opt/fusionstack/lich/bin/lich.tgt         /usr/bin/lich.tgt 

    #echo '/opt/fusionstack/lich/lib' > /etc/ld.so.conf.d/lich.conf
    #ldconfig

    for node in $nodes
    do
        echo "$node ldconfig"
        ssh $node 'rm -rf /usr/bin/lich*'
        ssh $node 'ln -s  /opt/fusionstack/lich/bin/lich /usr/bin/lich'
        ssh $node 'ln -s /opt/fusionstack/lich/bin/lich.cleancore /usr/bin/lich.cleancore'
        ssh $node 'ln -s  /opt/fusionstack/lich/bin/lich.cleanlog /usr/bin/lich.cleanlog '
        ssh $node 'ln -s /opt/fusionstack/lich/bin/lichfs  /usr/bin/lichfs  '
        ssh $node 'ln -s /opt/fusionstack/lich/bin/lich.inspect  /usr/bin/lich.inspect  '
        ssh $node 'ln -s /opt/fusionstack/lich/bin/lich.node               /usr/bin/lich.node'  
        ssh $node 'ln -s   /opt/fusionstack/lich/bin/lich.snapshot       /usr/bin/lich.snapshot'
        ssh $node 'ln -s  /opt/fusionstack/lich/bin/lich.tgt         /usr/bin/lich.tgt '

        ssh $node 'echo "/opt/fusionstack/lich/lib" > /etc/ld.so.conf.d/lich.conf'
        ssh $node 'ldconfig'
    done

#5.
    lich.cluster start
