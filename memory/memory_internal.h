

#ifndef __MEMORY_INTERNAL_H__
#define __MEMORY_INTERNAL_H__

enum lich_mem_map_notify_action {
	LICH_MEM_MAP_NOTIFY_REGISTER,
	LICH_MEM_MAP_NOTIFY_UNREGISTER,
};

struct lich_mem_map;

typedef int (*lich_mem_map_notify_cb)(void *cb_ctx, struct lich_mem_map *map,
				      enum lich_mem_map_notify_action action,
				      void *vaddr, size_t size);

struct lich_mem_map *lich_mem_map_alloc(uint64_t default_translation, lich_mem_map_notify_cb notify_cb, void *cb_ctx);
void lich_mem_map_free(struct lich_mem_map **pmap);

uint64_t lich_mem_map_translate(const struct lich_mem_map *map, uint64_t vaddr);
int lich_mem_map_set_translation(struct lich_mem_map *map, uint64_t vaddr, uint64_t size, uint64_t translation);
int lich_mem_map_clear_translation(struct lich_mem_map *map, uint64_t vaddr, uint64_t size);

void scale_mem_set_callback(int (*mem_alloc_callback)(void *vaddr, size_t len),  int (*mem_free_callback)(void *vaddr, size_t len));

#endif