libnvme模块基于开源libnvme(https://github.com/hgst/libnvme.git),master分支,lastest commit.

commit ba0800f8745b04793565d7605d8215331d5ef4de
Author: Damien Le Moal <damien.lemoal@wdc.com>
Date:   Fri Mar 31 08:30:56 2017 +0900

    PCI: Do not allow VFIO binding

    Since VFIO is not yet supported, do not allow a controller to be binded
    to vfio-pci driver, nor to uio driver.

    Signed-off-by: Damien Le Moal <damien.lemoal@wdc.com>


仅对内存管理单元进行了修改,调用lich的内存管理,重写了common/nvme_mem.c的部分接口,参照宏 USER_MM.


编译方法:

    cmake .. -Dlibnvme=ON
    make -j8
    make install -j8
