#define SPDK_COUNTOF(arr) (sizeof(arr) / sizeof((arr)[0]))

struct spdk_nvme_intel_smart_attribute {
	uint8_t			code;
	uint8_t			reserved[2];
	uint8_t			normalized_value;
	uint8_t			reserved2;
	uint8_t			raw_value[6];
	uint8_t			reserved3;
};

struct __attribute__((packed)) spdk_nvme_intel_smart_information_page {
	struct spdk_nvme_intel_smart_attribute	attributes[13];
};
static_assert(sizeof(struct spdk_nvme_intel_smart_information_page) == 156, "Incorrect size");

union spdk_nvme_critical_warning_state {
	uint8_t		raw;

	struct {
		uint8_t	available_spare		: 1;
		uint8_t	temperature		: 1;
		uint8_t	device_reliability	: 1;
		uint8_t	read_only		: 1;
		uint8_t	volatile_memory_backup	: 1;
		uint8_t	reserved		: 3;
	} bits;
};
static_assert(sizeof(union spdk_nvme_critical_warning_state) == 1, "Incorrect size");

struct __attribute__((packed)) spdk_nvme_health_information_page {
	union spdk_nvme_critical_warning_state	critical_warning;

	uint16_t		temperature;
	uint8_t			available_spare;
	uint8_t			available_spare_threshold;
	uint8_t			percentage_used;

	uint8_t			reserved[26];

	/*
	 * Note that the following are 128-bit values, but are
	 *  defined as an array of 2 64-bit values.
	 */
	/* Data Units Read is always in 512-byte units. */
	uint64_t		data_units_read[2];
	/* Data Units Written is always in 512-byte units. */
	uint64_t		data_units_written[2];
	/* For NVM command set, this includes Compare commands. */
	uint64_t		host_read_commands[2];
	uint64_t		host_write_commands[2];
	/* Controller Busy Time is reported in minutes. */
	uint64_t		controller_busy_time[2];
	uint64_t		power_cycles[2];
	uint64_t		power_on_hours[2];
	uint64_t		unsafe_shutdowns[2];
	uint64_t		media_errors[2];
	uint64_t		num_error_info_log_entries[2];
	/* Controller temperature related. */
	uint32_t		warning_temp_time;
	uint32_t		critical_temp_time;
	uint16_t		temp_sensor[8];

	uint8_t			reserved2[296];
};
static_assert(sizeof(struct spdk_nvme_health_information_page) == 512, "Incorrect size");

int get_intel_smart_log_page(struct nvme_ctrlr *ctrlr, struct spdk_nvme_intel_smart_information_page *page);

int get_health_log_page(struct nvme_ctrlr *ctrlr, struct spdk_nvme_health_information_page *health_page);