#!/usr/bin/env python2
# coding:utf-8

import os
import re
import sys
import time
import copy
import fcntl
import types
import errno
import getopt

from node import Node
from disk_manage import DiskManage
from utils import Exp, _dmsg, _dwarn, _derror, _exec_shell, _exec_pipe, \
    mutil_exec, _exec_pipe1, _human_readable, _exec_system, _str2dict

class Gbase_disk_manage(DiskManage):
    def __init__(self, gbase = True):
        self.node = Node()
        super(Gbase_disk_manage, self).__init__(self.node)
        self.tier_withtype = gbase
        self.lichfs =  self.config.lichexec
        self.chunksize = 1024 * 1024

    def is_dir(self, path):
        cmd = [self.lichfs, "--stat", path]
        try:
            (out, err) = _exec_pipe1(cmd, 0, False)
            for line in out.splitlines():
                m = re.match(".*Id:\s+(pool).*", line)
                if m is not None:
                    return True 
        except Exp, e:
            if e.err:
                _derror(str(e.err))
                exit(e.errno)
        return False

    def gettier(self, path):
        #replica : 1908
        #allocated : 0
        #tier0 : 0
        #tier1 : 0

        tier_info = []
        cmd = [self.config.inspect, "--tier", path]
        try:
            (out, err) = _exec_pipe1(cmd, 0, False)
            for line in out.splitlines():
                m1 = re.match("\s*tier0 :\s+(\d*).*", line)
                if m1 is not None:
                    tier0 = m1.group(1)
                m2 = re.match("\s*tier1 :\s+(\d*).*", line)
                if m2 is not None:
                    tier1 = m2.group(1)
            tier_info.append((tier0, tier1))
        except Exp, e:
            if e.err:
                _derror(str(e.err))
                exit(e.errno)

        return tier_info

    def getsize(self, path):
        cmd = [self.lichfs, "--stat", path]
        try:
            (out, err) = _exec_pipe1(cmd, 0, False)
            for line in out.splitlines():
                m = re.match("\s+Size:\s+(\d*).*", line)
                if m is not None:
                    return int(m.group(1))
        except Exp, e:
            if e.err:
                _derror(str(e.err))
                exit(e.errno)
        return 0

    def getallocated(self, path):
        cmd = [self.config.inspect, "--stat", path]
        try:
            (out, err) = _exec_pipe1(cmd, 0, False)
            for line in out.splitlines():
                m = re.match("\s*allocated :\s+(\d*).*", line)
                if m is not None:
                    return int(m.group(1))
        except Exp, e:
            if e.err:
                _derror(str(e.err))
                exit(e.errno)

        return 0

    def __lichfs_list(self, cmd):
        try:
            (out, err) = _exec_pipe1(cmd, 0, False)
            return out
        except Exp, e:
            if e.errno == errno.EAGAIN:
                _derror('Resource temporarily unavailable, please try again later.')
                exit(e.errno)
            else:
                _derror(str(e.err))
                exit(e.errno)

    def _lun_list(self):
        lun = []
        pool = []
        tgt = []

        dir = '/'
        cmd = [self.lichfs, '--list', dir]
        res = self.__lichfs_list(cmd)
        for r in res.splitlines():
            r = dir + r.split(' ')[-1]
            if r == '/system':
                continue
            cmd = [self.lichfs, '--list', r]
            res = self.__lichfs_list(cmd)
            for p in res.splitlines():
                p = p.split(' ')[-1]
                pool.append(r + dir + p)

        for p in pool:
            cmd = [self.lichfs, '--list', p]
            res = self.__lichfs_list(cmd)
            for l in res.splitlines():
                l = l.split(' ')[-1]
                path = p + dir + l
                if self.is_dir(path):
                    tgt.append(path)
                else:
                    lun.append(path)

        for t in tgt:
            cmd = [self.lichfs, '--list', t]
            res = self.__lichfs_list(cmd)
            res = [x.split(' ')[-1] for x in res.splitlines()]
            for l in res:
                path = t + dir + l
                if(self.is_dir(path)):
                    _dmsg('%s is dir' % path)
                    _derror('list lun fail')
                    #exit(e.errno)
                else:
                    lun.append(path)

        return lun

    def lun_info(self, lun, ext=None):
        lun_info = []
        tier0 = 0
        tier1 = 0
        size = self.getsize(lun)
        allocated = self.getallocated(lun) * self.chunksize
        free = size - allocated
        if free < 0:
            free = 0

        tier = self.gettier(lun)
        lun_info.append((lun, _human_readable(size), _human_readable(allocated), _human_readable(free), tier))

        for l in lun_info:
            for t in l[4]:
                tier0 = int(t[0])
                tier1 = int(t[1])
            if ext == '0' and tier0 >= 0:
                print ("%s \t%s \t%s \t%s \t(tier0 :%s)" % (l[0], l[1], l[2], l[3], tier0))
            elif ext == '1' and tier1 >= 0:
                print ("%s \t%s \t%s \t%s \t(tier1 :%s)" % (l[0], l[1], l[2], l[3], tier1))
            elif ext is None:
                print ("%s \t%s \t%s \t %s \t(tier0 :%s\ttier1 :%s)" % (l[0], l[1], l[2], l[3], tier0, tier1))

    def lun_list(self):
        lun_list = []

        lun_list = self._lun_list()
        for l in lun_list:
            print l

    def lun_set(self, op, path, ext):
        if ext not in ['0', '1']:
            _derror('set lun only support 0 and 1')
            exit(errno.EINVAL)

        cmd = [self.config.inspect, '--' + op, path, ext]
        try:
            (out, err) = _exec_pipe1(cmd, 0, False)
            ret = _exec_system("echo $?", False, False)
            if ret == 0:
                _dmsg('set %s %s %s successful' % (path, op, ext))
        except Exp, e:
            if e.errno == errno.ENOSPC:
                _derror('There is no tier%s disk.' % (ext))
                exit(errno.ENOSPC)
            else:
                _derror(str(e.err))
                exit(e.errno)

def usage():
    print ("usage:")
    print (sys.argv[0] + " --list\t<disk|lun|tier>")
    print (sys.argv[0] + " --lun\t<info|priority> [lun path] [0|1]")
    exit(1)

def main():
    op = ''
    ext = None
    verbose = 0
    is_json = 0
    gbase = True
    force = False

    try:
        opts, args = getopt.getopt(
                sys.argv[1:], 
                'hvjf', ['list=', 'help', 'verbose', 'force', 'json', 'lun=']
                )
    except getopt.GetoptError, err:
        print str(err)
        usage()

    newopts = copy.copy(opts)
    for o, a in opts:
        if o in ('-v', '--verbose'):
            verbose = 1
            newopts.remove((o, a))
        elif o in ('-j', '--json'):
            is_json = 1
            newopts.remove((o, a))
        elif o in ('-f', '--force'):
            force = True
            newopts.remove((o, a))

    newargs = copy.copy(args)
    for o in args:
        if o in ('-v', '--verbose'):
            verbose = 1
            newargs.remove(o)
        elif o in ('-j', '--json'):
            is_json = 1
            newargs.remove(o)
        elif o in ('-f', '--force'):
            force = True
            newargs.remove(o)

    for o, a in newopts:
        if o in ('--help'):
            usage()
            exit(0)
        elif o == '--list':
            if a not in ['disk', 'lun', 'tier']:
                _derror('list operate only support disk lun and tier')
                exit(errno.EINVAL)
            op = o
            mode = a
            ext = newargs
        elif o == '--lun':
            op = o
            mode = a
            ext = newargs
            if a not in ['info', 'priority']:
                _derror('lun operate support info priority')
                exit(errno.EINVAL)
        else:
            _derror('oops, unhandled option: %s, -h for help' % o)
            usage()
            exit(1)

    gbase_manage = Gbase_disk_manage(gbase)
    if (op == '--list'):
        if (mode == 'disk'):
            gbase_manage.disk_list(is_json)
        elif (mode == 'lun'):
            gbase_manage.lun_list()
        elif (mode == 'tier'):
            gbase_manage.disk_check(mode)
        else:
            _derror('list only support disk lun tier')
            exit(errno.EINVAL)
    elif (op == '--lun'):
        if (mode == 'info'):
            if len(ext) == 1:
                gbase_manage.lun_info(ext[0])
            elif len(ext) == 2 and ext[1] in ['0', '1']:
                gbase_manage.lun_info(ext[0], ext[1])
            else:
                _derror('lun info need path or tier type')
                exit(errno.EINVAL)
        elif (mode == 'priority'):
            if len(ext) != 2:
                _derror('need path and set num')
                exit(errno.EINVAL)
            if ext[1] not in ['0', '1']:
                _derror('set lun only support tier0 tier1, please input 0 or 1')
                exit(errno.EINVAL)
            gbase_manage.lun_set(mode, ext[0], ext[1])
        else:
            _derror('lun only support info tier priority.')
            exit(errno.EINVAL)
    else:
        _derror('oops, unhandled option: %s, -h for help' % o)
        usage()
        exit(1)

if __name__ == '__main__':
    if (len(sys.argv) == 1):
        usage()
    else:
        main()
