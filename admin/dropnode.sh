#!/bin/bash
if [ "$1" = "" ];then
    echo "no input argument";exit 1;
fi

main(){
    echo $OPTARG 
    CODEFILE=$OPTARG/log/dropnode_recode.log 
    echo '' > $CODEFILE
    
    for index in {1..30}
    do
        echo '' > $OPTARG/log/dropnode.log;
        $OPTARG/lich/admin/cluster.py health scan>> $OPTARG/log/dropnode.log 2>&1;
        HEALTH_RESULT=`cat $OPTARG/log/dropnode.log |grep 'chunk need recovery\|chunk recovery lost\|chunk recovery fail'|awk -F: '{print $2}'`
        ISSUCC=1
        for x in $HEALTH_RESULT
        do
            if [ $x != 0 ];then
                ISSUCC=0
            fi
        done 
        if [ $ISSUCC == 1 ];then
            break
        fi
    done
    if [ $ISSUCC == 1 ];then
        echo '' > $CODEFILE
        $OPTARG/lich/admin/node.py --drop >> $OPTARG/log/dropnode.log 2>&1;echo $? > $CODEFILE
    else
        echo -e "\033[1;31m drop node fail\033[0m"
        echo '9005' > $CODEFILE
        exit 1
    fi
}
        
while getopts h: arg 
do 
    case $arg in 
        h) 
            main;
        ;; 
        \?)
        echo "unknown argument" ;
        exit 1;
        ;; 
    esac    
done 


