#!/bin/bash

expect <<- EOF
set timeout 2;
spawn ssh $1 echo
expect {
        "yes/no" {
                send "yes\r"; exp_continue;
        }
        "*No route to host" {exit 2}
        "*Connection refused" {exit 3}
}
EOF
