#!/usr/bin/env python
# -*- coding: utf-8 -*-


DISK_MAX = 256


class DevName(object):
    def __init__(self, dev):
        if dev.startswith('/dev/'):
            self.dev = dev
            self.name = dev[5:]
        elif dev.startswith('/etc/'):
            # TODO pci
            self.dev = dev
            self.name = dev[5:]
        else:
            assert dev[0] != '/'

            self.dev = '/dev/%s' % dev
            self.name = dev

    def is_bcache(self):
        return self.name.startswith('bcache') and len(self.name) > len('bcache')


class DiskName(object):
    def __init__(self, disk):

        if isinstance(disk, str) and disk.startswith('disk'):
            self.disk = disk
            self.num = int(disk[4:])
        else:
            self.disk = 'disk%s' % disk
            self.num = int(disk)

        assert self.num >= 0 and self.num < DISK_MAX
