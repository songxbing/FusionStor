#!/bin/bash

usage()
{
    echo "Usage: $0 [OPTIONS]"
    echo "-i install [path]"
    echo "-e remove [path]"

    exit 1
}

install()
{
    DIST=$1/admin
    CONFIG=$1/../etc
    BIN=$1/bin

    rm -rf $DIST
    mkdir -p $DIST
    mkdir -p $BIN
    cp *.py $DIST

    mkdir -p $DIST/buddha
    cp buddha/*.py $DIST/buddha

    #mkdir -p $DIST/mod
    #cp mod/*.py $DIST/mod
    #touch $DIST/mod/__init__.py

    chmod a+x $DIST/*.py
    cp gen_sshkey.sh $DIST
    cp gen_hostkey.sh $DIST

    cp lichd_init_centos $DIST
    cp lichd.service $DIST
    cp lichd-halt.service $DIST
    cp lichd-poweroff.service $DIST
    cp lichd-reboot.service $DIST
    cp lichd_init_ubuntu $DIST

    cp cleanlog.sh $DIST
    cp cleancore.sh $DIST
    cp dropnode.sh $DIST
    cp dropinstance.sh $DIST
    cp stopnode.sh $DIST
    cp read.sh $DIST
    cp write.sh $DIST

    cp ../arp/check_client_ip.sh $DIST
    cp ../tools/smartctl_test.sh $DIST
    cp ../tools/sqlite3.sh $DIST
    chmod a+x $DIST/*.sh

    mkdir -p $CONFIG


#    if [ ! -f $CONFIG/lich.admin.conf ];  then
#        cp lich.admin.conf $CONFIG/ && sed -i "s:HOME:"$1":g" "$CONFIG/lich.admin.conf";
#    fi

    if [ ! -f $CONFIG/cluster.conf ]; then
        cp cluster.conf $CONFIG/;
    fi

    rm -rf $BIN/lich
    rm -rf $BIN/lich.node
    rm -rf $BIN/lich.cleanlog
    rm -rf $BIN/lich.cleancore
    rm -rf $BIN/ceph
    rm -rf $BIN/lich.balance

    ln -s $DIST/cluster.py $BIN/lich
    ln -s $DIST/node.py $BIN/lich.node
    ln -s $DIST/cleanlog.sh $BIN/lich.cleanlog
    ln -s $DIST/cleancore.sh $BIN/lich.cleancore
    ln -s $DIST/ceph.py $BIN/ceph
    ln -s $DIST/volbalance.py $BIN/lich.balance
#    ln -s $DIST/storage.py $BIN/lich.storage
#    ln -s $DIST/fence.py $BIN/lich.fence
#    ln -s $DIST/shell.py $BIN/lich.shell

    cd gbase && sh install.sh -i $1
}

remove()
{
    echo "do nothing"
}

if [ $# -lt 1 ]
then 
    usage
fi

while getopts ieh options
do
    case $options in
        i)
        echo "install scripts $2"
        install $2
        ;;
        e)
        echo "remove scripts"
        remove
        ;; 
        h)
        usage
        ;;
        \?)
        usage
        ;;
    esac
done
