#!/bin/bash

#max lich_*.log file size 100M
((MAX_LOGFILE_SIZE=100*1024*1024))
#default backup files number
((NUM_BACKUP_FILES=100))
#default log file backup path, must be absolute path
LOG_BACKUP_PATH="/opt/fusionstack/log/backup"


SYSTIME="date +%Y-%m-%d_%H:%M:%S"

echo "`${SYSTIME}` : task begnning ..."
RUNNING=`ps -ef | grep /cleanlog.sh | grep -v "grep" | wc -l`
if [ "${RUNNING}" -gt 3 ] ; then
        echo ""
        echo "`${SYSTIME}` : WARNNING : cleanlog.sh is already running. next will exit !"
        exit 3
fi

if [ $# != 1 ] ; then
        echo "Usage:"
        echo "./cleanlog.sh <path>"
        echo ""
        exit 1
fi

LOG_PATH=$1

get_file_size()
{
        file=$1;
        size=`ls -l $file | awk '{print $5}'`
        echo "${size}"
}

check_max_log_filesize()
{
        log_filesize=`/opt/fusionstack/lich/libexec/lich.admin --configdump 2>/dev/null | grep "log_max_bytes" | awk -F":" '{print $2}'`
        if [ "${log_filesize}" != "" ] ; then
               MAX_LOGFILE_SIZE="${log_filesize}"
        fi
}

check_backup_files()
{
        backup_path=$1;
        logfile=$2

        if [ ! -d "${backup_path}" ]; then
                echo "`${SYSTIME}` : ERROR:[${backup_path}]NO SUCH FILE OR DIRECTORY";
                return 2;
        fi
        
        cd ${backup_path};
        #for (( num=`ls $logfile* | wc -l` ; `echo ${num}|bc` > `echo ${NUM_BACKUP_FILES} | bc`; ((num=$num-1)) )) 
        num=`ls $logfile* 2>&1 | wc -l`
        if [ "${num}" -gt "${NUM_BACKUP_FILES}" ]; then
        #check backup files, if more than default number, delete the old backup file
                until [ "${num}" -eq "${NUM_BACKUP_FILES}" ]
                do
                        ((old_num=99999999999999))
                        for backup_file in `ls $logfile*`; do
                                tmp_num=`echo ${backup_file} | awk -F'.' '{print $3}'`
                                if [ "${tmp_num}" != "" -a "${tmp_num}" -lt "${old_num}" ]; then
                                        old_num="${tmp_num}"
                                fi
                        done
                        old_backup_file=${logfile}".${old_num}.*";
                        rm -f ${old_backup_file};
                        echo "`${SYSTIME}` : delete backup filename : ${old_backup_file}";
                        num=$((${num}-1))
                done
        fi

        cd - > /dev/null;
        return 0;
}

if [ ! -d "${LOG_PATH}" ] ; then
        echo "`${SYSTIME}` : ERROR:($1)NO SUCH FILE OR DIRECTORY"
        exit 2
fi

if [ ! -d "${LOG_BACKUP_PATH}" ] ; then
        mkdir -p ${LOG_BACKUP_PATH}
        echo "`${SYSTIME}` : create backup ok"
fi

if [ -d "/var/spool/clientmqueue" ] ; then
        rm -rf /var/spool/clientmqueue
        echo "`${SYSTIME}` : delete rubbish files in directory /var/spool/clientmqueue"
fi

check_max_log_filesize

cd ${LOG_PATH}
#for log_file in `ls lich_[0-9]*.log`; do
for log_file in `ls *.log*`; do
        is_rollover_file=0
        log_file_size=`get_file_size ${log_file}` #get lich_*.log file size, if more than default value, cp to backup and clear it
        echo ""
        echo "`${SYSTIME}` : ******************** (${log_file}) *******************************"
        echo "`${SYSTIME}` : file size ${log_file_size}"

        echo "`${SYSTIME}` : max log file size:${MAX_LOGFILE_SIZE}"
        if [[ "${log_file_size}"  -ge "${MAX_LOGFILE_SIZE}" ]] ; then
                echo "`${SYSTIME}` : ${log_file} clear begin ..."

                #backup file format: lich_0.log.20141117000000.gz
                if [ "`echo ${log_file} | grep '-'`" != "" ]; then
                        is_rollover_file=1
                        src_name=`echo ${log_file} | awk -F"-" '{print $1}'`
                        backup_file_name=${src_name}".`date +%Y%m%d%H%M%S`.gz"
                else
                        backup_file_name=${log_file}".`date +%Y%m%d%H%M%S`.gz"
                fi

                gzip -c ${log_file} > ${LOG_BACKUP_PATH}/${backup_file_name}
                ret=$?
                if [ $ret -eq 0 ]; then
                        echo "`${SYSTIME}` : new backup file name : ${backup_file_name}"
                        if [[ ${is_rollover_file} -eq 1 ]]; then
                                rm -f ${log_file}
                        else
                                echo "" > ${log_file}
                        fi
                        echo "`${SYSTIME}` : clear log file(${log_file}) OK !"
                else
                        echo "`${SYSTIME}` : gzip ${log_file} command ERROR , ret $ret"
                fi
        else
                echo "`${SYSTIME}` : ${log_file} no need to clear !"
        fi

        check_backup_files ${LOG_BACKUP_PATH} ${log_file}
        ret=$?
        if [ ${ret} != 0 ]; then
                echo "`${SYSTIME}` : check backup file(${log_file}) ERROR !"
                exit $ret
        fi
done

echo ""
echo "`${SYSTIME}` : task over ..."
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
exit 0
