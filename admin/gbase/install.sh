#!/bin/bash

usage()
{
    echo "Usage: $0 [OPTIONS]"
    echo "-i install [path]"
    echo "-e remove [path]"

    exit 1
}

install()
{
    BIN=$1/bin
    ETC=$1/../etc
    ADMIN=$1/admin
    DIST=$ADMIN/gbase
    CONF=gbase.yml

    rm -rf $DIST
    mkdir -p $DIST
    cp *.py $DIST
    chmod a+x $DIST/*.py

    rm -rf $ETC/$CONF
    mkdir -p $ETC
    cp $CONF $ETC


    CLUSTER=gbase_cluster_deploy
    NODE=gbase_node_deploy

    rm -rf $BIN/$CLUSTER
    rm -rf $BIN/$NODE
    mkdir -p $BIN
    ln -s $DIST/${CLUSTER}.py $BIN/$CLUSTER
    ln -s $DIST/${NODE}.py $BIN/$NODE
}

remove()
{
    echo "do nothing"
}

if [ $# -lt 1 ]
then 
    usage
fi

while getopts ieh options
do
    case $options in
        i)
        echo "gbase install scripts $2"
        install $2
        ;;
        e)
        echo "gbase remove scripts"
        remove
        ;; 
        h)
        usage
        ;;
        \?)
        usage
        ;;
    esac
done
