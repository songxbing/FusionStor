#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import errno
import time
import sys
from StringIO import StringIO


from utils import _exec_shell1, Exp, _str2list


class LSBLK(object):
    def __init__(self, dev):
        self.dev = dev

    def _run(self, key='TYPE'):
        cmd = 'lsblk -d -o %s %s' % (key, self.dev)
        out, err = _exec_shell1(cmd, retry=1, p=False)
        l = [x.strip() for x in out.split('\n') if x.strip()]
        return l

    def dev_type(self):
        try:
            l = self._run()
            return l[1]
        except:
            return None


if __name__ == '__main__':
    lsblk = LSBLK('/dev/sr0')
    print lsblk.dev_type()
