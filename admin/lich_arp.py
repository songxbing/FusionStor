#!/usr/bin/env python2

import os
import sys
import socket
import time
import subprocess
import fcntl
import types
import errno
import getopt
import random
import threading

from daemon import Daemon
from config import Config
from utils import Exp, _dmsg, _dwarn, _derror, _str2dict, _getrack, _exec_shell

class Lich_Arp(Daemon):
    def __init__(self, config):
        self.config = config
        pidfile = '/var/run/lich_arp.pid'
        log = self.config.home + '/log/lich_arp.log'
        os.system('touch ' + log)
        super(Lich_Arp, self).__init__(pidfile, '/dev/null', log, log, 'lich_arp')
        
    def run(self):
        cmd = self.config.home + '/lich/admin/check_client_ip.sh'
        os.system(cmd)

def usage():
    print ("usage:")
    print (sys.argv[0] + " --start")
    print (sys.argv[0] + " --stop")
    print (sys.argv[0] + " --test")
    print (sys.argv[0])

def main():
    op = ''
    ext = None
    try:
        opts, args = getopt.getopt(
                sys.argv[1:], 
            'h', ['start', 'stop', 'help', 'test']
                )
    except getopt.GetoptError, err:
        print str(err)
        usage()

    for o, a in opts:
        if o in ('--help'):
            usage()
            exit(0)
        elif o == '--start':
            op = o
            #lich_cron.start()
        elif o == '--stop':
            op = o
        elif o == '--test':
            op = o
        else:
            assert False, 'oops, unhandled option: %s, -h for help' % o
            exit(1)

    config = Config()
    lich_arp = Lich_Arp(config)
    if (op == '--start'):
        lich_arp.start()
    elif (op == '--stop'):
        lich_arp.stop()
    elif (op == '--test'):
        lich_arp.run()
    else:
        assert False, 'oops, unhandled option: %s, -h for help' % o
        exit(1)


if __name__ == '__main__':
    if (len(sys.argv) == 1):
        usage()
    else:
        main()
