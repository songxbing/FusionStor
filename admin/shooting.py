#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import StringIO
import traceback
import pprint
from optparse import OptionParser

from utils import _exec_shell1, _exec_remote1, _str2dict
from db_sqlite3 import Sqlite


class Shooting(object):
    def __init__(self):
        pass

    @staticmethod
    def chunk_info(chunk):
        out, err = _exec_shell1('lich.inspect --chunkinfo %s' % chunk)
        d = _str2dict(out)
        return d

    @staticmethod
    def chunk_dump(hosts, chunk):
        for host in hosts:
            out, err = _exec_remote1(host, 'lich.inspect --chunkdump %s' % chunk)
            print out, err

            out, err = _exec_remote1(host, 'md5sum %s.dump' % chunk)
            print out, err


def shooting_chunk_dump():
    sql = Sqlite()
    print sql.find('vol.11.0')

    shoot = Shooting()
    print shoot.chunk_info('vol.11.0')
    print shoot.chunk_dump(['192.168.1.151',
                            '192.168.1.152',
                            '192.168.1.153',
                            '192.168.1.154',
                            '192.168.1.155',
                            '192.168.1.156',
                            ], 'vol.11.0')


def shooting_core(polling, vols=[]):
    d = {}
    for vol in vols:
        d[vol] = vol % polling

    pprint.pprint(d)


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-m', '--human-unreadable', action='store_false', dest='human', default=True, help='human unreadable')
    parser.add_option('-v', '--verbose', action='store_true', dest='verbose', default=False, help='verbose')
    parser.add_option('-f', '--force', action='store_true', dest='force', default=False, help='force')
    parser.add_option('-j', '--json', action='store_true', dest='json', default=False, help='json')
    parser.add_option('-u', '--unhide', action='store_true', dest='unhide', default=False, help='unhide')
    parser.add_option('-p', '--password', action='store', type='string', dest='password', default='')
    parser.add_option('-t', '--task', action='store', type='string', dest='task', default='core')
    options, args = parser.parse_args()

    print options, args

    # shooting_chunk_dump()

    if options.task == 'core':
        polling = int(args[0])
        vols = [int (x) for x in args[1:]]
        shooting_core(polling, vols)
